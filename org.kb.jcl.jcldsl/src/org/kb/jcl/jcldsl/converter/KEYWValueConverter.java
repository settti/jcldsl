package org.kb.jcl.jcldsl.converter;

import java.util.Set;

import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.GrammarUtil;

import com.google.common.collect.ImmutableSet;

public class KEYWValueConverter extends AbstractKEYWValueConverter {
	
	public KEYWValueConverter() {
		super();
	}
	
	@Override
	protected Set<String> computeValuesToEscape(Grammar grammar) {
		return ImmutableSet.copyOf(GrammarUtil.getAllKeywords(grammar));
	}

	@Override
	protected boolean mustEscape(String value) {
		return getValuesToEscape().contains(value);
	}
}