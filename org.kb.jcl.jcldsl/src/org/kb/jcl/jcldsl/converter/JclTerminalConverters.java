package org.kb.jcl.jcldsl.converter;

import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.kb.jcl.jcldsl.converter.AbstractKEYWValueConverter;

import com.google.inject.Inject;

public class JclTerminalConverters extends DefaultTerminalConverters {
	@Inject
	private AbstractKEYWValueConverter keywValueConverter;

	@ValueConverter(rule = "KEYW")
	public IValueConverter<String> KEYW() {
		return keywValueConverter;
	}
}
