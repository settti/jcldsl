/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>exec Acct</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execAcct#getValue <em>Value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execAcct#getVALUE2 <em>VALUE2</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecAcct()
 * @model
 * @generated
 */
public interface execAcct extends execsection
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecAcct_Value()
   * @model
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execAcct#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

  /**
   * Returns the value of the '<em><b>VALUE2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>VALUE2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>VALUE2</em>' attribute.
   * @see #setVALUE2(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecAcct_VALUE2()
   * @model
   * @generated
   */
  String getVALUE2();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execAcct#getVALUE2 <em>VALUE2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>VALUE2</em>' attribute.
   * @see #getVALUE2()
   * @generated
   */
  void setVALUE2(String value);

} // execAcct
