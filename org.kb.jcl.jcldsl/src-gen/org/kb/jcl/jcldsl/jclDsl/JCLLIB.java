/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JCLLIB</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.JCLLIB#getJCLLIBNAME <em>JCLLIBNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.JCLLIB#getDSNAMES <em>DSNAMES</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.JCLLIB#getDSNAMES2 <em>DSNAMES2</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJCLLIB()
 * @model
 * @generated
 */
public interface JCLLIB extends Keyword
{
  /**
   * Returns the value of the '<em><b>JCLLIBNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>JCLLIBNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>JCLLIBNAME</em>' attribute.
   * @see #setJCLLIBNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJCLLIB_JCLLIBNAME()
   * @model
   * @generated
   */
  String getJCLLIBNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.JCLLIB#getJCLLIBNAME <em>JCLLIBNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>JCLLIBNAME</em>' attribute.
   * @see #getJCLLIBNAME()
   * @generated
   */
  void setJCLLIBNAME(String value);

  /**
   * Returns the value of the '<em><b>DSNAMES</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>DSNAMES</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>DSNAMES</em>' attribute.
   * @see #setDSNAMES(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJCLLIB_DSNAMES()
   * @model
   * @generated
   */
  String getDSNAMES();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.JCLLIB#getDSNAMES <em>DSNAMES</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>DSNAMES</em>' attribute.
   * @see #getDSNAMES()
   * @generated
   */
  void setDSNAMES(String value);

  /**
   * Returns the value of the '<em><b>DSNAMES2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>DSNAMES2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>DSNAMES2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJCLLIB_DSNAMES2()
   * @model unique="false"
   * @generated
   */
  EList<String> getDSNAMES2();

} // JCLLIB
