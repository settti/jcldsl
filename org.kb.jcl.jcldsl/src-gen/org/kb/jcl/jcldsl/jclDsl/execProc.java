/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>exec Proc</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execProc#getPROCNAME <em>PROCNAME</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecProc()
 * @model
 * @generated
 */
public interface execProc extends execsection
{
  /**
   * Returns the value of the '<em><b>PROCNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>PROCNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>PROCNAME</em>' attribute.
   * @see #setPROCNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecProc_PROCNAME()
   * @model
   * @generated
   */
  String getPROCNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execProc#getPROCNAME <em>PROCNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>PROCNAME</em>' attribute.
   * @see #getPROCNAME()
   * @generated
   */
  void setPROCNAME(String value);

} // execProc
