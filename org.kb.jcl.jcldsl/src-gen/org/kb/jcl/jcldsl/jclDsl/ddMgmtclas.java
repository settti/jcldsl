/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Mgmtclas</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddMgmtclas#getCLASSNAME <em>CLASSNAME</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddMgmtclas()
 * @model
 * @generated
 */
public interface ddMgmtclas extends ddsection
{
  /**
   * Returns the value of the '<em><b>CLASSNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>CLASSNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>CLASSNAME</em>' attribute.
   * @see #setCLASSNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddMgmtclas_CLASSNAME()
   * @model
   * @generated
   */
  String getCLASSNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddMgmtclas#getCLASSNAME <em>CLASSNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>CLASSNAME</em>' attribute.
   * @see #getCLASSNAME()
   * @generated
   */
  void setCLASSNAME(String value);

} // ddMgmtclas
