/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslFactory
 * @model kind="package"
 * @generated
 */
public interface JclDslPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "jclDsl";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.kb.org/jcl/jcldsl/JclDsl";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "jclDsl";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  JclDslPackage eINSTANCE = org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl.init();

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ModelImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.KeywordImpl <em>Keyword</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.KeywordImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getKeyword()
   * @generated
   */
  int KEYWORD = 1;

  /**
   * The number of structural features of the '<em>Keyword</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int KEYWORD_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.STEPCATDDImpl <em>STEPCATDD</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.STEPCATDDImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getSTEPCATDD()
   * @generated
   */
  int STEPCATDD = 2;

  /**
   * The feature id for the '<em><b>Section</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STEPCATDD__SECTION = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Sections</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STEPCATDD__SECTIONS = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>STEPCATDD</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STEPCATDD_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.JOBLIBDDImpl <em>JOBLIBDD</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JOBLIBDDImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getJOBLIBDD()
   * @generated
   */
  int JOBLIBDD = 3;

  /**
   * The feature id for the '<em><b>Section</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOBLIBDD__SECTION = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Sections</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOBLIBDD__SECTIONS = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>JOBLIBDD</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOBLIBDD_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.JOBCATDDImpl <em>JOBCATDD</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JOBCATDDImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getJOBCATDD()
   * @generated
   */
  int JOBCATDD = 4;

  /**
   * The feature id for the '<em><b>Section</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOBCATDD__SECTION = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Sections</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOBCATDD__SECTIONS = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>JOBCATDD</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOBCATDD_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.specddSectionImpl <em>specdd Section</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.specddSectionImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getspecddSection()
   * @generated
   */
  int SPECDD_SECTION = 5;

  /**
   * The number of structural features of the '<em>specdd Section</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECDD_SECTION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.specddDispImpl <em>specdd Disp</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.specddDispImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getspecddDisp()
   * @generated
   */
  int SPECDD_DISP = 6;

  /**
   * The feature id for the '<em><b>Disp</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECDD_DISP__DISP = SPECDD_SECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>specdd Disp</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECDD_DISP_FEATURE_COUNT = SPECDD_SECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.specddDsnameImpl <em>specdd Dsname</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.specddDsnameImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getspecddDsname()
   * @generated
   */
  int SPECDD_DSNAME = 7;

  /**
   * The feature id for the '<em><b>Specname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECDD_DSNAME__SPECNAME = SPECDD_SECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Dsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECDD_DSNAME__DSNAME = SPECDD_SECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>specdd Dsname</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECDD_DSNAME_FEATURE_COUNT = SPECDD_SECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.IFImpl <em>IF</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.IFImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getIF()
   * @generated
   */
  int IF = 8;

  /**
   * The feature id for the '<em><b>IFNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__IFNAME = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>STEPNAME</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__STEPNAME = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Expression keyword</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__EXPRESSION_KEYWORD = KEYWORD_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__OPERATOR = KEYWORD_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__VALUE = KEYWORD_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Logical op</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__LOGICAL_OP = KEYWORD_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Expression keyword2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__EXPRESSION_KEYWORD2 = KEYWORD_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Operator2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__OPERATOR2 = KEYWORD_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Value2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__VALUE2 = KEYWORD_FEATURE_COUNT + 8;

  /**
   * The number of structural features of the '<em>IF</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 9;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ELSEImpl <em>ELSE</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ELSEImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getELSE()
   * @generated
   */
  int ELSE = 9;

  /**
   * The feature id for the '<em><b>ELSENAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE__ELSENAME = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>ELSE</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ENDIFImpl <em>ENDIF</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ENDIFImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getENDIF()
   * @generated
   */
  int ENDIF = 10;

  /**
   * The feature id for the '<em><b>ENDIFNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENDIF__ENDIFNAME = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>ENDIF</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENDIF_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.PROCImpl <em>PROC</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.PROCImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getPROC()
   * @generated
   */
  int PROC = 11;

  /**
   * The feature id for the '<em><b>PROCNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC__PROCNAME = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>PARMNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC__PARMNAME = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Parm value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC__PARM_VALUE = KEYWORD_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>DSNAMES</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC__DSNAMES = KEYWORD_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC__VALUE = KEYWORD_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>PROC</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.PENDImpl <em>PEND</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.PENDImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getPEND()
   * @generated
   */
  int PEND = 12;

  /**
   * The feature id for the '<em><b>PENDNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PEND__PENDNAME = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>PEND</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PEND_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.SETImpl <em>SET</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.SETImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getSET()
   * @generated
   */
  int SET = 13;

  /**
   * The feature id for the '<em><b>SETNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET__SETNAME = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Parmname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET__PARMNAME = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Parm value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET__PARM_VALUE = KEYWORD_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Parm valueb</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET__PARM_VALUEB = KEYWORD_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Parmname2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET__PARMNAME2 = KEYWORD_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Parm value2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET__PARM_VALUE2 = KEYWORD_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Parm valueb2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET__PARM_VALUEB2 = KEYWORD_FEATURE_COUNT + 6;

  /**
   * The number of structural features of the '<em>SET</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 7;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.JCLLIBImpl <em>JCLLIB</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JCLLIBImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getJCLLIB()
   * @generated
   */
  int JCLLIB = 14;

  /**
   * The feature id for the '<em><b>JCLLIBNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JCLLIB__JCLLIBNAME = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>DSNAMES</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JCLLIB__DSNAMES = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>DSNAMES2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JCLLIB__DSNAMES2 = KEYWORD_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>JCLLIB</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JCLLIB_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.EXECImpl <em>EXEC</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.EXECImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getEXEC()
   * @generated
   */
  int EXEC = 15;

  /**
   * The feature id for the '<em><b>EXECNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC__EXECNAME = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Section</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC__SECTION = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>PROCEDURE</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC__PROCEDURE = KEYWORD_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Sections</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC__SECTIONS = KEYWORD_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Dds</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC__DDS = KEYWORD_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>EXEC</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execsectionImpl <em>execsection</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execsectionImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecsection()
   * @generated
   */
  int EXECSECTION = 16;

  /**
   * The number of structural features of the '<em>execsection</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECSECTION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execTimeImpl <em>exec Time</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execTimeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecTime()
   * @generated
   */
  int EXEC_TIME = 17;

  /**
   * The feature id for the '<em><b>Exec Timevalue</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_TIME__EXEC_TIMEVALUE = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>exec Time</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_TIME_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execTimevalueImpl <em>exec Timevalue</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execTimevalueImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecTimevalue()
   * @generated
   */
  int EXEC_TIMEVALUE = 18;

  /**
   * The feature id for the '<em><b>Minutes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_TIMEVALUE__MINUTES = 0;

  /**
   * The feature id for the '<em><b>Seconds</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_TIMEVALUE__SECONDS = 1;

  /**
   * The number of structural features of the '<em>exec Timevalue</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_TIMEVALUE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execPerformImpl <em>exec Perform</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execPerformImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecPerform()
   * @generated
   */
  int EXEC_PERFORM = 19;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PERFORM__VALUE = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>exec Perform</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PERFORM_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execRegionImpl <em>exec Region</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execRegionImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecRegion()
   * @generated
   */
  int EXEC_REGION = 20;

  /**
   * The feature id for the '<em><b>Region size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_REGION__REGION_SIZE = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_REGION__SIZE = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>exec Region</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_REGION_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execRdImpl <em>exec Rd</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execRdImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecRd()
   * @generated
   */
  int EXEC_RD = 21;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_RD__VALUE = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>exec Rd</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_RD_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execParmImpl <em>exec Parm</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execParmImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecParm()
   * @generated
   */
  int EXEC_PARM = 22;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PARM__PARM = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Parmkeyw</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PARM__PARMKEYW = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Parm2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PARM__PARM2 = EXECSECTION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Parmkeyw2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PARM__PARMKEYW2 = EXECSECTION_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Parmint</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PARM__PARMINT = EXECSECTION_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>exec Parm</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PARM_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execDynambrImpl <em>exec Dynambr</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execDynambrImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecDynambr()
   * @generated
   */
  int EXEC_DYNAMBR = 23;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_DYNAMBR__VALUE = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>exec Dynambr</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_DYNAMBR_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execCondImpl <em>exec Cond</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execCondImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecCond()
   * @generated
   */
  int EXEC_COND = 24;

  /**
   * The feature id for the '<em><b>Rc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_COND__RC = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Execcondop</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_COND__EXECCONDOP = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Stepname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_COND__STEPNAME = EXECSECTION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Rc2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_COND__RC2 = EXECSECTION_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Stepname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_COND__STEPNAME2 = EXECSECTION_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Rc3</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_COND__RC3 = EXECSECTION_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Execcondop2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_COND__EXECCONDOP2 = EXECSECTION_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Stepname3</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_COND__STEPNAME3 = EXECSECTION_FEATURE_COUNT + 7;

  /**
   * The number of structural features of the '<em>exec Cond</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_COND_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 8;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execAddrspcImpl <em>exec Addrspc</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execAddrspcImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecAddrspc()
   * @generated
   */
  int EXEC_ADDRSPC = 25;

  /**
   * The feature id for the '<em><b>Addr</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_ADDRSPC__ADDR = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>exec Addrspc</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_ADDRSPC_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execCcsidImpl <em>exec Ccsid</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execCcsidImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecCcsid()
   * @generated
   */
  int EXEC_CCSID = 26;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_CCSID__VALUE = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>exec Ccsid</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_CCSID_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execAcctImpl <em>exec Acct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execAcctImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecAcct()
   * @generated
   */
  int EXEC_ACCT = 27;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_ACCT__VALUE = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>VALUE2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_ACCT__VALUE2 = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>exec Acct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_ACCT_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execProcImpl <em>exec Proc</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execProcImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecProc()
   * @generated
   */
  int EXEC_PROC = 28;

  /**
   * The feature id for the '<em><b>PROCNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PROC__PROCNAME = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>exec Proc</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PROC_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.execPgmImpl <em>exec Pgm</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.execPgmImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getexecPgm()
   * @generated
   */
  int EXEC_PGM = 29;

  /**
   * The feature id for the '<em><b>PGMNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PGM__PGMNAME = EXECSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>exec Pgm</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXEC_PGM_FEATURE_COUNT = EXECSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.INCLUDEImpl <em>INCLUDE</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.INCLUDEImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getINCLUDE()
   * @generated
   */
  int INCLUDE = 30;

  /**
   * The feature id for the '<em><b>INCLUDENAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCLUDE__INCLUDENAME = KEYWORD_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>MEMBERNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCLUDE__MEMBERNAME = KEYWORD_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>INCLUDE</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCLUDE_FEATURE_COUNT = KEYWORD_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.JOBImpl <em>JOB</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JOBImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getJOB()
   * @generated
   */
  int JOB = 31;

  /**
   * The feature id for the '<em><b>Keywords</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB__KEYWORDS = MODEL_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>JOBNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB__JOBNAME = MODEL_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Section</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB__SECTION = MODEL_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Sections</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB__SECTIONS = MODEL_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>JOB</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_FEATURE_COUNT = MODEL_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobsectionImpl <em>jobsection</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobsectionImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobsection()
   * @generated
   */
  int JOBSECTION = 32;

  /**
   * The number of structural features of the '<em>jobsection</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOBSECTION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobUserImpl <em>job User</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobUserImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobUser()
   * @generated
   */
  int JOB_USER = 33;

  /**
   * The feature id for the '<em><b>USERNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_USER__USERNAME = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job User</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_USER_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobTyprunImpl <em>job Typrun</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobTyprunImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobTyprun()
   * @generated
   */
  int JOB_TYPRUN = 34;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_TYPRUN__TYPE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Typrun</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_TYPRUN_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobTimeImpl <em>job Time</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobTimeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobTime()
   * @generated
   */
  int JOB_TIME = 35;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_TIME__VALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Job Timevalue</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_TIME__JOB_TIMEVALUE = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>job Time</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_TIME_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobTimevalueImpl <em>job Timevalue</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobTimevalueImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobTimevalue()
   * @generated
   */
  int JOB_TIMEVALUE = 36;

  /**
   * The feature id for the '<em><b>Minutes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_TIMEVALUE__MINUTES = 0;

  /**
   * The feature id for the '<em><b>Seconds</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_TIMEVALUE__SECONDS = 1;

  /**
   * The number of structural features of the '<em>job Timevalue</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_TIMEVALUE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobSchenvImpl <em>job Schenv</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobSchenvImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobSchenv()
   * @generated
   */
  int JOB_SCHENV = 37;

  /**
   * The feature id for the '<em><b>ENVIRONMENT</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_SCHENV__ENVIRONMENT = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Schenv</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_SCHENV_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobSeclabelImpl <em>job Seclabel</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobSeclabelImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobSeclabel()
   * @generated
   */
  int JOB_SECLABEL = 38;

  /**
   * The feature id for the '<em><b>SECURITYLABEL</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_SECLABEL__SECURITYLABEL = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Seclabel</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_SECLABEL_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobRestartImpl <em>job Restart</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobRestartImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobRestart()
   * @generated
   */
  int JOB_RESTART = 39;

  /**
   * The feature id for the '<em><b>JOBSTEP</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_RESTART__JOBSTEP = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Restart</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_RESTART_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobRegionImpl <em>job Region</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobRegionImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobRegion()
   * @generated
   */
  int JOB_REGION = 40;

  /**
   * The feature id for the '<em><b>Region size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_REGION__REGION_SIZE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_REGION__SIZE = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>job Region</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_REGION_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobRdImpl <em>job Rd</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobRdImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobRd()
   * @generated
   */
  int JOB_RD = 41;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_RD__VALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Rd</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_RD_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobPrtyImpl <em>job Prty</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobPrtyImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobPrty()
   * @generated
   */
  int JOB_PRTY = 42;

  /**
   * The feature id for the '<em><b>Prtyvalue</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PRTY__PRTYVALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Prty</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PRTY_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobPrognameImpl <em>job Progname</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobPrognameImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobProgname()
   * @generated
   */
  int JOB_PROGNAME = 43;

  /**
   * The feature id for the '<em><b>Programmer name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PROGNAME__PROGRAMMER_NAME = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>PROGRAMMER ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PROGNAME__PROGRAMMER_ID = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>job Progname</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PROGNAME_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobPerformImpl <em>job Perform</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobPerformImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobPerform()
   * @generated
   */
  int JOB_PERFORM = 44;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PERFORM__VALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Perform</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PERFORM_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobPasswordImpl <em>job Password</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobPasswordImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobPassword()
   * @generated
   */
  int JOB_PASSWORD = 45;

  /**
   * The feature id for the '<em><b>PASSWORD</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PASSWORD__PASSWORD = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>NEWPASSWORD</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PASSWORD__NEWPASSWORD = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>job Password</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PASSWORD_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobPagesImpl <em>job Pages</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobPagesImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobPages()
   * @generated
   */
  int JOB_PAGES = 46;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PAGES__VALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Pages Def</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PAGES__PAGES_DEF = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>job Pages</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_PAGES_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobNotifyImpl <em>job Notify</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobNotifyImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobNotify()
   * @generated
   */
  int JOB_NOTIFY = 47;

  /**
   * The feature id for the '<em><b>USERID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_NOTIFY__USERID = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Notify</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_NOTIFY_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobMsglevelImpl <em>job Msglevel</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobMsglevelImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobMsglevel()
   * @generated
   */
  int JOB_MSGLEVEL = 48;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_MSGLEVEL__VALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Job Msglevel Statements</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_MSGLEVEL__JOB_MSGLEVEL_STATEMENTS = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Job Msglevel Messages</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_MSGLEVEL__JOB_MSGLEVEL_MESSAGES = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>job Msglevel</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_MSGLEVEL_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobMsgClassImpl <em>job Msg Class</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobMsgClassImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobMsgClass()
   * @generated
   */
  int JOB_MSG_CLASS = 49;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_MSG_CLASS__VALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Msg Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_MSG_CLASS_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobLinesImpl <em>job Lines</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobLinesImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobLines()
   * @generated
   */
  int JOB_LINES = 50;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_LINES__VALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Pages Def</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_LINES__PAGES_DEF = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>job Lines</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_LINES_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobGroupImpl <em>job Group</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobGroupImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobGroup()
   * @generated
   */
  int JOB_GROUP = 51;

  /**
   * The feature id for the '<em><b>GROUP</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_GROUP__GROUP = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_GROUP_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobCondImpl <em>job Cond</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobCondImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobCond()
   * @generated
   */
  int JOB_COND = 52;

  /**
   * The feature id for the '<em><b>Return code</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_COND__RETURN_CODE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Jobcondop</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_COND__JOBCONDOP = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Return code2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_COND__RETURN_CODE2 = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Jobcondop2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_COND__JOBCONDOP2 = JOBSECTION_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>job Cond</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_COND_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobClassImpl <em>job Class</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobClassImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobClass()
   * @generated
   */
  int JOB_CLASS = 53;

  /**
   * The feature id for the '<em><b>CLASS</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_CLASS__CLASS = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_CLASS_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobCcsidImpl <em>job Ccsid</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobCcsidImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobCcsid()
   * @generated
   */
  int JOB_CCSID = 54;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_CCSID__VALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Ccsid</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_CCSID_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobCardsImpl <em>job Cards</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobCardsImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobCards()
   * @generated
   */
  int JOB_CARDS = 55;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_CARDS__VALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Pages Def</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_CARDS__PAGES_DEF = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>job Cards</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_CARDS_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobBytesImpl <em>job Bytes</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobBytesImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobBytes()
   * @generated
   */
  int JOB_BYTES = 56;

  /**
   * The feature id for the '<em><b>Values</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_BYTES__VALUES = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Pages Def</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_BYTES__PAGES_DEF = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>job Bytes</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_BYTES_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobAddrspcImpl <em>job Addrspc</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobAddrspcImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobAddrspc()
   * @generated
   */
  int JOB_ADDRSPC = 57;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_ADDRSPC__VALUE = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>job Addrspc</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_ADDRSPC_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.jobAccinfoImpl <em>job Accinfo</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.jobAccinfoImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobAccinfo()
   * @generated
   */
  int JOB_ACCINFO = 58;

  /**
   * The feature id for the '<em><b>Pano</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_ACCINFO__PANO = JOBSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Acc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_ACCINFO__ACC = JOBSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Acc2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_ACCINFO__ACC2 = JOBSECTION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Acc3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_ACCINFO__ACC3 = JOBSECTION_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>job Accinfo</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOB_ACCINFO_FEATURE_COUNT = JOBSECTION_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.DDImpl <em>DD</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.DDImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getDD()
   * @generated
   */
  int DD = 59;

  /**
   * The feature id for the '<em><b>DDNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD__DDNAME = 0;

  /**
   * The feature id for the '<em><b>Sections</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD__SECTIONS = 1;

  /**
   * The number of structural features of the '<em>DD</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddsectionImpl <em>ddsection</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddsectionImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddsection()
   * @generated
   */
  int DDSECTION = 60;

  /**
   * The number of structural features of the '<em>ddsection</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DDSECTION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddVolumeImpl <em>dd Volume</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddVolumeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddVolume()
   * @generated
   */
  int DD_VOLUME = 61;

  /**
   * The feature id for the '<em><b>Private</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_VOLUME__PRIVATE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Retain</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_VOLUME__RETAIN = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Volseq</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_VOLUME__VOLSEQ = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Volcnt</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_VOLUME__VOLCNT = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Serial</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_VOLUME__SERIAL = DDSECTION_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Serials</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_VOLUME__SERIALS = DDSECTION_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Dsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_VOLUME__DSNAME = DDSECTION_FEATURE_COUNT + 6;

  /**
   * The number of structural features of the '<em>dd Volume</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_VOLUME_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 7;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddUnitImpl <em>dd Unit</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddUnitImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddUnit()
   * @generated
   */
  int DD_UNIT = 62;

  /**
   * The feature id for the '<em><b>Device</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_UNIT__DEVICE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_UNIT__COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>P</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_UNIT__P = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Defer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_UNIT__DEFER = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>dd Unit</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_UNIT_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddUcsImpl <em>dd Ucs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddUcsImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddUcs()
   * @generated
   */
  int DD_UCS = 63;

  /**
   * The feature id for the '<em><b>Charset</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_UCS__CHARSET = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Fold</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_UCS__FOLD = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Verify</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_UCS__VERIFY = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>dd Ucs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_UCS_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddTermImpl <em>dd Term</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddTermImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddTerm()
   * @generated
   */
  int DD_TERM = 64;

  /**
   * The number of structural features of the '<em>dd Term</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_TERM_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSysoutImpl <em>dd Sysout</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddSysoutImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddSysout()
   * @generated
   */
  int DD_SYSOUT = 65;

  /**
   * The feature id for the '<em><b>Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SYSOUT__CLASS = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Wtrname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SYSOUT__WTRNAME = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Formname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SYSOUT__FORMNAME = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>dd Sysout</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SYSOUT_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSubsysImpl <em>dd Subsys</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddSubsysImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddSubsys()
   * @generated
   */
  int DD_SUBSYS = 66;

  /**
   * The feature id for the '<em><b>Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SUBSYS__LABEL = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Subsys</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SUBSYS__SUBSYS = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Subparm</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SUBSYS__SUBPARM = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>dd Subsys</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SUBSYS_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSubsysSubparmsImpl <em>dd Subsys Subparms</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddSubsysSubparmsImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddSubsysSubparms()
   * @generated
   */
  int DD_SUBSYS_SUBPARMS = 67;

  /**
   * The feature id for the '<em><b>Subparm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SUBSYS_SUBPARMS__SUBPARM = 0;

  /**
   * The feature id for the '<em><b>Subparm3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SUBSYS_SUBPARMS__SUBPARM3 = 1;

  /**
   * The number of structural features of the '<em>dd Subsys Subparms</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SUBSYS_SUBPARMS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddStorclasImpl <em>dd Storclas</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddStorclasImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddStorclas()
   * @generated
   */
  int DD_STORCLAS = 68;

  /**
   * The feature id for the '<em><b>Storageclass</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_STORCLAS__STORAGECLASS = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Storclas</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_STORCLAS_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSpinImpl <em>dd Spin</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddSpinImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddSpin()
   * @generated
   */
  int DD_SPIN = 69;

  /**
   * The feature id for the '<em><b>Unalloc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SPIN__UNALLOC = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Spin</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SPIN_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSpaceImpl <em>dd Space</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddSpaceImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddSpace()
   * @generated
   */
  int DD_SPACE = 70;

  /**
   * The feature id for the '<em><b>Length</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SPACE__LENGTH = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Primary</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SPACE__PRIMARY = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Secondary</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SPACE__SECONDARY = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Dirorindx</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SPACE__DIRORINDX = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SPACE__PARM = DDSECTION_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>dd Space</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SPACE_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSegmentImpl <em>dd Segment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddSegmentImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddSegment()
   * @generated
   */
  int DD_SEGMENT = 71;

  /**
   * The feature id for the '<em><b>Page count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SEGMENT__PAGE_COUNT = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Segment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SEGMENT_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSecmodelImpl <em>dd Secmodel</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddSecmodelImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddSecmodel()
   * @generated
   */
  int DD_SECMODEL = 72;

  /**
   * The feature id for the '<em><b>Dsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SECMODEL__DSNAME = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Generic</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SECMODEL__GENERIC = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Secmodel</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_SECMODEL_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddRlsImpl <em>dd Rls</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddRlsImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddRls()
   * @generated
   */
  int DD_RLS = 73;

  /**
   * The feature id for the '<em><b>Rls</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RLS__RLS = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Rls</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RLS_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddRetpdImpl <em>dd Retpd</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddRetpdImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddRetpd()
   * @generated
   */
  int DD_RETPD = 74;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RETPD__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Retpd</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RETPD_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddRefddImpl <em>dd Refdd</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddRefddImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddRefdd()
   * @generated
   */
  int DD_REFDD = 75;

  /**
   * The feature id for the '<em><b>Dsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_REFDD__DSNAME = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Refdd</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_REFDD_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddRecorgImpl <em>dd Recorg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddRecorgImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddRecorg()
   * @generated
   */
  int DD_RECORG = 76;

  /**
   * The feature id for the '<em><b>Recorg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RECORG__RECORG = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Recorg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RECORG_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddRecfmImpl <em>dd Recfm</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddRecfmImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddRecfm()
   * @generated
   */
  int DD_RECFM = 77;

  /**
   * The feature id for the '<em><b>Recfm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RECFM__RECFM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Recfm</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RECFM_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddQnameImpl <em>dd Qname</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddQnameImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddQname()
   * @generated
   */
  int DD_QNAME = 78;

  /**
   * The feature id for the '<em><b>Procname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_QNAME__PROCNAME = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Qname</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_QNAME_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddProtectImpl <em>dd Protect</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddProtectImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddProtect()
   * @generated
   */
  int DD_PROTECT = 79;

  /**
   * The feature id for the '<em><b>Yes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PROTECT__YES = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Protect</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PROTECT_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPathmodeImpl <em>dd Pathmode</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddPathmodeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddPathmode()
   * @generated
   */
  int DD_PATHMODE = 80;

  /**
   * The feature id for the '<em><b>Option</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATHMODE__OPTION = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Option2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATHMODE__OPTION2 = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Pathmode</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATHMODE_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPathoptsImpl <em>dd Pathopts</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddPathoptsImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddPathopts()
   * @generated
   */
  int DD_PATHOPTS = 81;

  /**
   * The feature id for the '<em><b>Option</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATHOPTS__OPTION = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Option2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATHOPTS__OPTION2 = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Pathopts</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATHOPTS_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPathdispImpl <em>dd Pathdisp</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddPathdispImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddPathdisp()
   * @generated
   */
  int DD_PATHDISP = 82;

  /**
   * The feature id for the '<em><b>Disp</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATHDISP__DISP = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Disp2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATHDISP__DISP2 = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Disp3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATHDISP__DISP3 = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>dd Pathdisp</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATHDISP_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPathImpl <em>dd Path</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddPathImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddPath()
   * @generated
   */
  int DD_PATH = 83;

  /**
   * The feature id for the '<em><b>Path</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATH__PATH = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Path</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PATH_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddOutputImpl <em>dd Output</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddOutputImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddOutput()
   * @generated
   */
  int DD_OUTPUT = 84;

  /**
   * The feature id for the '<em><b>Output</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_OUTPUT__OUTPUT = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Output2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_OUTPUT__OUTPUT2 = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Output</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_OUTPUT_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddOutlimImpl <em>dd Outlim</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddOutlimImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddOutlim()
   * @generated
   */
  int DD_OUTLIM = 85;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_OUTLIM__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Outlim</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_OUTLIM_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddModifyImpl <em>dd Modify</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddModifyImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddModify()
   * @generated
   */
  int DD_MODIFY = 86;

  /**
   * The feature id for the '<em><b>Module</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_MODIFY__MODULE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Trc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_MODIFY__TRC = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Modify</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_MODIFY_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddMgmtclasImpl <em>dd Mgmtclas</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddMgmtclasImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddMgmtclas()
   * @generated
   */
  int DD_MGMTCLAS = 87;

  /**
   * The feature id for the '<em><b>CLASSNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_MGMTCLAS__CLASSNAME = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Mgmtclas</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_MGMTCLAS_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLreclImpl <em>dd Lrecl</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddLreclImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddLrecl()
   * @generated
   */
  int DD_LRECL = 88;

  /**
   * The feature id for the '<em><b>Length in bytes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LRECL__LENGTH_IN_BYTES = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Lrecl</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LRECL_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLikeImpl <em>dd Like</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddLikeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddLike()
   * @generated
   */
  int DD_LIKE = 89;

  /**
   * The feature id for the '<em><b>Dsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LIKE__DSNAME = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Like</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LIKE_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLgstreamImpl <em>dd Lgstream</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddLgstreamImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddLgstream()
   * @generated
   */
  int DD_LGSTREAM = 90;

  /**
   * The feature id for the '<em><b>Logstream</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LGSTREAM__LOGSTREAM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Lgstream</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LGSTREAM_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelImpl <em>dd Label</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddLabelImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddLabel()
   * @generated
   */
  int DD_LABEL = 91;

  /**
   * The feature id for the '<em><b>Sequence number</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL__SEQUENCE_NUMBER = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL__LABEL = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Password</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL__PASSWORD = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Inorout</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL__INOROUT = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Expire</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL__EXPIRE = DDSECTION_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>dd Label</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelExpireImpl <em>dd Label Expire</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddLabelExpireImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddLabelExpire()
   * @generated
   */
  int DD_LABEL_EXPIRE = 92;

  /**
   * The number of structural features of the '<em>dd Label Expire</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL_EXPIRE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelExpdtImpl <em>dd Label Expdt</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddLabelExpdtImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddLabelExpdt()
   * @generated
   */
  int DD_LABEL_EXPDT = 93;

  /**
   * The feature id for the '<em><b>Yyddd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL_EXPDT__YYDDD = DD_LABEL_EXPIRE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Yyyy</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL_EXPDT__YYYY = DD_LABEL_EXPIRE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ddd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL_EXPDT__DDD = DD_LABEL_EXPIRE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>dd Label Expdt</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL_EXPDT_FEATURE_COUNT = DD_LABEL_EXPIRE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelRetpdImpl <em>dd Label Retpd</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddLabelRetpdImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddLabelRetpd()
   * @generated
   */
  int DD_LABEL_RETPD = 94;

  /**
   * The feature id for the '<em><b>Nnnn</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL_RETPD__NNNN = DD_LABEL_EXPIRE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Label Retpd</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LABEL_RETPD_FEATURE_COUNT = DD_LABEL_EXPIRE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddKeyoffImpl <em>dd Keyoff</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddKeyoffImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddKeyoff()
   * @generated
   */
  int DD_KEYOFF = 95;

  /**
   * The feature id for the '<em><b>Length</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_KEYOFF__LENGTH = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Keyoff</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_KEYOFF_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddKeylenImpl <em>dd Keylen</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddKeylenImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddKeylen()
   * @generated
   */
  int DD_KEYLEN = 96;

  /**
   * The feature id for the '<em><b>Length</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_KEYLEN__LENGTH = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Keylen</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_KEYLEN_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddHoldImpl <em>dd Hold</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddHoldImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddHold()
   * @generated
   */
  int DD_HOLD = 97;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_HOLD__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Hold</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_HOLD_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddFreeImpl <em>dd Free</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddFreeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddFree()
   * @generated
   */
  int DD_FREE = 98;

  /**
   * The number of structural features of the '<em>dd Free</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_FREE_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddFlashImpl <em>dd Flash</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddFlashImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddFlash()
   * @generated
   */
  int DD_FLASH = 99;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_FLASH__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_FLASH__COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Flash</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_FLASH_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddFiledataImpl <em>dd Filedata</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddFiledataImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddFiledata()
   * @generated
   */
  int DD_FILEDATA = 100;

  /**
   * The number of structural features of the '<em>dd Filedata</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_FILEDATA_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddFcbImpl <em>dd Fcb</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddFcbImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddFcb()
   * @generated
   */
  int DD_FCB = 101;

  /**
   * The feature id for the '<em><b>Fcbname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_FCB__FCBNAME = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Fcb</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_FCB_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddExpdtImpl <em>dd Expdt</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddExpdtImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddExpdt()
   * @generated
   */
  int DD_EXPDT = 102;

  /**
   * The feature id for the '<em><b>Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_EXPDT__DATE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Year</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_EXPDT__YEAR = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Day</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_EXPDT__DAY = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>dd Expdt</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_EXPDT_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDsorgImpl <em>dd Dsorg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDsorgImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDsorg()
   * @generated
   */
  int DD_DSORG = 103;

  /**
   * The feature id for the '<em><b>Dsorg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DSORG__DSORG = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Dsorg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DSORG_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDsnameImpl <em>dd Dsname</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDsnameImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDsname()
   * @generated
   */
  int DD_DSNAME = 104;

  /**
   * The feature id for the '<em><b>DSNAMES</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DSNAME__DSNAMES = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Dsname</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DSNAME_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDsntypeImpl <em>dd Dsntype</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDsntypeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDsntype()
   * @generated
   */
  int DD_DSNTYPE = 105;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DSNTYPE__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Dsntype</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DSNTYPE_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDsidImpl <em>dd Dsid</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDsidImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDsid()
   * @generated
   */
  int DD_DSID = 106;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DSID__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>V</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DSID__V = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Dsid</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DSID_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDlmImpl <em>dd Dlm</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDlmImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDlm()
   * @generated
   */
  int DD_DLM = 107;

  /**
   * The feature id for the '<em><b>Delimiter</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DLM__DELIMITER = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Delim</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DLM__DELIM = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Dlm</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DLM_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDispImpl <em>dd Disp</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDispImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDisp()
   * @generated
   */
  int DD_DISP = 108;

  /**
   * The feature id for the '<em><b>Status</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DISP__STATUS = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Normal disp</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DISP__NORMAL_DISP = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Abnormal disp</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DISP__ABNORMAL_DISP = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>dd Disp</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DISP_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDestImpl <em>dd Dest</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDestImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDest()
   * @generated
   */
  int DD_DEST = 109;

  /**
   * The feature id for the '<em><b>Destination</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DEST__DESTINATION = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Dest</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DEST_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDdnameImpl <em>dd Ddname</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDdnameImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDdname()
   * @generated
   */
  int DD_DDNAME = 110;

  /**
   * The feature id for the '<em><b>Ddname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DDNAME__DDNAME = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Ddname</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DDNAME_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDcbImpl <em>dd Dcb</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDcbImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDcb()
   * @generated
   */
  int DD_DCB = 111;

  /**
   * The feature id for the '<em><b>Subparm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DCB__SUBPARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Subparm2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DCB__SUBPARM2 = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Dsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DCB__DSNAME = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>dd Dcb</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DCB_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDcbSubparmsImpl <em>dd Dcb Subparms</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDcbSubparmsImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDcbSubparms()
   * @generated
   */
  int DD_DCB_SUBPARMS = 112;

  /**
   * The number of structural features of the '<em>dd Dcb Subparms</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DCB_SUBPARMS_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddTrtchImpl <em>dd Trtch</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddTrtchImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddTrtch()
   * @generated
   */
  int DD_TRTCH = 113;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_TRTCH__PARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Trtch</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_TRTCH_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddThreshImpl <em>dd Thresh</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddThreshImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddThresh()
   * @generated
   */
  int DD_THRESH = 114;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_THRESH__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Thresh</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_THRESH_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddStackImpl <em>dd Stack</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddStackImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddStack()
   * @generated
   */
  int DD_STACK = 115;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_STACK__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Stack</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_STACK_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddRkpImpl <em>dd Rkp</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddRkpImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddRkp()
   * @generated
   */
  int DD_RKP = 116;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RKP__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Rkp</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RKP_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddReserveImpl <em>dd Reserve</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddReserveImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddReserve()
   * @generated
   */
  int DD_RESERVE = 117;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RESERVE__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RESERVE__VALUE2 = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Reserve</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_RESERVE_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPrtspImpl <em>dd Prtsp</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddPrtspImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddPrtsp()
   * @generated
   */
  int DD_PRTSP = 118;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PRTSP__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Prtsp</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PRTSP_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPciImpl <em>dd Pci</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddPciImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddPci()
   * @generated
   */
  int DD_PCI = 119;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PCI__PARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Parm2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PCI__PARM2 = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Pci</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_PCI_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddOptcdImpl <em>dd Optcd</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddOptcdImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddOptcd()
   * @generated
   */
  int DD_OPTCD = 120;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_OPTCD__PARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Optcd</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_OPTCD_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddNtmImpl <em>dd Ntm</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddNtmImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddNtm()
   * @generated
   */
  int DD_NTM = 121;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_NTM__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Ntm</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_NTM_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddNcpImpl <em>dd Ncp</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddNcpImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddNcp()
   * @generated
   */
  int DD_NCP = 122;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_NCP__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Ncp</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_NCP_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddModeImpl <em>dd Mode</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddModeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddMode()
   * @generated
   */
  int DD_MODE = 123;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_MODE__PARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Mode</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_MODE_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLimctImpl <em>dd Limct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddLimctImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddLimct()
   * @generated
   */
  int DD_LIMCT = 124;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LIMCT__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Limct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_LIMCT_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddIpltxidImpl <em>dd Ipltxid</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddIpltxidImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddIpltxid()
   * @generated
   */
  int DD_IPLTXID = 125;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_IPLTXID__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Ipltxid</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_IPLTXID_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddIntvlImpl <em>dd Intvl</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddIntvlImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddIntvl()
   * @generated
   */
  int DD_INTVL = 126;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_INTVL__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Intvl</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_INTVL_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddGncpImpl <em>dd Gncp</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddGncpImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddGncp()
   * @generated
   */
  int DD_GNCP = 127;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_GNCP__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Gncp</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_GNCP_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddFuncImpl <em>dd Func</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddFuncImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddFunc()
   * @generated
   */
  int DD_FUNC = 128;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_FUNC__PARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Func</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_FUNC_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddEroptImpl <em>dd Eropt</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddEroptImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddEropt()
   * @generated
   */
  int DD_EROPT = 129;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_EROPT__PARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Eropt</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_EROPT_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDiagnsImpl <em>dd Diagns</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDiagnsImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDiagns()
   * @generated
   */
  int DD_DIAGNS = 130;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DIAGNS__PARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Diagns</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DIAGNS_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDenImpl <em>dd Den</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDenImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDen()
   * @generated
   */
  int DD_DEN = 131;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DEN__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Den</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DEN_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddCyloflImpl <em>dd Cylofl</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddCyloflImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddCylofl()
   * @generated
   */
  int DD_CYLOFL = 132;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CYLOFL__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Cylofl</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CYLOFL_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddCpriImpl <em>dd Cpri</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddCpriImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddCpri()
   * @generated
   */
  int DD_CPRI = 133;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CPRI__PARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Cpri</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CPRI_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBufsizeImpl <em>dd Bufsize</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBufsizeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBufsize()
   * @generated
   */
  int DD_BUFSIZE = 134;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFSIZE__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Bufsize</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFSIZE_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBufoutImpl <em>dd Bufout</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBufoutImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBufout()
   * @generated
   */
  int DD_BUFOUT = 135;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFOUT__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Bufout</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFOUT_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBufoffImpl <em>dd Bufoff</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBufoffImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBufoff()
   * @generated
   */
  int DD_BUFOFF = 136;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFOFF__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>L</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFOFF__L = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Bufoff</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFOFF_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBufnoImpl <em>dd Bufno</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBufnoImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBufno()
   * @generated
   */
  int DD_BUFNO = 137;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFNO__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Bufno</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFNO_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBufmaxImpl <em>dd Bufmax</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBufmaxImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBufmax()
   * @generated
   */
  int DD_BUFMAX = 138;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFMAX__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Bufmax</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFMAX_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBuflImpl <em>dd Bufl</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBuflImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBufl()
   * @generated
   */
  int DD_BUFL = 139;

  /**
   * The feature id for the '<em><b>Bytes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFL__BYTES = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Bufl</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFL_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBufinImpl <em>dd Bufin</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBufinImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBufin()
   * @generated
   */
  int DD_BUFIN = 140;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFIN__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Bufin</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BUFIN_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBftekImpl <em>dd Bftek</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBftekImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBftek()
   * @generated
   */
  int DD_BFTEK = 141;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BFTEK__PARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Bftek</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BFTEK_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBfalnImpl <em>dd Bfaln</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBfalnImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBfaln()
   * @generated
   */
  int DD_BFALN = 142;

  /**
   * The feature id for the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BFALN__PARM = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Bfaln</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BFALN_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDataclasImpl <em>dd Dataclas</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDataclasImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddDataclas()
   * @generated
   */
  int DD_DATACLAS = 143;

  /**
   * The feature id for the '<em><b>Dataclass</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DATACLAS__DATACLASS = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Dataclas</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DATACLAS_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDataImpl <em>dd Data</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddDataImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddData()
   * @generated
   */
  int DD_DATA = 144;

  /**
   * The feature id for the '<em><b>Dddata</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DATA__DDDATA = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Data</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_DATA_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddCopiesImpl <em>dd Copies</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddCopiesImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddCopies()
   * @generated
   */
  int DD_COPIES = 145;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_COPIES__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Group value</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_COPIES__GROUP_VALUE = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Copies</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_COPIES_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddCntlImpl <em>dd Cntl</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddCntlImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddCntl()
   * @generated
   */
  int DD_CNTL = 146;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CNTL__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Cntl</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CNTL_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddChkptImpl <em>dd Chkpt</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddChkptImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddChkpt()
   * @generated
   */
  int DD_CHKPT = 147;

  /**
   * The number of structural features of the '<em>dd Chkpt</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CHKPT_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddCharsImpl <em>dd Chars</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddCharsImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddChars()
   * @generated
   */
  int DD_CHARS = 148;

  /**
   * The feature id for the '<em><b>Tablename</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CHARS__TABLENAME = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>That</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CHARS__THAT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Tablename2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CHARS__TABLENAME2 = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>dd Chars</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CHARS_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddCcsidImpl <em>dd Ccsid</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddCcsidImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddCcsid()
   * @generated
   */
  int DD_CCSID = 149;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CCSID__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Ccsid</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_CCSID_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBurstImpl <em>dd Burst</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBurstImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBurst()
   * @generated
   */
  int DD_BURST = 150;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BURST__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Burst</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BURST_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBlkszlimImpl <em>dd Blkszlim</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBlkszlimImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBlkszlim()
   * @generated
   */
  int DD_BLKSZLIM = 151;

  /**
   * The feature id for the '<em><b>Size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BLKSZLIM__SIZE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Blkszlim</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BLKSZLIM_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddBlksizeImpl <em>dd Blksize</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddBlksizeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddBlksize()
   * @generated
   */
  int DD_BLKSIZE = 152;

  /**
   * The feature id for the '<em><b>Size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BLKSIZE__SIZE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Unit</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BLKSIZE__UNIT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Blksize</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_BLKSIZE_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddAvgrecImpl <em>dd Avgrec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddAvgrecImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddAvgrec()
   * @generated
   */
  int DD_AVGREC = 153;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_AVGREC__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Avgrec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_AVGREC_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddAmpImpl <em>dd Amp</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddAmpImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddAmp()
   * @generated
   */
  int DD_AMP = 154;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_AMP__VALUE = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_AMP__VALUE2 = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>dd Amp</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_AMP_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.impl.ddAccodeImpl <em>dd Accode</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.impl.ddAccodeImpl
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddAccode()
   * @generated
   */
  int DD_ACCODE = 155;

  /**
   * The feature id for the '<em><b>Access</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_ACCODE__ACCESS = DDSECTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>dd Accode</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DD_ACCODE_FEATURE_COUNT = DDSECTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.Typrun_type <em>Typrun type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.Typrun_type
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getTyprun_type()
   * @generated
   */
  int TYPRUN_TYPE = 156;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.jobRdValues <em>job Rd Values</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.jobRdValues
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getjobRdValues()
   * @generated
   */
  int JOB_RD_VALUES = 157;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.pagesDef <em>pages Def</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.pagesDef
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getpagesDef()
   * @generated
   */
  int PAGES_DEF = 158;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.ddRecorgValues <em>dd Recorg Values</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.ddRecorgValues
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddRecorgValues()
   * @generated
   */
  int DD_RECORG_VALUES = 159;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.ddRecfmValues <em>dd Recfm Values</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.ddRecfmValues
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddRecfmValues()
   * @generated
   */
  int DD_RECFM_VALUES = 160;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.ddPathmodeOpts <em>dd Pathmode Opts</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathmodeOpts
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddPathmodeOpts()
   * @generated
   */
  int DD_PATHMODE_OPTS = 161;

  /**
   * The meta object id for the '{@link org.kb.jcl.jcldsl.jclDsl.ddPathoptsOpts <em>dd Pathopts Opts</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathoptsOpts
   * @see org.kb.jcl.jcldsl.jclDsl.impl.JclDslPackageImpl#getddPathoptsOpts()
   * @generated
   */
  int DD_PATHOPTS_OPTS = 162;


  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.Keyword <em>Keyword</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Keyword</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.Keyword
   * @generated
   */
  EClass getKeyword();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.STEPCATDD <em>STEPCATDD</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>STEPCATDD</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.STEPCATDD
   * @generated
   */
  EClass getSTEPCATDD();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.STEPCATDD#getSection <em>Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Section</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.STEPCATDD#getSection()
   * @see #getSTEPCATDD()
   * @generated
   */
  EReference getSTEPCATDD_Section();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.STEPCATDD#getSections <em>Sections</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sections</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.STEPCATDD#getSections()
   * @see #getSTEPCATDD()
   * @generated
   */
  EReference getSTEPCATDD_Sections();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.JOBLIBDD <em>JOBLIBDD</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>JOBLIBDD</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOBLIBDD
   * @generated
   */
  EClass getJOBLIBDD();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.JOBLIBDD#getSection <em>Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Section</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOBLIBDD#getSection()
   * @see #getJOBLIBDD()
   * @generated
   */
  EReference getJOBLIBDD_Section();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.JOBLIBDD#getSections <em>Sections</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sections</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOBLIBDD#getSections()
   * @see #getJOBLIBDD()
   * @generated
   */
  EReference getJOBLIBDD_Sections();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.JOBCATDD <em>JOBCATDD</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>JOBCATDD</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOBCATDD
   * @generated
   */
  EClass getJOBCATDD();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.JOBCATDD#getSection <em>Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Section</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOBCATDD#getSection()
   * @see #getJOBCATDD()
   * @generated
   */
  EReference getJOBCATDD_Section();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.JOBCATDD#getSections <em>Sections</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sections</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOBCATDD#getSections()
   * @see #getJOBCATDD()
   * @generated
   */
  EReference getJOBCATDD_Sections();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.specddSection <em>specdd Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>specdd Section</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.specddSection
   * @generated
   */
  EClass getspecddSection();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.specddDisp <em>specdd Disp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>specdd Disp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.specddDisp
   * @generated
   */
  EClass getspecddDisp();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.specddDisp#getDisp <em>Disp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Disp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.specddDisp#getDisp()
   * @see #getspecddDisp()
   * @generated
   */
  EAttribute getspecddDisp_Disp();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.specddDsname <em>specdd Dsname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>specdd Dsname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.specddDsname
   * @generated
   */
  EClass getspecddDsname();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.specddDsname#getSpecname <em>Specname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Specname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.specddDsname#getSpecname()
   * @see #getspecddDsname()
   * @generated
   */
  EAttribute getspecddDsname_Specname();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.specddDsname#getDsname <em>Dsname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dsname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.specddDsname#getDsname()
   * @see #getspecddDsname()
   * @generated
   */
  EAttribute getspecddDsname_Dsname();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.IF <em>IF</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>IF</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.IF
   * @generated
   */
  EClass getIF();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.IF#isIFNAME <em>IFNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>IFNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.IF#isIFNAME()
   * @see #getIF()
   * @generated
   */
  EAttribute getIF_IFNAME();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.IF#getSTEPNAME <em>STEPNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>STEPNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.IF#getSTEPNAME()
   * @see #getIF()
   * @generated
   */
  EAttribute getIF_STEPNAME();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.IF#getExpression_keyword <em>Expression keyword</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Expression keyword</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.IF#getExpression_keyword()
   * @see #getIF()
   * @generated
   */
  EAttribute getIF_Expression_keyword();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.IF#getOperator <em>Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operator</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.IF#getOperator()
   * @see #getIF()
   * @generated
   */
  EAttribute getIF_Operator();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.IF#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.IF#getValue()
   * @see #getIF()
   * @generated
   */
  EAttribute getIF_Value();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.IF#getLogical_op <em>Logical op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Logical op</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.IF#getLogical_op()
   * @see #getIF()
   * @generated
   */
  EAttribute getIF_Logical_op();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.IF#getExpression_keyword2 <em>Expression keyword2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Expression keyword2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.IF#getExpression_keyword2()
   * @see #getIF()
   * @generated
   */
  EAttribute getIF_Expression_keyword2();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.IF#getOperator2 <em>Operator2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Operator2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.IF#getOperator2()
   * @see #getIF()
   * @generated
   */
  EAttribute getIF_Operator2();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.IF#getValue2 <em>Value2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Value2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.IF#getValue2()
   * @see #getIF()
   * @generated
   */
  EAttribute getIF_Value2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ELSE <em>ELSE</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>ELSE</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ELSE
   * @generated
   */
  EClass getELSE();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ELSE#getELSENAME <em>ELSENAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>ELSENAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ELSE#getELSENAME()
   * @see #getELSE()
   * @generated
   */
  EAttribute getELSE_ELSENAME();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ENDIF <em>ENDIF</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>ENDIF</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ENDIF
   * @generated
   */
  EClass getENDIF();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ENDIF#getENDIFNAME <em>ENDIFNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>ENDIFNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ENDIF#getENDIFNAME()
   * @see #getENDIF()
   * @generated
   */
  EAttribute getENDIF_ENDIFNAME();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.PROC <em>PROC</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PROC</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.PROC
   * @generated
   */
  EClass getPROC();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.PROC#isPROCNAME <em>PROCNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>PROCNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.PROC#isPROCNAME()
   * @see #getPROC()
   * @generated
   */
  EAttribute getPROC_PROCNAME();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.PROC#getPARMNAME <em>PARMNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>PARMNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.PROC#getPARMNAME()
   * @see #getPROC()
   * @generated
   */
  EAttribute getPROC_PARMNAME();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.PROC#getParm_value <em>Parm value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.PROC#getParm_value()
   * @see #getPROC()
   * @generated
   */
  EAttribute getPROC_Parm_value();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.PROC#getDSNAMES <em>DSNAMES</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>DSNAMES</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.PROC#getDSNAMES()
   * @see #getPROC()
   * @generated
   */
  EAttribute getPROC_DSNAMES();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.PROC#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.PROC#getValue()
   * @see #getPROC()
   * @generated
   */
  EAttribute getPROC_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.PEND <em>PEND</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PEND</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.PEND
   * @generated
   */
  EClass getPEND();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.PEND#isPENDNAME <em>PENDNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>PENDNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.PEND#isPENDNAME()
   * @see #getPEND()
   * @generated
   */
  EAttribute getPEND_PENDNAME();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.SET <em>SET</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>SET</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.SET
   * @generated
   */
  EClass getSET();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.SET#getSETNAME <em>SETNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>SETNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.SET#getSETNAME()
   * @see #getSET()
   * @generated
   */
  EAttribute getSET_SETNAME();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.SET#getParmname <em>Parmname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parmname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.SET#getParmname()
   * @see #getSET()
   * @generated
   */
  EAttribute getSET_Parmname();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.SET#getParm_value <em>Parm value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.SET#getParm_value()
   * @see #getSET()
   * @generated
   */
  EAttribute getSET_Parm_value();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.SET#getParm_valueb <em>Parm valueb</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm valueb</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.SET#getParm_valueb()
   * @see #getSET()
   * @generated
   */
  EAttribute getSET_Parm_valueb();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.SET#getParmname2 <em>Parmname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Parmname2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.SET#getParmname2()
   * @see #getSET()
   * @generated
   */
  EAttribute getSET_Parmname2();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.SET#getParm_value2 <em>Parm value2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Parm value2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.SET#getParm_value2()
   * @see #getSET()
   * @generated
   */
  EAttribute getSET_Parm_value2();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.SET#getParm_valueb2 <em>Parm valueb2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Parm valueb2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.SET#getParm_valueb2()
   * @see #getSET()
   * @generated
   */
  EAttribute getSET_Parm_valueb2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.JCLLIB <em>JCLLIB</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>JCLLIB</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JCLLIB
   * @generated
   */
  EClass getJCLLIB();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.JCLLIB#getJCLLIBNAME <em>JCLLIBNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>JCLLIBNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JCLLIB#getJCLLIBNAME()
   * @see #getJCLLIB()
   * @generated
   */
  EAttribute getJCLLIB_JCLLIBNAME();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.JCLLIB#getDSNAMES <em>DSNAMES</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>DSNAMES</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JCLLIB#getDSNAMES()
   * @see #getJCLLIB()
   * @generated
   */
  EAttribute getJCLLIB_DSNAMES();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.JCLLIB#getDSNAMES2 <em>DSNAMES2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>DSNAMES2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JCLLIB#getDSNAMES2()
   * @see #getJCLLIB()
   * @generated
   */
  EAttribute getJCLLIB_DSNAMES2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.EXEC <em>EXEC</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>EXEC</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.EXEC
   * @generated
   */
  EClass getEXEC();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getEXECNAME <em>EXECNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>EXECNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.EXEC#getEXECNAME()
   * @see #getEXEC()
   * @generated
   */
  EAttribute getEXEC_EXECNAME();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getSection <em>Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Section</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.EXEC#getSection()
   * @see #getEXEC()
   * @generated
   */
  EReference getEXEC_Section();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getPROCEDURE <em>PROCEDURE</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>PROCEDURE</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.EXEC#getPROCEDURE()
   * @see #getEXEC()
   * @generated
   */
  EAttribute getEXEC_PROCEDURE();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getSections <em>Sections</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sections</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.EXEC#getSections()
   * @see #getEXEC()
   * @generated
   */
  EReference getEXEC_Sections();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getDds <em>Dds</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Dds</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.EXEC#getDds()
   * @see #getEXEC()
   * @generated
   */
  EReference getEXEC_Dds();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execsection <em>execsection</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>execsection</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execsection
   * @generated
   */
  EClass getexecsection();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execTime <em>exec Time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Time</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execTime
   * @generated
   */
  EClass getexecTime();

  /**
   * Returns the meta object for the containment reference '{@link org.kb.jcl.jcldsl.jclDsl.execTime#getExecTimevalue <em>Exec Timevalue</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exec Timevalue</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execTime#getExecTimevalue()
   * @see #getexecTime()
   * @generated
   */
  EReference getexecTime_ExecTimevalue();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execTimevalue <em>exec Timevalue</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Timevalue</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execTimevalue
   * @generated
   */
  EClass getexecTimevalue();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execTimevalue#getMinutes <em>Minutes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Minutes</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execTimevalue#getMinutes()
   * @see #getexecTimevalue()
   * @generated
   */
  EAttribute getexecTimevalue_Minutes();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execTimevalue#isSeconds <em>Seconds</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Seconds</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execTimevalue#isSeconds()
   * @see #getexecTimevalue()
   * @generated
   */
  EAttribute getexecTimevalue_Seconds();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execPerform <em>exec Perform</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Perform</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execPerform
   * @generated
   */
  EClass getexecPerform();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execPerform#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execPerform#getValue()
   * @see #getexecPerform()
   * @generated
   */
  EAttribute getexecPerform_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execRegion <em>exec Region</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Region</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execRegion
   * @generated
   */
  EClass getexecRegion();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execRegion#getRegion_size <em>Region size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Region size</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execRegion#getRegion_size()
   * @see #getexecRegion()
   * @generated
   */
  EAttribute getexecRegion_Region_size();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execRegion#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Size</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execRegion#getSize()
   * @see #getexecRegion()
   * @generated
   */
  EAttribute getexecRegion_Size();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execRd <em>exec Rd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Rd</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execRd
   * @generated
   */
  EClass getexecRd();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execRd#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execRd#getValue()
   * @see #getexecRd()
   * @generated
   */
  EAttribute getexecRd_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execParm <em>exec Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execParm
   * @generated
   */
  EClass getexecParm();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execParm#getParm()
   * @see #getexecParm()
   * @generated
   */
  EAttribute getexecParm_Parm();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParmkeyw <em>Parmkeyw</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parmkeyw</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execParm#getParmkeyw()
   * @see #getexecParm()
   * @generated
   */
  EAttribute getexecParm_Parmkeyw();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParm2 <em>Parm2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Parm2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execParm#getParm2()
   * @see #getexecParm()
   * @generated
   */
  EAttribute getexecParm_Parm2();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParmkeyw2 <em>Parmkeyw2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Parmkeyw2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execParm#getParmkeyw2()
   * @see #getexecParm()
   * @generated
   */
  EAttribute getexecParm_Parmkeyw2();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParmint <em>Parmint</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parmint</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execParm#getParmint()
   * @see #getexecParm()
   * @generated
   */
  EAttribute getexecParm_Parmint();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execDynambr <em>exec Dynambr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Dynambr</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execDynambr
   * @generated
   */
  EClass getexecDynambr();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execDynambr#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execDynambr#getValue()
   * @see #getexecDynambr()
   * @generated
   */
  EAttribute getexecDynambr_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execCond <em>exec Cond</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Cond</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCond
   * @generated
   */
  EClass getexecCond();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execCond#getRc <em>Rc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Rc</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCond#getRc()
   * @see #getexecCond()
   * @generated
   */
  EAttribute getexecCond_Rc();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execCond#getExeccondop <em>Execcondop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Execcondop</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCond#getExeccondop()
   * @see #getexecCond()
   * @generated
   */
  EAttribute getexecCond_Execcondop();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execCond#isStepname <em>Stepname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Stepname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCond#isStepname()
   * @see #getexecCond()
   * @generated
   */
  EAttribute getexecCond_Stepname();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execCond#getRc2 <em>Rc2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Rc2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCond#getRc2()
   * @see #getexecCond()
   * @generated
   */
  EAttribute getexecCond_Rc2();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execCond#isStepname2 <em>Stepname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Stepname2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCond#isStepname2()
   * @see #getexecCond()
   * @generated
   */
  EAttribute getexecCond_Stepname2();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.execCond#getRc3 <em>Rc3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Rc3</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCond#getRc3()
   * @see #getexecCond()
   * @generated
   */
  EAttribute getexecCond_Rc3();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.execCond#getExeccondop2 <em>Execcondop2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Execcondop2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCond#getExeccondop2()
   * @see #getexecCond()
   * @generated
   */
  EAttribute getexecCond_Execcondop2();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.execCond#getStepname3 <em>Stepname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Stepname3</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCond#getStepname3()
   * @see #getexecCond()
   * @generated
   */
  EAttribute getexecCond_Stepname3();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execAddrspc <em>exec Addrspc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Addrspc</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execAddrspc
   * @generated
   */
  EClass getexecAddrspc();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execAddrspc#getAddr <em>Addr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Addr</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execAddrspc#getAddr()
   * @see #getexecAddrspc()
   * @generated
   */
  EAttribute getexecAddrspc_Addr();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execCcsid <em>exec Ccsid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Ccsid</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCcsid
   * @generated
   */
  EClass getexecCcsid();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execCcsid#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execCcsid#getValue()
   * @see #getexecCcsid()
   * @generated
   */
  EAttribute getexecCcsid_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execAcct <em>exec Acct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Acct</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execAcct
   * @generated
   */
  EClass getexecAcct();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execAcct#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execAcct#getValue()
   * @see #getexecAcct()
   * @generated
   */
  EAttribute getexecAcct_Value();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execAcct#getVALUE2 <em>VALUE2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>VALUE2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execAcct#getVALUE2()
   * @see #getexecAcct()
   * @generated
   */
  EAttribute getexecAcct_VALUE2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execProc <em>exec Proc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Proc</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execProc
   * @generated
   */
  EClass getexecProc();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execProc#getPROCNAME <em>PROCNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>PROCNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execProc#getPROCNAME()
   * @see #getexecProc()
   * @generated
   */
  EAttribute getexecProc_PROCNAME();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.execPgm <em>exec Pgm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exec Pgm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execPgm
   * @generated
   */
  EClass getexecPgm();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.execPgm#getPGMNAME <em>PGMNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>PGMNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.execPgm#getPGMNAME()
   * @see #getexecPgm()
   * @generated
   */
  EAttribute getexecPgm_PGMNAME();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.INCLUDE <em>INCLUDE</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>INCLUDE</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.INCLUDE
   * @generated
   */
  EClass getINCLUDE();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.INCLUDE#getINCLUDENAME <em>INCLUDENAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>INCLUDENAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.INCLUDE#getINCLUDENAME()
   * @see #getINCLUDE()
   * @generated
   */
  EAttribute getINCLUDE_INCLUDENAME();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.INCLUDE#getMEMBERNAME <em>MEMBERNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>MEMBERNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.INCLUDE#getMEMBERNAME()
   * @see #getINCLUDE()
   * @generated
   */
  EAttribute getINCLUDE_MEMBERNAME();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.JOB <em>JOB</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>JOB</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOB
   * @generated
   */
  EClass getJOB();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.JOB#getKeywords <em>Keywords</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Keywords</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOB#getKeywords()
   * @see #getJOB()
   * @generated
   */
  EReference getJOB_Keywords();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.JOB#getJOBNAME <em>JOBNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>JOBNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOB#getJOBNAME()
   * @see #getJOB()
   * @generated
   */
  EAttribute getJOB_JOBNAME();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.JOB#getSection <em>Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Section</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOB#getSection()
   * @see #getJOB()
   * @generated
   */
  EReference getJOB_Section();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.JOB#getSections <em>Sections</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sections</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.JOB#getSections()
   * @see #getJOB()
   * @generated
   */
  EReference getJOB_Sections();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobsection <em>jobsection</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>jobsection</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobsection
   * @generated
   */
  EClass getjobsection();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobUser <em>job User</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job User</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobUser
   * @generated
   */
  EClass getjobUser();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobUser#getUSERNAME <em>USERNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>USERNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobUser#getUSERNAME()
   * @see #getjobUser()
   * @generated
   */
  EAttribute getjobUser_USERNAME();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobTyprun <em>job Typrun</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Typrun</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTyprun
   * @generated
   */
  EClass getjobTyprun();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobTyprun#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTyprun#getType()
   * @see #getjobTyprun()
   * @generated
   */
  EAttribute getjobTyprun_Type();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobTime <em>job Time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Time</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTime
   * @generated
   */
  EClass getjobTime();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobTime#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTime#getValue()
   * @see #getjobTime()
   * @generated
   */
  EAttribute getjobTime_Value();

  /**
   * Returns the meta object for the containment reference '{@link org.kb.jcl.jcldsl.jclDsl.jobTime#getJobTimevalue <em>Job Timevalue</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Job Timevalue</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTime#getJobTimevalue()
   * @see #getjobTime()
   * @generated
   */
  EReference getjobTime_JobTimevalue();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobTimevalue <em>job Timevalue</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Timevalue</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTimevalue
   * @generated
   */
  EClass getjobTimevalue();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobTimevalue#getMinutes <em>Minutes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Minutes</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTimevalue#getMinutes()
   * @see #getjobTimevalue()
   * @generated
   */
  EAttribute getjobTimevalue_Minutes();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobTimevalue#isSeconds <em>Seconds</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Seconds</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTimevalue#isSeconds()
   * @see #getjobTimevalue()
   * @generated
   */
  EAttribute getjobTimevalue_Seconds();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobSchenv <em>job Schenv</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Schenv</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobSchenv
   * @generated
   */
  EClass getjobSchenv();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobSchenv#getENVIRONMENT <em>ENVIRONMENT</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>ENVIRONMENT</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobSchenv#getENVIRONMENT()
   * @see #getjobSchenv()
   * @generated
   */
  EAttribute getjobSchenv_ENVIRONMENT();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobSeclabel <em>job Seclabel</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Seclabel</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobSeclabel
   * @generated
   */
  EClass getjobSeclabel();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobSeclabel#getSECURITYLABEL <em>SECURITYLABEL</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>SECURITYLABEL</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobSeclabel#getSECURITYLABEL()
   * @see #getjobSeclabel()
   * @generated
   */
  EAttribute getjobSeclabel_SECURITYLABEL();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobRestart <em>job Restart</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Restart</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRestart
   * @generated
   */
  EClass getjobRestart();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobRestart#getJOBSTEP <em>JOBSTEP</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>JOBSTEP</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRestart#getJOBSTEP()
   * @see #getjobRestart()
   * @generated
   */
  EAttribute getjobRestart_JOBSTEP();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobRegion <em>job Region</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Region</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRegion
   * @generated
   */
  EClass getjobRegion();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobRegion#getRegion_size <em>Region size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Region size</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRegion#getRegion_size()
   * @see #getjobRegion()
   * @generated
   */
  EAttribute getjobRegion_Region_size();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobRegion#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Size</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRegion#getSize()
   * @see #getjobRegion()
   * @generated
   */
  EAttribute getjobRegion_Size();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobRd <em>job Rd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Rd</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRd
   * @generated
   */
  EClass getjobRd();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobRd#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRd#getValue()
   * @see #getjobRd()
   * @generated
   */
  EAttribute getjobRd_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobPrty <em>job Prty</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Prty</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPrty
   * @generated
   */
  EClass getjobPrty();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobPrty#getPrtyvalue <em>Prtyvalue</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Prtyvalue</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPrty#getPrtyvalue()
   * @see #getjobPrty()
   * @generated
   */
  EAttribute getjobPrty_Prtyvalue();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobProgname <em>job Progname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Progname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobProgname
   * @generated
   */
  EClass getjobProgname();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobProgname#getProgrammer_name <em>Programmer name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Programmer name</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobProgname#getProgrammer_name()
   * @see #getjobProgname()
   * @generated
   */
  EAttribute getjobProgname_Programmer_name();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobProgname#getPROGRAMMER_ID <em>PROGRAMMER ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>PROGRAMMER ID</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobProgname#getPROGRAMMER_ID()
   * @see #getjobProgname()
   * @generated
   */
  EAttribute getjobProgname_PROGRAMMER_ID();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobPerform <em>job Perform</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Perform</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPerform
   * @generated
   */
  EClass getjobPerform();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobPerform#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPerform#getValue()
   * @see #getjobPerform()
   * @generated
   */
  EAttribute getjobPerform_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobPassword <em>job Password</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Password</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPassword
   * @generated
   */
  EClass getjobPassword();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobPassword#getPASSWORD <em>PASSWORD</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>PASSWORD</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPassword#getPASSWORD()
   * @see #getjobPassword()
   * @generated
   */
  EAttribute getjobPassword_PASSWORD();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobPassword#getNEWPASSWORD <em>NEWPASSWORD</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>NEWPASSWORD</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPassword#getNEWPASSWORD()
   * @see #getjobPassword()
   * @generated
   */
  EAttribute getjobPassword_NEWPASSWORD();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobPages <em>job Pages</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Pages</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPages
   * @generated
   */
  EClass getjobPages();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobPages#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPages#getValue()
   * @see #getjobPages()
   * @generated
   */
  EAttribute getjobPages_Value();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.jobPages#getPagesDef <em>Pages Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Pages Def</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPages#getPagesDef()
   * @see #getjobPages()
   * @generated
   */
  EAttribute getjobPages_PagesDef();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobNotify <em>job Notify</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Notify</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobNotify
   * @generated
   */
  EClass getjobNotify();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobNotify#getUSERID <em>USERID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>USERID</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobNotify#getUSERID()
   * @see #getjobNotify()
   * @generated
   */
  EAttribute getjobNotify_USERID();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobMsglevel <em>job Msglevel</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Msglevel</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobMsglevel
   * @generated
   */
  EClass getjobMsglevel();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobMsglevel#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobMsglevel#getValue()
   * @see #getjobMsglevel()
   * @generated
   */
  EAttribute getjobMsglevel_Value();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.jobMsglevel#getJobMsglevelStatements <em>Job Msglevel Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Job Msglevel Statements</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobMsglevel#getJobMsglevelStatements()
   * @see #getjobMsglevel()
   * @generated
   */
  EAttribute getjobMsglevel_JobMsglevelStatements();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.jobMsglevel#getJobMsglevelMessages <em>Job Msglevel Messages</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Job Msglevel Messages</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobMsglevel#getJobMsglevelMessages()
   * @see #getjobMsglevel()
   * @generated
   */
  EAttribute getjobMsglevel_JobMsglevelMessages();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobMsgClass <em>job Msg Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Msg Class</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobMsgClass
   * @generated
   */
  EClass getjobMsgClass();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobMsgClass#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobMsgClass#getValue()
   * @see #getjobMsgClass()
   * @generated
   */
  EAttribute getjobMsgClass_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobLines <em>job Lines</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Lines</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobLines
   * @generated
   */
  EClass getjobLines();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobLines#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobLines#getValue()
   * @see #getjobLines()
   * @generated
   */
  EAttribute getjobLines_Value();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.jobLines#getPagesDef <em>Pages Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Pages Def</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobLines#getPagesDef()
   * @see #getjobLines()
   * @generated
   */
  EAttribute getjobLines_PagesDef();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobGroup <em>job Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Group</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobGroup
   * @generated
   */
  EClass getjobGroup();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobGroup#getGROUP <em>GROUP</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>GROUP</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobGroup#getGROUP()
   * @see #getjobGroup()
   * @generated
   */
  EAttribute getjobGroup_GROUP();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobCond <em>job Cond</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Cond</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCond
   * @generated
   */
  EClass getjobCond();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobCond#getReturn_code <em>Return code</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Return code</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCond#getReturn_code()
   * @see #getjobCond()
   * @generated
   */
  EAttribute getjobCond_Return_code();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobCond#getJobcondop <em>Jobcondop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Jobcondop</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCond#getJobcondop()
   * @see #getjobCond()
   * @generated
   */
  EAttribute getjobCond_Jobcondop();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.jobCond#getReturn_code2 <em>Return code2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Return code2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCond#getReturn_code2()
   * @see #getjobCond()
   * @generated
   */
  EAttribute getjobCond_Return_code2();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.jobCond#getJobcondop2 <em>Jobcondop2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Jobcondop2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCond#getJobcondop2()
   * @see #getjobCond()
   * @generated
   */
  EAttribute getjobCond_Jobcondop2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobClass <em>job Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Class</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobClass
   * @generated
   */
  EClass getjobClass();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobClass#getCLASS <em>CLASS</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>CLASS</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobClass#getCLASS()
   * @see #getjobClass()
   * @generated
   */
  EAttribute getjobClass_CLASS();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobCcsid <em>job Ccsid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Ccsid</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCcsid
   * @generated
   */
  EClass getjobCcsid();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobCcsid#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCcsid#getValue()
   * @see #getjobCcsid()
   * @generated
   */
  EAttribute getjobCcsid_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobCards <em>job Cards</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Cards</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCards
   * @generated
   */
  EClass getjobCards();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobCards#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCards#getValue()
   * @see #getjobCards()
   * @generated
   */
  EAttribute getjobCards_Value();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.jobCards#getPagesDef <em>Pages Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Pages Def</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCards#getPagesDef()
   * @see #getjobCards()
   * @generated
   */
  EAttribute getjobCards_PagesDef();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobBytes <em>job Bytes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Bytes</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobBytes
   * @generated
   */
  EClass getjobBytes();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobBytes#getValues <em>Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Values</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobBytes#getValues()
   * @see #getjobBytes()
   * @generated
   */
  EAttribute getjobBytes_Values();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.jobBytes#getPagesDef <em>Pages Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Pages Def</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobBytes#getPagesDef()
   * @see #getjobBytes()
   * @generated
   */
  EAttribute getjobBytes_PagesDef();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobAddrspc <em>job Addrspc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Addrspc</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobAddrspc
   * @generated
   */
  EClass getjobAddrspc();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobAddrspc#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobAddrspc#getValue()
   * @see #getjobAddrspc()
   * @generated
   */
  EAttribute getjobAddrspc_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo <em>job Accinfo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>job Accinfo</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobAccinfo
   * @generated
   */
  EClass getjobAccinfo();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getPano <em>Pano</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Pano</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getPano()
   * @see #getjobAccinfo()
   * @generated
   */
  EAttribute getjobAccinfo_Pano();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc <em>Acc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Acc</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc()
   * @see #getjobAccinfo()
   * @generated
   */
  EAttribute getjobAccinfo_Acc();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc2 <em>Acc2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Acc2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc2()
   * @see #getjobAccinfo()
   * @generated
   */
  EAttribute getjobAccinfo_Acc2();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc3 <em>Acc3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Acc3</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc3()
   * @see #getjobAccinfo()
   * @generated
   */
  EAttribute getjobAccinfo_Acc3();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.DD <em>DD</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>DD</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.DD
   * @generated
   */
  EClass getDD();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.DD#isDDNAME <em>DDNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>DDNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.DD#isDDNAME()
   * @see #getDD()
   * @generated
   */
  EAttribute getDD_DDNAME();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.DD#getSections <em>Sections</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sections</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.DD#getSections()
   * @see #getDD()
   * @generated
   */
  EReference getDD_Sections();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddsection <em>ddsection</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>ddsection</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddsection
   * @generated
   */
  EClass getddsection();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume <em>dd Volume</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Volume</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddVolume
   * @generated
   */
  EClass getddVolume();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getPrivate <em>Private</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Private</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddVolume#getPrivate()
   * @see #getddVolume()
   * @generated
   */
  EAttribute getddVolume_Private();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getRetain <em>Retain</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Retain</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddVolume#getRetain()
   * @see #getddVolume()
   * @generated
   */
  EAttribute getddVolume_Retain();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getVolseq <em>Volseq</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Volseq</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddVolume#getVolseq()
   * @see #getddVolume()
   * @generated
   */
  EAttribute getddVolume_Volseq();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getVolcnt <em>Volcnt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Volcnt</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddVolume#getVolcnt()
   * @see #getddVolume()
   * @generated
   */
  EAttribute getddVolume_Volcnt();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getSerial <em>Serial</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Serial</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddVolume#getSerial()
   * @see #getddVolume()
   * @generated
   */
  EAttribute getddVolume_Serial();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getSerials <em>Serials</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Serials</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddVolume#getSerials()
   * @see #getddVolume()
   * @generated
   */
  EAttribute getddVolume_Serials();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getDsname <em>Dsname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dsname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddVolume#getDsname()
   * @see #getddVolume()
   * @generated
   */
  EAttribute getddVolume_Dsname();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddUnit <em>dd Unit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Unit</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUnit
   * @generated
   */
  EClass getddUnit();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getDevice <em>Device</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Device</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUnit#getDevice()
   * @see #getddUnit()
   * @generated
   */
  EAttribute getddUnit_Device();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getCount <em>Count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Count</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUnit#getCount()
   * @see #getddUnit()
   * @generated
   */
  EAttribute getddUnit_Count();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getP <em>P</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>P</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUnit#getP()
   * @see #getddUnit()
   * @generated
   */
  EAttribute getddUnit_P();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getDefer <em>Defer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Defer</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUnit#getDefer()
   * @see #getddUnit()
   * @generated
   */
  EAttribute getddUnit_Defer();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddUcs <em>dd Ucs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Ucs</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUcs
   * @generated
   */
  EClass getddUcs();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddUcs#getCharset <em>Charset</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Charset</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUcs#getCharset()
   * @see #getddUcs()
   * @generated
   */
  EAttribute getddUcs_Charset();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddUcs#getFold <em>Fold</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fold</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUcs#getFold()
   * @see #getddUcs()
   * @generated
   */
  EAttribute getddUcs_Fold();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddUcs#getVerify <em>Verify</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Verify</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUcs#getVerify()
   * @see #getddUcs()
   * @generated
   */
  EAttribute getddUcs_Verify();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddTerm <em>dd Term</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Term</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddTerm
   * @generated
   */
  EClass getddTerm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddSysout <em>dd Sysout</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Sysout</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSysout
   * @generated
   */
  EClass getddSysout();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSysout#getClass_ <em>Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Class</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSysout#getClass_()
   * @see #getddSysout()
   * @generated
   */
  EAttribute getddSysout_Class();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSysout#getWtrname <em>Wtrname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Wtrname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSysout#getWtrname()
   * @see #getddSysout()
   * @generated
   */
  EAttribute getddSysout_Wtrname();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSysout#getFormname <em>Formname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Formname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSysout#getFormname()
   * @see #getddSysout()
   * @generated
   */
  EAttribute getddSysout_Formname();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsys <em>dd Subsys</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Subsys</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSubsys
   * @generated
   */
  EClass getddSubsys();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsys#getLabel <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Label</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSubsys#getLabel()
   * @see #getddSubsys()
   * @generated
   */
  EAttribute getddSubsys_Label();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsys#getSubsys <em>Subsys</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Subsys</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSubsys#getSubsys()
   * @see #getddSubsys()
   * @generated
   */
  EAttribute getddSubsys_Subsys();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsys#getSubparm <em>Subparm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Subparm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSubsys#getSubparm()
   * @see #getddSubsys()
   * @generated
   */
  EReference getddSubsys_Subparm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms <em>dd Subsys Subparms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Subsys Subparms</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms
   * @generated
   */
  EClass getddSubsysSubparms();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms#getSubparm <em>Subparm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Subparm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms#getSubparm()
   * @see #getddSubsysSubparms()
   * @generated
   */
  EAttribute getddSubsysSubparms_Subparm();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms#getSubparm3 <em>Subparm3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Subparm3</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms#getSubparm3()
   * @see #getddSubsysSubparms()
   * @generated
   */
  EAttribute getddSubsysSubparms_Subparm3();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddStorclas <em>dd Storclas</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Storclas</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddStorclas
   * @generated
   */
  EClass getddStorclas();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddStorclas#getStorageclass <em>Storageclass</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Storageclass</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddStorclas#getStorageclass()
   * @see #getddStorclas()
   * @generated
   */
  EAttribute getddStorclas_Storageclass();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddSpin <em>dd Spin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Spin</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSpin
   * @generated
   */
  EClass getddSpin();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSpin#getUnalloc <em>Unalloc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Unalloc</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSpin#getUnalloc()
   * @see #getddSpin()
   * @generated
   */
  EAttribute getddSpin_Unalloc();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace <em>dd Space</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Space</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSpace
   * @generated
   */
  EClass getddSpace();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getLength <em>Length</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Length</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSpace#getLength()
   * @see #getddSpace()
   * @generated
   */
  EAttribute getddSpace_Length();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getPrimary <em>Primary</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Primary</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSpace#getPrimary()
   * @see #getddSpace()
   * @generated
   */
  EAttribute getddSpace_Primary();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getSecondary <em>Secondary</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Secondary</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSpace#getSecondary()
   * @see #getddSpace()
   * @generated
   */
  EAttribute getddSpace_Secondary();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getDirorindx <em>Dirorindx</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dirorindx</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSpace#getDirorindx()
   * @see #getddSpace()
   * @generated
   */
  EAttribute getddSpace_Dirorindx();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSpace#getParm()
   * @see #getddSpace()
   * @generated
   */
  EAttribute getddSpace_Parm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddSegment <em>dd Segment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Segment</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSegment
   * @generated
   */
  EClass getddSegment();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSegment#getPage_count <em>Page count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Page count</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSegment#getPage_count()
   * @see #getddSegment()
   * @generated
   */
  EAttribute getddSegment_Page_count();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddSecmodel <em>dd Secmodel</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Secmodel</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSecmodel
   * @generated
   */
  EClass getddSecmodel();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSecmodel#getDsname <em>Dsname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dsname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSecmodel#getDsname()
   * @see #getddSecmodel()
   * @generated
   */
  EAttribute getddSecmodel_Dsname();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddSecmodel#getGeneric <em>Generic</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Generic</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSecmodel#getGeneric()
   * @see #getddSecmodel()
   * @generated
   */
  EAttribute getddSecmodel_Generic();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddRls <em>dd Rls</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Rls</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRls
   * @generated
   */
  EClass getddRls();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddRls#getRls <em>Rls</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Rls</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRls#getRls()
   * @see #getddRls()
   * @generated
   */
  EAttribute getddRls_Rls();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddRetpd <em>dd Retpd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Retpd</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRetpd
   * @generated
   */
  EClass getddRetpd();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddRetpd#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRetpd#getValue()
   * @see #getddRetpd()
   * @generated
   */
  EAttribute getddRetpd_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddRefdd <em>dd Refdd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Refdd</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRefdd
   * @generated
   */
  EClass getddRefdd();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddRefdd#getDsname <em>Dsname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dsname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRefdd#getDsname()
   * @see #getddRefdd()
   * @generated
   */
  EAttribute getddRefdd_Dsname();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddRecorg <em>dd Recorg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Recorg</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRecorg
   * @generated
   */
  EClass getddRecorg();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddRecorg#getRecorg <em>Recorg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Recorg</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRecorg#getRecorg()
   * @see #getddRecorg()
   * @generated
   */
  EAttribute getddRecorg_Recorg();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddRecfm <em>dd Recfm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Recfm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRecfm
   * @generated
   */
  EClass getddRecfm();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddRecfm#getRecfm <em>Recfm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Recfm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRecfm#getRecfm()
   * @see #getddRecfm()
   * @generated
   */
  EAttribute getddRecfm_Recfm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddQname <em>dd Qname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Qname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddQname
   * @generated
   */
  EClass getddQname();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddQname#getProcname <em>Procname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Procname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddQname#getProcname()
   * @see #getddQname()
   * @generated
   */
  EAttribute getddQname_Procname();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddProtect <em>dd Protect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Protect</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddProtect
   * @generated
   */
  EClass getddProtect();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddProtect#getYes <em>Yes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Yes</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddProtect#getYes()
   * @see #getddProtect()
   * @generated
   */
  EAttribute getddProtect_Yes();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddPathmode <em>dd Pathmode</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Pathmode</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathmode
   * @generated
   */
  EClass getddPathmode();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddPathmode#getOption <em>Option</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Option</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathmode#getOption()
   * @see #getddPathmode()
   * @generated
   */
  EAttribute getddPathmode_Option();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.ddPathmode#getOption2 <em>Option2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Option2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathmode#getOption2()
   * @see #getddPathmode()
   * @generated
   */
  EAttribute getddPathmode_Option2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddPathopts <em>dd Pathopts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Pathopts</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathopts
   * @generated
   */
  EClass getddPathopts();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddPathopts#getOption <em>Option</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Option</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathopts#getOption()
   * @see #getddPathopts()
   * @generated
   */
  EAttribute getddPathopts_Option();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.ddPathopts#getOption2 <em>Option2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Option2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathopts#getOption2()
   * @see #getddPathopts()
   * @generated
   */
  EAttribute getddPathopts_Option2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp <em>dd Pathdisp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Pathdisp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathdisp
   * @generated
   */
  EClass getddPathdisp();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp <em>Disp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Disp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp()
   * @see #getddPathdisp()
   * @generated
   */
  EAttribute getddPathdisp_Disp();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp2 <em>Disp2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Disp2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp2()
   * @see #getddPathdisp()
   * @generated
   */
  EAttribute getddPathdisp_Disp2();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp3 <em>Disp3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Disp3</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp3()
   * @see #getddPathdisp()
   * @generated
   */
  EAttribute getddPathdisp_Disp3();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddPath <em>dd Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Path</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPath
   * @generated
   */
  EClass getddPath();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddPath#getPath <em>Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Path</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPath#getPath()
   * @see #getddPath()
   * @generated
   */
  EAttribute getddPath_Path();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddOutput <em>dd Output</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Output</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddOutput
   * @generated
   */
  EClass getddOutput();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddOutput#getOutput <em>Output</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Output</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddOutput#getOutput()
   * @see #getddOutput()
   * @generated
   */
  EAttribute getddOutput_Output();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.ddOutput#getOutput2 <em>Output2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Output2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddOutput#getOutput2()
   * @see #getddOutput()
   * @generated
   */
  EAttribute getddOutput_Output2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddOutlim <em>dd Outlim</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Outlim</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddOutlim
   * @generated
   */
  EClass getddOutlim();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddOutlim#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddOutlim#getValue()
   * @see #getddOutlim()
   * @generated
   */
  EAttribute getddOutlim_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddModify <em>dd Modify</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Modify</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddModify
   * @generated
   */
  EClass getddModify();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddModify#getModule <em>Module</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Module</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddModify#getModule()
   * @see #getddModify()
   * @generated
   */
  EAttribute getddModify_Module();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddModify#getTrc <em>Trc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Trc</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddModify#getTrc()
   * @see #getddModify()
   * @generated
   */
  EAttribute getddModify_Trc();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddMgmtclas <em>dd Mgmtclas</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Mgmtclas</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddMgmtclas
   * @generated
   */
  EClass getddMgmtclas();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddMgmtclas#getCLASSNAME <em>CLASSNAME</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>CLASSNAME</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddMgmtclas#getCLASSNAME()
   * @see #getddMgmtclas()
   * @generated
   */
  EAttribute getddMgmtclas_CLASSNAME();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddLrecl <em>dd Lrecl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Lrecl</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLrecl
   * @generated
   */
  EClass getddLrecl();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLrecl#getLength_in_bytes <em>Length in bytes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Length in bytes</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLrecl#getLength_in_bytes()
   * @see #getddLrecl()
   * @generated
   */
  EAttribute getddLrecl_Length_in_bytes();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddLike <em>dd Like</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Like</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLike
   * @generated
   */
  EClass getddLike();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLike#getDsname <em>Dsname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dsname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLike#getDsname()
   * @see #getddLike()
   * @generated
   */
  EAttribute getddLike_Dsname();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddLgstream <em>dd Lgstream</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Lgstream</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLgstream
   * @generated
   */
  EClass getddLgstream();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLgstream#getLogstream <em>Logstream</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Logstream</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLgstream#getLogstream()
   * @see #getddLgstream()
   * @generated
   */
  EAttribute getddLgstream_Logstream();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel <em>dd Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Label</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabel
   * @generated
   */
  EClass getddLabel();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#isSequence_number <em>Sequence number</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Sequence number</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabel#isSequence_number()
   * @see #getddLabel()
   * @generated
   */
  EAttribute getddLabel_Sequence_number();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#isLabel <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Label</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabel#isLabel()
   * @see #getddLabel()
   * @generated
   */
  EAttribute getddLabel_Label();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#getPassword <em>Password</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Password</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabel#getPassword()
   * @see #getddLabel()
   * @generated
   */
  EAttribute getddLabel_Password();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#getInorout <em>Inorout</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Inorout</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabel#getInorout()
   * @see #getddLabel()
   * @generated
   */
  EAttribute getddLabel_Inorout();

  /**
   * Returns the meta object for the containment reference '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#getExpire <em>Expire</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expire</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabel#getExpire()
   * @see #getddLabel()
   * @generated
   */
  EReference getddLabel_Expire();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpire <em>dd Label Expire</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Label Expire</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabelExpire
   * @generated
   */
  EClass getddLabelExpire();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt <em>dd Label Expdt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Label Expdt</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt
   * @generated
   */
  EClass getddLabelExpdt();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getYyddd <em>Yyddd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Yyddd</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getYyddd()
   * @see #getddLabelExpdt()
   * @generated
   */
  EAttribute getddLabelExpdt_Yyddd();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getYyyy <em>Yyyy</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Yyyy</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getYyyy()
   * @see #getddLabelExpdt()
   * @generated
   */
  EAttribute getddLabelExpdt_Yyyy();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getDdd <em>Ddd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ddd</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getDdd()
   * @see #getddLabelExpdt()
   * @generated
   */
  EAttribute getddLabelExpdt_Ddd();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelRetpd <em>dd Label Retpd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Label Retpd</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabelRetpd
   * @generated
   */
  EClass getddLabelRetpd();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelRetpd#getNnnn <em>Nnnn</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nnnn</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabelRetpd#getNnnn()
   * @see #getddLabelRetpd()
   * @generated
   */
  EAttribute getddLabelRetpd_Nnnn();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddKeyoff <em>dd Keyoff</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Keyoff</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddKeyoff
   * @generated
   */
  EClass getddKeyoff();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddKeyoff#getLength <em>Length</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Length</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddKeyoff#getLength()
   * @see #getddKeyoff()
   * @generated
   */
  EAttribute getddKeyoff_Length();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddKeylen <em>dd Keylen</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Keylen</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddKeylen
   * @generated
   */
  EClass getddKeylen();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddKeylen#getLength <em>Length</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Length</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddKeylen#getLength()
   * @see #getddKeylen()
   * @generated
   */
  EAttribute getddKeylen_Length();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddHold <em>dd Hold</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Hold</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddHold
   * @generated
   */
  EClass getddHold();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddHold#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddHold#getValue()
   * @see #getddHold()
   * @generated
   */
  EAttribute getddHold_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddFree <em>dd Free</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Free</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFree
   * @generated
   */
  EClass getddFree();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddFlash <em>dd Flash</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Flash</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFlash
   * @generated
   */
  EClass getddFlash();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddFlash#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFlash#getValue()
   * @see #getddFlash()
   * @generated
   */
  EAttribute getddFlash_Value();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddFlash#getCount <em>Count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Count</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFlash#getCount()
   * @see #getddFlash()
   * @generated
   */
  EAttribute getddFlash_Count();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddFiledata <em>dd Filedata</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Filedata</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFiledata
   * @generated
   */
  EClass getddFiledata();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddFcb <em>dd Fcb</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Fcb</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFcb
   * @generated
   */
  EClass getddFcb();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddFcb#getFcbname <em>Fcbname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fcbname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFcb#getFcbname()
   * @see #getddFcb()
   * @generated
   */
  EAttribute getddFcb_Fcbname();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddExpdt <em>dd Expdt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Expdt</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddExpdt
   * @generated
   */
  EClass getddExpdt();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddExpdt#getDate <em>Date</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Date</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddExpdt#getDate()
   * @see #getddExpdt()
   * @generated
   */
  EAttribute getddExpdt_Date();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddExpdt#getYear <em>Year</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Year</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddExpdt#getYear()
   * @see #getddExpdt()
   * @generated
   */
  EAttribute getddExpdt_Year();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddExpdt#getDay <em>Day</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Day</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddExpdt#getDay()
   * @see #getddExpdt()
   * @generated
   */
  EAttribute getddExpdt_Day();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDsorg <em>dd Dsorg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Dsorg</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsorg
   * @generated
   */
  EClass getddDsorg();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDsorg#getDsorg <em>Dsorg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dsorg</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsorg#getDsorg()
   * @see #getddDsorg()
   * @generated
   */
  EAttribute getddDsorg_Dsorg();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDsname <em>dd Dsname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Dsname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsname
   * @generated
   */
  EClass getddDsname();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDsname#getDSNAMES <em>DSNAMES</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>DSNAMES</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsname#getDSNAMES()
   * @see #getddDsname()
   * @generated
   */
  EAttribute getddDsname_DSNAMES();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDsntype <em>dd Dsntype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Dsntype</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsntype
   * @generated
   */
  EClass getddDsntype();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDsntype#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsntype#getValue()
   * @see #getddDsntype()
   * @generated
   */
  EAttribute getddDsntype_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDsid <em>dd Dsid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Dsid</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsid
   * @generated
   */
  EClass getddDsid();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDsid#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsid#getValue()
   * @see #getddDsid()
   * @generated
   */
  EAttribute getddDsid_Value();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDsid#getV <em>V</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>V</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsid#getV()
   * @see #getddDsid()
   * @generated
   */
  EAttribute getddDsid_V();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDlm <em>dd Dlm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Dlm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDlm
   * @generated
   */
  EClass getddDlm();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDlm#getDelimiter <em>Delimiter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Delimiter</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDlm#getDelimiter()
   * @see #getddDlm()
   * @generated
   */
  EAttribute getddDlm_Delimiter();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDlm#getDelim <em>Delim</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Delim</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDlm#getDelim()
   * @see #getddDlm()
   * @generated
   */
  EAttribute getddDlm_Delim();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDisp <em>dd Disp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Disp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDisp
   * @generated
   */
  EClass getddDisp();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDisp#getStatus <em>Status</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Status</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDisp#getStatus()
   * @see #getddDisp()
   * @generated
   */
  EAttribute getddDisp_Status();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDisp#getNormal_disp <em>Normal disp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Normal disp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDisp#getNormal_disp()
   * @see #getddDisp()
   * @generated
   */
  EAttribute getddDisp_Normal_disp();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDisp#getAbnormal_disp <em>Abnormal disp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Abnormal disp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDisp#getAbnormal_disp()
   * @see #getddDisp()
   * @generated
   */
  EAttribute getddDisp_Abnormal_disp();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDest <em>dd Dest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Dest</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDest
   * @generated
   */
  EClass getddDest();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDest#getDestination <em>Destination</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Destination</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDest#getDestination()
   * @see #getddDest()
   * @generated
   */
  EAttribute getddDest_Destination();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDdname <em>dd Ddname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Ddname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDdname
   * @generated
   */
  EClass getddDdname();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDdname#getDdname <em>Ddname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ddname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDdname#getDdname()
   * @see #getddDdname()
   * @generated
   */
  EAttribute getddDdname_Ddname();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDcb <em>dd Dcb</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Dcb</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDcb
   * @generated
   */
  EClass getddDcb();

  /**
   * Returns the meta object for the containment reference '{@link org.kb.jcl.jcldsl.jclDsl.ddDcb#getSubparm <em>Subparm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subparm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDcb#getSubparm()
   * @see #getddDcb()
   * @generated
   */
  EReference getddDcb_Subparm();

  /**
   * Returns the meta object for the containment reference list '{@link org.kb.jcl.jcldsl.jclDsl.ddDcb#getSubparm2 <em>Subparm2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Subparm2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDcb#getSubparm2()
   * @see #getddDcb()
   * @generated
   */
  EReference getddDcb_Subparm2();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDcb#getDsname <em>Dsname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dsname</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDcb#getDsname()
   * @see #getddDcb()
   * @generated
   */
  EAttribute getddDcb_Dsname();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDcbSubparms <em>dd Dcb Subparms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Dcb Subparms</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDcbSubparms
   * @generated
   */
  EClass getddDcbSubparms();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddTrtch <em>dd Trtch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Trtch</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddTrtch
   * @generated
   */
  EClass getddTrtch();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddTrtch#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddTrtch#getParm()
   * @see #getddTrtch()
   * @generated
   */
  EAttribute getddTrtch_Parm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddThresh <em>dd Thresh</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Thresh</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddThresh
   * @generated
   */
  EClass getddThresh();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddThresh#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddThresh#getValue()
   * @see #getddThresh()
   * @generated
   */
  EAttribute getddThresh_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddStack <em>dd Stack</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Stack</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddStack
   * @generated
   */
  EClass getddStack();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddStack#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddStack#getValue()
   * @see #getddStack()
   * @generated
   */
  EAttribute getddStack_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddRkp <em>dd Rkp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Rkp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRkp
   * @generated
   */
  EClass getddRkp();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddRkp#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRkp#getValue()
   * @see #getddRkp()
   * @generated
   */
  EAttribute getddRkp_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddReserve <em>dd Reserve</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Reserve</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddReserve
   * @generated
   */
  EClass getddReserve();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddReserve#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddReserve#getValue()
   * @see #getddReserve()
   * @generated
   */
  EAttribute getddReserve_Value();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddReserve#getValue2 <em>Value2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddReserve#getValue2()
   * @see #getddReserve()
   * @generated
   */
  EAttribute getddReserve_Value2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddPrtsp <em>dd Prtsp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Prtsp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPrtsp
   * @generated
   */
  EClass getddPrtsp();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddPrtsp#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPrtsp#getValue()
   * @see #getddPrtsp()
   * @generated
   */
  EAttribute getddPrtsp_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddPci <em>dd Pci</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Pci</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPci
   * @generated
   */
  EClass getddPci();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddPci#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPci#getParm()
   * @see #getddPci()
   * @generated
   */
  EAttribute getddPci_Parm();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddPci#getParm2 <em>Parm2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPci#getParm2()
   * @see #getddPci()
   * @generated
   */
  EAttribute getddPci_Parm2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddOptcd <em>dd Optcd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Optcd</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddOptcd
   * @generated
   */
  EClass getddOptcd();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddOptcd#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddOptcd#getParm()
   * @see #getddOptcd()
   * @generated
   */
  EAttribute getddOptcd_Parm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddNtm <em>dd Ntm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Ntm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddNtm
   * @generated
   */
  EClass getddNtm();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddNtm#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddNtm#getValue()
   * @see #getddNtm()
   * @generated
   */
  EAttribute getddNtm_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddNcp <em>dd Ncp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Ncp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddNcp
   * @generated
   */
  EClass getddNcp();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddNcp#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddNcp#getValue()
   * @see #getddNcp()
   * @generated
   */
  EAttribute getddNcp_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddMode <em>dd Mode</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Mode</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddMode
   * @generated
   */
  EClass getddMode();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddMode#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddMode#getParm()
   * @see #getddMode()
   * @generated
   */
  EAttribute getddMode_Parm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddLimct <em>dd Limct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Limct</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLimct
   * @generated
   */
  EClass getddLimct();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddLimct#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLimct#getValue()
   * @see #getddLimct()
   * @generated
   */
  EAttribute getddLimct_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddIpltxid <em>dd Ipltxid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Ipltxid</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddIpltxid
   * @generated
   */
  EClass getddIpltxid();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddIpltxid#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddIpltxid#getValue()
   * @see #getddIpltxid()
   * @generated
   */
  EAttribute getddIpltxid_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddIntvl <em>dd Intvl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Intvl</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddIntvl
   * @generated
   */
  EClass getddIntvl();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddIntvl#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddIntvl#getValue()
   * @see #getddIntvl()
   * @generated
   */
  EAttribute getddIntvl_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddGncp <em>dd Gncp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Gncp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddGncp
   * @generated
   */
  EClass getddGncp();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddGncp#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddGncp#getValue()
   * @see #getddGncp()
   * @generated
   */
  EAttribute getddGncp_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddFunc <em>dd Func</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Func</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFunc
   * @generated
   */
  EClass getddFunc();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddFunc#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFunc#getParm()
   * @see #getddFunc()
   * @generated
   */
  EAttribute getddFunc_Parm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddEropt <em>dd Eropt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Eropt</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddEropt
   * @generated
   */
  EClass getddEropt();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddEropt#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddEropt#getParm()
   * @see #getddEropt()
   * @generated
   */
  EAttribute getddEropt_Parm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDiagns <em>dd Diagns</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Diagns</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDiagns
   * @generated
   */
  EClass getddDiagns();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDiagns#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDiagns#getParm()
   * @see #getddDiagns()
   * @generated
   */
  EAttribute getddDiagns_Parm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDen <em>dd Den</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Den</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDen
   * @generated
   */
  EClass getddDen();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDen#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDen#getValue()
   * @see #getddDen()
   * @generated
   */
  EAttribute getddDen_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddCylofl <em>dd Cylofl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Cylofl</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCylofl
   * @generated
   */
  EClass getddCylofl();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddCylofl#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCylofl#getValue()
   * @see #getddCylofl()
   * @generated
   */
  EAttribute getddCylofl_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddCpri <em>dd Cpri</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Cpri</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCpri
   * @generated
   */
  EClass getddCpri();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddCpri#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCpri#getParm()
   * @see #getddCpri()
   * @generated
   */
  EAttribute getddCpri_Parm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufsize <em>dd Bufsize</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Bufsize</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufsize
   * @generated
   */
  EClass getddBufsize();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBufsize#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufsize#getValue()
   * @see #getddBufsize()
   * @generated
   */
  EAttribute getddBufsize_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufout <em>dd Bufout</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Bufout</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufout
   * @generated
   */
  EClass getddBufout();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBufout#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufout#getValue()
   * @see #getddBufout()
   * @generated
   */
  EAttribute getddBufout_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufoff <em>dd Bufoff</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Bufoff</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufoff
   * @generated
   */
  EClass getddBufoff();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBufoff#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufoff#getValue()
   * @see #getddBufoff()
   * @generated
   */
  EAttribute getddBufoff_Value();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBufoff#getL <em>L</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>L</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufoff#getL()
   * @see #getddBufoff()
   * @generated
   */
  EAttribute getddBufoff_L();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufno <em>dd Bufno</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Bufno</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufno
   * @generated
   */
  EClass getddBufno();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBufno#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufno#getValue()
   * @see #getddBufno()
   * @generated
   */
  EAttribute getddBufno_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufmax <em>dd Bufmax</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Bufmax</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufmax
   * @generated
   */
  EClass getddBufmax();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBufmax#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufmax#getValue()
   * @see #getddBufmax()
   * @generated
   */
  EAttribute getddBufmax_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufl <em>dd Bufl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Bufl</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufl
   * @generated
   */
  EClass getddBufl();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBufl#getBytes <em>Bytes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bytes</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufl#getBytes()
   * @see #getddBufl()
   * @generated
   */
  EAttribute getddBufl_Bytes();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufin <em>dd Bufin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Bufin</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufin
   * @generated
   */
  EClass getddBufin();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBufin#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufin#getValue()
   * @see #getddBufin()
   * @generated
   */
  EAttribute getddBufin_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBftek <em>dd Bftek</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Bftek</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBftek
   * @generated
   */
  EClass getddBftek();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBftek#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBftek#getParm()
   * @see #getddBftek()
   * @generated
   */
  EAttribute getddBftek_Parm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBfaln <em>dd Bfaln</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Bfaln</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBfaln
   * @generated
   */
  EClass getddBfaln();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBfaln#getParm <em>Parm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Parm</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBfaln#getParm()
   * @see #getddBfaln()
   * @generated
   */
  EAttribute getddBfaln_Parm();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddDataclas <em>dd Dataclas</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Dataclas</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDataclas
   * @generated
   */
  EClass getddDataclas();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddDataclas#getDataclass <em>Dataclass</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dataclass</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDataclas#getDataclass()
   * @see #getddDataclas()
   * @generated
   */
  EAttribute getddDataclas_Dataclass();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddData <em>dd Data</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Data</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddData
   * @generated
   */
  EClass getddData();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddData#getDddata <em>Dddata</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dddata</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddData#getDddata()
   * @see #getddData()
   * @generated
   */
  EAttribute getddData_Dddata();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddCopies <em>dd Copies</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Copies</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCopies
   * @generated
   */
  EClass getddCopies();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddCopies#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCopies#getValue()
   * @see #getddCopies()
   * @generated
   */
  EAttribute getddCopies_Value();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.ddCopies#getGroup_value <em>Group value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCopies#getGroup_value()
   * @see #getddCopies()
   * @generated
   */
  EAttribute getddCopies_Group_value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddCntl <em>dd Cntl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Cntl</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCntl
   * @generated
   */
  EClass getddCntl();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddCntl#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCntl#getValue()
   * @see #getddCntl()
   * @generated
   */
  EAttribute getddCntl_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddChkpt <em>dd Chkpt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Chkpt</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddChkpt
   * @generated
   */
  EClass getddChkpt();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddChars <em>dd Chars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Chars</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddChars
   * @generated
   */
  EClass getddChars();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddChars#getTablename <em>Tablename</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Tablename</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddChars#getTablename()
   * @see #getddChars()
   * @generated
   */
  EAttribute getddChars_Tablename();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddChars#getThat <em>That</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>That</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddChars#getThat()
   * @see #getddChars()
   * @generated
   */
  EAttribute getddChars_That();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.ddChars#getTablename2 <em>Tablename2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Tablename2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddChars#getTablename2()
   * @see #getddChars()
   * @generated
   */
  EAttribute getddChars_Tablename2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddCcsid <em>dd Ccsid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Ccsid</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCcsid
   * @generated
   */
  EClass getddCcsid();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddCcsid#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCcsid#getValue()
   * @see #getddCcsid()
   * @generated
   */
  EAttribute getddCcsid_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBurst <em>dd Burst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Burst</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBurst
   * @generated
   */
  EClass getddBurst();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBurst#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBurst#getValue()
   * @see #getddBurst()
   * @generated
   */
  EAttribute getddBurst_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBlkszlim <em>dd Blkszlim</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Blkszlim</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBlkszlim
   * @generated
   */
  EClass getddBlkszlim();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBlkszlim#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Size</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBlkszlim#getSize()
   * @see #getddBlkszlim()
   * @generated
   */
  EAttribute getddBlkszlim_Size();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddBlksize <em>dd Blksize</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Blksize</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBlksize
   * @generated
   */
  EClass getddBlksize();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBlksize#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Size</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBlksize#getSize()
   * @see #getddBlksize()
   * @generated
   */
  EAttribute getddBlksize_Size();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddBlksize#getUnit <em>Unit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Unit</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBlksize#getUnit()
   * @see #getddBlksize()
   * @generated
   */
  EAttribute getddBlksize_Unit();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddAvgrec <em>dd Avgrec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Avgrec</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddAvgrec
   * @generated
   */
  EClass getddAvgrec();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddAvgrec#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddAvgrec#getValue()
   * @see #getddAvgrec()
   * @generated
   */
  EAttribute getddAvgrec_Value();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddAmp <em>dd Amp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Amp</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddAmp
   * @generated
   */
  EClass getddAmp();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddAmp#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddAmp#getValue()
   * @see #getddAmp()
   * @generated
   */
  EAttribute getddAmp_Value();

  /**
   * Returns the meta object for the attribute list '{@link org.kb.jcl.jcldsl.jclDsl.ddAmp#getValue2 <em>Value2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Value2</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddAmp#getValue2()
   * @see #getddAmp()
   * @generated
   */
  EAttribute getddAmp_Value2();

  /**
   * Returns the meta object for class '{@link org.kb.jcl.jcldsl.jclDsl.ddAccode <em>dd Accode</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dd Accode</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddAccode
   * @generated
   */
  EClass getddAccode();

  /**
   * Returns the meta object for the attribute '{@link org.kb.jcl.jcldsl.jclDsl.ddAccode#getAccess <em>Access</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Access</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddAccode#getAccess()
   * @see #getddAccode()
   * @generated
   */
  EAttribute getddAccode_Access();

  /**
   * Returns the meta object for enum '{@link org.kb.jcl.jcldsl.jclDsl.Typrun_type <em>Typrun type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Typrun type</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.Typrun_type
   * @generated
   */
  EEnum getTyprun_type();

  /**
   * Returns the meta object for enum '{@link org.kb.jcl.jcldsl.jclDsl.jobRdValues <em>job Rd Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>job Rd Values</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRdValues
   * @generated
   */
  EEnum getjobRdValues();

  /**
   * Returns the meta object for enum '{@link org.kb.jcl.jcldsl.jclDsl.pagesDef <em>pages Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>pages Def</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.pagesDef
   * @generated
   */
  EEnum getpagesDef();

  /**
   * Returns the meta object for enum '{@link org.kb.jcl.jcldsl.jclDsl.ddRecorgValues <em>dd Recorg Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>dd Recorg Values</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRecorgValues
   * @generated
   */
  EEnum getddRecorgValues();

  /**
   * Returns the meta object for enum '{@link org.kb.jcl.jcldsl.jclDsl.ddRecfmValues <em>dd Recfm Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>dd Recfm Values</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRecfmValues
   * @generated
   */
  EEnum getddRecfmValues();

  /**
   * Returns the meta object for enum '{@link org.kb.jcl.jcldsl.jclDsl.ddPathmodeOpts <em>dd Pathmode Opts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>dd Pathmode Opts</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathmodeOpts
   * @generated
   */
  EEnum getddPathmodeOpts();

  /**
   * Returns the meta object for enum '{@link org.kb.jcl.jcldsl.jclDsl.ddPathoptsOpts <em>dd Pathopts Opts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>dd Pathopts Opts</em>'.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathoptsOpts
   * @generated
   */
  EEnum getddPathoptsOpts();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  JclDslFactory getJclDslFactory();

} //JclDslPackage
