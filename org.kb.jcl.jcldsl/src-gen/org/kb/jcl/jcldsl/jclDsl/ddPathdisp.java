/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Pathdisp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp <em>Disp</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp2 <em>Disp2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp3 <em>Disp3</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPathdisp()
 * @model
 * @generated
 */
public interface ddPathdisp extends ddsection
{
  /**
   * Returns the value of the '<em><b>Disp</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Disp</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Disp</em>' attribute.
   * @see #setDisp(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPathdisp_Disp()
   * @model
   * @generated
   */
  String getDisp();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp <em>Disp</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Disp</em>' attribute.
   * @see #getDisp()
   * @generated
   */
  void setDisp(String value);

  /**
   * Returns the value of the '<em><b>Disp2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Disp2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Disp2</em>' attribute.
   * @see #setDisp2(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPathdisp_Disp2()
   * @model
   * @generated
   */
  String getDisp2();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp2 <em>Disp2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Disp2</em>' attribute.
   * @see #getDisp2()
   * @generated
   */
  void setDisp2(String value);

  /**
   * Returns the value of the '<em><b>Disp3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Disp3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Disp3</em>' attribute.
   * @see #setDisp3(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPathdisp_Disp3()
   * @model
   * @generated
   */
  String getDisp3();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp#getDisp3 <em>Disp3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Disp3</em>' attribute.
   * @see #getDisp3()
   * @generated
   */
  void setDisp3(String value);

} // ddPathdisp
