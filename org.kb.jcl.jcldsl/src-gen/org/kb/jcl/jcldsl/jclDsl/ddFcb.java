/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Fcb</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddFcb#getFcbname <em>Fcbname</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddFcb()
 * @model
 * @generated
 */
public interface ddFcb extends ddsection
{
  /**
   * Returns the value of the '<em><b>Fcbname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fcbname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fcbname</em>' attribute.
   * @see #setFcbname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddFcb_Fcbname()
   * @model
   * @generated
   */
  String getFcbname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddFcb#getFcbname <em>Fcbname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fcbname</em>' attribute.
   * @see #getFcbname()
   * @generated
   */
  void setFcbname(String value);

} // ddFcb
