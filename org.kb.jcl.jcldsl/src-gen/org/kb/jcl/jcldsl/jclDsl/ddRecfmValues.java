/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>dd Recfm Values</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddRecfmValues()
 * @model
 * @generated
 */
public enum ddRecfmValues implements Enumerator
{
  /**
   * The '<em><b>U</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #U_VALUE
   * @generated
   * @ordered
   */
  U(0, "U", "U"),

  /**
   * The '<em><b>UT</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #UT_VALUE
   * @generated
   * @ordered
   */
  UT(1, "UT", "UT"),

  /**
   * The '<em><b>F</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #F_VALUE
   * @generated
   * @ordered
   */
  F(2, "F", "F"),

  /**
   * The '<em><b>FB</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FB_VALUE
   * @generated
   * @ordered
   */
  FB(3, "FB", "FB"),

  /**
   * The '<em><b>FS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FS_VALUE
   * @generated
   * @ordered
   */
  FS(4, "FS", "FS"),

  /**
   * The '<em><b>FT</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FT_VALUE
   * @generated
   * @ordered
   */
  FT(5, "FT", "FT"),

  /**
   * The '<em><b>FBS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FBS_VALUE
   * @generated
   * @ordered
   */
  FBS(6, "FBS", "FBS"),

  /**
   * The '<em><b>FBT</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FBT_VALUE
   * @generated
   * @ordered
   */
  FBT(7, "FBT", "FBT"),

  /**
   * The '<em><b>V</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #V_VALUE
   * @generated
   * @ordered
   */
  V(8, "V", "V"),

  /**
   * The '<em><b>VB</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VB_VALUE
   * @generated
   * @ordered
   */
  VB(9, "VB", "VB"),

  /**
   * The '<em><b>VS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VS_VALUE
   * @generated
   * @ordered
   */
  VS(10, "VS", "VS"),

  /**
   * The '<em><b>VT</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VT_VALUE
   * @generated
   * @ordered
   */
  VT(11, "VT", "VT"),

  /**
   * The '<em><b>VBS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBS_VALUE
   * @generated
   * @ordered
   */
  VBS(12, "VBS", "VBS"),

  /**
   * The '<em><b>VBT</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBT_VALUE
   * @generated
   * @ordered
   */
  VBT(13, "VBT", "VBT"),

  /**
   * The '<em><b>VBST</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBST_VALUE
   * @generated
   * @ordered
   */
  VBST(14, "VBST", "VBST"),

  /**
   * The '<em><b>UA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #UA_VALUE
   * @generated
   * @ordered
   */
  UA(15, "UA", "UA"),

  /**
   * The '<em><b>UTA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #UTA_VALUE
   * @generated
   * @ordered
   */
  UTA(16, "UTA", "UTA"),

  /**
   * The '<em><b>FA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FA_VALUE
   * @generated
   * @ordered
   */
  FA(17, "FA", "FA"),

  /**
   * The '<em><b>FBA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FBA_VALUE
   * @generated
   * @ordered
   */
  FBA(18, "FBA", "FBA"),

  /**
   * The '<em><b>FSA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FSA_VALUE
   * @generated
   * @ordered
   */
  FSA(19, "FSA", "FSA"),

  /**
   * The '<em><b>FTA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FTA_VALUE
   * @generated
   * @ordered
   */
  FTA(20, "FTA", "FTA"),

  /**
   * The '<em><b>FBSA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FBSA_VALUE
   * @generated
   * @ordered
   */
  FBSA(21, "FBSA", "FBSA"),

  /**
   * The '<em><b>FBTA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FBTA_VALUE
   * @generated
   * @ordered
   */
  FBTA(22, "FBTA", "FBTA"),

  /**
   * The '<em><b>VA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VA_VALUE
   * @generated
   * @ordered
   */
  VA(23, "VA", "VA"),

  /**
   * The '<em><b>VBA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBA_VALUE
   * @generated
   * @ordered
   */
  VBA(24, "VBA", "VBA"),

  /**
   * The '<em><b>VSA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VSA_VALUE
   * @generated
   * @ordered
   */
  VSA(25, "VSA", "VSA"),

  /**
   * The '<em><b>VTA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VTA_VALUE
   * @generated
   * @ordered
   */
  VTA(26, "VTA", "VTA"),

  /**
   * The '<em><b>VBSA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBSA_VALUE
   * @generated
   * @ordered
   */
  VBSA(27, "VBSA", "VBSA"),

  /**
   * The '<em><b>VBTA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBTA_VALUE
   * @generated
   * @ordered
   */
  VBTA(28, "VBTA", "VBTA"),

  /**
   * The '<em><b>VBSTA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBSTA_VALUE
   * @generated
   * @ordered
   */
  VBSTA(29, "VBSTA", "VBSTA"),

  /**
   * The '<em><b>UM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #UM_VALUE
   * @generated
   * @ordered
   */
  UM(30, "UM", "UM"),

  /**
   * The '<em><b>UTM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #UTM_VALUE
   * @generated
   * @ordered
   */
  UTM(31, "UTM", "UTM"),

  /**
   * The '<em><b>FM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FM_VALUE
   * @generated
   * @ordered
   */
  FM(32, "FM", "FM"),

  /**
   * The '<em><b>FBM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FBM_VALUE
   * @generated
   * @ordered
   */
  FBM(33, "FBM", "FBM"),

  /**
   * The '<em><b>FSM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FSM_VALUE
   * @generated
   * @ordered
   */
  FSM(34, "FSM", "FSM"),

  /**
   * The '<em><b>FTM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FTM_VALUE
   * @generated
   * @ordered
   */
  FTM(35, "FTM", "FTM"),

  /**
   * The '<em><b>FBSM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FBSM_VALUE
   * @generated
   * @ordered
   */
  FBSM(36, "FBSM", "FBSM"),

  /**
   * The '<em><b>FBTM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FBTM_VALUE
   * @generated
   * @ordered
   */
  FBTM(37, "FBTM", "FBTM"),

  /**
   * The '<em><b>VM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VM_VALUE
   * @generated
   * @ordered
   */
  VM(38, "VM", "VM"),

  /**
   * The '<em><b>VBM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBM_VALUE
   * @generated
   * @ordered
   */
  VBM(39, "VBM", "VBM"),

  /**
   * The '<em><b>VSM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VSM_VALUE
   * @generated
   * @ordered
   */
  VSM(40, "VSM", "VSM"),

  /**
   * The '<em><b>VTM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VTM_VALUE
   * @generated
   * @ordered
   */
  VTM(41, "VTM", "VTM"),

  /**
   * The '<em><b>VBSM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBSM_VALUE
   * @generated
   * @ordered
   */
  VBSM(42, "VBSM", "VBSM"),

  /**
   * The '<em><b>VBTM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBTM_VALUE
   * @generated
   * @ordered
   */
  VBTM(43, "VBTM", "VBTM"),

  /**
   * The '<em><b>VBSTM</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VBSTM_VALUE
   * @generated
   * @ordered
   */
  VBSTM(44, "VBSTM", "VBSTM"),

  /**
   * The '<em><b>D</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #D_VALUE
   * @generated
   * @ordered
   */
  D(45, "D", "D"),

  /**
   * The '<em><b>DB</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DB_VALUE
   * @generated
   * @ordered
   */
  DB(46, "DB", "DB"),

  /**
   * The '<em><b>DS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DS_VALUE
   * @generated
   * @ordered
   */
  DS(47, "DS", "DS"),

  /**
   * The '<em><b>DBS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DBS_VALUE
   * @generated
   * @ordered
   */
  DBS(48, "DBS", "DBS"),

  /**
   * The '<em><b>DA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DA_VALUE
   * @generated
   * @ordered
   */
  DA(49, "DA", "DA"),

  /**
   * The '<em><b>DBA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DBA_VALUE
   * @generated
   * @ordered
   */
  DBA(50, "DBA", "DBA"),

  /**
   * The '<em><b>DSA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DSA_VALUE
   * @generated
   * @ordered
   */
  DSA(51, "DSA", "DSA"),

  /**
   * The '<em><b>DBSA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DBSA_VALUE
   * @generated
   * @ordered
   */
  DBSA(52, "DBSA", "DBSA");

  /**
   * The '<em><b>U</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>U</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #U
   * @model
   * @generated
   * @ordered
   */
  public static final int U_VALUE = 0;

  /**
   * The '<em><b>UT</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>UT</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #UT
   * @model
   * @generated
   * @ordered
   */
  public static final int UT_VALUE = 1;

  /**
   * The '<em><b>F</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>F</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #F
   * @model
   * @generated
   * @ordered
   */
  public static final int F_VALUE = 2;

  /**
   * The '<em><b>FB</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FB</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FB
   * @model
   * @generated
   * @ordered
   */
  public static final int FB_VALUE = 3;

  /**
   * The '<em><b>FS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FS
   * @model
   * @generated
   * @ordered
   */
  public static final int FS_VALUE = 4;

  /**
   * The '<em><b>FT</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FT</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FT
   * @model
   * @generated
   * @ordered
   */
  public static final int FT_VALUE = 5;

  /**
   * The '<em><b>FBS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FBS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FBS
   * @model
   * @generated
   * @ordered
   */
  public static final int FBS_VALUE = 6;

  /**
   * The '<em><b>FBT</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FBT</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FBT
   * @model
   * @generated
   * @ordered
   */
  public static final int FBT_VALUE = 7;

  /**
   * The '<em><b>V</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>V</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #V
   * @model
   * @generated
   * @ordered
   */
  public static final int V_VALUE = 8;

  /**
   * The '<em><b>VB</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VB</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VB
   * @model
   * @generated
   * @ordered
   */
  public static final int VB_VALUE = 9;

  /**
   * The '<em><b>VS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VS
   * @model
   * @generated
   * @ordered
   */
  public static final int VS_VALUE = 10;

  /**
   * The '<em><b>VT</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VT</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VT
   * @model
   * @generated
   * @ordered
   */
  public static final int VT_VALUE = 11;

  /**
   * The '<em><b>VBS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBS
   * @model
   * @generated
   * @ordered
   */
  public static final int VBS_VALUE = 12;

  /**
   * The '<em><b>VBT</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBT</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBT
   * @model
   * @generated
   * @ordered
   */
  public static final int VBT_VALUE = 13;

  /**
   * The '<em><b>VBST</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBST</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBST
   * @model
   * @generated
   * @ordered
   */
  public static final int VBST_VALUE = 14;

  /**
   * The '<em><b>UA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>UA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #UA
   * @model
   * @generated
   * @ordered
   */
  public static final int UA_VALUE = 15;

  /**
   * The '<em><b>UTA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>UTA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #UTA
   * @model
   * @generated
   * @ordered
   */
  public static final int UTA_VALUE = 16;

  /**
   * The '<em><b>FA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FA
   * @model
   * @generated
   * @ordered
   */
  public static final int FA_VALUE = 17;

  /**
   * The '<em><b>FBA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FBA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FBA
   * @model
   * @generated
   * @ordered
   */
  public static final int FBA_VALUE = 18;

  /**
   * The '<em><b>FSA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FSA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FSA
   * @model
   * @generated
   * @ordered
   */
  public static final int FSA_VALUE = 19;

  /**
   * The '<em><b>FTA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FTA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FTA
   * @model
   * @generated
   * @ordered
   */
  public static final int FTA_VALUE = 20;

  /**
   * The '<em><b>FBSA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FBSA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FBSA
   * @model
   * @generated
   * @ordered
   */
  public static final int FBSA_VALUE = 21;

  /**
   * The '<em><b>FBTA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FBTA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FBTA
   * @model
   * @generated
   * @ordered
   */
  public static final int FBTA_VALUE = 22;

  /**
   * The '<em><b>VA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VA
   * @model
   * @generated
   * @ordered
   */
  public static final int VA_VALUE = 23;

  /**
   * The '<em><b>VBA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBA
   * @model
   * @generated
   * @ordered
   */
  public static final int VBA_VALUE = 24;

  /**
   * The '<em><b>VSA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VSA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VSA
   * @model
   * @generated
   * @ordered
   */
  public static final int VSA_VALUE = 25;

  /**
   * The '<em><b>VTA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VTA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VTA
   * @model
   * @generated
   * @ordered
   */
  public static final int VTA_VALUE = 26;

  /**
   * The '<em><b>VBSA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBSA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBSA
   * @model
   * @generated
   * @ordered
   */
  public static final int VBSA_VALUE = 27;

  /**
   * The '<em><b>VBTA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBTA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBTA
   * @model
   * @generated
   * @ordered
   */
  public static final int VBTA_VALUE = 28;

  /**
   * The '<em><b>VBSTA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBSTA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBSTA
   * @model
   * @generated
   * @ordered
   */
  public static final int VBSTA_VALUE = 29;

  /**
   * The '<em><b>UM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>UM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #UM
   * @model
   * @generated
   * @ordered
   */
  public static final int UM_VALUE = 30;

  /**
   * The '<em><b>UTM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>UTM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #UTM
   * @model
   * @generated
   * @ordered
   */
  public static final int UTM_VALUE = 31;

  /**
   * The '<em><b>FM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FM
   * @model
   * @generated
   * @ordered
   */
  public static final int FM_VALUE = 32;

  /**
   * The '<em><b>FBM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FBM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FBM
   * @model
   * @generated
   * @ordered
   */
  public static final int FBM_VALUE = 33;

  /**
   * The '<em><b>FSM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FSM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FSM
   * @model
   * @generated
   * @ordered
   */
  public static final int FSM_VALUE = 34;

  /**
   * The '<em><b>FTM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FTM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FTM
   * @model
   * @generated
   * @ordered
   */
  public static final int FTM_VALUE = 35;

  /**
   * The '<em><b>FBSM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FBSM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FBSM
   * @model
   * @generated
   * @ordered
   */
  public static final int FBSM_VALUE = 36;

  /**
   * The '<em><b>FBTM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>FBTM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FBTM
   * @model
   * @generated
   * @ordered
   */
  public static final int FBTM_VALUE = 37;

  /**
   * The '<em><b>VM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VM
   * @model
   * @generated
   * @ordered
   */
  public static final int VM_VALUE = 38;

  /**
   * The '<em><b>VBM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBM
   * @model
   * @generated
   * @ordered
   */
  public static final int VBM_VALUE = 39;

  /**
   * The '<em><b>VSM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VSM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VSM
   * @model
   * @generated
   * @ordered
   */
  public static final int VSM_VALUE = 40;

  /**
   * The '<em><b>VTM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VTM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VTM
   * @model
   * @generated
   * @ordered
   */
  public static final int VTM_VALUE = 41;

  /**
   * The '<em><b>VBSM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBSM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBSM
   * @model
   * @generated
   * @ordered
   */
  public static final int VBSM_VALUE = 42;

  /**
   * The '<em><b>VBTM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBTM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBTM
   * @model
   * @generated
   * @ordered
   */
  public static final int VBTM_VALUE = 43;

  /**
   * The '<em><b>VBSTM</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VBSTM</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VBSTM
   * @model
   * @generated
   * @ordered
   */
  public static final int VBSTM_VALUE = 44;

  /**
   * The '<em><b>D</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>D</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #D
   * @model
   * @generated
   * @ordered
   */
  public static final int D_VALUE = 45;

  /**
   * The '<em><b>DB</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DB</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DB
   * @model
   * @generated
   * @ordered
   */
  public static final int DB_VALUE = 46;

  /**
   * The '<em><b>DS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DS
   * @model
   * @generated
   * @ordered
   */
  public static final int DS_VALUE = 47;

  /**
   * The '<em><b>DBS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DBS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DBS
   * @model
   * @generated
   * @ordered
   */
  public static final int DBS_VALUE = 48;

  /**
   * The '<em><b>DA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DA
   * @model
   * @generated
   * @ordered
   */
  public static final int DA_VALUE = 49;

  /**
   * The '<em><b>DBA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DBA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DBA
   * @model
   * @generated
   * @ordered
   */
  public static final int DBA_VALUE = 50;

  /**
   * The '<em><b>DSA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DSA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DSA
   * @model
   * @generated
   * @ordered
   */
  public static final int DSA_VALUE = 51;

  /**
   * The '<em><b>DBSA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DBSA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DBSA
   * @model
   * @generated
   * @ordered
   */
  public static final int DBSA_VALUE = 52;

  /**
   * An array of all the '<em><b>dd Recfm Values</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final ddRecfmValues[] VALUES_ARRAY =
    new ddRecfmValues[]
    {
      U,
      UT,
      F,
      FB,
      FS,
      FT,
      FBS,
      FBT,
      V,
      VB,
      VS,
      VT,
      VBS,
      VBT,
      VBST,
      UA,
      UTA,
      FA,
      FBA,
      FSA,
      FTA,
      FBSA,
      FBTA,
      VA,
      VBA,
      VSA,
      VTA,
      VBSA,
      VBTA,
      VBSTA,
      UM,
      UTM,
      FM,
      FBM,
      FSM,
      FTM,
      FBSM,
      FBTM,
      VM,
      VBM,
      VSM,
      VTM,
      VBSM,
      VBTM,
      VBSTM,
      D,
      DB,
      DS,
      DBS,
      DA,
      DBA,
      DSA,
      DBSA,
    };

  /**
   * A public read-only list of all the '<em><b>dd Recfm Values</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<ddRecfmValues> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>dd Recfm Values</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddRecfmValues get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      ddRecfmValues result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>dd Recfm Values</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddRecfmValues getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      ddRecfmValues result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>dd Recfm Values</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddRecfmValues get(int value)
  {
    switch (value)
    {
      case U_VALUE: return U;
      case UT_VALUE: return UT;
      case F_VALUE: return F;
      case FB_VALUE: return FB;
      case FS_VALUE: return FS;
      case FT_VALUE: return FT;
      case FBS_VALUE: return FBS;
      case FBT_VALUE: return FBT;
      case V_VALUE: return V;
      case VB_VALUE: return VB;
      case VS_VALUE: return VS;
      case VT_VALUE: return VT;
      case VBS_VALUE: return VBS;
      case VBT_VALUE: return VBT;
      case VBST_VALUE: return VBST;
      case UA_VALUE: return UA;
      case UTA_VALUE: return UTA;
      case FA_VALUE: return FA;
      case FBA_VALUE: return FBA;
      case FSA_VALUE: return FSA;
      case FTA_VALUE: return FTA;
      case FBSA_VALUE: return FBSA;
      case FBTA_VALUE: return FBTA;
      case VA_VALUE: return VA;
      case VBA_VALUE: return VBA;
      case VSA_VALUE: return VSA;
      case VTA_VALUE: return VTA;
      case VBSA_VALUE: return VBSA;
      case VBTA_VALUE: return VBTA;
      case VBSTA_VALUE: return VBSTA;
      case UM_VALUE: return UM;
      case UTM_VALUE: return UTM;
      case FM_VALUE: return FM;
      case FBM_VALUE: return FBM;
      case FSM_VALUE: return FSM;
      case FTM_VALUE: return FTM;
      case FBSM_VALUE: return FBSM;
      case FBTM_VALUE: return FBTM;
      case VM_VALUE: return VM;
      case VBM_VALUE: return VBM;
      case VSM_VALUE: return VSM;
      case VTM_VALUE: return VTM;
      case VBSM_VALUE: return VBSM;
      case VBTM_VALUE: return VBTM;
      case VBSTM_VALUE: return VBSTM;
      case D_VALUE: return D;
      case DB_VALUE: return DB;
      case DS_VALUE: return DS;
      case DBS_VALUE: return DBS;
      case DA_VALUE: return DA;
      case DBA_VALUE: return DBA;
      case DSA_VALUE: return DSA;
      case DBSA_VALUE: return DBSA;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private ddRecfmValues(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //ddRecfmValues
