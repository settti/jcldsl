/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Diagns</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDiagns#getParm <em>Parm</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDiagns()
 * @model
 * @generated
 */
public interface ddDiagns extends ddsection, ddDcbSubparms
{
  /**
   * Returns the value of the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm</em>' attribute.
   * @see #setParm(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDiagns_Parm()
   * @model
   * @generated
   */
  String getParm();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDiagns#getParm <em>Parm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parm</em>' attribute.
   * @see #getParm()
   * @generated
   */
  void setParm(String value);

} // ddDiagns
