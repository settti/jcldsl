/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getKeyword()
 * @model
 * @generated
 */
public interface Keyword extends EObject
{
} // Keyword
