/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobSeclabel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>job Seclabel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobSeclabelImpl#getSECURITYLABEL <em>SECURITYLABEL</em>}</li>
 * </ul>
 *
 * @generated
 */
public class jobSeclabelImpl extends jobsectionImpl implements jobSeclabel
{
  /**
   * The default value of the '{@link #getSECURITYLABEL() <em>SECURITYLABEL</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSECURITYLABEL()
   * @generated
   * @ordered
   */
  protected static final String SECURITYLABEL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSECURITYLABEL() <em>SECURITYLABEL</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSECURITYLABEL()
   * @generated
   * @ordered
   */
  protected String securitylabel = SECURITYLABEL_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobSeclabelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobSeclabel();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSECURITYLABEL()
  {
    return securitylabel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSECURITYLABEL(String newSECURITYLABEL)
  {
    String oldSECURITYLABEL = securitylabel;
    securitylabel = newSECURITYLABEL;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_SECLABEL__SECURITYLABEL, oldSECURITYLABEL, securitylabel));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_SECLABEL__SECURITYLABEL:
        return getSECURITYLABEL();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_SECLABEL__SECURITYLABEL:
        setSECURITYLABEL((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_SECLABEL__SECURITYLABEL:
        setSECURITYLABEL(SECURITYLABEL_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_SECLABEL__SECURITYLABEL:
        return SECURITYLABEL_EDEFAULT == null ? securitylabel != null : !SECURITYLABEL_EDEFAULT.equals(securitylabel);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (SECURITYLABEL: ");
    result.append(securitylabel);
    result.append(')');
    return result.toString();
  }

} //jobSeclabelImpl
