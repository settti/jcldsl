/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobCond;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>job Cond</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobCondImpl#getReturn_code <em>Return code</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobCondImpl#getJobcondop <em>Jobcondop</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobCondImpl#getReturn_code2 <em>Return code2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobCondImpl#getJobcondop2 <em>Jobcondop2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class jobCondImpl extends jobsectionImpl implements jobCond
{
  /**
   * The default value of the '{@link #getReturn_code() <em>Return code</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReturn_code()
   * @generated
   * @ordered
   */
  protected static final int RETURN_CODE_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getReturn_code() <em>Return code</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReturn_code()
   * @generated
   * @ordered
   */
  protected int return_code = RETURN_CODE_EDEFAULT;

  /**
   * The default value of the '{@link #getJobcondop() <em>Jobcondop</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJobcondop()
   * @generated
   * @ordered
   */
  protected static final String JOBCONDOP_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getJobcondop() <em>Jobcondop</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJobcondop()
   * @generated
   * @ordered
   */
  protected String jobcondop = JOBCONDOP_EDEFAULT;

  /**
   * The cached value of the '{@link #getReturn_code2() <em>Return code2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReturn_code2()
   * @generated
   * @ordered
   */
  protected EList<Integer> return_code2;

  /**
   * The cached value of the '{@link #getJobcondop2() <em>Jobcondop2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJobcondop2()
   * @generated
   * @ordered
   */
  protected EList<String> jobcondop2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobCondImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobCond();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getReturn_code()
  {
    return return_code;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReturn_code(int newReturn_code)
  {
    int oldReturn_code = return_code;
    return_code = newReturn_code;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_COND__RETURN_CODE, oldReturn_code, return_code));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getJobcondop()
  {
    return jobcondop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setJobcondop(String newJobcondop)
  {
    String oldJobcondop = jobcondop;
    jobcondop = newJobcondop;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_COND__JOBCONDOP, oldJobcondop, jobcondop));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Integer> getReturn_code2()
  {
    if (return_code2 == null)
    {
      return_code2 = new EDataTypeEList<Integer>(Integer.class, this, JclDslPackage.JOB_COND__RETURN_CODE2);
    }
    return return_code2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getJobcondop2()
  {
    if (jobcondop2 == null)
    {
      jobcondop2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.JOB_COND__JOBCONDOP2);
    }
    return jobcondop2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_COND__RETURN_CODE:
        return getReturn_code();
      case JclDslPackage.JOB_COND__JOBCONDOP:
        return getJobcondop();
      case JclDslPackage.JOB_COND__RETURN_CODE2:
        return getReturn_code2();
      case JclDslPackage.JOB_COND__JOBCONDOP2:
        return getJobcondop2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_COND__RETURN_CODE:
        setReturn_code((Integer)newValue);
        return;
      case JclDslPackage.JOB_COND__JOBCONDOP:
        setJobcondop((String)newValue);
        return;
      case JclDslPackage.JOB_COND__RETURN_CODE2:
        getReturn_code2().clear();
        getReturn_code2().addAll((Collection<? extends Integer>)newValue);
        return;
      case JclDslPackage.JOB_COND__JOBCONDOP2:
        getJobcondop2().clear();
        getJobcondop2().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_COND__RETURN_CODE:
        setReturn_code(RETURN_CODE_EDEFAULT);
        return;
      case JclDslPackage.JOB_COND__JOBCONDOP:
        setJobcondop(JOBCONDOP_EDEFAULT);
        return;
      case JclDslPackage.JOB_COND__RETURN_CODE2:
        getReturn_code2().clear();
        return;
      case JclDslPackage.JOB_COND__JOBCONDOP2:
        getJobcondop2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_COND__RETURN_CODE:
        return return_code != RETURN_CODE_EDEFAULT;
      case JclDslPackage.JOB_COND__JOBCONDOP:
        return JOBCONDOP_EDEFAULT == null ? jobcondop != null : !JOBCONDOP_EDEFAULT.equals(jobcondop);
      case JclDslPackage.JOB_COND__RETURN_CODE2:
        return return_code2 != null && !return_code2.isEmpty();
      case JclDslPackage.JOB_COND__JOBCONDOP2:
        return jobcondop2 != null && !jobcondop2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (return_code: ");
    result.append(return_code);
    result.append(", jobcondop: ");
    result.append(jobcondop);
    result.append(", return_code2: ");
    result.append(return_code2);
    result.append(", jobcondop2: ");
    result.append(jobcondop2);
    result.append(')');
    return result.toString();
  }

} //jobCondImpl
