/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.execTime;
import org.kb.jcl.jcldsl.jclDsl.execTimevalue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>exec Time</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execTimeImpl#getExecTimevalue <em>Exec Timevalue</em>}</li>
 * </ul>
 *
 * @generated
 */
public class execTimeImpl extends execsectionImpl implements execTime
{
  /**
   * The cached value of the '{@link #getExecTimevalue() <em>Exec Timevalue</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExecTimevalue()
   * @generated
   * @ordered
   */
  protected execTimevalue execTimevalue;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected execTimeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getexecTime();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execTimevalue getExecTimevalue()
  {
    return execTimevalue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExecTimevalue(execTimevalue newExecTimevalue, NotificationChain msgs)
  {
    execTimevalue oldExecTimevalue = execTimevalue;
    execTimevalue = newExecTimevalue;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_TIME__EXEC_TIMEVALUE, oldExecTimevalue, newExecTimevalue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExecTimevalue(execTimevalue newExecTimevalue)
  {
    if (newExecTimevalue != execTimevalue)
    {
      NotificationChain msgs = null;
      if (execTimevalue != null)
        msgs = ((InternalEObject)execTimevalue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JclDslPackage.EXEC_TIME__EXEC_TIMEVALUE, null, msgs);
      if (newExecTimevalue != null)
        msgs = ((InternalEObject)newExecTimevalue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JclDslPackage.EXEC_TIME__EXEC_TIMEVALUE, null, msgs);
      msgs = basicSetExecTimevalue(newExecTimevalue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_TIME__EXEC_TIMEVALUE, newExecTimevalue, newExecTimevalue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_TIME__EXEC_TIMEVALUE:
        return basicSetExecTimevalue(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_TIME__EXEC_TIMEVALUE:
        return getExecTimevalue();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_TIME__EXEC_TIMEVALUE:
        setExecTimevalue((execTimevalue)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_TIME__EXEC_TIMEVALUE:
        setExecTimevalue((execTimevalue)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_TIME__EXEC_TIMEVALUE:
        return execTimevalue != null;
    }
    return super.eIsSet(featureID);
  }

} //execTimeImpl
