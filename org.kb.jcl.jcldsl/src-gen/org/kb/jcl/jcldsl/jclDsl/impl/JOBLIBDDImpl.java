/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.kb.jcl.jcldsl.jclDsl.JOBLIBDD;
import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.specddSection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>JOBLIBDD</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.JOBLIBDDImpl#getSection <em>Section</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.JOBLIBDDImpl#getSections <em>Sections</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JOBLIBDDImpl extends KeywordImpl implements JOBLIBDD
{
  /**
   * The cached value of the '{@link #getSection() <em>Section</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSection()
   * @generated
   * @ordered
   */
  protected EList<specddSection> section;

  /**
   * The cached value of the '{@link #getSections() <em>Sections</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSections()
   * @generated
   * @ordered
   */
  protected EList<specddSection> sections;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected JOBLIBDDImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getJOBLIBDD();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<specddSection> getSection()
  {
    if (section == null)
    {
      section = new EObjectContainmentEList<specddSection>(specddSection.class, this, JclDslPackage.JOBLIBDD__SECTION);
    }
    return section;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<specddSection> getSections()
  {
    if (sections == null)
    {
      sections = new EObjectContainmentEList<specddSection>(specddSection.class, this, JclDslPackage.JOBLIBDD__SECTIONS);
    }
    return sections;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JclDslPackage.JOBLIBDD__SECTION:
        return ((InternalEList<?>)getSection()).basicRemove(otherEnd, msgs);
      case JclDslPackage.JOBLIBDD__SECTIONS:
        return ((InternalEList<?>)getSections()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOBLIBDD__SECTION:
        return getSection();
      case JclDslPackage.JOBLIBDD__SECTIONS:
        return getSections();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOBLIBDD__SECTION:
        getSection().clear();
        getSection().addAll((Collection<? extends specddSection>)newValue);
        return;
      case JclDslPackage.JOBLIBDD__SECTIONS:
        getSections().clear();
        getSections().addAll((Collection<? extends specddSection>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOBLIBDD__SECTION:
        getSection().clear();
        return;
      case JclDslPackage.JOBLIBDD__SECTIONS:
        getSections().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOBLIBDD__SECTION:
        return section != null && !section.isEmpty();
      case JclDslPackage.JOBLIBDD__SECTIONS:
        return sections != null && !sections.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //JOBLIBDDImpl
