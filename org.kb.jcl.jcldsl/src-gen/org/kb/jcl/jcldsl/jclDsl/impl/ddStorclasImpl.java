/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddStorclas;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Storclas</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddStorclasImpl#getStorageclass <em>Storageclass</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddStorclasImpl extends ddsectionImpl implements ddStorclas
{
  /**
   * The default value of the '{@link #getStorageclass() <em>Storageclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStorageclass()
   * @generated
   * @ordered
   */
  protected static final String STORAGECLASS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getStorageclass() <em>Storageclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStorageclass()
   * @generated
   * @ordered
   */
  protected String storageclass = STORAGECLASS_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddStorclasImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddStorclas();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getStorageclass()
  {
    return storageclass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStorageclass(String newStorageclass)
  {
    String oldStorageclass = storageclass;
    storageclass = newStorageclass;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_STORCLAS__STORAGECLASS, oldStorageclass, storageclass));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_STORCLAS__STORAGECLASS:
        return getStorageclass();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_STORCLAS__STORAGECLASS:
        setStorageclass((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_STORCLAS__STORAGECLASS:
        setStorageclass(STORAGECLASS_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_STORCLAS__STORAGECLASS:
        return STORAGECLASS_EDEFAULT == null ? storageclass != null : !STORAGECLASS_EDEFAULT.equals(storageclass);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (storageclass: ");
    result.append(storageclass);
    result.append(')');
    return result.toString();
  }

} //ddStorclasImpl
