/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddLabelExpire;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Label Expire</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ddLabelExpireImpl extends MinimalEObjectImpl.Container implements ddLabelExpire
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddLabelExpireImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddLabelExpire();
  }

} //ddLabelExpireImpl
