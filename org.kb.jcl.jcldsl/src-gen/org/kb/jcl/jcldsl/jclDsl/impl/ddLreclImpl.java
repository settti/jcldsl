/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddLrecl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Lrecl</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLreclImpl#getLength_in_bytes <em>Length in bytes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddLreclImpl extends ddsectionImpl implements ddLrecl
{
  /**
   * The default value of the '{@link #getLength_in_bytes() <em>Length in bytes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLength_in_bytes()
   * @generated
   * @ordered
   */
  protected static final int LENGTH_IN_BYTES_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getLength_in_bytes() <em>Length in bytes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLength_in_bytes()
   * @generated
   * @ordered
   */
  protected int length_in_bytes = LENGTH_IN_BYTES_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddLreclImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddLrecl();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getLength_in_bytes()
  {
    return length_in_bytes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLength_in_bytes(int newLength_in_bytes)
  {
    int oldLength_in_bytes = length_in_bytes;
    length_in_bytes = newLength_in_bytes;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_LRECL__LENGTH_IN_BYTES, oldLength_in_bytes, length_in_bytes));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LRECL__LENGTH_IN_BYTES:
        return getLength_in_bytes();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LRECL__LENGTH_IN_BYTES:
        setLength_in_bytes((Integer)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LRECL__LENGTH_IN_BYTES:
        setLength_in_bytes(LENGTH_IN_BYTES_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LRECL__LENGTH_IN_BYTES:
        return length_in_bytes != LENGTH_IN_BYTES_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (length_in_bytes: ");
    result.append(length_in_bytes);
    result.append(')');
    return result.toString();
  }

} //ddLreclImpl
