/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddChars;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Chars</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddCharsImpl#getTablename <em>Tablename</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddCharsImpl#getThat <em>That</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddCharsImpl#getTablename2 <em>Tablename2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddCharsImpl extends ddsectionImpl implements ddChars
{
  /**
   * The default value of the '{@link #getTablename() <em>Tablename</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTablename()
   * @generated
   * @ordered
   */
  protected static final String TABLENAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTablename() <em>Tablename</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTablename()
   * @generated
   * @ordered
   */
  protected String tablename = TABLENAME_EDEFAULT;

  /**
   * The default value of the '{@link #getThat() <em>That</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThat()
   * @generated
   * @ordered
   */
  protected static final String THAT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getThat() <em>That</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThat()
   * @generated
   * @ordered
   */
  protected String that = THAT_EDEFAULT;

  /**
   * The cached value of the '{@link #getTablename2() <em>Tablename2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTablename2()
   * @generated
   * @ordered
   */
  protected EList<String> tablename2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddCharsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddChars();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTablename()
  {
    return tablename;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTablename(String newTablename)
  {
    String oldTablename = tablename;
    tablename = newTablename;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_CHARS__TABLENAME, oldTablename, tablename));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getThat()
  {
    return that;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setThat(String newThat)
  {
    String oldThat = that;
    that = newThat;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_CHARS__THAT, oldThat, that));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getTablename2()
  {
    if (tablename2 == null)
    {
      tablename2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.DD_CHARS__TABLENAME2);
    }
    return tablename2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_CHARS__TABLENAME:
        return getTablename();
      case JclDslPackage.DD_CHARS__THAT:
        return getThat();
      case JclDslPackage.DD_CHARS__TABLENAME2:
        return getTablename2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_CHARS__TABLENAME:
        setTablename((String)newValue);
        return;
      case JclDslPackage.DD_CHARS__THAT:
        setThat((String)newValue);
        return;
      case JclDslPackage.DD_CHARS__TABLENAME2:
        getTablename2().clear();
        getTablename2().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_CHARS__TABLENAME:
        setTablename(TABLENAME_EDEFAULT);
        return;
      case JclDslPackage.DD_CHARS__THAT:
        setThat(THAT_EDEFAULT);
        return;
      case JclDslPackage.DD_CHARS__TABLENAME2:
        getTablename2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_CHARS__TABLENAME:
        return TABLENAME_EDEFAULT == null ? tablename != null : !TABLENAME_EDEFAULT.equals(tablename);
      case JclDslPackage.DD_CHARS__THAT:
        return THAT_EDEFAULT == null ? that != null : !THAT_EDEFAULT.equals(that);
      case JclDslPackage.DD_CHARS__TABLENAME2:
        return tablename2 != null && !tablename2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (tablename: ");
    result.append(tablename);
    result.append(", that: ");
    result.append(that);
    result.append(", tablename2: ");
    result.append(tablename2);
    result.append(')');
    return result.toString();
  }

} //ddCharsImpl
