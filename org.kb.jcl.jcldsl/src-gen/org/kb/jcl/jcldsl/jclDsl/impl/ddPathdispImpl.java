/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddPathdisp;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Pathdisp</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPathdispImpl#getDisp <em>Disp</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPathdispImpl#getDisp2 <em>Disp2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPathdispImpl#getDisp3 <em>Disp3</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddPathdispImpl extends ddsectionImpl implements ddPathdisp
{
  /**
   * The default value of the '{@link #getDisp() <em>Disp</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDisp()
   * @generated
   * @ordered
   */
  protected static final String DISP_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDisp() <em>Disp</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDisp()
   * @generated
   * @ordered
   */
  protected String disp = DISP_EDEFAULT;

  /**
   * The default value of the '{@link #getDisp2() <em>Disp2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDisp2()
   * @generated
   * @ordered
   */
  protected static final String DISP2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDisp2() <em>Disp2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDisp2()
   * @generated
   * @ordered
   */
  protected String disp2 = DISP2_EDEFAULT;

  /**
   * The default value of the '{@link #getDisp3() <em>Disp3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDisp3()
   * @generated
   * @ordered
   */
  protected static final String DISP3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDisp3() <em>Disp3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDisp3()
   * @generated
   * @ordered
   */
  protected String disp3 = DISP3_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddPathdispImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddPathdisp();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDisp()
  {
    return disp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDisp(String newDisp)
  {
    String oldDisp = disp;
    disp = newDisp;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_PATHDISP__DISP, oldDisp, disp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDisp2()
  {
    return disp2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDisp2(String newDisp2)
  {
    String oldDisp2 = disp2;
    disp2 = newDisp2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_PATHDISP__DISP2, oldDisp2, disp2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDisp3()
  {
    return disp3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDisp3(String newDisp3)
  {
    String oldDisp3 = disp3;
    disp3 = newDisp3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_PATHDISP__DISP3, oldDisp3, disp3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_PATHDISP__DISP:
        return getDisp();
      case JclDslPackage.DD_PATHDISP__DISP2:
        return getDisp2();
      case JclDslPackage.DD_PATHDISP__DISP3:
        return getDisp3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_PATHDISP__DISP:
        setDisp((String)newValue);
        return;
      case JclDslPackage.DD_PATHDISP__DISP2:
        setDisp2((String)newValue);
        return;
      case JclDslPackage.DD_PATHDISP__DISP3:
        setDisp3((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_PATHDISP__DISP:
        setDisp(DISP_EDEFAULT);
        return;
      case JclDslPackage.DD_PATHDISP__DISP2:
        setDisp2(DISP2_EDEFAULT);
        return;
      case JclDslPackage.DD_PATHDISP__DISP3:
        setDisp3(DISP3_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_PATHDISP__DISP:
        return DISP_EDEFAULT == null ? disp != null : !DISP_EDEFAULT.equals(disp);
      case JclDslPackage.DD_PATHDISP__DISP2:
        return DISP2_EDEFAULT == null ? disp2 != null : !DISP2_EDEFAULT.equals(disp2);
      case JclDslPackage.DD_PATHDISP__DISP3:
        return DISP3_EDEFAULT == null ? disp3 != null : !DISP3_EDEFAULT.equals(disp3);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (disp: ");
    result.append(disp);
    result.append(", disp2: ");
    result.append(disp2);
    result.append(", disp3: ");
    result.append(disp3);
    result.append(')');
    return result.toString();
  }

} //ddPathdispImpl
