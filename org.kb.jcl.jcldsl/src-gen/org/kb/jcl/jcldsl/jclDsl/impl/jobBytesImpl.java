/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobBytes;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>job Bytes</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobBytesImpl#getValues <em>Values</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobBytesImpl#getPagesDef <em>Pages Def</em>}</li>
 * </ul>
 *
 * @generated
 */
public class jobBytesImpl extends jobsectionImpl implements jobBytes
{
  /**
   * The default value of the '{@link #getValues() <em>Values</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValues()
   * @generated
   * @ordered
   */
  protected static final int VALUES_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getValues() <em>Values</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValues()
   * @generated
   * @ordered
   */
  protected int values = VALUES_EDEFAULT;

  /**
   * The cached value of the '{@link #getPagesDef() <em>Pages Def</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPagesDef()
   * @generated
   * @ordered
   */
  protected EList<String> pagesDef;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobBytesImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobBytes();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValues()
  {
    return values;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValues(int newValues)
  {
    int oldValues = values;
    values = newValues;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_BYTES__VALUES, oldValues, values));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getPagesDef()
  {
    if (pagesDef == null)
    {
      pagesDef = new EDataTypeEList<String>(String.class, this, JclDslPackage.JOB_BYTES__PAGES_DEF);
    }
    return pagesDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_BYTES__VALUES:
        return getValues();
      case JclDslPackage.JOB_BYTES__PAGES_DEF:
        return getPagesDef();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_BYTES__VALUES:
        setValues((Integer)newValue);
        return;
      case JclDslPackage.JOB_BYTES__PAGES_DEF:
        getPagesDef().clear();
        getPagesDef().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_BYTES__VALUES:
        setValues(VALUES_EDEFAULT);
        return;
      case JclDslPackage.JOB_BYTES__PAGES_DEF:
        getPagesDef().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_BYTES__VALUES:
        return values != VALUES_EDEFAULT;
      case JclDslPackage.JOB_BYTES__PAGES_DEF:
        return pagesDef != null && !pagesDef.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (values: ");
    result.append(values);
    result.append(", pagesDef: ");
    result.append(pagesDef);
    result.append(')');
    return result.toString();
  }

} //jobBytesImpl
