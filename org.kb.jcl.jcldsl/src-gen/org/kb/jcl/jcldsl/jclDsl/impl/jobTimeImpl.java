/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobTime;
import org.kb.jcl.jcldsl.jclDsl.jobTimevalue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>job Time</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobTimeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobTimeImpl#getJobTimevalue <em>Job Timevalue</em>}</li>
 * </ul>
 *
 * @generated
 */
public class jobTimeImpl extends jobsectionImpl implements jobTime
{
  /**
   * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected static final String VALUE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected String value = VALUE_EDEFAULT;

  /**
   * The cached value of the '{@link #getJobTimevalue() <em>Job Timevalue</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJobTimevalue()
   * @generated
   * @ordered
   */
  protected jobTimevalue jobTimevalue;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobTimeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobTime();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(String newValue)
  {
    String oldValue = value;
    value = newValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_TIME__VALUE, oldValue, value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobTimevalue getJobTimevalue()
  {
    return jobTimevalue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetJobTimevalue(jobTimevalue newJobTimevalue, NotificationChain msgs)
  {
    jobTimevalue oldJobTimevalue = jobTimevalue;
    jobTimevalue = newJobTimevalue;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_TIME__JOB_TIMEVALUE, oldJobTimevalue, newJobTimevalue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setJobTimevalue(jobTimevalue newJobTimevalue)
  {
    if (newJobTimevalue != jobTimevalue)
    {
      NotificationChain msgs = null;
      if (jobTimevalue != null)
        msgs = ((InternalEObject)jobTimevalue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JclDslPackage.JOB_TIME__JOB_TIMEVALUE, null, msgs);
      if (newJobTimevalue != null)
        msgs = ((InternalEObject)newJobTimevalue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JclDslPackage.JOB_TIME__JOB_TIMEVALUE, null, msgs);
      msgs = basicSetJobTimevalue(newJobTimevalue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_TIME__JOB_TIMEVALUE, newJobTimevalue, newJobTimevalue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_TIME__JOB_TIMEVALUE:
        return basicSetJobTimevalue(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_TIME__VALUE:
        return getValue();
      case JclDslPackage.JOB_TIME__JOB_TIMEVALUE:
        return getJobTimevalue();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_TIME__VALUE:
        setValue((String)newValue);
        return;
      case JclDslPackage.JOB_TIME__JOB_TIMEVALUE:
        setJobTimevalue((jobTimevalue)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_TIME__VALUE:
        setValue(VALUE_EDEFAULT);
        return;
      case JclDslPackage.JOB_TIME__JOB_TIMEVALUE:
        setJobTimevalue((jobTimevalue)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_TIME__VALUE:
        return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
      case JclDslPackage.JOB_TIME__JOB_TIMEVALUE:
        return jobTimevalue != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (value: ");
    result.append(value);
    result.append(')');
    return result.toString();
  }

} //jobTimeImpl
