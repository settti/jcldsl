/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddDcbSubparms;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Dcb Subparms</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ddDcbSubparmsImpl extends MinimalEObjectImpl.Container implements ddDcbSubparms
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddDcbSubparmsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddDcbSubparms();
  }

} //ddDcbSubparmsImpl
