/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobAccinfo;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>job Accinfo</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobAccinfoImpl#getPano <em>Pano</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobAccinfoImpl#getAcc <em>Acc</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobAccinfoImpl#getAcc2 <em>Acc2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobAccinfoImpl#getAcc3 <em>Acc3</em>}</li>
 * </ul>
 *
 * @generated
 */
public class jobAccinfoImpl extends jobsectionImpl implements jobAccinfo
{
  /**
   * The default value of the '{@link #getPano() <em>Pano</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPano()
   * @generated
   * @ordered
   */
  protected static final String PANO_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPano() <em>Pano</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPano()
   * @generated
   * @ordered
   */
  protected String pano = PANO_EDEFAULT;

  /**
   * The default value of the '{@link #getAcc() <em>Acc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAcc()
   * @generated
   * @ordered
   */
  protected static final int ACC_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getAcc() <em>Acc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAcc()
   * @generated
   * @ordered
   */
  protected int acc = ACC_EDEFAULT;

  /**
   * The default value of the '{@link #getAcc2() <em>Acc2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAcc2()
   * @generated
   * @ordered
   */
  protected static final String ACC2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAcc2() <em>Acc2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAcc2()
   * @generated
   * @ordered
   */
  protected String acc2 = ACC2_EDEFAULT;

  /**
   * The default value of the '{@link #getAcc3() <em>Acc3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAcc3()
   * @generated
   * @ordered
   */
  protected static final String ACC3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAcc3() <em>Acc3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAcc3()
   * @generated
   * @ordered
   */
  protected String acc3 = ACC3_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobAccinfoImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobAccinfo();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPano()
  {
    return pano;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPano(String newPano)
  {
    String oldPano = pano;
    pano = newPano;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_ACCINFO__PANO, oldPano, pano));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getAcc()
  {
    return acc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAcc(int newAcc)
  {
    int oldAcc = acc;
    acc = newAcc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_ACCINFO__ACC, oldAcc, acc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAcc2()
  {
    return acc2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAcc2(String newAcc2)
  {
    String oldAcc2 = acc2;
    acc2 = newAcc2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_ACCINFO__ACC2, oldAcc2, acc2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAcc3()
  {
    return acc3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAcc3(String newAcc3)
  {
    String oldAcc3 = acc3;
    acc3 = newAcc3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_ACCINFO__ACC3, oldAcc3, acc3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_ACCINFO__PANO:
        return getPano();
      case JclDslPackage.JOB_ACCINFO__ACC:
        return getAcc();
      case JclDslPackage.JOB_ACCINFO__ACC2:
        return getAcc2();
      case JclDslPackage.JOB_ACCINFO__ACC3:
        return getAcc3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_ACCINFO__PANO:
        setPano((String)newValue);
        return;
      case JclDslPackage.JOB_ACCINFO__ACC:
        setAcc((Integer)newValue);
        return;
      case JclDslPackage.JOB_ACCINFO__ACC2:
        setAcc2((String)newValue);
        return;
      case JclDslPackage.JOB_ACCINFO__ACC3:
        setAcc3((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_ACCINFO__PANO:
        setPano(PANO_EDEFAULT);
        return;
      case JclDslPackage.JOB_ACCINFO__ACC:
        setAcc(ACC_EDEFAULT);
        return;
      case JclDslPackage.JOB_ACCINFO__ACC2:
        setAcc2(ACC2_EDEFAULT);
        return;
      case JclDslPackage.JOB_ACCINFO__ACC3:
        setAcc3(ACC3_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_ACCINFO__PANO:
        return PANO_EDEFAULT == null ? pano != null : !PANO_EDEFAULT.equals(pano);
      case JclDslPackage.JOB_ACCINFO__ACC:
        return acc != ACC_EDEFAULT;
      case JclDslPackage.JOB_ACCINFO__ACC2:
        return ACC2_EDEFAULT == null ? acc2 != null : !ACC2_EDEFAULT.equals(acc2);
      case JclDslPackage.JOB_ACCINFO__ACC3:
        return ACC3_EDEFAULT == null ? acc3 != null : !ACC3_EDEFAULT.equals(acc3);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (pano: ");
    result.append(pano);
    result.append(", acc: ");
    result.append(acc);
    result.append(", acc2: ");
    result.append(acc2);
    result.append(", acc3: ");
    result.append(acc3);
    result.append(')');
    return result.toString();
  }

} //jobAccinfoImpl
