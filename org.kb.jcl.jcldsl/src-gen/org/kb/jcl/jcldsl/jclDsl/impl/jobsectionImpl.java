/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobsection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>jobsection</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class jobsectionImpl extends MinimalEObjectImpl.Container implements jobsection
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobsectionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobsection();
  }

} //jobsectionImpl
