/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobMsglevel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>job Msglevel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobMsglevelImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobMsglevelImpl#getJobMsglevelStatements <em>Job Msglevel Statements</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobMsglevelImpl#getJobMsglevelMessages <em>Job Msglevel Messages</em>}</li>
 * </ul>
 *
 * @generated
 */
public class jobMsglevelImpl extends jobsectionImpl implements jobMsglevel
{
  /**
   * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected static final int VALUE_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected int value = VALUE_EDEFAULT;

  /**
   * The cached value of the '{@link #getJobMsglevelStatements() <em>Job Msglevel Statements</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJobMsglevelStatements()
   * @generated
   * @ordered
   */
  protected EList<Integer> jobMsglevelStatements;

  /**
   * The cached value of the '{@link #getJobMsglevelMessages() <em>Job Msglevel Messages</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJobMsglevelMessages()
   * @generated
   * @ordered
   */
  protected EList<Integer> jobMsglevelMessages;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobMsglevelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobMsglevel();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(int newValue)
  {
    int oldValue = value;
    value = newValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_MSGLEVEL__VALUE, oldValue, value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Integer> getJobMsglevelStatements()
  {
    if (jobMsglevelStatements == null)
    {
      jobMsglevelStatements = new EDataTypeEList<Integer>(Integer.class, this, JclDslPackage.JOB_MSGLEVEL__JOB_MSGLEVEL_STATEMENTS);
    }
    return jobMsglevelStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Integer> getJobMsglevelMessages()
  {
    if (jobMsglevelMessages == null)
    {
      jobMsglevelMessages = new EDataTypeEList<Integer>(Integer.class, this, JclDslPackage.JOB_MSGLEVEL__JOB_MSGLEVEL_MESSAGES);
    }
    return jobMsglevelMessages;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_MSGLEVEL__VALUE:
        return getValue();
      case JclDslPackage.JOB_MSGLEVEL__JOB_MSGLEVEL_STATEMENTS:
        return getJobMsglevelStatements();
      case JclDslPackage.JOB_MSGLEVEL__JOB_MSGLEVEL_MESSAGES:
        return getJobMsglevelMessages();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_MSGLEVEL__VALUE:
        setValue((Integer)newValue);
        return;
      case JclDslPackage.JOB_MSGLEVEL__JOB_MSGLEVEL_STATEMENTS:
        getJobMsglevelStatements().clear();
        getJobMsglevelStatements().addAll((Collection<? extends Integer>)newValue);
        return;
      case JclDslPackage.JOB_MSGLEVEL__JOB_MSGLEVEL_MESSAGES:
        getJobMsglevelMessages().clear();
        getJobMsglevelMessages().addAll((Collection<? extends Integer>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_MSGLEVEL__VALUE:
        setValue(VALUE_EDEFAULT);
        return;
      case JclDslPackage.JOB_MSGLEVEL__JOB_MSGLEVEL_STATEMENTS:
        getJobMsglevelStatements().clear();
        return;
      case JclDslPackage.JOB_MSGLEVEL__JOB_MSGLEVEL_MESSAGES:
        getJobMsglevelMessages().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_MSGLEVEL__VALUE:
        return value != VALUE_EDEFAULT;
      case JclDslPackage.JOB_MSGLEVEL__JOB_MSGLEVEL_STATEMENTS:
        return jobMsglevelStatements != null && !jobMsglevelStatements.isEmpty();
      case JclDslPackage.JOB_MSGLEVEL__JOB_MSGLEVEL_MESSAGES:
        return jobMsglevelMessages != null && !jobMsglevelMessages.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (value: ");
    result.append(value);
    result.append(", jobMsglevelStatements: ");
    result.append(jobMsglevelStatements);
    result.append(", jobMsglevelMessages: ");
    result.append(jobMsglevelMessages);
    result.append(')');
    return result.toString();
  }

} //jobMsglevelImpl
