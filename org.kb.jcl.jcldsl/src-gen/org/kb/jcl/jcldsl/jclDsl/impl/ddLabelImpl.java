/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddLabel;
import org.kb.jcl.jcldsl.jclDsl.ddLabelExpire;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Label</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelImpl#isSequence_number <em>Sequence number</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelImpl#isLabel <em>Label</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelImpl#getPassword <em>Password</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelImpl#getInorout <em>Inorout</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelImpl#getExpire <em>Expire</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddLabelImpl extends ddsectionImpl implements ddLabel
{
  /**
   * The default value of the '{@link #isSequence_number() <em>Sequence number</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSequence_number()
   * @generated
   * @ordered
   */
  protected static final boolean SEQUENCE_NUMBER_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isSequence_number() <em>Sequence number</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSequence_number()
   * @generated
   * @ordered
   */
  protected boolean sequence_number = SEQUENCE_NUMBER_EDEFAULT;

  /**
   * The default value of the '{@link #isLabel() <em>Label</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isLabel()
   * @generated
   * @ordered
   */
  protected static final boolean LABEL_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isLabel() <em>Label</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isLabel()
   * @generated
   * @ordered
   */
  protected boolean label = LABEL_EDEFAULT;

  /**
   * The default value of the '{@link #getPassword() <em>Password</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPassword()
   * @generated
   * @ordered
   */
  protected static final String PASSWORD_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPassword() <em>Password</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPassword()
   * @generated
   * @ordered
   */
  protected String password = PASSWORD_EDEFAULT;

  /**
   * The default value of the '{@link #getInorout() <em>Inorout</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInorout()
   * @generated
   * @ordered
   */
  protected static final String INOROUT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getInorout() <em>Inorout</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInorout()
   * @generated
   * @ordered
   */
  protected String inorout = INOROUT_EDEFAULT;

  /**
   * The cached value of the '{@link #getExpire() <em>Expire</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpire()
   * @generated
   * @ordered
   */
  protected ddLabelExpire expire;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddLabelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddLabel();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSequence_number()
  {
    return sequence_number;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSequence_number(boolean newSequence_number)
  {
    boolean oldSequence_number = sequence_number;
    sequence_number = newSequence_number;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_LABEL__SEQUENCE_NUMBER, oldSequence_number, sequence_number));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isLabel()
  {
    return label;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLabel(boolean newLabel)
  {
    boolean oldLabel = label;
    label = newLabel;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_LABEL__LABEL, oldLabel, label));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPassword()
  {
    return password;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPassword(String newPassword)
  {
    String oldPassword = password;
    password = newPassword;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_LABEL__PASSWORD, oldPassword, password));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getInorout()
  {
    return inorout;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInorout(String newInorout)
  {
    String oldInorout = inorout;
    inorout = newInorout;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_LABEL__INOROUT, oldInorout, inorout));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddLabelExpire getExpire()
  {
    return expire;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpire(ddLabelExpire newExpire, NotificationChain msgs)
  {
    ddLabelExpire oldExpire = expire;
    expire = newExpire;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_LABEL__EXPIRE, oldExpire, newExpire);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpire(ddLabelExpire newExpire)
  {
    if (newExpire != expire)
    {
      NotificationChain msgs = null;
      if (expire != null)
        msgs = ((InternalEObject)expire).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JclDslPackage.DD_LABEL__EXPIRE, null, msgs);
      if (newExpire != null)
        msgs = ((InternalEObject)newExpire).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JclDslPackage.DD_LABEL__EXPIRE, null, msgs);
      msgs = basicSetExpire(newExpire, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_LABEL__EXPIRE, newExpire, newExpire));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LABEL__EXPIRE:
        return basicSetExpire(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LABEL__SEQUENCE_NUMBER:
        return isSequence_number();
      case JclDslPackage.DD_LABEL__LABEL:
        return isLabel();
      case JclDslPackage.DD_LABEL__PASSWORD:
        return getPassword();
      case JclDslPackage.DD_LABEL__INOROUT:
        return getInorout();
      case JclDslPackage.DD_LABEL__EXPIRE:
        return getExpire();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LABEL__SEQUENCE_NUMBER:
        setSequence_number((Boolean)newValue);
        return;
      case JclDslPackage.DD_LABEL__LABEL:
        setLabel((Boolean)newValue);
        return;
      case JclDslPackage.DD_LABEL__PASSWORD:
        setPassword((String)newValue);
        return;
      case JclDslPackage.DD_LABEL__INOROUT:
        setInorout((String)newValue);
        return;
      case JclDslPackage.DD_LABEL__EXPIRE:
        setExpire((ddLabelExpire)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LABEL__SEQUENCE_NUMBER:
        setSequence_number(SEQUENCE_NUMBER_EDEFAULT);
        return;
      case JclDslPackage.DD_LABEL__LABEL:
        setLabel(LABEL_EDEFAULT);
        return;
      case JclDslPackage.DD_LABEL__PASSWORD:
        setPassword(PASSWORD_EDEFAULT);
        return;
      case JclDslPackage.DD_LABEL__INOROUT:
        setInorout(INOROUT_EDEFAULT);
        return;
      case JclDslPackage.DD_LABEL__EXPIRE:
        setExpire((ddLabelExpire)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LABEL__SEQUENCE_NUMBER:
        return sequence_number != SEQUENCE_NUMBER_EDEFAULT;
      case JclDslPackage.DD_LABEL__LABEL:
        return label != LABEL_EDEFAULT;
      case JclDslPackage.DD_LABEL__PASSWORD:
        return PASSWORD_EDEFAULT == null ? password != null : !PASSWORD_EDEFAULT.equals(password);
      case JclDslPackage.DD_LABEL__INOROUT:
        return INOROUT_EDEFAULT == null ? inorout != null : !INOROUT_EDEFAULT.equals(inorout);
      case JclDslPackage.DD_LABEL__EXPIRE:
        return expire != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (sequence_number: ");
    result.append(sequence_number);
    result.append(", label: ");
    result.append(label);
    result.append(", password: ");
    result.append(password);
    result.append(", inorout: ");
    result.append(inorout);
    result.append(')');
    return result.toString();
  }

} //ddLabelImpl
