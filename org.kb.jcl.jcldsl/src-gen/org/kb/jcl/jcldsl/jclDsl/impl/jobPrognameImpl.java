/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobProgname;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>job Progname</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobPrognameImpl#getProgrammer_name <em>Programmer name</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobPrognameImpl#getPROGRAMMER_ID <em>PROGRAMMER ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class jobPrognameImpl extends jobsectionImpl implements jobProgname
{
  /**
   * The default value of the '{@link #getProgrammer_name() <em>Programmer name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProgrammer_name()
   * @generated
   * @ordered
   */
  protected static final String PROGRAMMER_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getProgrammer_name() <em>Programmer name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProgrammer_name()
   * @generated
   * @ordered
   */
  protected String programmer_name = PROGRAMMER_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getPROGRAMMER_ID() <em>PROGRAMMER ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPROGRAMMER_ID()
   * @generated
   * @ordered
   */
  protected static final String PROGRAMMER_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPROGRAMMER_ID() <em>PROGRAMMER ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPROGRAMMER_ID()
   * @generated
   * @ordered
   */
  protected String programmeR_ID = PROGRAMMER_ID_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobPrognameImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobProgname();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getProgrammer_name()
  {
    return programmer_name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setProgrammer_name(String newProgrammer_name)
  {
    String oldProgrammer_name = programmer_name;
    programmer_name = newProgrammer_name;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_PROGNAME__PROGRAMMER_NAME, oldProgrammer_name, programmer_name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPROGRAMMER_ID()
  {
    return programmeR_ID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPROGRAMMER_ID(String newPROGRAMMER_ID)
  {
    String oldPROGRAMMER_ID = programmeR_ID;
    programmeR_ID = newPROGRAMMER_ID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_PROGNAME__PROGRAMMER_ID, oldPROGRAMMER_ID, programmeR_ID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_PROGNAME__PROGRAMMER_NAME:
        return getProgrammer_name();
      case JclDslPackage.JOB_PROGNAME__PROGRAMMER_ID:
        return getPROGRAMMER_ID();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_PROGNAME__PROGRAMMER_NAME:
        setProgrammer_name((String)newValue);
        return;
      case JclDslPackage.JOB_PROGNAME__PROGRAMMER_ID:
        setPROGRAMMER_ID((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_PROGNAME__PROGRAMMER_NAME:
        setProgrammer_name(PROGRAMMER_NAME_EDEFAULT);
        return;
      case JclDslPackage.JOB_PROGNAME__PROGRAMMER_ID:
        setPROGRAMMER_ID(PROGRAMMER_ID_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_PROGNAME__PROGRAMMER_NAME:
        return PROGRAMMER_NAME_EDEFAULT == null ? programmer_name != null : !PROGRAMMER_NAME_EDEFAULT.equals(programmer_name);
      case JclDslPackage.JOB_PROGNAME__PROGRAMMER_ID:
        return PROGRAMMER_ID_EDEFAULT == null ? programmeR_ID != null : !PROGRAMMER_ID_EDEFAULT.equals(programmeR_ID);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (programmer_name: ");
    result.append(programmer_name);
    result.append(", PROGRAMMER_ID: ");
    result.append(programmeR_ID);
    result.append(')');
    return result.toString();
  }

} //jobPrognameImpl
