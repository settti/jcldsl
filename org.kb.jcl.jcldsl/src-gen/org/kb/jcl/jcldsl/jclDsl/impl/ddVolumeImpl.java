/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddVolume;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Volume</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddVolumeImpl#getPrivate <em>Private</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddVolumeImpl#getRetain <em>Retain</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddVolumeImpl#getVolseq <em>Volseq</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddVolumeImpl#getVolcnt <em>Volcnt</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddVolumeImpl#getSerial <em>Serial</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddVolumeImpl#getSerials <em>Serials</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddVolumeImpl#getDsname <em>Dsname</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddVolumeImpl extends ddsectionImpl implements ddVolume
{
  /**
   * The default value of the '{@link #getPrivate() <em>Private</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrivate()
   * @generated
   * @ordered
   */
  protected static final String PRIVATE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPrivate() <em>Private</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrivate()
   * @generated
   * @ordered
   */
  protected String private_ = PRIVATE_EDEFAULT;

  /**
   * The default value of the '{@link #getRetain() <em>Retain</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRetain()
   * @generated
   * @ordered
   */
  protected static final String RETAIN_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRetain() <em>Retain</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRetain()
   * @generated
   * @ordered
   */
  protected String retain = RETAIN_EDEFAULT;

  /**
   * The default value of the '{@link #getVolseq() <em>Volseq</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVolseq()
   * @generated
   * @ordered
   */
  protected static final int VOLSEQ_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getVolseq() <em>Volseq</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVolseq()
   * @generated
   * @ordered
   */
  protected int volseq = VOLSEQ_EDEFAULT;

  /**
   * The default value of the '{@link #getVolcnt() <em>Volcnt</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVolcnt()
   * @generated
   * @ordered
   */
  protected static final int VOLCNT_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getVolcnt() <em>Volcnt</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVolcnt()
   * @generated
   * @ordered
   */
  protected int volcnt = VOLCNT_EDEFAULT;

  /**
   * The default value of the '{@link #getSerial() <em>Serial</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSerial()
   * @generated
   * @ordered
   */
  protected static final String SERIAL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSerial() <em>Serial</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSerial()
   * @generated
   * @ordered
   */
  protected String serial = SERIAL_EDEFAULT;

  /**
   * The cached value of the '{@link #getSerials() <em>Serials</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSerials()
   * @generated
   * @ordered
   */
  protected EList<String> serials;

  /**
   * The default value of the '{@link #getDsname() <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDsname()
   * @generated
   * @ordered
   */
  protected static final String DSNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDsname() <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDsname()
   * @generated
   * @ordered
   */
  protected String dsname = DSNAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddVolumeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddVolume();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPrivate()
  {
    return private_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrivate(String newPrivate)
  {
    String oldPrivate = private_;
    private_ = newPrivate;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_VOLUME__PRIVATE, oldPrivate, private_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRetain()
  {
    return retain;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRetain(String newRetain)
  {
    String oldRetain = retain;
    retain = newRetain;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_VOLUME__RETAIN, oldRetain, retain));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getVolseq()
  {
    return volseq;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVolseq(int newVolseq)
  {
    int oldVolseq = volseq;
    volseq = newVolseq;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_VOLUME__VOLSEQ, oldVolseq, volseq));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getVolcnt()
  {
    return volcnt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVolcnt(int newVolcnt)
  {
    int oldVolcnt = volcnt;
    volcnt = newVolcnt;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_VOLUME__VOLCNT, oldVolcnt, volcnt));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSerial()
  {
    return serial;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSerial(String newSerial)
  {
    String oldSerial = serial;
    serial = newSerial;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_VOLUME__SERIAL, oldSerial, serial));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getSerials()
  {
    if (serials == null)
    {
      serials = new EDataTypeEList<String>(String.class, this, JclDslPackage.DD_VOLUME__SERIALS);
    }
    return serials;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDsname()
  {
    return dsname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDsname(String newDsname)
  {
    String oldDsname = dsname;
    dsname = newDsname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_VOLUME__DSNAME, oldDsname, dsname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_VOLUME__PRIVATE:
        return getPrivate();
      case JclDslPackage.DD_VOLUME__RETAIN:
        return getRetain();
      case JclDslPackage.DD_VOLUME__VOLSEQ:
        return getVolseq();
      case JclDslPackage.DD_VOLUME__VOLCNT:
        return getVolcnt();
      case JclDslPackage.DD_VOLUME__SERIAL:
        return getSerial();
      case JclDslPackage.DD_VOLUME__SERIALS:
        return getSerials();
      case JclDslPackage.DD_VOLUME__DSNAME:
        return getDsname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_VOLUME__PRIVATE:
        setPrivate((String)newValue);
        return;
      case JclDslPackage.DD_VOLUME__RETAIN:
        setRetain((String)newValue);
        return;
      case JclDslPackage.DD_VOLUME__VOLSEQ:
        setVolseq((Integer)newValue);
        return;
      case JclDslPackage.DD_VOLUME__VOLCNT:
        setVolcnt((Integer)newValue);
        return;
      case JclDslPackage.DD_VOLUME__SERIAL:
        setSerial((String)newValue);
        return;
      case JclDslPackage.DD_VOLUME__SERIALS:
        getSerials().clear();
        getSerials().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.DD_VOLUME__DSNAME:
        setDsname((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_VOLUME__PRIVATE:
        setPrivate(PRIVATE_EDEFAULT);
        return;
      case JclDslPackage.DD_VOLUME__RETAIN:
        setRetain(RETAIN_EDEFAULT);
        return;
      case JclDslPackage.DD_VOLUME__VOLSEQ:
        setVolseq(VOLSEQ_EDEFAULT);
        return;
      case JclDslPackage.DD_VOLUME__VOLCNT:
        setVolcnt(VOLCNT_EDEFAULT);
        return;
      case JclDslPackage.DD_VOLUME__SERIAL:
        setSerial(SERIAL_EDEFAULT);
        return;
      case JclDslPackage.DD_VOLUME__SERIALS:
        getSerials().clear();
        return;
      case JclDslPackage.DD_VOLUME__DSNAME:
        setDsname(DSNAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_VOLUME__PRIVATE:
        return PRIVATE_EDEFAULT == null ? private_ != null : !PRIVATE_EDEFAULT.equals(private_);
      case JclDslPackage.DD_VOLUME__RETAIN:
        return RETAIN_EDEFAULT == null ? retain != null : !RETAIN_EDEFAULT.equals(retain);
      case JclDslPackage.DD_VOLUME__VOLSEQ:
        return volseq != VOLSEQ_EDEFAULT;
      case JclDslPackage.DD_VOLUME__VOLCNT:
        return volcnt != VOLCNT_EDEFAULT;
      case JclDslPackage.DD_VOLUME__SERIAL:
        return SERIAL_EDEFAULT == null ? serial != null : !SERIAL_EDEFAULT.equals(serial);
      case JclDslPackage.DD_VOLUME__SERIALS:
        return serials != null && !serials.isEmpty();
      case JclDslPackage.DD_VOLUME__DSNAME:
        return DSNAME_EDEFAULT == null ? dsname != null : !DSNAME_EDEFAULT.equals(dsname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (private: ");
    result.append(private_);
    result.append(", retain: ");
    result.append(retain);
    result.append(", volseq: ");
    result.append(volseq);
    result.append(", volcnt: ");
    result.append(volcnt);
    result.append(", serial: ");
    result.append(serial);
    result.append(", serials: ");
    result.append(serials);
    result.append(", dsname: ");
    result.append(dsname);
    result.append(')');
    return result.toString();
  }

} //ddVolumeImpl
