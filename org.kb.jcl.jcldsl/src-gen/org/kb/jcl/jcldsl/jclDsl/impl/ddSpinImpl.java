/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddSpin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Spin</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSpinImpl#getUnalloc <em>Unalloc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddSpinImpl extends ddsectionImpl implements ddSpin
{
  /**
   * The default value of the '{@link #getUnalloc() <em>Unalloc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnalloc()
   * @generated
   * @ordered
   */
  protected static final String UNALLOC_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUnalloc() <em>Unalloc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnalloc()
   * @generated
   * @ordered
   */
  protected String unalloc = UNALLOC_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddSpinImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddSpin();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUnalloc()
  {
    return unalloc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUnalloc(String newUnalloc)
  {
    String oldUnalloc = unalloc;
    unalloc = newUnalloc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SPIN__UNALLOC, oldUnalloc, unalloc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SPIN__UNALLOC:
        return getUnalloc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SPIN__UNALLOC:
        setUnalloc((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SPIN__UNALLOC:
        setUnalloc(UNALLOC_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SPIN__UNALLOC:
        return UNALLOC_EDEFAULT == null ? unalloc != null : !UNALLOC_EDEFAULT.equals(unalloc);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (unalloc: ");
    result.append(unalloc);
    result.append(')');
    return result.toString();
  }

} //ddSpinImpl
