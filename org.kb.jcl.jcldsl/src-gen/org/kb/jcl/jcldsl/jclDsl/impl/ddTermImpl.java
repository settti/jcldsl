/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddTerm;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ddTermImpl extends ddsectionImpl implements ddTerm
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddTermImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddTerm();
  }

} //ddTermImpl
