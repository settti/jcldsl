/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddSecmodel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Secmodel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSecmodelImpl#getDsname <em>Dsname</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSecmodelImpl#getGeneric <em>Generic</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddSecmodelImpl extends ddsectionImpl implements ddSecmodel
{
  /**
   * The default value of the '{@link #getDsname() <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDsname()
   * @generated
   * @ordered
   */
  protected static final String DSNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDsname() <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDsname()
   * @generated
   * @ordered
   */
  protected String dsname = DSNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getGeneric() <em>Generic</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGeneric()
   * @generated
   * @ordered
   */
  protected static final String GENERIC_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getGeneric() <em>Generic</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGeneric()
   * @generated
   * @ordered
   */
  protected String generic = GENERIC_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddSecmodelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddSecmodel();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDsname()
  {
    return dsname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDsname(String newDsname)
  {
    String oldDsname = dsname;
    dsname = newDsname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SECMODEL__DSNAME, oldDsname, dsname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getGeneric()
  {
    return generic;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGeneric(String newGeneric)
  {
    String oldGeneric = generic;
    generic = newGeneric;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SECMODEL__GENERIC, oldGeneric, generic));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SECMODEL__DSNAME:
        return getDsname();
      case JclDslPackage.DD_SECMODEL__GENERIC:
        return getGeneric();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SECMODEL__DSNAME:
        setDsname((String)newValue);
        return;
      case JclDslPackage.DD_SECMODEL__GENERIC:
        setGeneric((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SECMODEL__DSNAME:
        setDsname(DSNAME_EDEFAULT);
        return;
      case JclDslPackage.DD_SECMODEL__GENERIC:
        setGeneric(GENERIC_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SECMODEL__DSNAME:
        return DSNAME_EDEFAULT == null ? dsname != null : !DSNAME_EDEFAULT.equals(dsname);
      case JclDslPackage.DD_SECMODEL__GENERIC:
        return GENERIC_EDEFAULT == null ? generic != null : !GENERIC_EDEFAULT.equals(generic);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (dsname: ");
    result.append(dsname);
    result.append(", generic: ");
    result.append(generic);
    result.append(')');
    return result.toString();
  }

} //ddSecmodelImpl
