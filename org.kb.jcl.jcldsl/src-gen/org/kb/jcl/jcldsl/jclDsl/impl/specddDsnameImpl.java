/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.specddDsname;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>specdd Dsname</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.specddDsnameImpl#getSpecname <em>Specname</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.specddDsnameImpl#getDsname <em>Dsname</em>}</li>
 * </ul>
 *
 * @generated
 */
public class specddDsnameImpl extends specddSectionImpl implements specddDsname
{
  /**
   * The default value of the '{@link #getSpecname() <em>Specname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpecname()
   * @generated
   * @ordered
   */
  protected static final String SPECNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSpecname() <em>Specname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpecname()
   * @generated
   * @ordered
   */
  protected String specname = SPECNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDsname() <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDsname()
   * @generated
   * @ordered
   */
  protected static final String DSNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDsname() <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDsname()
   * @generated
   * @ordered
   */
  protected String dsname = DSNAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected specddDsnameImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getspecddDsname();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSpecname()
  {
    return specname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSpecname(String newSpecname)
  {
    String oldSpecname = specname;
    specname = newSpecname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.SPECDD_DSNAME__SPECNAME, oldSpecname, specname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDsname()
  {
    return dsname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDsname(String newDsname)
  {
    String oldDsname = dsname;
    dsname = newDsname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.SPECDD_DSNAME__DSNAME, oldDsname, dsname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.SPECDD_DSNAME__SPECNAME:
        return getSpecname();
      case JclDslPackage.SPECDD_DSNAME__DSNAME:
        return getDsname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.SPECDD_DSNAME__SPECNAME:
        setSpecname((String)newValue);
        return;
      case JclDslPackage.SPECDD_DSNAME__DSNAME:
        setDsname((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.SPECDD_DSNAME__SPECNAME:
        setSpecname(SPECNAME_EDEFAULT);
        return;
      case JclDslPackage.SPECDD_DSNAME__DSNAME:
        setDsname(DSNAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.SPECDD_DSNAME__SPECNAME:
        return SPECNAME_EDEFAULT == null ? specname != null : !SPECNAME_EDEFAULT.equals(specname);
      case JclDslPackage.SPECDD_DSNAME__DSNAME:
        return DSNAME_EDEFAULT == null ? dsname != null : !DSNAME_EDEFAULT.equals(dsname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (specname: ");
    result.append(specname);
    result.append(", dsname: ");
    result.append(dsname);
    result.append(')');
    return result.toString();
  }

} //specddDsnameImpl
