/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.ELSE;
import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ELSE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ELSEImpl#getELSENAME <em>ELSENAME</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ELSEImpl extends KeywordImpl implements ELSE
{
  /**
   * The default value of the '{@link #getELSENAME() <em>ELSENAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getELSENAME()
   * @generated
   * @ordered
   */
  protected static final String ELSENAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getELSENAME() <em>ELSENAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getELSENAME()
   * @generated
   * @ordered
   */
  protected String elsename = ELSENAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ELSEImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getELSE();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getELSENAME()
  {
    return elsename;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setELSENAME(String newELSENAME)
  {
    String oldELSENAME = elsename;
    elsename = newELSENAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.ELSE__ELSENAME, oldELSENAME, elsename));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.ELSE__ELSENAME:
        return getELSENAME();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.ELSE__ELSENAME:
        setELSENAME((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.ELSE__ELSENAME:
        setELSENAME(ELSENAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.ELSE__ELSENAME:
        return ELSENAME_EDEFAULT == null ? elsename != null : !ELSENAME_EDEFAULT.equals(elsename);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (ELSENAME: ");
    result.append(elsename);
    result.append(')');
    return result.toString();
  }

} //ELSEImpl
