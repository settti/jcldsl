/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.kb.jcl.jcldsl.jclDsl.JOB;
import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.Keyword;
import org.kb.jcl.jcldsl.jclDsl.jobsection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>JOB</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.JOBImpl#getKeywords <em>Keywords</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.JOBImpl#getJOBNAME <em>JOBNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.JOBImpl#getSection <em>Section</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.JOBImpl#getSections <em>Sections</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JOBImpl extends ModelImpl implements JOB
{
  /**
   * The cached value of the '{@link #getKeywords() <em>Keywords</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKeywords()
   * @generated
   * @ordered
   */
  protected EList<Keyword> keywords;

  /**
   * The default value of the '{@link #getJOBNAME() <em>JOBNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJOBNAME()
   * @generated
   * @ordered
   */
  protected static final String JOBNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getJOBNAME() <em>JOBNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJOBNAME()
   * @generated
   * @ordered
   */
  protected String jobname = JOBNAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getSection() <em>Section</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSection()
   * @generated
   * @ordered
   */
  protected EList<jobsection> section;

  /**
   * The cached value of the '{@link #getSections() <em>Sections</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSections()
   * @generated
   * @ordered
   */
  protected EList<jobsection> sections;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected JOBImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getJOB();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Keyword> getKeywords()
  {
    if (keywords == null)
    {
      keywords = new EObjectContainmentEList<Keyword>(Keyword.class, this, JclDslPackage.JOB__KEYWORDS);
    }
    return keywords;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getJOBNAME()
  {
    return jobname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setJOBNAME(String newJOBNAME)
  {
    String oldJOBNAME = jobname;
    jobname = newJOBNAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB__JOBNAME, oldJOBNAME, jobname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<jobsection> getSection()
  {
    if (section == null)
    {
      section = new EObjectContainmentEList<jobsection>(jobsection.class, this, JclDslPackage.JOB__SECTION);
    }
    return section;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<jobsection> getSections()
  {
    if (sections == null)
    {
      sections = new EObjectContainmentEList<jobsection>(jobsection.class, this, JclDslPackage.JOB__SECTIONS);
    }
    return sections;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB__KEYWORDS:
        return ((InternalEList<?>)getKeywords()).basicRemove(otherEnd, msgs);
      case JclDslPackage.JOB__SECTION:
        return ((InternalEList<?>)getSection()).basicRemove(otherEnd, msgs);
      case JclDslPackage.JOB__SECTIONS:
        return ((InternalEList<?>)getSections()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB__KEYWORDS:
        return getKeywords();
      case JclDslPackage.JOB__JOBNAME:
        return getJOBNAME();
      case JclDslPackage.JOB__SECTION:
        return getSection();
      case JclDslPackage.JOB__SECTIONS:
        return getSections();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB__KEYWORDS:
        getKeywords().clear();
        getKeywords().addAll((Collection<? extends Keyword>)newValue);
        return;
      case JclDslPackage.JOB__JOBNAME:
        setJOBNAME((String)newValue);
        return;
      case JclDslPackage.JOB__SECTION:
        getSection().clear();
        getSection().addAll((Collection<? extends jobsection>)newValue);
        return;
      case JclDslPackage.JOB__SECTIONS:
        getSections().clear();
        getSections().addAll((Collection<? extends jobsection>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB__KEYWORDS:
        getKeywords().clear();
        return;
      case JclDslPackage.JOB__JOBNAME:
        setJOBNAME(JOBNAME_EDEFAULT);
        return;
      case JclDslPackage.JOB__SECTION:
        getSection().clear();
        return;
      case JclDslPackage.JOB__SECTIONS:
        getSections().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB__KEYWORDS:
        return keywords != null && !keywords.isEmpty();
      case JclDslPackage.JOB__JOBNAME:
        return JOBNAME_EDEFAULT == null ? jobname != null : !JOBNAME_EDEFAULT.equals(jobname);
      case JclDslPackage.JOB__SECTION:
        return section != null && !section.isEmpty();
      case JclDslPackage.JOB__SECTIONS:
        return sections != null && !sections.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (JOBNAME: ");
    result.append(jobname);
    result.append(')');
    return result.toString();
  }

} //JOBImpl
