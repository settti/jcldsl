/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddSpace;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Space</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSpaceImpl#getLength <em>Length</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSpaceImpl#getPrimary <em>Primary</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSpaceImpl#getSecondary <em>Secondary</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSpaceImpl#getDirorindx <em>Dirorindx</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSpaceImpl#getParm <em>Parm</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddSpaceImpl extends ddsectionImpl implements ddSpace
{
  /**
   * The default value of the '{@link #getLength() <em>Length</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLength()
   * @generated
   * @ordered
   */
  protected static final int LENGTH_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getLength() <em>Length</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLength()
   * @generated
   * @ordered
   */
  protected int length = LENGTH_EDEFAULT;

  /**
   * The default value of the '{@link #getPrimary() <em>Primary</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrimary()
   * @generated
   * @ordered
   */
  protected static final int PRIMARY_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getPrimary() <em>Primary</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrimary()
   * @generated
   * @ordered
   */
  protected int primary = PRIMARY_EDEFAULT;

  /**
   * The default value of the '{@link #getSecondary() <em>Secondary</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondary()
   * @generated
   * @ordered
   */
  protected static final int SECONDARY_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getSecondary() <em>Secondary</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondary()
   * @generated
   * @ordered
   */
  protected int secondary = SECONDARY_EDEFAULT;

  /**
   * The default value of the '{@link #getDirorindx() <em>Dirorindx</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDirorindx()
   * @generated
   * @ordered
   */
  protected static final int DIRORINDX_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getDirorindx() <em>Dirorindx</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDirorindx()
   * @generated
   * @ordered
   */
  protected int dirorindx = DIRORINDX_EDEFAULT;

  /**
   * The cached value of the '{@link #getParm() <em>Parm</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm()
   * @generated
   * @ordered
   */
  protected EList<String> parm;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddSpaceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddSpace();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getLength()
  {
    return length;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLength(int newLength)
  {
    int oldLength = length;
    length = newLength;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SPACE__LENGTH, oldLength, length));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getPrimary()
  {
    return primary;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrimary(int newPrimary)
  {
    int oldPrimary = primary;
    primary = newPrimary;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SPACE__PRIMARY, oldPrimary, primary));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getSecondary()
  {
    return secondary;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSecondary(int newSecondary)
  {
    int oldSecondary = secondary;
    secondary = newSecondary;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SPACE__SECONDARY, oldSecondary, secondary));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getDirorindx()
  {
    return dirorindx;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDirorindx(int newDirorindx)
  {
    int oldDirorindx = dirorindx;
    dirorindx = newDirorindx;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SPACE__DIRORINDX, oldDirorindx, dirorindx));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getParm()
  {
    if (parm == null)
    {
      parm = new EDataTypeEList<String>(String.class, this, JclDslPackage.DD_SPACE__PARM);
    }
    return parm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SPACE__LENGTH:
        return getLength();
      case JclDslPackage.DD_SPACE__PRIMARY:
        return getPrimary();
      case JclDslPackage.DD_SPACE__SECONDARY:
        return getSecondary();
      case JclDslPackage.DD_SPACE__DIRORINDX:
        return getDirorindx();
      case JclDslPackage.DD_SPACE__PARM:
        return getParm();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SPACE__LENGTH:
        setLength((Integer)newValue);
        return;
      case JclDslPackage.DD_SPACE__PRIMARY:
        setPrimary((Integer)newValue);
        return;
      case JclDslPackage.DD_SPACE__SECONDARY:
        setSecondary((Integer)newValue);
        return;
      case JclDslPackage.DD_SPACE__DIRORINDX:
        setDirorindx((Integer)newValue);
        return;
      case JclDslPackage.DD_SPACE__PARM:
        getParm().clear();
        getParm().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SPACE__LENGTH:
        setLength(LENGTH_EDEFAULT);
        return;
      case JclDslPackage.DD_SPACE__PRIMARY:
        setPrimary(PRIMARY_EDEFAULT);
        return;
      case JclDslPackage.DD_SPACE__SECONDARY:
        setSecondary(SECONDARY_EDEFAULT);
        return;
      case JclDslPackage.DD_SPACE__DIRORINDX:
        setDirorindx(DIRORINDX_EDEFAULT);
        return;
      case JclDslPackage.DD_SPACE__PARM:
        getParm().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SPACE__LENGTH:
        return length != LENGTH_EDEFAULT;
      case JclDslPackage.DD_SPACE__PRIMARY:
        return primary != PRIMARY_EDEFAULT;
      case JclDslPackage.DD_SPACE__SECONDARY:
        return secondary != SECONDARY_EDEFAULT;
      case JclDslPackage.DD_SPACE__DIRORINDX:
        return dirorindx != DIRORINDX_EDEFAULT;
      case JclDslPackage.DD_SPACE__PARM:
        return parm != null && !parm.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (length: ");
    result.append(length);
    result.append(", primary: ");
    result.append(primary);
    result.append(", secondary: ");
    result.append(secondary);
    result.append(", dirorindx: ");
    result.append(dirorindx);
    result.append(", parm: ");
    result.append(parm);
    result.append(')');
    return result.toString();
  }

} //ddSpaceImpl
