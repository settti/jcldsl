/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JCLLIB;
import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>JCLLIB</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.JCLLIBImpl#getJCLLIBNAME <em>JCLLIBNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.JCLLIBImpl#getDSNAMES <em>DSNAMES</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.JCLLIBImpl#getDSNAMES2 <em>DSNAMES2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JCLLIBImpl extends KeywordImpl implements JCLLIB
{
  /**
   * The default value of the '{@link #getJCLLIBNAME() <em>JCLLIBNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJCLLIBNAME()
   * @generated
   * @ordered
   */
  protected static final String JCLLIBNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getJCLLIBNAME() <em>JCLLIBNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJCLLIBNAME()
   * @generated
   * @ordered
   */
  protected String jcllibname = JCLLIBNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDSNAMES() <em>DSNAMES</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDSNAMES()
   * @generated
   * @ordered
   */
  protected static final String DSNAMES_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDSNAMES() <em>DSNAMES</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDSNAMES()
   * @generated
   * @ordered
   */
  protected String dsnames = DSNAMES_EDEFAULT;

  /**
   * The cached value of the '{@link #getDSNAMES2() <em>DSNAMES2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDSNAMES2()
   * @generated
   * @ordered
   */
  protected EList<String> dsnames2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected JCLLIBImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getJCLLIB();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getJCLLIBNAME()
  {
    return jcllibname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setJCLLIBNAME(String newJCLLIBNAME)
  {
    String oldJCLLIBNAME = jcllibname;
    jcllibname = newJCLLIBNAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JCLLIB__JCLLIBNAME, oldJCLLIBNAME, jcllibname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDSNAMES()
  {
    return dsnames;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDSNAMES(String newDSNAMES)
  {
    String oldDSNAMES = dsnames;
    dsnames = newDSNAMES;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JCLLIB__DSNAMES, oldDSNAMES, dsnames));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getDSNAMES2()
  {
    if (dsnames2 == null)
    {
      dsnames2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.JCLLIB__DSNAMES2);
    }
    return dsnames2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JCLLIB__JCLLIBNAME:
        return getJCLLIBNAME();
      case JclDslPackage.JCLLIB__DSNAMES:
        return getDSNAMES();
      case JclDslPackage.JCLLIB__DSNAMES2:
        return getDSNAMES2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JCLLIB__JCLLIBNAME:
        setJCLLIBNAME((String)newValue);
        return;
      case JclDslPackage.JCLLIB__DSNAMES:
        setDSNAMES((String)newValue);
        return;
      case JclDslPackage.JCLLIB__DSNAMES2:
        getDSNAMES2().clear();
        getDSNAMES2().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JCLLIB__JCLLIBNAME:
        setJCLLIBNAME(JCLLIBNAME_EDEFAULT);
        return;
      case JclDslPackage.JCLLIB__DSNAMES:
        setDSNAMES(DSNAMES_EDEFAULT);
        return;
      case JclDslPackage.JCLLIB__DSNAMES2:
        getDSNAMES2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JCLLIB__JCLLIBNAME:
        return JCLLIBNAME_EDEFAULT == null ? jcllibname != null : !JCLLIBNAME_EDEFAULT.equals(jcllibname);
      case JclDslPackage.JCLLIB__DSNAMES:
        return DSNAMES_EDEFAULT == null ? dsnames != null : !DSNAMES_EDEFAULT.equals(dsnames);
      case JclDslPackage.JCLLIB__DSNAMES2:
        return dsnames2 != null && !dsnames2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (JCLLIBNAME: ");
    result.append(jcllibname);
    result.append(", DSNAMES: ");
    result.append(dsnames);
    result.append(", DSNAMES2: ");
    result.append(dsnames2);
    result.append(')');
    return result.toString();
  }

} //JCLLIBImpl
