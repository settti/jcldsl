/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.ENDIF;
import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ENDIF</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ENDIFImpl#getENDIFNAME <em>ENDIFNAME</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ENDIFImpl extends KeywordImpl implements ENDIF
{
  /**
   * The default value of the '{@link #getENDIFNAME() <em>ENDIFNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getENDIFNAME()
   * @generated
   * @ordered
   */
  protected static final String ENDIFNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getENDIFNAME() <em>ENDIFNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getENDIFNAME()
   * @generated
   * @ordered
   */
  protected String endifname = ENDIFNAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ENDIFImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getENDIF();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getENDIFNAME()
  {
    return endifname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setENDIFNAME(String newENDIFNAME)
  {
    String oldENDIFNAME = endifname;
    endifname = newENDIFNAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.ENDIF__ENDIFNAME, oldENDIFNAME, endifname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.ENDIF__ENDIFNAME:
        return getENDIFNAME();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.ENDIF__ENDIFNAME:
        setENDIFNAME((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.ENDIF__ENDIFNAME:
        setENDIFNAME(ENDIFNAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.ENDIF__ENDIFNAME:
        return ENDIFNAME_EDEFAULT == null ? endifname != null : !ENDIFNAME_EDEFAULT.equals(endifname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (ENDIFNAME: ");
    result.append(endifname);
    result.append(')');
    return result.toString();
  }

} //ENDIFImpl
