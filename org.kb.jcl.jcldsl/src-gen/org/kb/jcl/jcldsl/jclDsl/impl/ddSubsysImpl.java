/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddSubsys;
import org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Subsys</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSubsysImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSubsysImpl#getSubsys <em>Subsys</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSubsysImpl#getSubparm <em>Subparm</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddSubsysImpl extends ddsectionImpl implements ddSubsys
{
  /**
   * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabel()
   * @generated
   * @ordered
   */
  protected static final String LABEL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabel()
   * @generated
   * @ordered
   */
  protected String label = LABEL_EDEFAULT;

  /**
   * The default value of the '{@link #getSubsys() <em>Subsys</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubsys()
   * @generated
   * @ordered
   */
  protected static final String SUBSYS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSubsys() <em>Subsys</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubsys()
   * @generated
   * @ordered
   */
  protected String subsys = SUBSYS_EDEFAULT;

  /**
   * The cached value of the '{@link #getSubparm() <em>Subparm</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubparm()
   * @generated
   * @ordered
   */
  protected EList<ddSubsysSubparms> subparm;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddSubsysImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddSubsys();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLabel()
  {
    return label;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLabel(String newLabel)
  {
    String oldLabel = label;
    label = newLabel;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SUBSYS__LABEL, oldLabel, label));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSubsys()
  {
    return subsys;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubsys(String newSubsys)
  {
    String oldSubsys = subsys;
    subsys = newSubsys;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SUBSYS__SUBSYS, oldSubsys, subsys));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ddSubsysSubparms> getSubparm()
  {
    if (subparm == null)
    {
      subparm = new EObjectContainmentEList<ddSubsysSubparms>(ddSubsysSubparms.class, this, JclDslPackage.DD_SUBSYS__SUBPARM);
    }
    return subparm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SUBSYS__SUBPARM:
        return ((InternalEList<?>)getSubparm()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SUBSYS__LABEL:
        return getLabel();
      case JclDslPackage.DD_SUBSYS__SUBSYS:
        return getSubsys();
      case JclDslPackage.DD_SUBSYS__SUBPARM:
        return getSubparm();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SUBSYS__LABEL:
        setLabel((String)newValue);
        return;
      case JclDslPackage.DD_SUBSYS__SUBSYS:
        setSubsys((String)newValue);
        return;
      case JclDslPackage.DD_SUBSYS__SUBPARM:
        getSubparm().clear();
        getSubparm().addAll((Collection<? extends ddSubsysSubparms>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SUBSYS__LABEL:
        setLabel(LABEL_EDEFAULT);
        return;
      case JclDslPackage.DD_SUBSYS__SUBSYS:
        setSubsys(SUBSYS_EDEFAULT);
        return;
      case JclDslPackage.DD_SUBSYS__SUBPARM:
        getSubparm().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SUBSYS__LABEL:
        return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
      case JclDslPackage.DD_SUBSYS__SUBSYS:
        return SUBSYS_EDEFAULT == null ? subsys != null : !SUBSYS_EDEFAULT.equals(subsys);
      case JclDslPackage.DD_SUBSYS__SUBPARM:
        return subparm != null && !subparm.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (label: ");
    result.append(label);
    result.append(", subsys: ");
    result.append(subsys);
    result.append(')');
    return result.toString();
  }

} //ddSubsysImpl
