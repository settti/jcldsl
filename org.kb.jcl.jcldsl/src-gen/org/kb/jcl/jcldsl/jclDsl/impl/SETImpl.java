/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.SET;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SET</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.SETImpl#getSETNAME <em>SETNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.SETImpl#getParmname <em>Parmname</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.SETImpl#getParm_value <em>Parm value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.SETImpl#getParm_valueb <em>Parm valueb</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.SETImpl#getParmname2 <em>Parmname2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.SETImpl#getParm_value2 <em>Parm value2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.SETImpl#getParm_valueb2 <em>Parm valueb2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SETImpl extends KeywordImpl implements SET
{
  /**
   * The default value of the '{@link #getSETNAME() <em>SETNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSETNAME()
   * @generated
   * @ordered
   */
  protected static final String SETNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSETNAME() <em>SETNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSETNAME()
   * @generated
   * @ordered
   */
  protected String setname = SETNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getParmname() <em>Parmname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParmname()
   * @generated
   * @ordered
   */
  protected static final String PARMNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getParmname() <em>Parmname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParmname()
   * @generated
   * @ordered
   */
  protected String parmname = PARMNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getParm_value() <em>Parm value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm_value()
   * @generated
   * @ordered
   */
  protected static final String PARM_VALUE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getParm_value() <em>Parm value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm_value()
   * @generated
   * @ordered
   */
  protected String parm_value = PARM_VALUE_EDEFAULT;

  /**
   * The default value of the '{@link #getParm_valueb() <em>Parm valueb</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm_valueb()
   * @generated
   * @ordered
   */
  protected static final String PARM_VALUEB_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getParm_valueb() <em>Parm valueb</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm_valueb()
   * @generated
   * @ordered
   */
  protected String parm_valueb = PARM_VALUEB_EDEFAULT;

  /**
   * The cached value of the '{@link #getParmname2() <em>Parmname2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParmname2()
   * @generated
   * @ordered
   */
  protected EList<String> parmname2;

  /**
   * The cached value of the '{@link #getParm_value2() <em>Parm value2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm_value2()
   * @generated
   * @ordered
   */
  protected EList<String> parm_value2;

  /**
   * The cached value of the '{@link #getParm_valueb2() <em>Parm valueb2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm_valueb2()
   * @generated
   * @ordered
   */
  protected EList<String> parm_valueb2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SETImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getSET();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSETNAME()
  {
    return setname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSETNAME(String newSETNAME)
  {
    String oldSETNAME = setname;
    setname = newSETNAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.SET__SETNAME, oldSETNAME, setname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getParmname()
  {
    return parmname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParmname(String newParmname)
  {
    String oldParmname = parmname;
    parmname = newParmname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.SET__PARMNAME, oldParmname, parmname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getParm_value()
  {
    return parm_value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParm_value(String newParm_value)
  {
    String oldParm_value = parm_value;
    parm_value = newParm_value;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.SET__PARM_VALUE, oldParm_value, parm_value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getParm_valueb()
  {
    return parm_valueb;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParm_valueb(String newParm_valueb)
  {
    String oldParm_valueb = parm_valueb;
    parm_valueb = newParm_valueb;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.SET__PARM_VALUEB, oldParm_valueb, parm_valueb));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getParmname2()
  {
    if (parmname2 == null)
    {
      parmname2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.SET__PARMNAME2);
    }
    return parmname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getParm_value2()
  {
    if (parm_value2 == null)
    {
      parm_value2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.SET__PARM_VALUE2);
    }
    return parm_value2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getParm_valueb2()
  {
    if (parm_valueb2 == null)
    {
      parm_valueb2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.SET__PARM_VALUEB2);
    }
    return parm_valueb2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.SET__SETNAME:
        return getSETNAME();
      case JclDslPackage.SET__PARMNAME:
        return getParmname();
      case JclDslPackage.SET__PARM_VALUE:
        return getParm_value();
      case JclDslPackage.SET__PARM_VALUEB:
        return getParm_valueb();
      case JclDslPackage.SET__PARMNAME2:
        return getParmname2();
      case JclDslPackage.SET__PARM_VALUE2:
        return getParm_value2();
      case JclDslPackage.SET__PARM_VALUEB2:
        return getParm_valueb2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.SET__SETNAME:
        setSETNAME((String)newValue);
        return;
      case JclDslPackage.SET__PARMNAME:
        setParmname((String)newValue);
        return;
      case JclDslPackage.SET__PARM_VALUE:
        setParm_value((String)newValue);
        return;
      case JclDslPackage.SET__PARM_VALUEB:
        setParm_valueb((String)newValue);
        return;
      case JclDslPackage.SET__PARMNAME2:
        getParmname2().clear();
        getParmname2().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.SET__PARM_VALUE2:
        getParm_value2().clear();
        getParm_value2().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.SET__PARM_VALUEB2:
        getParm_valueb2().clear();
        getParm_valueb2().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.SET__SETNAME:
        setSETNAME(SETNAME_EDEFAULT);
        return;
      case JclDslPackage.SET__PARMNAME:
        setParmname(PARMNAME_EDEFAULT);
        return;
      case JclDslPackage.SET__PARM_VALUE:
        setParm_value(PARM_VALUE_EDEFAULT);
        return;
      case JclDslPackage.SET__PARM_VALUEB:
        setParm_valueb(PARM_VALUEB_EDEFAULT);
        return;
      case JclDslPackage.SET__PARMNAME2:
        getParmname2().clear();
        return;
      case JclDslPackage.SET__PARM_VALUE2:
        getParm_value2().clear();
        return;
      case JclDslPackage.SET__PARM_VALUEB2:
        getParm_valueb2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.SET__SETNAME:
        return SETNAME_EDEFAULT == null ? setname != null : !SETNAME_EDEFAULT.equals(setname);
      case JclDslPackage.SET__PARMNAME:
        return PARMNAME_EDEFAULT == null ? parmname != null : !PARMNAME_EDEFAULT.equals(parmname);
      case JclDslPackage.SET__PARM_VALUE:
        return PARM_VALUE_EDEFAULT == null ? parm_value != null : !PARM_VALUE_EDEFAULT.equals(parm_value);
      case JclDslPackage.SET__PARM_VALUEB:
        return PARM_VALUEB_EDEFAULT == null ? parm_valueb != null : !PARM_VALUEB_EDEFAULT.equals(parm_valueb);
      case JclDslPackage.SET__PARMNAME2:
        return parmname2 != null && !parmname2.isEmpty();
      case JclDslPackage.SET__PARM_VALUE2:
        return parm_value2 != null && !parm_value2.isEmpty();
      case JclDslPackage.SET__PARM_VALUEB2:
        return parm_valueb2 != null && !parm_valueb2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (SETNAME: ");
    result.append(setname);
    result.append(", parmname: ");
    result.append(parmname);
    result.append(", parm_value: ");
    result.append(parm_value);
    result.append(", parm_valueb: ");
    result.append(parm_valueb);
    result.append(", parmname2: ");
    result.append(parmname2);
    result.append(", parm_value2: ");
    result.append(parm_value2);
    result.append(", parm_valueb2: ");
    result.append(parm_valueb2);
    result.append(')');
    return result.toString();
  }

} //SETImpl
