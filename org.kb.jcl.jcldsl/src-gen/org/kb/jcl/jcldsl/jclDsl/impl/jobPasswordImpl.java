/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobPassword;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>job Password</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobPasswordImpl#getPASSWORD <em>PASSWORD</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobPasswordImpl#getNEWPASSWORD <em>NEWPASSWORD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class jobPasswordImpl extends jobsectionImpl implements jobPassword
{
  /**
   * The default value of the '{@link #getPASSWORD() <em>PASSWORD</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPASSWORD()
   * @generated
   * @ordered
   */
  protected static final String PASSWORD_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPASSWORD() <em>PASSWORD</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPASSWORD()
   * @generated
   * @ordered
   */
  protected String password = PASSWORD_EDEFAULT;

  /**
   * The default value of the '{@link #getNEWPASSWORD() <em>NEWPASSWORD</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNEWPASSWORD()
   * @generated
   * @ordered
   */
  protected static final String NEWPASSWORD_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNEWPASSWORD() <em>NEWPASSWORD</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNEWPASSWORD()
   * @generated
   * @ordered
   */
  protected String newpassword = NEWPASSWORD_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobPasswordImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobPassword();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPASSWORD()
  {
    return password;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPASSWORD(String newPASSWORD)
  {
    String oldPASSWORD = password;
    password = newPASSWORD;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_PASSWORD__PASSWORD, oldPASSWORD, password));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getNEWPASSWORD()
  {
    return newpassword;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNEWPASSWORD(String newNEWPASSWORD)
  {
    String oldNEWPASSWORD = newpassword;
    newpassword = newNEWPASSWORD;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_PASSWORD__NEWPASSWORD, oldNEWPASSWORD, newpassword));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_PASSWORD__PASSWORD:
        return getPASSWORD();
      case JclDslPackage.JOB_PASSWORD__NEWPASSWORD:
        return getNEWPASSWORD();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_PASSWORD__PASSWORD:
        setPASSWORD((String)newValue);
        return;
      case JclDslPackage.JOB_PASSWORD__NEWPASSWORD:
        setNEWPASSWORD((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_PASSWORD__PASSWORD:
        setPASSWORD(PASSWORD_EDEFAULT);
        return;
      case JclDslPackage.JOB_PASSWORD__NEWPASSWORD:
        setNEWPASSWORD(NEWPASSWORD_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_PASSWORD__PASSWORD:
        return PASSWORD_EDEFAULT == null ? password != null : !PASSWORD_EDEFAULT.equals(password);
      case JclDslPackage.JOB_PASSWORD__NEWPASSWORD:
        return NEWPASSWORD_EDEFAULT == null ? newpassword != null : !NEWPASSWORD_EDEFAULT.equals(newpassword);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (PASSWORD: ");
    result.append(password);
    result.append(", NEWPASSWORD: ");
    result.append(newpassword);
    result.append(')');
    return result.toString();
  }

} //jobPasswordImpl
