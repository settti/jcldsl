/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.execCond;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>exec Cond</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execCondImpl#getRc <em>Rc</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execCondImpl#getExeccondop <em>Execcondop</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execCondImpl#isStepname <em>Stepname</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execCondImpl#getRc2 <em>Rc2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execCondImpl#isStepname2 <em>Stepname2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execCondImpl#getRc3 <em>Rc3</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execCondImpl#getExeccondop2 <em>Execcondop2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execCondImpl#getStepname3 <em>Stepname3</em>}</li>
 * </ul>
 *
 * @generated
 */
public class execCondImpl extends execsectionImpl implements execCond
{
  /**
   * The default value of the '{@link #getRc() <em>Rc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRc()
   * @generated
   * @ordered
   */
  protected static final int RC_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getRc() <em>Rc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRc()
   * @generated
   * @ordered
   */
  protected int rc = RC_EDEFAULT;

  /**
   * The default value of the '{@link #getExeccondop() <em>Execcondop</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExeccondop()
   * @generated
   * @ordered
   */
  protected static final String EXECCONDOP_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getExeccondop() <em>Execcondop</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExeccondop()
   * @generated
   * @ordered
   */
  protected String execcondop = EXECCONDOP_EDEFAULT;

  /**
   * The default value of the '{@link #isStepname() <em>Stepname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStepname()
   * @generated
   * @ordered
   */
  protected static final boolean STEPNAME_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isStepname() <em>Stepname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStepname()
   * @generated
   * @ordered
   */
  protected boolean stepname = STEPNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getRc2() <em>Rc2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRc2()
   * @generated
   * @ordered
   */
  protected static final int RC2_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getRc2() <em>Rc2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRc2()
   * @generated
   * @ordered
   */
  protected int rc2 = RC2_EDEFAULT;

  /**
   * The default value of the '{@link #isStepname2() <em>Stepname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStepname2()
   * @generated
   * @ordered
   */
  protected static final boolean STEPNAME2_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isStepname2() <em>Stepname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStepname2()
   * @generated
   * @ordered
   */
  protected boolean stepname2 = STEPNAME2_EDEFAULT;

  /**
   * The cached value of the '{@link #getRc3() <em>Rc3</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRc3()
   * @generated
   * @ordered
   */
  protected EList<Integer> rc3;

  /**
   * The cached value of the '{@link #getExeccondop2() <em>Execcondop2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExeccondop2()
   * @generated
   * @ordered
   */
  protected EList<String> execcondop2;

  /**
   * The cached value of the '{@link #getStepname3() <em>Stepname3</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStepname3()
   * @generated
   * @ordered
   */
  protected EList<String> stepname3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected execCondImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getexecCond();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getRc()
  {
    return rc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRc(int newRc)
  {
    int oldRc = rc;
    rc = newRc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_COND__RC, oldRc, rc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getExeccondop()
  {
    return execcondop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExeccondop(String newExeccondop)
  {
    String oldExeccondop = execcondop;
    execcondop = newExeccondop;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_COND__EXECCONDOP, oldExeccondop, execcondop));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isStepname()
  {
    return stepname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStepname(boolean newStepname)
  {
    boolean oldStepname = stepname;
    stepname = newStepname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_COND__STEPNAME, oldStepname, stepname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getRc2()
  {
    return rc2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRc2(int newRc2)
  {
    int oldRc2 = rc2;
    rc2 = newRc2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_COND__RC2, oldRc2, rc2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isStepname2()
  {
    return stepname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStepname2(boolean newStepname2)
  {
    boolean oldStepname2 = stepname2;
    stepname2 = newStepname2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_COND__STEPNAME2, oldStepname2, stepname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Integer> getRc3()
  {
    if (rc3 == null)
    {
      rc3 = new EDataTypeEList<Integer>(Integer.class, this, JclDslPackage.EXEC_COND__RC3);
    }
    return rc3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getExeccondop2()
  {
    if (execcondop2 == null)
    {
      execcondop2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.EXEC_COND__EXECCONDOP2);
    }
    return execcondop2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getStepname3()
  {
    if (stepname3 == null)
    {
      stepname3 = new EDataTypeEList<String>(String.class, this, JclDslPackage.EXEC_COND__STEPNAME3);
    }
    return stepname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_COND__RC:
        return getRc();
      case JclDslPackage.EXEC_COND__EXECCONDOP:
        return getExeccondop();
      case JclDslPackage.EXEC_COND__STEPNAME:
        return isStepname();
      case JclDslPackage.EXEC_COND__RC2:
        return getRc2();
      case JclDslPackage.EXEC_COND__STEPNAME2:
        return isStepname2();
      case JclDslPackage.EXEC_COND__RC3:
        return getRc3();
      case JclDslPackage.EXEC_COND__EXECCONDOP2:
        return getExeccondop2();
      case JclDslPackage.EXEC_COND__STEPNAME3:
        return getStepname3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_COND__RC:
        setRc((Integer)newValue);
        return;
      case JclDslPackage.EXEC_COND__EXECCONDOP:
        setExeccondop((String)newValue);
        return;
      case JclDslPackage.EXEC_COND__STEPNAME:
        setStepname((Boolean)newValue);
        return;
      case JclDslPackage.EXEC_COND__RC2:
        setRc2((Integer)newValue);
        return;
      case JclDslPackage.EXEC_COND__STEPNAME2:
        setStepname2((Boolean)newValue);
        return;
      case JclDslPackage.EXEC_COND__RC3:
        getRc3().clear();
        getRc3().addAll((Collection<? extends Integer>)newValue);
        return;
      case JclDslPackage.EXEC_COND__EXECCONDOP2:
        getExeccondop2().clear();
        getExeccondop2().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.EXEC_COND__STEPNAME3:
        getStepname3().clear();
        getStepname3().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_COND__RC:
        setRc(RC_EDEFAULT);
        return;
      case JclDslPackage.EXEC_COND__EXECCONDOP:
        setExeccondop(EXECCONDOP_EDEFAULT);
        return;
      case JclDslPackage.EXEC_COND__STEPNAME:
        setStepname(STEPNAME_EDEFAULT);
        return;
      case JclDslPackage.EXEC_COND__RC2:
        setRc2(RC2_EDEFAULT);
        return;
      case JclDslPackage.EXEC_COND__STEPNAME2:
        setStepname2(STEPNAME2_EDEFAULT);
        return;
      case JclDslPackage.EXEC_COND__RC3:
        getRc3().clear();
        return;
      case JclDslPackage.EXEC_COND__EXECCONDOP2:
        getExeccondop2().clear();
        return;
      case JclDslPackage.EXEC_COND__STEPNAME3:
        getStepname3().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_COND__RC:
        return rc != RC_EDEFAULT;
      case JclDslPackage.EXEC_COND__EXECCONDOP:
        return EXECCONDOP_EDEFAULT == null ? execcondop != null : !EXECCONDOP_EDEFAULT.equals(execcondop);
      case JclDslPackage.EXEC_COND__STEPNAME:
        return stepname != STEPNAME_EDEFAULT;
      case JclDslPackage.EXEC_COND__RC2:
        return rc2 != RC2_EDEFAULT;
      case JclDslPackage.EXEC_COND__STEPNAME2:
        return stepname2 != STEPNAME2_EDEFAULT;
      case JclDslPackage.EXEC_COND__RC3:
        return rc3 != null && !rc3.isEmpty();
      case JclDslPackage.EXEC_COND__EXECCONDOP2:
        return execcondop2 != null && !execcondop2.isEmpty();
      case JclDslPackage.EXEC_COND__STEPNAME3:
        return stepname3 != null && !stepname3.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (rc: ");
    result.append(rc);
    result.append(", execcondop: ");
    result.append(execcondop);
    result.append(", stepname: ");
    result.append(stepname);
    result.append(", rc2: ");
    result.append(rc2);
    result.append(", stepname2: ");
    result.append(stepname2);
    result.append(", rc3: ");
    result.append(rc3);
    result.append(", execcondop2: ");
    result.append(execcondop2);
    result.append(", stepname3: ");
    result.append(stepname3);
    result.append(')');
    return result.toString();
  }

} //execCondImpl
