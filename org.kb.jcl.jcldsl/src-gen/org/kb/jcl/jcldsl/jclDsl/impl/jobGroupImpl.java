/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobGroup;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>job Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobGroupImpl#getGROUP <em>GROUP</em>}</li>
 * </ul>
 *
 * @generated
 */
public class jobGroupImpl extends jobsectionImpl implements jobGroup
{
  /**
   * The default value of the '{@link #getGROUP() <em>GROUP</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGROUP()
   * @generated
   * @ordered
   */
  protected static final String GROUP_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getGROUP() <em>GROUP</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGROUP()
   * @generated
   * @ordered
   */
  protected String group = GROUP_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobGroupImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobGroup();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getGROUP()
  {
    return group;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGROUP(String newGROUP)
  {
    String oldGROUP = group;
    group = newGROUP;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_GROUP__GROUP, oldGROUP, group));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_GROUP__GROUP:
        return getGROUP();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_GROUP__GROUP:
        setGROUP((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_GROUP__GROUP:
        setGROUP(GROUP_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_GROUP__GROUP:
        return GROUP_EDEFAULT == null ? group != null : !GROUP_EDEFAULT.equals(group);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (GROUP: ");
    result.append(group);
    result.append(')');
    return result.toString();
  }

} //jobGroupImpl
