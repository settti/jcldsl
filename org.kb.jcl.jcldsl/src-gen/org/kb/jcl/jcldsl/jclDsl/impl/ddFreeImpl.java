/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddFree;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Free</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ddFreeImpl extends ddsectionImpl implements ddFree
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddFreeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddFree();
  }

} //ddFreeImpl
