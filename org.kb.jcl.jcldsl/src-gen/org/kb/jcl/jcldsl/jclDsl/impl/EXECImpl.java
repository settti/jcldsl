/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.kb.jcl.jcldsl.jclDsl.DD;
import org.kb.jcl.jcldsl.jclDsl.EXEC;
import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.execsection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EXEC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.EXECImpl#getEXECNAME <em>EXECNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.EXECImpl#getSection <em>Section</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.EXECImpl#getPROCEDURE <em>PROCEDURE</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.EXECImpl#getSections <em>Sections</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.EXECImpl#getDds <em>Dds</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EXECImpl extends KeywordImpl implements EXEC
{
  /**
   * The default value of the '{@link #getEXECNAME() <em>EXECNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEXECNAME()
   * @generated
   * @ordered
   */
  protected static final String EXECNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getEXECNAME() <em>EXECNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEXECNAME()
   * @generated
   * @ordered
   */
  protected String execname = EXECNAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getSection() <em>Section</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSection()
   * @generated
   * @ordered
   */
  protected EList<execsection> section;

  /**
   * The default value of the '{@link #getPROCEDURE() <em>PROCEDURE</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPROCEDURE()
   * @generated
   * @ordered
   */
  protected static final String PROCEDURE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPROCEDURE() <em>PROCEDURE</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPROCEDURE()
   * @generated
   * @ordered
   */
  protected String procedure = PROCEDURE_EDEFAULT;

  /**
   * The cached value of the '{@link #getSections() <em>Sections</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSections()
   * @generated
   * @ordered
   */
  protected EList<execsection> sections;

  /**
   * The cached value of the '{@link #getDds() <em>Dds</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDds()
   * @generated
   * @ordered
   */
  protected EList<DD> dds;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EXECImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getEXEC();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getEXECNAME()
  {
    return execname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEXECNAME(String newEXECNAME)
  {
    String oldEXECNAME = execname;
    execname = newEXECNAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC__EXECNAME, oldEXECNAME, execname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<execsection> getSection()
  {
    if (section == null)
    {
      section = new EObjectContainmentEList<execsection>(execsection.class, this, JclDslPackage.EXEC__SECTION);
    }
    return section;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPROCEDURE()
  {
    return procedure;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPROCEDURE(String newPROCEDURE)
  {
    String oldPROCEDURE = procedure;
    procedure = newPROCEDURE;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC__PROCEDURE, oldPROCEDURE, procedure));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<execsection> getSections()
  {
    if (sections == null)
    {
      sections = new EObjectContainmentEList<execsection>(execsection.class, this, JclDslPackage.EXEC__SECTIONS);
    }
    return sections;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<DD> getDds()
  {
    if (dds == null)
    {
      dds = new EObjectContainmentEList<DD>(DD.class, this, JclDslPackage.EXEC__DDS);
    }
    return dds;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC__SECTION:
        return ((InternalEList<?>)getSection()).basicRemove(otherEnd, msgs);
      case JclDslPackage.EXEC__SECTIONS:
        return ((InternalEList<?>)getSections()).basicRemove(otherEnd, msgs);
      case JclDslPackage.EXEC__DDS:
        return ((InternalEList<?>)getDds()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC__EXECNAME:
        return getEXECNAME();
      case JclDslPackage.EXEC__SECTION:
        return getSection();
      case JclDslPackage.EXEC__PROCEDURE:
        return getPROCEDURE();
      case JclDslPackage.EXEC__SECTIONS:
        return getSections();
      case JclDslPackage.EXEC__DDS:
        return getDds();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC__EXECNAME:
        setEXECNAME((String)newValue);
        return;
      case JclDslPackage.EXEC__SECTION:
        getSection().clear();
        getSection().addAll((Collection<? extends execsection>)newValue);
        return;
      case JclDslPackage.EXEC__PROCEDURE:
        setPROCEDURE((String)newValue);
        return;
      case JclDslPackage.EXEC__SECTIONS:
        getSections().clear();
        getSections().addAll((Collection<? extends execsection>)newValue);
        return;
      case JclDslPackage.EXEC__DDS:
        getDds().clear();
        getDds().addAll((Collection<? extends DD>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC__EXECNAME:
        setEXECNAME(EXECNAME_EDEFAULT);
        return;
      case JclDslPackage.EXEC__SECTION:
        getSection().clear();
        return;
      case JclDslPackage.EXEC__PROCEDURE:
        setPROCEDURE(PROCEDURE_EDEFAULT);
        return;
      case JclDslPackage.EXEC__SECTIONS:
        getSections().clear();
        return;
      case JclDslPackage.EXEC__DDS:
        getDds().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC__EXECNAME:
        return EXECNAME_EDEFAULT == null ? execname != null : !EXECNAME_EDEFAULT.equals(execname);
      case JclDslPackage.EXEC__SECTION:
        return section != null && !section.isEmpty();
      case JclDslPackage.EXEC__PROCEDURE:
        return PROCEDURE_EDEFAULT == null ? procedure != null : !PROCEDURE_EDEFAULT.equals(procedure);
      case JclDslPackage.EXEC__SECTIONS:
        return sections != null && !sections.isEmpty();
      case JclDslPackage.EXEC__DDS:
        return dds != null && !dds.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (EXECNAME: ");
    result.append(execname);
    result.append(", PROCEDURE: ");
    result.append(procedure);
    result.append(')');
    return result.toString();
  }

} //EXECImpl
