/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddDcb;
import org.kb.jcl.jcldsl.jclDsl.ddDcbSubparms;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Dcb</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDcbImpl#getSubparm <em>Subparm</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDcbImpl#getSubparm2 <em>Subparm2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDcbImpl#getDsname <em>Dsname</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddDcbImpl extends ddsectionImpl implements ddDcb
{
  /**
   * The cached value of the '{@link #getSubparm() <em>Subparm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubparm()
   * @generated
   * @ordered
   */
  protected ddDcbSubparms subparm;

  /**
   * The cached value of the '{@link #getSubparm2() <em>Subparm2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubparm2()
   * @generated
   * @ordered
   */
  protected EList<ddDcbSubparms> subparm2;

  /**
   * The default value of the '{@link #getDsname() <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDsname()
   * @generated
   * @ordered
   */
  protected static final String DSNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDsname() <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDsname()
   * @generated
   * @ordered
   */
  protected String dsname = DSNAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddDcbImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddDcb();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDcbSubparms getSubparm()
  {
    return subparm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSubparm(ddDcbSubparms newSubparm, NotificationChain msgs)
  {
    ddDcbSubparms oldSubparm = subparm;
    subparm = newSubparm;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_DCB__SUBPARM, oldSubparm, newSubparm);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubparm(ddDcbSubparms newSubparm)
  {
    if (newSubparm != subparm)
    {
      NotificationChain msgs = null;
      if (subparm != null)
        msgs = ((InternalEObject)subparm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JclDslPackage.DD_DCB__SUBPARM, null, msgs);
      if (newSubparm != null)
        msgs = ((InternalEObject)newSubparm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JclDslPackage.DD_DCB__SUBPARM, null, msgs);
      msgs = basicSetSubparm(newSubparm, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_DCB__SUBPARM, newSubparm, newSubparm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ddDcbSubparms> getSubparm2()
  {
    if (subparm2 == null)
    {
      subparm2 = new EObjectContainmentEList<ddDcbSubparms>(ddDcbSubparms.class, this, JclDslPackage.DD_DCB__SUBPARM2);
    }
    return subparm2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDsname()
  {
    return dsname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDsname(String newDsname)
  {
    String oldDsname = dsname;
    dsname = newDsname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_DCB__DSNAME, oldDsname, dsname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DCB__SUBPARM:
        return basicSetSubparm(null, msgs);
      case JclDslPackage.DD_DCB__SUBPARM2:
        return ((InternalEList<?>)getSubparm2()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DCB__SUBPARM:
        return getSubparm();
      case JclDslPackage.DD_DCB__SUBPARM2:
        return getSubparm2();
      case JclDslPackage.DD_DCB__DSNAME:
        return getDsname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DCB__SUBPARM:
        setSubparm((ddDcbSubparms)newValue);
        return;
      case JclDslPackage.DD_DCB__SUBPARM2:
        getSubparm2().clear();
        getSubparm2().addAll((Collection<? extends ddDcbSubparms>)newValue);
        return;
      case JclDslPackage.DD_DCB__DSNAME:
        setDsname((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DCB__SUBPARM:
        setSubparm((ddDcbSubparms)null);
        return;
      case JclDslPackage.DD_DCB__SUBPARM2:
        getSubparm2().clear();
        return;
      case JclDslPackage.DD_DCB__DSNAME:
        setDsname(DSNAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DCB__SUBPARM:
        return subparm != null;
      case JclDslPackage.DD_DCB__SUBPARM2:
        return subparm2 != null && !subparm2.isEmpty();
      case JclDslPackage.DD_DCB__DSNAME:
        return DSNAME_EDEFAULT == null ? dsname != null : !DSNAME_EDEFAULT.equals(dsname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (dsname: ");
    result.append(dsname);
    result.append(')');
    return result.toString();
  }

} //ddDcbImpl
