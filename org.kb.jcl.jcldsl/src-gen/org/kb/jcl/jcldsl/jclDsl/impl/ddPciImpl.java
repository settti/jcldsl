/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddPci;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Pci</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPciImpl#getParm <em>Parm</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddPciImpl#getParm2 <em>Parm2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddPciImpl extends ddsectionImpl implements ddPci
{
  /**
   * The default value of the '{@link #getParm() <em>Parm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm()
   * @generated
   * @ordered
   */
  protected static final String PARM_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getParm() <em>Parm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm()
   * @generated
   * @ordered
   */
  protected String parm = PARM_EDEFAULT;

  /**
   * The default value of the '{@link #getParm2() <em>Parm2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm2()
   * @generated
   * @ordered
   */
  protected static final String PARM2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getParm2() <em>Parm2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm2()
   * @generated
   * @ordered
   */
  protected String parm2 = PARM2_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddPciImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddPci();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getParm()
  {
    return parm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParm(String newParm)
  {
    String oldParm = parm;
    parm = newParm;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_PCI__PARM, oldParm, parm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getParm2()
  {
    return parm2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParm2(String newParm2)
  {
    String oldParm2 = parm2;
    parm2 = newParm2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_PCI__PARM2, oldParm2, parm2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_PCI__PARM:
        return getParm();
      case JclDslPackage.DD_PCI__PARM2:
        return getParm2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_PCI__PARM:
        setParm((String)newValue);
        return;
      case JclDslPackage.DD_PCI__PARM2:
        setParm2((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_PCI__PARM:
        setParm(PARM_EDEFAULT);
        return;
      case JclDslPackage.DD_PCI__PARM2:
        setParm2(PARM2_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_PCI__PARM:
        return PARM_EDEFAULT == null ? parm != null : !PARM_EDEFAULT.equals(parm);
      case JclDslPackage.DD_PCI__PARM2:
        return PARM2_EDEFAULT == null ? parm2 != null : !PARM2_EDEFAULT.equals(parm2);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (parm: ");
    result.append(parm);
    result.append(", parm2: ");
    result.append(parm2);
    result.append(')');
    return result.toString();
  }

} //ddPciImpl
