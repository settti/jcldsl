/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddChkpt;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Chkpt</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ddChkptImpl extends ddsectionImpl implements ddChkpt
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddChkptImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddChkpt();
  }

} //ddChkptImpl
