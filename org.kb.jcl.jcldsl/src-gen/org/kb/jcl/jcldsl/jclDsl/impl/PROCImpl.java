/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.PROC;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PROC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.PROCImpl#isPROCNAME <em>PROCNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.PROCImpl#getPARMNAME <em>PARMNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.PROCImpl#getParm_value <em>Parm value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.PROCImpl#getDSNAMES <em>DSNAMES</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.PROCImpl#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PROCImpl extends KeywordImpl implements PROC
{
  /**
   * The default value of the '{@link #isPROCNAME() <em>PROCNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isPROCNAME()
   * @generated
   * @ordered
   */
  protected static final boolean PROCNAME_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isPROCNAME() <em>PROCNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isPROCNAME()
   * @generated
   * @ordered
   */
  protected boolean procname = PROCNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getPARMNAME() <em>PARMNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPARMNAME()
   * @generated
   * @ordered
   */
  protected static final String PARMNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPARMNAME() <em>PARMNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPARMNAME()
   * @generated
   * @ordered
   */
  protected String parmname = PARMNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getParm_value() <em>Parm value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm_value()
   * @generated
   * @ordered
   */
  protected static final String PARM_VALUE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getParm_value() <em>Parm value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm_value()
   * @generated
   * @ordered
   */
  protected String parm_value = PARM_VALUE_EDEFAULT;

  /**
   * The cached value of the '{@link #getDSNAMES() <em>DSNAMES</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDSNAMES()
   * @generated
   * @ordered
   */
  protected EList<String> dsnames;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected EList<String> value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PROCImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getPROC();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isPROCNAME()
  {
    return procname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPROCNAME(boolean newPROCNAME)
  {
    boolean oldPROCNAME = procname;
    procname = newPROCNAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.PROC__PROCNAME, oldPROCNAME, procname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPARMNAME()
  {
    return parmname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPARMNAME(String newPARMNAME)
  {
    String oldPARMNAME = parmname;
    parmname = newPARMNAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.PROC__PARMNAME, oldPARMNAME, parmname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getParm_value()
  {
    return parm_value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParm_value(String newParm_value)
  {
    String oldParm_value = parm_value;
    parm_value = newParm_value;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.PROC__PARM_VALUE, oldParm_value, parm_value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getDSNAMES()
  {
    if (dsnames == null)
    {
      dsnames = new EDataTypeEList<String>(String.class, this, JclDslPackage.PROC__DSNAMES);
    }
    return dsnames;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getValue()
  {
    if (value == null)
    {
      value = new EDataTypeEList<String>(String.class, this, JclDslPackage.PROC__VALUE);
    }
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.PROC__PROCNAME:
        return isPROCNAME();
      case JclDslPackage.PROC__PARMNAME:
        return getPARMNAME();
      case JclDslPackage.PROC__PARM_VALUE:
        return getParm_value();
      case JclDslPackage.PROC__DSNAMES:
        return getDSNAMES();
      case JclDslPackage.PROC__VALUE:
        return getValue();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.PROC__PROCNAME:
        setPROCNAME((Boolean)newValue);
        return;
      case JclDslPackage.PROC__PARMNAME:
        setPARMNAME((String)newValue);
        return;
      case JclDslPackage.PROC__PARM_VALUE:
        setParm_value((String)newValue);
        return;
      case JclDslPackage.PROC__DSNAMES:
        getDSNAMES().clear();
        getDSNAMES().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.PROC__VALUE:
        getValue().clear();
        getValue().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.PROC__PROCNAME:
        setPROCNAME(PROCNAME_EDEFAULT);
        return;
      case JclDslPackage.PROC__PARMNAME:
        setPARMNAME(PARMNAME_EDEFAULT);
        return;
      case JclDslPackage.PROC__PARM_VALUE:
        setParm_value(PARM_VALUE_EDEFAULT);
        return;
      case JclDslPackage.PROC__DSNAMES:
        getDSNAMES().clear();
        return;
      case JclDslPackage.PROC__VALUE:
        getValue().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.PROC__PROCNAME:
        return procname != PROCNAME_EDEFAULT;
      case JclDslPackage.PROC__PARMNAME:
        return PARMNAME_EDEFAULT == null ? parmname != null : !PARMNAME_EDEFAULT.equals(parmname);
      case JclDslPackage.PROC__PARM_VALUE:
        return PARM_VALUE_EDEFAULT == null ? parm_value != null : !PARM_VALUE_EDEFAULT.equals(parm_value);
      case JclDslPackage.PROC__DSNAMES:
        return dsnames != null && !dsnames.isEmpty();
      case JclDslPackage.PROC__VALUE:
        return value != null && !value.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (PROCNAME: ");
    result.append(procname);
    result.append(", PARMNAME: ");
    result.append(parmname);
    result.append(", parm_value: ");
    result.append(parm_value);
    result.append(", DSNAMES: ");
    result.append(dsnames);
    result.append(", value: ");
    result.append(value);
    result.append(')');
    return result.toString();
  }

} //PROCImpl
