/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddFiledata;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Filedata</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ddFiledataImpl extends ddsectionImpl implements ddFiledata
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddFiledataImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddFiledata();
  }

} //ddFiledataImpl
