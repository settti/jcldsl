/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.execPgm;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>exec Pgm</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execPgmImpl#getPGMNAME <em>PGMNAME</em>}</li>
 * </ul>
 *
 * @generated
 */
public class execPgmImpl extends execsectionImpl implements execPgm
{
  /**
   * The default value of the '{@link #getPGMNAME() <em>PGMNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPGMNAME()
   * @generated
   * @ordered
   */
  protected static final String PGMNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPGMNAME() <em>PGMNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPGMNAME()
   * @generated
   * @ordered
   */
  protected String pgmname = PGMNAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected execPgmImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getexecPgm();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPGMNAME()
  {
    return pgmname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPGMNAME(String newPGMNAME)
  {
    String oldPGMNAME = pgmname;
    pgmname = newPGMNAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_PGM__PGMNAME, oldPGMNAME, pgmname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_PGM__PGMNAME:
        return getPGMNAME();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_PGM__PGMNAME:
        setPGMNAME((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_PGM__PGMNAME:
        setPGMNAME(PGMNAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_PGM__PGMNAME:
        return PGMNAME_EDEFAULT == null ? pgmname != null : !PGMNAME_EDEFAULT.equals(pgmname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (PGMNAME: ");
    result.append(pgmname);
    result.append(')');
    return result.toString();
  }

} //execPgmImpl
