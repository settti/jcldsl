/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddDisp;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Disp</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDispImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDispImpl#getNormal_disp <em>Normal disp</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDispImpl#getAbnormal_disp <em>Abnormal disp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddDispImpl extends ddsectionImpl implements ddDisp
{
  /**
   * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatus()
   * @generated
   * @ordered
   */
  protected static final String STATUS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatus()
   * @generated
   * @ordered
   */
  protected String status = STATUS_EDEFAULT;

  /**
   * The default value of the '{@link #getNormal_disp() <em>Normal disp</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNormal_disp()
   * @generated
   * @ordered
   */
  protected static final String NORMAL_DISP_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNormal_disp() <em>Normal disp</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNormal_disp()
   * @generated
   * @ordered
   */
  protected String normal_disp = NORMAL_DISP_EDEFAULT;

  /**
   * The default value of the '{@link #getAbnormal_disp() <em>Abnormal disp</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAbnormal_disp()
   * @generated
   * @ordered
   */
  protected static final String ABNORMAL_DISP_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAbnormal_disp() <em>Abnormal disp</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAbnormal_disp()
   * @generated
   * @ordered
   */
  protected String abnormal_disp = ABNORMAL_DISP_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddDispImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddDisp();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getStatus()
  {
    return status;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatus(String newStatus)
  {
    String oldStatus = status;
    status = newStatus;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_DISP__STATUS, oldStatus, status));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getNormal_disp()
  {
    return normal_disp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNormal_disp(String newNormal_disp)
  {
    String oldNormal_disp = normal_disp;
    normal_disp = newNormal_disp;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_DISP__NORMAL_DISP, oldNormal_disp, normal_disp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAbnormal_disp()
  {
    return abnormal_disp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAbnormal_disp(String newAbnormal_disp)
  {
    String oldAbnormal_disp = abnormal_disp;
    abnormal_disp = newAbnormal_disp;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_DISP__ABNORMAL_DISP, oldAbnormal_disp, abnormal_disp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DISP__STATUS:
        return getStatus();
      case JclDslPackage.DD_DISP__NORMAL_DISP:
        return getNormal_disp();
      case JclDslPackage.DD_DISP__ABNORMAL_DISP:
        return getAbnormal_disp();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DISP__STATUS:
        setStatus((String)newValue);
        return;
      case JclDslPackage.DD_DISP__NORMAL_DISP:
        setNormal_disp((String)newValue);
        return;
      case JclDslPackage.DD_DISP__ABNORMAL_DISP:
        setAbnormal_disp((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DISP__STATUS:
        setStatus(STATUS_EDEFAULT);
        return;
      case JclDslPackage.DD_DISP__NORMAL_DISP:
        setNormal_disp(NORMAL_DISP_EDEFAULT);
        return;
      case JclDslPackage.DD_DISP__ABNORMAL_DISP:
        setAbnormal_disp(ABNORMAL_DISP_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DISP__STATUS:
        return STATUS_EDEFAULT == null ? status != null : !STATUS_EDEFAULT.equals(status);
      case JclDslPackage.DD_DISP__NORMAL_DISP:
        return NORMAL_DISP_EDEFAULT == null ? normal_disp != null : !NORMAL_DISP_EDEFAULT.equals(normal_disp);
      case JclDslPackage.DD_DISP__ABNORMAL_DISP:
        return ABNORMAL_DISP_EDEFAULT == null ? abnormal_disp != null : !ABNORMAL_DISP_EDEFAULT.equals(abnormal_disp);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (status: ");
    result.append(status);
    result.append(", normal_disp: ");
    result.append(normal_disp);
    result.append(", abnormal_disp: ");
    result.append(abnormal_disp);
    result.append(')');
    return result.toString();
  }

} //ddDispImpl
