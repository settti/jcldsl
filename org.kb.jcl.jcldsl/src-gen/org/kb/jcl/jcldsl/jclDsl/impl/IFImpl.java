/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.IF;
import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IF</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.IFImpl#isIFNAME <em>IFNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.IFImpl#getSTEPNAME <em>STEPNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.IFImpl#getExpression_keyword <em>Expression keyword</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.IFImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.IFImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.IFImpl#getLogical_op <em>Logical op</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.IFImpl#getExpression_keyword2 <em>Expression keyword2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.IFImpl#getOperator2 <em>Operator2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.IFImpl#getValue2 <em>Value2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IFImpl extends KeywordImpl implements IF
{
  /**
   * The default value of the '{@link #isIFNAME() <em>IFNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isIFNAME()
   * @generated
   * @ordered
   */
  protected static final boolean IFNAME_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isIFNAME() <em>IFNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isIFNAME()
   * @generated
   * @ordered
   */
  protected boolean ifname = IFNAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getSTEPNAME() <em>STEPNAME</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSTEPNAME()
   * @generated
   * @ordered
   */
  protected EList<String> stepname;

  /**
   * The default value of the '{@link #getExpression_keyword() <em>Expression keyword</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpression_keyword()
   * @generated
   * @ordered
   */
  protected static final String EXPRESSION_KEYWORD_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getExpression_keyword() <em>Expression keyword</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpression_keyword()
   * @generated
   * @ordered
   */
  protected String expression_keyword = EXPRESSION_KEYWORD_EDEFAULT;

  /**
   * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOperator()
   * @generated
   * @ordered
   */
  protected static final String OPERATOR_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOperator()
   * @generated
   * @ordered
   */
  protected String operator = OPERATOR_EDEFAULT;

  /**
   * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected static final int VALUE_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected int value = VALUE_EDEFAULT;

  /**
   * The cached value of the '{@link #getLogical_op() <em>Logical op</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLogical_op()
   * @generated
   * @ordered
   */
  protected EList<String> logical_op;

  /**
   * The cached value of the '{@link #getExpression_keyword2() <em>Expression keyword2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpression_keyword2()
   * @generated
   * @ordered
   */
  protected EList<String> expression_keyword2;

  /**
   * The cached value of the '{@link #getOperator2() <em>Operator2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOperator2()
   * @generated
   * @ordered
   */
  protected EList<String> operator2;

  /**
   * The cached value of the '{@link #getValue2() <em>Value2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue2()
   * @generated
   * @ordered
   */
  protected EList<Integer> value2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IFImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getIF();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isIFNAME()
  {
    return ifname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIFNAME(boolean newIFNAME)
  {
    boolean oldIFNAME = ifname;
    ifname = newIFNAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.IF__IFNAME, oldIFNAME, ifname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getSTEPNAME()
  {
    if (stepname == null)
    {
      stepname = new EDataTypeEList<String>(String.class, this, JclDslPackage.IF__STEPNAME);
    }
    return stepname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getExpression_keyword()
  {
    return expression_keyword;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpression_keyword(String newExpression_keyword)
  {
    String oldExpression_keyword = expression_keyword;
    expression_keyword = newExpression_keyword;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.IF__EXPRESSION_KEYWORD, oldExpression_keyword, expression_keyword));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOperator()
  {
    return operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOperator(String newOperator)
  {
    String oldOperator = operator;
    operator = newOperator;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.IF__OPERATOR, oldOperator, operator));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(int newValue)
  {
    int oldValue = value;
    value = newValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.IF__VALUE, oldValue, value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getLogical_op()
  {
    if (logical_op == null)
    {
      logical_op = new EDataTypeEList<String>(String.class, this, JclDslPackage.IF__LOGICAL_OP);
    }
    return logical_op;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getExpression_keyword2()
  {
    if (expression_keyword2 == null)
    {
      expression_keyword2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.IF__EXPRESSION_KEYWORD2);
    }
    return expression_keyword2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getOperator2()
  {
    if (operator2 == null)
    {
      operator2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.IF__OPERATOR2);
    }
    return operator2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Integer> getValue2()
  {
    if (value2 == null)
    {
      value2 = new EDataTypeEList<Integer>(Integer.class, this, JclDslPackage.IF__VALUE2);
    }
    return value2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.IF__IFNAME:
        return isIFNAME();
      case JclDslPackage.IF__STEPNAME:
        return getSTEPNAME();
      case JclDslPackage.IF__EXPRESSION_KEYWORD:
        return getExpression_keyword();
      case JclDslPackage.IF__OPERATOR:
        return getOperator();
      case JclDslPackage.IF__VALUE:
        return getValue();
      case JclDslPackage.IF__LOGICAL_OP:
        return getLogical_op();
      case JclDslPackage.IF__EXPRESSION_KEYWORD2:
        return getExpression_keyword2();
      case JclDslPackage.IF__OPERATOR2:
        return getOperator2();
      case JclDslPackage.IF__VALUE2:
        return getValue2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.IF__IFNAME:
        setIFNAME((Boolean)newValue);
        return;
      case JclDslPackage.IF__STEPNAME:
        getSTEPNAME().clear();
        getSTEPNAME().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.IF__EXPRESSION_KEYWORD:
        setExpression_keyword((String)newValue);
        return;
      case JclDslPackage.IF__OPERATOR:
        setOperator((String)newValue);
        return;
      case JclDslPackage.IF__VALUE:
        setValue((Integer)newValue);
        return;
      case JclDslPackage.IF__LOGICAL_OP:
        getLogical_op().clear();
        getLogical_op().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.IF__EXPRESSION_KEYWORD2:
        getExpression_keyword2().clear();
        getExpression_keyword2().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.IF__OPERATOR2:
        getOperator2().clear();
        getOperator2().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.IF__VALUE2:
        getValue2().clear();
        getValue2().addAll((Collection<? extends Integer>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.IF__IFNAME:
        setIFNAME(IFNAME_EDEFAULT);
        return;
      case JclDslPackage.IF__STEPNAME:
        getSTEPNAME().clear();
        return;
      case JclDslPackage.IF__EXPRESSION_KEYWORD:
        setExpression_keyword(EXPRESSION_KEYWORD_EDEFAULT);
        return;
      case JclDslPackage.IF__OPERATOR:
        setOperator(OPERATOR_EDEFAULT);
        return;
      case JclDslPackage.IF__VALUE:
        setValue(VALUE_EDEFAULT);
        return;
      case JclDslPackage.IF__LOGICAL_OP:
        getLogical_op().clear();
        return;
      case JclDslPackage.IF__EXPRESSION_KEYWORD2:
        getExpression_keyword2().clear();
        return;
      case JclDslPackage.IF__OPERATOR2:
        getOperator2().clear();
        return;
      case JclDslPackage.IF__VALUE2:
        getValue2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.IF__IFNAME:
        return ifname != IFNAME_EDEFAULT;
      case JclDslPackage.IF__STEPNAME:
        return stepname != null && !stepname.isEmpty();
      case JclDslPackage.IF__EXPRESSION_KEYWORD:
        return EXPRESSION_KEYWORD_EDEFAULT == null ? expression_keyword != null : !EXPRESSION_KEYWORD_EDEFAULT.equals(expression_keyword);
      case JclDslPackage.IF__OPERATOR:
        return OPERATOR_EDEFAULT == null ? operator != null : !OPERATOR_EDEFAULT.equals(operator);
      case JclDslPackage.IF__VALUE:
        return value != VALUE_EDEFAULT;
      case JclDslPackage.IF__LOGICAL_OP:
        return logical_op != null && !logical_op.isEmpty();
      case JclDslPackage.IF__EXPRESSION_KEYWORD2:
        return expression_keyword2 != null && !expression_keyword2.isEmpty();
      case JclDslPackage.IF__OPERATOR2:
        return operator2 != null && !operator2.isEmpty();
      case JclDslPackage.IF__VALUE2:
        return value2 != null && !value2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (IFNAME: ");
    result.append(ifname);
    result.append(", STEPNAME: ");
    result.append(stepname);
    result.append(", expression_keyword: ");
    result.append(expression_keyword);
    result.append(", operator: ");
    result.append(operator);
    result.append(", value: ");
    result.append(value);
    result.append(", logical_op: ");
    result.append(logical_op);
    result.append(", expression_keyword2: ");
    result.append(expression_keyword2);
    result.append(", operator2: ");
    result.append(operator2);
    result.append(", value2: ");
    result.append(value2);
    result.append(')');
    return result.toString();
  }

} //IFImpl
