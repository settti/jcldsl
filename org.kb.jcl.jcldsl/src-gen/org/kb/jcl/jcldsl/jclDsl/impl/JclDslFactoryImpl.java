/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.kb.jcl.jcldsl.jclDsl.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JclDslFactoryImpl extends EFactoryImpl implements JclDslFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static JclDslFactory init()
  {
    try
    {
      JclDslFactory theJclDslFactory = (JclDslFactory)EPackage.Registry.INSTANCE.getEFactory(JclDslPackage.eNS_URI);
      if (theJclDslFactory != null)
      {
        return theJclDslFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new JclDslFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JclDslFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case JclDslPackage.MODEL: return createModel();
      case JclDslPackage.KEYWORD: return createKeyword();
      case JclDslPackage.STEPCATDD: return createSTEPCATDD();
      case JclDslPackage.JOBLIBDD: return createJOBLIBDD();
      case JclDslPackage.JOBCATDD: return createJOBCATDD();
      case JclDslPackage.SPECDD_SECTION: return createspecddSection();
      case JclDslPackage.SPECDD_DISP: return createspecddDisp();
      case JclDslPackage.SPECDD_DSNAME: return createspecddDsname();
      case JclDslPackage.IF: return createIF();
      case JclDslPackage.ELSE: return createELSE();
      case JclDslPackage.ENDIF: return createENDIF();
      case JclDslPackage.PROC: return createPROC();
      case JclDslPackage.PEND: return createPEND();
      case JclDslPackage.SET: return createSET();
      case JclDslPackage.JCLLIB: return createJCLLIB();
      case JclDslPackage.EXEC: return createEXEC();
      case JclDslPackage.EXECSECTION: return createexecsection();
      case JclDslPackage.EXEC_TIME: return createexecTime();
      case JclDslPackage.EXEC_TIMEVALUE: return createexecTimevalue();
      case JclDslPackage.EXEC_PERFORM: return createexecPerform();
      case JclDslPackage.EXEC_REGION: return createexecRegion();
      case JclDslPackage.EXEC_RD: return createexecRd();
      case JclDslPackage.EXEC_PARM: return createexecParm();
      case JclDslPackage.EXEC_DYNAMBR: return createexecDynambr();
      case JclDslPackage.EXEC_COND: return createexecCond();
      case JclDslPackage.EXEC_ADDRSPC: return createexecAddrspc();
      case JclDslPackage.EXEC_CCSID: return createexecCcsid();
      case JclDslPackage.EXEC_ACCT: return createexecAcct();
      case JclDslPackage.EXEC_PROC: return createexecProc();
      case JclDslPackage.EXEC_PGM: return createexecPgm();
      case JclDslPackage.INCLUDE: return createINCLUDE();
      case JclDslPackage.JOB: return createJOB();
      case JclDslPackage.JOBSECTION: return createjobsection();
      case JclDslPackage.JOB_USER: return createjobUser();
      case JclDslPackage.JOB_TYPRUN: return createjobTyprun();
      case JclDslPackage.JOB_TIME: return createjobTime();
      case JclDslPackage.JOB_TIMEVALUE: return createjobTimevalue();
      case JclDslPackage.JOB_SCHENV: return createjobSchenv();
      case JclDslPackage.JOB_SECLABEL: return createjobSeclabel();
      case JclDslPackage.JOB_RESTART: return createjobRestart();
      case JclDslPackage.JOB_REGION: return createjobRegion();
      case JclDslPackage.JOB_RD: return createjobRd();
      case JclDslPackage.JOB_PRTY: return createjobPrty();
      case JclDslPackage.JOB_PROGNAME: return createjobProgname();
      case JclDslPackage.JOB_PERFORM: return createjobPerform();
      case JclDslPackage.JOB_PASSWORD: return createjobPassword();
      case JclDslPackage.JOB_PAGES: return createjobPages();
      case JclDslPackage.JOB_NOTIFY: return createjobNotify();
      case JclDslPackage.JOB_MSGLEVEL: return createjobMsglevel();
      case JclDslPackage.JOB_MSG_CLASS: return createjobMsgClass();
      case JclDslPackage.JOB_LINES: return createjobLines();
      case JclDslPackage.JOB_GROUP: return createjobGroup();
      case JclDslPackage.JOB_COND: return createjobCond();
      case JclDslPackage.JOB_CLASS: return createjobClass();
      case JclDslPackage.JOB_CCSID: return createjobCcsid();
      case JclDslPackage.JOB_CARDS: return createjobCards();
      case JclDslPackage.JOB_BYTES: return createjobBytes();
      case JclDslPackage.JOB_ADDRSPC: return createjobAddrspc();
      case JclDslPackage.JOB_ACCINFO: return createjobAccinfo();
      case JclDslPackage.DD: return createDD();
      case JclDslPackage.DDSECTION: return createddsection();
      case JclDslPackage.DD_VOLUME: return createddVolume();
      case JclDslPackage.DD_UNIT: return createddUnit();
      case JclDslPackage.DD_UCS: return createddUcs();
      case JclDslPackage.DD_TERM: return createddTerm();
      case JclDslPackage.DD_SYSOUT: return createddSysout();
      case JclDslPackage.DD_SUBSYS: return createddSubsys();
      case JclDslPackage.DD_SUBSYS_SUBPARMS: return createddSubsysSubparms();
      case JclDslPackage.DD_STORCLAS: return createddStorclas();
      case JclDslPackage.DD_SPIN: return createddSpin();
      case JclDslPackage.DD_SPACE: return createddSpace();
      case JclDslPackage.DD_SEGMENT: return createddSegment();
      case JclDslPackage.DD_SECMODEL: return createddSecmodel();
      case JclDslPackage.DD_RLS: return createddRls();
      case JclDslPackage.DD_RETPD: return createddRetpd();
      case JclDslPackage.DD_REFDD: return createddRefdd();
      case JclDslPackage.DD_RECORG: return createddRecorg();
      case JclDslPackage.DD_RECFM: return createddRecfm();
      case JclDslPackage.DD_QNAME: return createddQname();
      case JclDslPackage.DD_PROTECT: return createddProtect();
      case JclDslPackage.DD_PATHMODE: return createddPathmode();
      case JclDslPackage.DD_PATHOPTS: return createddPathopts();
      case JclDslPackage.DD_PATHDISP: return createddPathdisp();
      case JclDslPackage.DD_PATH: return createddPath();
      case JclDslPackage.DD_OUTPUT: return createddOutput();
      case JclDslPackage.DD_OUTLIM: return createddOutlim();
      case JclDslPackage.DD_MODIFY: return createddModify();
      case JclDslPackage.DD_MGMTCLAS: return createddMgmtclas();
      case JclDslPackage.DD_LRECL: return createddLrecl();
      case JclDslPackage.DD_LIKE: return createddLike();
      case JclDslPackage.DD_LGSTREAM: return createddLgstream();
      case JclDslPackage.DD_LABEL: return createddLabel();
      case JclDslPackage.DD_LABEL_EXPIRE: return createddLabelExpire();
      case JclDslPackage.DD_LABEL_EXPDT: return createddLabelExpdt();
      case JclDslPackage.DD_LABEL_RETPD: return createddLabelRetpd();
      case JclDslPackage.DD_KEYOFF: return createddKeyoff();
      case JclDslPackage.DD_KEYLEN: return createddKeylen();
      case JclDslPackage.DD_HOLD: return createddHold();
      case JclDslPackage.DD_FREE: return createddFree();
      case JclDslPackage.DD_FLASH: return createddFlash();
      case JclDslPackage.DD_FILEDATA: return createddFiledata();
      case JclDslPackage.DD_FCB: return createddFcb();
      case JclDslPackage.DD_EXPDT: return createddExpdt();
      case JclDslPackage.DD_DSORG: return createddDsorg();
      case JclDslPackage.DD_DSNAME: return createddDsname();
      case JclDslPackage.DD_DSNTYPE: return createddDsntype();
      case JclDslPackage.DD_DSID: return createddDsid();
      case JclDslPackage.DD_DLM: return createddDlm();
      case JclDslPackage.DD_DISP: return createddDisp();
      case JclDslPackage.DD_DEST: return createddDest();
      case JclDslPackage.DD_DDNAME: return createddDdname();
      case JclDslPackage.DD_DCB: return createddDcb();
      case JclDslPackage.DD_DCB_SUBPARMS: return createddDcbSubparms();
      case JclDslPackage.DD_TRTCH: return createddTrtch();
      case JclDslPackage.DD_THRESH: return createddThresh();
      case JclDslPackage.DD_STACK: return createddStack();
      case JclDslPackage.DD_RKP: return createddRkp();
      case JclDslPackage.DD_RESERVE: return createddReserve();
      case JclDslPackage.DD_PRTSP: return createddPrtsp();
      case JclDslPackage.DD_PCI: return createddPci();
      case JclDslPackage.DD_OPTCD: return createddOptcd();
      case JclDslPackage.DD_NTM: return createddNtm();
      case JclDslPackage.DD_NCP: return createddNcp();
      case JclDslPackage.DD_MODE: return createddMode();
      case JclDslPackage.DD_LIMCT: return createddLimct();
      case JclDslPackage.DD_IPLTXID: return createddIpltxid();
      case JclDslPackage.DD_INTVL: return createddIntvl();
      case JclDslPackage.DD_GNCP: return createddGncp();
      case JclDslPackage.DD_FUNC: return createddFunc();
      case JclDslPackage.DD_EROPT: return createddEropt();
      case JclDslPackage.DD_DIAGNS: return createddDiagns();
      case JclDslPackage.DD_DEN: return createddDen();
      case JclDslPackage.DD_CYLOFL: return createddCylofl();
      case JclDslPackage.DD_CPRI: return createddCpri();
      case JclDslPackage.DD_BUFSIZE: return createddBufsize();
      case JclDslPackage.DD_BUFOUT: return createddBufout();
      case JclDslPackage.DD_BUFOFF: return createddBufoff();
      case JclDslPackage.DD_BUFNO: return createddBufno();
      case JclDslPackage.DD_BUFMAX: return createddBufmax();
      case JclDslPackage.DD_BUFL: return createddBufl();
      case JclDslPackage.DD_BUFIN: return createddBufin();
      case JclDslPackage.DD_BFTEK: return createddBftek();
      case JclDslPackage.DD_BFALN: return createddBfaln();
      case JclDslPackage.DD_DATACLAS: return createddDataclas();
      case JclDslPackage.DD_DATA: return createddData();
      case JclDslPackage.DD_COPIES: return createddCopies();
      case JclDslPackage.DD_CNTL: return createddCntl();
      case JclDslPackage.DD_CHKPT: return createddChkpt();
      case JclDslPackage.DD_CHARS: return createddChars();
      case JclDslPackage.DD_CCSID: return createddCcsid();
      case JclDslPackage.DD_BURST: return createddBurst();
      case JclDslPackage.DD_BLKSZLIM: return createddBlkszlim();
      case JclDslPackage.DD_BLKSIZE: return createddBlksize();
      case JclDslPackage.DD_AVGREC: return createddAvgrec();
      case JclDslPackage.DD_AMP: return createddAmp();
      case JclDslPackage.DD_ACCODE: return createddAccode();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case JclDslPackage.TYPRUN_TYPE:
        return createTyprun_typeFromString(eDataType, initialValue);
      case JclDslPackage.JOB_RD_VALUES:
        return createjobRdValuesFromString(eDataType, initialValue);
      case JclDslPackage.PAGES_DEF:
        return createpagesDefFromString(eDataType, initialValue);
      case JclDslPackage.DD_RECORG_VALUES:
        return createddRecorgValuesFromString(eDataType, initialValue);
      case JclDslPackage.DD_RECFM_VALUES:
        return createddRecfmValuesFromString(eDataType, initialValue);
      case JclDslPackage.DD_PATHMODE_OPTS:
        return createddPathmodeOptsFromString(eDataType, initialValue);
      case JclDslPackage.DD_PATHOPTS_OPTS:
        return createddPathoptsOptsFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case JclDslPackage.TYPRUN_TYPE:
        return convertTyprun_typeToString(eDataType, instanceValue);
      case JclDslPackage.JOB_RD_VALUES:
        return convertjobRdValuesToString(eDataType, instanceValue);
      case JclDslPackage.PAGES_DEF:
        return convertpagesDefToString(eDataType, instanceValue);
      case JclDslPackage.DD_RECORG_VALUES:
        return convertddRecorgValuesToString(eDataType, instanceValue);
      case JclDslPackage.DD_RECFM_VALUES:
        return convertddRecfmValuesToString(eDataType, instanceValue);
      case JclDslPackage.DD_PATHMODE_OPTS:
        return convertddPathmodeOptsToString(eDataType, instanceValue);
      case JclDslPackage.DD_PATHOPTS_OPTS:
        return convertddPathoptsOptsToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Model createModel()
  {
    ModelImpl model = new ModelImpl();
    return model;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Keyword createKeyword()
  {
    KeywordImpl keyword = new KeywordImpl();
    return keyword;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public STEPCATDD createSTEPCATDD()
  {
    STEPCATDDImpl stepcatdd = new STEPCATDDImpl();
    return stepcatdd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JOBLIBDD createJOBLIBDD()
  {
    JOBLIBDDImpl joblibdd = new JOBLIBDDImpl();
    return joblibdd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JOBCATDD createJOBCATDD()
  {
    JOBCATDDImpl jobcatdd = new JOBCATDDImpl();
    return jobcatdd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public specddSection createspecddSection()
  {
    specddSectionImpl specddSection = new specddSectionImpl();
    return specddSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public specddDisp createspecddDisp()
  {
    specddDispImpl specddDisp = new specddDispImpl();
    return specddDisp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public specddDsname createspecddDsname()
  {
    specddDsnameImpl specddDsname = new specddDsnameImpl();
    return specddDsname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IF createIF()
  {
    IFImpl if_ = new IFImpl();
    return if_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ELSE createELSE()
  {
    ELSEImpl else_ = new ELSEImpl();
    return else_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ENDIF createENDIF()
  {
    ENDIFImpl endif = new ENDIFImpl();
    return endif;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PROC createPROC()
  {
    PROCImpl proc = new PROCImpl();
    return proc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PEND createPEND()
  {
    PENDImpl pend = new PENDImpl();
    return pend;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SET createSET()
  {
    SETImpl set = new SETImpl();
    return set;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JCLLIB createJCLLIB()
  {
    JCLLIBImpl jcllib = new JCLLIBImpl();
    return jcllib;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EXEC createEXEC()
  {
    EXECImpl exec = new EXECImpl();
    return exec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execsection createexecsection()
  {
    execsectionImpl execsection = new execsectionImpl();
    return execsection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execTime createexecTime()
  {
    execTimeImpl execTime = new execTimeImpl();
    return execTime;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execTimevalue createexecTimevalue()
  {
    execTimevalueImpl execTimevalue = new execTimevalueImpl();
    return execTimevalue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execPerform createexecPerform()
  {
    execPerformImpl execPerform = new execPerformImpl();
    return execPerform;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execRegion createexecRegion()
  {
    execRegionImpl execRegion = new execRegionImpl();
    return execRegion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execRd createexecRd()
  {
    execRdImpl execRd = new execRdImpl();
    return execRd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execParm createexecParm()
  {
    execParmImpl execParm = new execParmImpl();
    return execParm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execDynambr createexecDynambr()
  {
    execDynambrImpl execDynambr = new execDynambrImpl();
    return execDynambr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execCond createexecCond()
  {
    execCondImpl execCond = new execCondImpl();
    return execCond;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execAddrspc createexecAddrspc()
  {
    execAddrspcImpl execAddrspc = new execAddrspcImpl();
    return execAddrspc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execCcsid createexecCcsid()
  {
    execCcsidImpl execCcsid = new execCcsidImpl();
    return execCcsid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execAcct createexecAcct()
  {
    execAcctImpl execAcct = new execAcctImpl();
    return execAcct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execProc createexecProc()
  {
    execProcImpl execProc = new execProcImpl();
    return execProc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execPgm createexecPgm()
  {
    execPgmImpl execPgm = new execPgmImpl();
    return execPgm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public INCLUDE createINCLUDE()
  {
    INCLUDEImpl include = new INCLUDEImpl();
    return include;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JOB createJOB()
  {
    JOBImpl job = new JOBImpl();
    return job;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobsection createjobsection()
  {
    jobsectionImpl jobsection = new jobsectionImpl();
    return jobsection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobUser createjobUser()
  {
    jobUserImpl jobUser = new jobUserImpl();
    return jobUser;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobTyprun createjobTyprun()
  {
    jobTyprunImpl jobTyprun = new jobTyprunImpl();
    return jobTyprun;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobTime createjobTime()
  {
    jobTimeImpl jobTime = new jobTimeImpl();
    return jobTime;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobTimevalue createjobTimevalue()
  {
    jobTimevalueImpl jobTimevalue = new jobTimevalueImpl();
    return jobTimevalue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobSchenv createjobSchenv()
  {
    jobSchenvImpl jobSchenv = new jobSchenvImpl();
    return jobSchenv;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobSeclabel createjobSeclabel()
  {
    jobSeclabelImpl jobSeclabel = new jobSeclabelImpl();
    return jobSeclabel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobRestart createjobRestart()
  {
    jobRestartImpl jobRestart = new jobRestartImpl();
    return jobRestart;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobRegion createjobRegion()
  {
    jobRegionImpl jobRegion = new jobRegionImpl();
    return jobRegion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobRd createjobRd()
  {
    jobRdImpl jobRd = new jobRdImpl();
    return jobRd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobPrty createjobPrty()
  {
    jobPrtyImpl jobPrty = new jobPrtyImpl();
    return jobPrty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobProgname createjobProgname()
  {
    jobPrognameImpl jobProgname = new jobPrognameImpl();
    return jobProgname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobPerform createjobPerform()
  {
    jobPerformImpl jobPerform = new jobPerformImpl();
    return jobPerform;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobPassword createjobPassword()
  {
    jobPasswordImpl jobPassword = new jobPasswordImpl();
    return jobPassword;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobPages createjobPages()
  {
    jobPagesImpl jobPages = new jobPagesImpl();
    return jobPages;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobNotify createjobNotify()
  {
    jobNotifyImpl jobNotify = new jobNotifyImpl();
    return jobNotify;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobMsglevel createjobMsglevel()
  {
    jobMsglevelImpl jobMsglevel = new jobMsglevelImpl();
    return jobMsglevel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobMsgClass createjobMsgClass()
  {
    jobMsgClassImpl jobMsgClass = new jobMsgClassImpl();
    return jobMsgClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobLines createjobLines()
  {
    jobLinesImpl jobLines = new jobLinesImpl();
    return jobLines;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobGroup createjobGroup()
  {
    jobGroupImpl jobGroup = new jobGroupImpl();
    return jobGroup;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobCond createjobCond()
  {
    jobCondImpl jobCond = new jobCondImpl();
    return jobCond;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobClass createjobClass()
  {
    jobClassImpl jobClass = new jobClassImpl();
    return jobClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobCcsid createjobCcsid()
  {
    jobCcsidImpl jobCcsid = new jobCcsidImpl();
    return jobCcsid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobCards createjobCards()
  {
    jobCardsImpl jobCards = new jobCardsImpl();
    return jobCards;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobBytes createjobBytes()
  {
    jobBytesImpl jobBytes = new jobBytesImpl();
    return jobBytes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobAddrspc createjobAddrspc()
  {
    jobAddrspcImpl jobAddrspc = new jobAddrspcImpl();
    return jobAddrspc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobAccinfo createjobAccinfo()
  {
    jobAccinfoImpl jobAccinfo = new jobAccinfoImpl();
    return jobAccinfo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DD createDD()
  {
    DDImpl dd = new DDImpl();
    return dd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddsection createddsection()
  {
    ddsectionImpl ddsection = new ddsectionImpl();
    return ddsection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddVolume createddVolume()
  {
    ddVolumeImpl ddVolume = new ddVolumeImpl();
    return ddVolume;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddUnit createddUnit()
  {
    ddUnitImpl ddUnit = new ddUnitImpl();
    return ddUnit;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddUcs createddUcs()
  {
    ddUcsImpl ddUcs = new ddUcsImpl();
    return ddUcs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddTerm createddTerm()
  {
    ddTermImpl ddTerm = new ddTermImpl();
    return ddTerm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddSysout createddSysout()
  {
    ddSysoutImpl ddSysout = new ddSysoutImpl();
    return ddSysout;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddSubsys createddSubsys()
  {
    ddSubsysImpl ddSubsys = new ddSubsysImpl();
    return ddSubsys;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddSubsysSubparms createddSubsysSubparms()
  {
    ddSubsysSubparmsImpl ddSubsysSubparms = new ddSubsysSubparmsImpl();
    return ddSubsysSubparms;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddStorclas createddStorclas()
  {
    ddStorclasImpl ddStorclas = new ddStorclasImpl();
    return ddStorclas;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddSpin createddSpin()
  {
    ddSpinImpl ddSpin = new ddSpinImpl();
    return ddSpin;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddSpace createddSpace()
  {
    ddSpaceImpl ddSpace = new ddSpaceImpl();
    return ddSpace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddSegment createddSegment()
  {
    ddSegmentImpl ddSegment = new ddSegmentImpl();
    return ddSegment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddSecmodel createddSecmodel()
  {
    ddSecmodelImpl ddSecmodel = new ddSecmodelImpl();
    return ddSecmodel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddRls createddRls()
  {
    ddRlsImpl ddRls = new ddRlsImpl();
    return ddRls;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddRetpd createddRetpd()
  {
    ddRetpdImpl ddRetpd = new ddRetpdImpl();
    return ddRetpd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddRefdd createddRefdd()
  {
    ddRefddImpl ddRefdd = new ddRefddImpl();
    return ddRefdd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddRecorg createddRecorg()
  {
    ddRecorgImpl ddRecorg = new ddRecorgImpl();
    return ddRecorg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddRecfm createddRecfm()
  {
    ddRecfmImpl ddRecfm = new ddRecfmImpl();
    return ddRecfm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddQname createddQname()
  {
    ddQnameImpl ddQname = new ddQnameImpl();
    return ddQname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddProtect createddProtect()
  {
    ddProtectImpl ddProtect = new ddProtectImpl();
    return ddProtect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddPathmode createddPathmode()
  {
    ddPathmodeImpl ddPathmode = new ddPathmodeImpl();
    return ddPathmode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddPathopts createddPathopts()
  {
    ddPathoptsImpl ddPathopts = new ddPathoptsImpl();
    return ddPathopts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddPathdisp createddPathdisp()
  {
    ddPathdispImpl ddPathdisp = new ddPathdispImpl();
    return ddPathdisp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddPath createddPath()
  {
    ddPathImpl ddPath = new ddPathImpl();
    return ddPath;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddOutput createddOutput()
  {
    ddOutputImpl ddOutput = new ddOutputImpl();
    return ddOutput;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddOutlim createddOutlim()
  {
    ddOutlimImpl ddOutlim = new ddOutlimImpl();
    return ddOutlim;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddModify createddModify()
  {
    ddModifyImpl ddModify = new ddModifyImpl();
    return ddModify;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddMgmtclas createddMgmtclas()
  {
    ddMgmtclasImpl ddMgmtclas = new ddMgmtclasImpl();
    return ddMgmtclas;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddLrecl createddLrecl()
  {
    ddLreclImpl ddLrecl = new ddLreclImpl();
    return ddLrecl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddLike createddLike()
  {
    ddLikeImpl ddLike = new ddLikeImpl();
    return ddLike;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddLgstream createddLgstream()
  {
    ddLgstreamImpl ddLgstream = new ddLgstreamImpl();
    return ddLgstream;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddLabel createddLabel()
  {
    ddLabelImpl ddLabel = new ddLabelImpl();
    return ddLabel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddLabelExpire createddLabelExpire()
  {
    ddLabelExpireImpl ddLabelExpire = new ddLabelExpireImpl();
    return ddLabelExpire;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddLabelExpdt createddLabelExpdt()
  {
    ddLabelExpdtImpl ddLabelExpdt = new ddLabelExpdtImpl();
    return ddLabelExpdt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddLabelRetpd createddLabelRetpd()
  {
    ddLabelRetpdImpl ddLabelRetpd = new ddLabelRetpdImpl();
    return ddLabelRetpd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddKeyoff createddKeyoff()
  {
    ddKeyoffImpl ddKeyoff = new ddKeyoffImpl();
    return ddKeyoff;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddKeylen createddKeylen()
  {
    ddKeylenImpl ddKeylen = new ddKeylenImpl();
    return ddKeylen;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddHold createddHold()
  {
    ddHoldImpl ddHold = new ddHoldImpl();
    return ddHold;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddFree createddFree()
  {
    ddFreeImpl ddFree = new ddFreeImpl();
    return ddFree;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddFlash createddFlash()
  {
    ddFlashImpl ddFlash = new ddFlashImpl();
    return ddFlash;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddFiledata createddFiledata()
  {
    ddFiledataImpl ddFiledata = new ddFiledataImpl();
    return ddFiledata;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddFcb createddFcb()
  {
    ddFcbImpl ddFcb = new ddFcbImpl();
    return ddFcb;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddExpdt createddExpdt()
  {
    ddExpdtImpl ddExpdt = new ddExpdtImpl();
    return ddExpdt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDsorg createddDsorg()
  {
    ddDsorgImpl ddDsorg = new ddDsorgImpl();
    return ddDsorg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDsname createddDsname()
  {
    ddDsnameImpl ddDsname = new ddDsnameImpl();
    return ddDsname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDsntype createddDsntype()
  {
    ddDsntypeImpl ddDsntype = new ddDsntypeImpl();
    return ddDsntype;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDsid createddDsid()
  {
    ddDsidImpl ddDsid = new ddDsidImpl();
    return ddDsid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDlm createddDlm()
  {
    ddDlmImpl ddDlm = new ddDlmImpl();
    return ddDlm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDisp createddDisp()
  {
    ddDispImpl ddDisp = new ddDispImpl();
    return ddDisp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDest createddDest()
  {
    ddDestImpl ddDest = new ddDestImpl();
    return ddDest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDdname createddDdname()
  {
    ddDdnameImpl ddDdname = new ddDdnameImpl();
    return ddDdname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDcb createddDcb()
  {
    ddDcbImpl ddDcb = new ddDcbImpl();
    return ddDcb;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDcbSubparms createddDcbSubparms()
  {
    ddDcbSubparmsImpl ddDcbSubparms = new ddDcbSubparmsImpl();
    return ddDcbSubparms;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddTrtch createddTrtch()
  {
    ddTrtchImpl ddTrtch = new ddTrtchImpl();
    return ddTrtch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddThresh createddThresh()
  {
    ddThreshImpl ddThresh = new ddThreshImpl();
    return ddThresh;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddStack createddStack()
  {
    ddStackImpl ddStack = new ddStackImpl();
    return ddStack;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddRkp createddRkp()
  {
    ddRkpImpl ddRkp = new ddRkpImpl();
    return ddRkp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddReserve createddReserve()
  {
    ddReserveImpl ddReserve = new ddReserveImpl();
    return ddReserve;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddPrtsp createddPrtsp()
  {
    ddPrtspImpl ddPrtsp = new ddPrtspImpl();
    return ddPrtsp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddPci createddPci()
  {
    ddPciImpl ddPci = new ddPciImpl();
    return ddPci;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddOptcd createddOptcd()
  {
    ddOptcdImpl ddOptcd = new ddOptcdImpl();
    return ddOptcd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddNtm createddNtm()
  {
    ddNtmImpl ddNtm = new ddNtmImpl();
    return ddNtm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddNcp createddNcp()
  {
    ddNcpImpl ddNcp = new ddNcpImpl();
    return ddNcp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddMode createddMode()
  {
    ddModeImpl ddMode = new ddModeImpl();
    return ddMode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddLimct createddLimct()
  {
    ddLimctImpl ddLimct = new ddLimctImpl();
    return ddLimct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddIpltxid createddIpltxid()
  {
    ddIpltxidImpl ddIpltxid = new ddIpltxidImpl();
    return ddIpltxid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddIntvl createddIntvl()
  {
    ddIntvlImpl ddIntvl = new ddIntvlImpl();
    return ddIntvl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddGncp createddGncp()
  {
    ddGncpImpl ddGncp = new ddGncpImpl();
    return ddGncp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddFunc createddFunc()
  {
    ddFuncImpl ddFunc = new ddFuncImpl();
    return ddFunc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddEropt createddEropt()
  {
    ddEroptImpl ddEropt = new ddEroptImpl();
    return ddEropt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDiagns createddDiagns()
  {
    ddDiagnsImpl ddDiagns = new ddDiagnsImpl();
    return ddDiagns;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDen createddDen()
  {
    ddDenImpl ddDen = new ddDenImpl();
    return ddDen;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddCylofl createddCylofl()
  {
    ddCyloflImpl ddCylofl = new ddCyloflImpl();
    return ddCylofl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddCpri createddCpri()
  {
    ddCpriImpl ddCpri = new ddCpriImpl();
    return ddCpri;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBufsize createddBufsize()
  {
    ddBufsizeImpl ddBufsize = new ddBufsizeImpl();
    return ddBufsize;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBufout createddBufout()
  {
    ddBufoutImpl ddBufout = new ddBufoutImpl();
    return ddBufout;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBufoff createddBufoff()
  {
    ddBufoffImpl ddBufoff = new ddBufoffImpl();
    return ddBufoff;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBufno createddBufno()
  {
    ddBufnoImpl ddBufno = new ddBufnoImpl();
    return ddBufno;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBufmax createddBufmax()
  {
    ddBufmaxImpl ddBufmax = new ddBufmaxImpl();
    return ddBufmax;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBufl createddBufl()
  {
    ddBuflImpl ddBufl = new ddBuflImpl();
    return ddBufl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBufin createddBufin()
  {
    ddBufinImpl ddBufin = new ddBufinImpl();
    return ddBufin;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBftek createddBftek()
  {
    ddBftekImpl ddBftek = new ddBftekImpl();
    return ddBftek;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBfaln createddBfaln()
  {
    ddBfalnImpl ddBfaln = new ddBfalnImpl();
    return ddBfaln;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddDataclas createddDataclas()
  {
    ddDataclasImpl ddDataclas = new ddDataclasImpl();
    return ddDataclas;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddData createddData()
  {
    ddDataImpl ddData = new ddDataImpl();
    return ddData;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddCopies createddCopies()
  {
    ddCopiesImpl ddCopies = new ddCopiesImpl();
    return ddCopies;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddCntl createddCntl()
  {
    ddCntlImpl ddCntl = new ddCntlImpl();
    return ddCntl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddChkpt createddChkpt()
  {
    ddChkptImpl ddChkpt = new ddChkptImpl();
    return ddChkpt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddChars createddChars()
  {
    ddCharsImpl ddChars = new ddCharsImpl();
    return ddChars;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddCcsid createddCcsid()
  {
    ddCcsidImpl ddCcsid = new ddCcsidImpl();
    return ddCcsid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBurst createddBurst()
  {
    ddBurstImpl ddBurst = new ddBurstImpl();
    return ddBurst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBlkszlim createddBlkszlim()
  {
    ddBlkszlimImpl ddBlkszlim = new ddBlkszlimImpl();
    return ddBlkszlim;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddBlksize createddBlksize()
  {
    ddBlksizeImpl ddBlksize = new ddBlksizeImpl();
    return ddBlksize;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddAvgrec createddAvgrec()
  {
    ddAvgrecImpl ddAvgrec = new ddAvgrecImpl();
    return ddAvgrec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddAmp createddAmp()
  {
    ddAmpImpl ddAmp = new ddAmpImpl();
    return ddAmp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddAccode createddAccode()
  {
    ddAccodeImpl ddAccode = new ddAccodeImpl();
    return ddAccode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Typrun_type createTyprun_typeFromString(EDataType eDataType, String initialValue)
  {
    Typrun_type result = Typrun_type.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTyprun_typeToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public jobRdValues createjobRdValuesFromString(EDataType eDataType, String initialValue)
  {
    jobRdValues result = jobRdValues.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertjobRdValuesToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public pagesDef createpagesDefFromString(EDataType eDataType, String initialValue)
  {
    pagesDef result = pagesDef.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertpagesDefToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddRecorgValues createddRecorgValuesFromString(EDataType eDataType, String initialValue)
  {
    ddRecorgValues result = ddRecorgValues.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertddRecorgValuesToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddRecfmValues createddRecfmValuesFromString(EDataType eDataType, String initialValue)
  {
    ddRecfmValues result = ddRecfmValues.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertddRecfmValuesToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddPathmodeOpts createddPathmodeOptsFromString(EDataType eDataType, String initialValue)
  {
    ddPathmodeOpts result = ddPathmodeOpts.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertddPathmodeOptsToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ddPathoptsOpts createddPathoptsOptsFromString(EDataType eDataType, String initialValue)
  {
    ddPathoptsOpts result = ddPathoptsOpts.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertddPathoptsOptsToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JclDslPackage getJclDslPackage()
  {
    return (JclDslPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static JclDslPackage getPackage()
  {
    return JclDslPackage.eINSTANCE;
  }

} //JclDslFactoryImpl
