/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Subsys Subparms</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSubsysSubparmsImpl#getSubparm <em>Subparm</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSubsysSubparmsImpl#getSubparm3 <em>Subparm3</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddSubsysSubparmsImpl extends MinimalEObjectImpl.Container implements ddSubsysSubparms
{
  /**
   * The default value of the '{@link #getSubparm() <em>Subparm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubparm()
   * @generated
   * @ordered
   */
  protected static final String SUBPARM_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSubparm() <em>Subparm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubparm()
   * @generated
   * @ordered
   */
  protected String subparm = SUBPARM_EDEFAULT;

  /**
   * The default value of the '{@link #getSubparm3() <em>Subparm3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubparm3()
   * @generated
   * @ordered
   */
  protected static final String SUBPARM3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSubparm3() <em>Subparm3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubparm3()
   * @generated
   * @ordered
   */
  protected String subparm3 = SUBPARM3_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddSubsysSubparmsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddSubsysSubparms();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSubparm()
  {
    return subparm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubparm(String newSubparm)
  {
    String oldSubparm = subparm;
    subparm = newSubparm;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SUBSYS_SUBPARMS__SUBPARM, oldSubparm, subparm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSubparm3()
  {
    return subparm3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubparm3(String newSubparm3)
  {
    String oldSubparm3 = subparm3;
    subparm3 = newSubparm3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SUBSYS_SUBPARMS__SUBPARM3, oldSubparm3, subparm3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SUBSYS_SUBPARMS__SUBPARM:
        return getSubparm();
      case JclDslPackage.DD_SUBSYS_SUBPARMS__SUBPARM3:
        return getSubparm3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SUBSYS_SUBPARMS__SUBPARM:
        setSubparm((String)newValue);
        return;
      case JclDslPackage.DD_SUBSYS_SUBPARMS__SUBPARM3:
        setSubparm3((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SUBSYS_SUBPARMS__SUBPARM:
        setSubparm(SUBPARM_EDEFAULT);
        return;
      case JclDslPackage.DD_SUBSYS_SUBPARMS__SUBPARM3:
        setSubparm3(SUBPARM3_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SUBSYS_SUBPARMS__SUBPARM:
        return SUBPARM_EDEFAULT == null ? subparm != null : !SUBPARM_EDEFAULT.equals(subparm);
      case JclDslPackage.DD_SUBSYS_SUBPARMS__SUBPARM3:
        return SUBPARM3_EDEFAULT == null ? subparm3 != null : !SUBPARM3_EDEFAULT.equals(subparm3);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (subparm: ");
    result.append(subparm);
    result.append(", subparm3: ");
    result.append(subparm3);
    result.append(')');
    return result.toString();
  }

} //ddSubsysSubparmsImpl
