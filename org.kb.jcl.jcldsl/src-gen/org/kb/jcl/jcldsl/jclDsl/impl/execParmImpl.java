/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.execParm;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>exec Parm</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execParmImpl#getParm <em>Parm</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execParmImpl#getParmkeyw <em>Parmkeyw</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execParmImpl#getParm2 <em>Parm2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execParmImpl#getParmkeyw2 <em>Parmkeyw2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.execParmImpl#getParmint <em>Parmint</em>}</li>
 * </ul>
 *
 * @generated
 */
public class execParmImpl extends execsectionImpl implements execParm
{
  /**
   * The default value of the '{@link #getParm() <em>Parm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm()
   * @generated
   * @ordered
   */
  protected static final String PARM_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getParm() <em>Parm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm()
   * @generated
   * @ordered
   */
  protected String parm = PARM_EDEFAULT;

  /**
   * The default value of the '{@link #getParmkeyw() <em>Parmkeyw</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParmkeyw()
   * @generated
   * @ordered
   */
  protected static final String PARMKEYW_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getParmkeyw() <em>Parmkeyw</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParmkeyw()
   * @generated
   * @ordered
   */
  protected String parmkeyw = PARMKEYW_EDEFAULT;

  /**
   * The cached value of the '{@link #getParm2() <em>Parm2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParm2()
   * @generated
   * @ordered
   */
  protected EList<String> parm2;

  /**
   * The cached value of the '{@link #getParmkeyw2() <em>Parmkeyw2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParmkeyw2()
   * @generated
   * @ordered
   */
  protected EList<String> parmkeyw2;

  /**
   * The default value of the '{@link #getParmint() <em>Parmint</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParmint()
   * @generated
   * @ordered
   */
  protected static final int PARMINT_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getParmint() <em>Parmint</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParmint()
   * @generated
   * @ordered
   */
  protected int parmint = PARMINT_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected execParmImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getexecParm();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getParm()
  {
    return parm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParm(String newParm)
  {
    String oldParm = parm;
    parm = newParm;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_PARM__PARM, oldParm, parm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getParmkeyw()
  {
    return parmkeyw;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParmkeyw(String newParmkeyw)
  {
    String oldParmkeyw = parmkeyw;
    parmkeyw = newParmkeyw;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_PARM__PARMKEYW, oldParmkeyw, parmkeyw));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getParm2()
  {
    if (parm2 == null)
    {
      parm2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.EXEC_PARM__PARM2);
    }
    return parm2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getParmkeyw2()
  {
    if (parmkeyw2 == null)
    {
      parmkeyw2 = new EDataTypeEList<String>(String.class, this, JclDslPackage.EXEC_PARM__PARMKEYW2);
    }
    return parmkeyw2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getParmint()
  {
    return parmint;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParmint(int newParmint)
  {
    int oldParmint = parmint;
    parmint = newParmint;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.EXEC_PARM__PARMINT, oldParmint, parmint));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_PARM__PARM:
        return getParm();
      case JclDslPackage.EXEC_PARM__PARMKEYW:
        return getParmkeyw();
      case JclDslPackage.EXEC_PARM__PARM2:
        return getParm2();
      case JclDslPackage.EXEC_PARM__PARMKEYW2:
        return getParmkeyw2();
      case JclDslPackage.EXEC_PARM__PARMINT:
        return getParmint();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_PARM__PARM:
        setParm((String)newValue);
        return;
      case JclDslPackage.EXEC_PARM__PARMKEYW:
        setParmkeyw((String)newValue);
        return;
      case JclDslPackage.EXEC_PARM__PARM2:
        getParm2().clear();
        getParm2().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.EXEC_PARM__PARMKEYW2:
        getParmkeyw2().clear();
        getParmkeyw2().addAll((Collection<? extends String>)newValue);
        return;
      case JclDslPackage.EXEC_PARM__PARMINT:
        setParmint((Integer)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_PARM__PARM:
        setParm(PARM_EDEFAULT);
        return;
      case JclDslPackage.EXEC_PARM__PARMKEYW:
        setParmkeyw(PARMKEYW_EDEFAULT);
        return;
      case JclDslPackage.EXEC_PARM__PARM2:
        getParm2().clear();
        return;
      case JclDslPackage.EXEC_PARM__PARMKEYW2:
        getParmkeyw2().clear();
        return;
      case JclDslPackage.EXEC_PARM__PARMINT:
        setParmint(PARMINT_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.EXEC_PARM__PARM:
        return PARM_EDEFAULT == null ? parm != null : !PARM_EDEFAULT.equals(parm);
      case JclDslPackage.EXEC_PARM__PARMKEYW:
        return PARMKEYW_EDEFAULT == null ? parmkeyw != null : !PARMKEYW_EDEFAULT.equals(parmkeyw);
      case JclDslPackage.EXEC_PARM__PARM2:
        return parm2 != null && !parm2.isEmpty();
      case JclDslPackage.EXEC_PARM__PARMKEYW2:
        return parmkeyw2 != null && !parmkeyw2.isEmpty();
      case JclDslPackage.EXEC_PARM__PARMINT:
        return parmint != PARMINT_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (parm: ");
    result.append(parm);
    result.append(", parmkeyw: ");
    result.append(parmkeyw);
    result.append(", parm2: ");
    result.append(parm2);
    result.append(", parmkeyw2: ");
    result.append(parmkeyw2);
    result.append(", parmint: ");
    result.append(parmint);
    result.append(')');
    return result.toString();
  }

} //execParmImpl
