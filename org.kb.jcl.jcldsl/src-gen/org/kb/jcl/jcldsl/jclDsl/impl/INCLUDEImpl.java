/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.INCLUDE;
import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>INCLUDE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.INCLUDEImpl#getINCLUDENAME <em>INCLUDENAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.INCLUDEImpl#getMEMBERNAME <em>MEMBERNAME</em>}</li>
 * </ul>
 *
 * @generated
 */
public class INCLUDEImpl extends KeywordImpl implements INCLUDE
{
  /**
   * The default value of the '{@link #getINCLUDENAME() <em>INCLUDENAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getINCLUDENAME()
   * @generated
   * @ordered
   */
  protected static final String INCLUDENAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getINCLUDENAME() <em>INCLUDENAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getINCLUDENAME()
   * @generated
   * @ordered
   */
  protected String includename = INCLUDENAME_EDEFAULT;

  /**
   * The default value of the '{@link #getMEMBERNAME() <em>MEMBERNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMEMBERNAME()
   * @generated
   * @ordered
   */
  protected static final String MEMBERNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMEMBERNAME() <em>MEMBERNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMEMBERNAME()
   * @generated
   * @ordered
   */
  protected String membername = MEMBERNAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected INCLUDEImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getINCLUDE();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getINCLUDENAME()
  {
    return includename;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setINCLUDENAME(String newINCLUDENAME)
  {
    String oldINCLUDENAME = includename;
    includename = newINCLUDENAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.INCLUDE__INCLUDENAME, oldINCLUDENAME, includename));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMEMBERNAME()
  {
    return membername;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMEMBERNAME(String newMEMBERNAME)
  {
    String oldMEMBERNAME = membername;
    membername = newMEMBERNAME;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.INCLUDE__MEMBERNAME, oldMEMBERNAME, membername));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.INCLUDE__INCLUDENAME:
        return getINCLUDENAME();
      case JclDslPackage.INCLUDE__MEMBERNAME:
        return getMEMBERNAME();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.INCLUDE__INCLUDENAME:
        setINCLUDENAME((String)newValue);
        return;
      case JclDslPackage.INCLUDE__MEMBERNAME:
        setMEMBERNAME((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.INCLUDE__INCLUDENAME:
        setINCLUDENAME(INCLUDENAME_EDEFAULT);
        return;
      case JclDslPackage.INCLUDE__MEMBERNAME:
        setMEMBERNAME(MEMBERNAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.INCLUDE__INCLUDENAME:
        return INCLUDENAME_EDEFAULT == null ? includename != null : !INCLUDENAME_EDEFAULT.equals(includename);
      case JclDslPackage.INCLUDE__MEMBERNAME:
        return MEMBERNAME_EDEFAULT == null ? membername != null : !MEMBERNAME_EDEFAULT.equals(membername);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (INCLUDENAME: ");
    result.append(includename);
    result.append(", MEMBERNAME: ");
    result.append(membername);
    result.append(')');
    return result.toString();
  }

} //INCLUDEImpl
