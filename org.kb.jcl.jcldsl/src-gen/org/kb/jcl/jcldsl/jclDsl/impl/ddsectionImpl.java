/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddsection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ddsection</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ddsectionImpl extends MinimalEObjectImpl.Container implements ddsection
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddsectionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddsection();
  }

} //ddsectionImpl
