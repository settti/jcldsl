/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.jobRestart;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>job Restart</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.jobRestartImpl#getJOBSTEP <em>JOBSTEP</em>}</li>
 * </ul>
 *
 * @generated
 */
public class jobRestartImpl extends jobsectionImpl implements jobRestart
{
  /**
   * The default value of the '{@link #getJOBSTEP() <em>JOBSTEP</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJOBSTEP()
   * @generated
   * @ordered
   */
  protected static final String JOBSTEP_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getJOBSTEP() <em>JOBSTEP</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJOBSTEP()
   * @generated
   * @ordered
   */
  protected String jobstep = JOBSTEP_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected jobRestartImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getjobRestart();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getJOBSTEP()
  {
    return jobstep;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setJOBSTEP(String newJOBSTEP)
  {
    String oldJOBSTEP = jobstep;
    jobstep = newJOBSTEP;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.JOB_RESTART__JOBSTEP, oldJOBSTEP, jobstep));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_RESTART__JOBSTEP:
        return getJOBSTEP();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_RESTART__JOBSTEP:
        setJOBSTEP((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_RESTART__JOBSTEP:
        setJOBSTEP(JOBSTEP_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.JOB_RESTART__JOBSTEP:
        return JOBSTEP_EDEFAULT == null ? jobstep != null : !JOBSTEP_EDEFAULT.equals(jobstep);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (JOBSTEP: ");
    result.append(jobstep);
    result.append(')');
    return result.toString();
  }

} //jobRestartImpl
