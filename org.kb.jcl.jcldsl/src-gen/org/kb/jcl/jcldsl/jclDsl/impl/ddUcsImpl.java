/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddUcs;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Ucs</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddUcsImpl#getCharset <em>Charset</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddUcsImpl#getFold <em>Fold</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddUcsImpl#getVerify <em>Verify</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddUcsImpl extends ddsectionImpl implements ddUcs
{
  /**
   * The default value of the '{@link #getCharset() <em>Charset</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCharset()
   * @generated
   * @ordered
   */
  protected static final String CHARSET_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCharset() <em>Charset</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCharset()
   * @generated
   * @ordered
   */
  protected String charset = CHARSET_EDEFAULT;

  /**
   * The default value of the '{@link #getFold() <em>Fold</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFold()
   * @generated
   * @ordered
   */
  protected static final String FOLD_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFold() <em>Fold</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFold()
   * @generated
   * @ordered
   */
  protected String fold = FOLD_EDEFAULT;

  /**
   * The default value of the '{@link #getVerify() <em>Verify</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVerify()
   * @generated
   * @ordered
   */
  protected static final String VERIFY_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVerify() <em>Verify</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVerify()
   * @generated
   * @ordered
   */
  protected String verify = VERIFY_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddUcsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddUcs();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCharset()
  {
    return charset;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCharset(String newCharset)
  {
    String oldCharset = charset;
    charset = newCharset;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_UCS__CHARSET, oldCharset, charset));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFold()
  {
    return fold;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFold(String newFold)
  {
    String oldFold = fold;
    fold = newFold;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_UCS__FOLD, oldFold, fold));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVerify()
  {
    return verify;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVerify(String newVerify)
  {
    String oldVerify = verify;
    verify = newVerify;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_UCS__VERIFY, oldVerify, verify));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_UCS__CHARSET:
        return getCharset();
      case JclDslPackage.DD_UCS__FOLD:
        return getFold();
      case JclDslPackage.DD_UCS__VERIFY:
        return getVerify();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_UCS__CHARSET:
        setCharset((String)newValue);
        return;
      case JclDslPackage.DD_UCS__FOLD:
        setFold((String)newValue);
        return;
      case JclDslPackage.DD_UCS__VERIFY:
        setVerify((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_UCS__CHARSET:
        setCharset(CHARSET_EDEFAULT);
        return;
      case JclDslPackage.DD_UCS__FOLD:
        setFold(FOLD_EDEFAULT);
        return;
      case JclDslPackage.DD_UCS__VERIFY:
        setVerify(VERIFY_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_UCS__CHARSET:
        return CHARSET_EDEFAULT == null ? charset != null : !CHARSET_EDEFAULT.equals(charset);
      case JclDslPackage.DD_UCS__FOLD:
        return FOLD_EDEFAULT == null ? fold != null : !FOLD_EDEFAULT.equals(fold);
      case JclDslPackage.DD_UCS__VERIFY:
        return VERIFY_EDEFAULT == null ? verify != null : !VERIFY_EDEFAULT.equals(verify);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (charset: ");
    result.append(charset);
    result.append(", fold: ");
    result.append(fold);
    result.append(", verify: ");
    result.append(verify);
    result.append(')');
    return result.toString();
  }

} //ddUcsImpl
