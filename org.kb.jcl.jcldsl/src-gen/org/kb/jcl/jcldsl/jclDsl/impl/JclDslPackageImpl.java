/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import java.io.IOException;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslFactory;
import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JclDslPackageImpl extends EPackageImpl implements JclDslPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected String packageFilename = "jclDsl.loadinitialization_ecore";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass modelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass keywordEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stepcatddEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass joblibddEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobcatddEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass specddSectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass specddDispEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass specddDsnameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ifEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass elseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass endifEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass procEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass pendEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass setEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jcllibEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execsectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execTimeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execTimevalueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execPerformEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execRegionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execRdEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execParmEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execDynambrEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execCondEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execAddrspcEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execCcsidEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execAcctEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execProcEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execPgmEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass includeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobsectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobUserEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobTyprunEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobTimeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobTimevalueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobSchenvEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobSeclabelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobRestartEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobRegionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobRdEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobPrtyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobPrognameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobPerformEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobPasswordEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobPagesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobNotifyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobMsglevelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobMsgClassEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobLinesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobGroupEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobCondEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobClassEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobCcsidEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobCardsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobBytesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobAddrspcEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jobAccinfoEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddsectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddVolumeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddUnitEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddUcsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddTermEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddSysoutEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddSubsysEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddSubsysSubparmsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddStorclasEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddSpinEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddSpaceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddSegmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddSecmodelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddRlsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddRetpdEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddRefddEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddRecorgEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddRecfmEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddQnameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddProtectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddPathmodeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddPathoptsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddPathdispEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddPathEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddOutputEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddOutlimEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddModifyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddMgmtclasEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddLreclEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddLikeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddLgstreamEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddLabelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddLabelExpireEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddLabelExpdtEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddLabelRetpdEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddKeyoffEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddKeylenEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddHoldEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddFreeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddFlashEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddFiledataEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddFcbEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddExpdtEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDsorgEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDsnameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDsntypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDsidEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDlmEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDispEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDestEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDdnameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDcbEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDcbSubparmsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddTrtchEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddThreshEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddStackEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddRkpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddReserveEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddPrtspEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddPciEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddOptcdEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddNtmEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddNcpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddModeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddLimctEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddIpltxidEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddIntvlEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddGncpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddFuncEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddEroptEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDiagnsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDenEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddCyloflEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddCpriEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBufsizeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBufoutEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBufoffEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBufnoEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBufmaxEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBuflEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBufinEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBftekEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBfalnEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDataclasEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddDataEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddCopiesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddCntlEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddChkptEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddCharsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddCcsidEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBurstEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBlkszlimEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddBlksizeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddAvgrecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddAmpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ddAccodeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum typrun_typeEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum jobRdValuesEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum pagesDefEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum ddRecorgValuesEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum ddRecfmValuesEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum ddPathmodeOptsEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum ddPathoptsOptsEEnum = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private JclDslPackageImpl()
  {
    super(eNS_URI, JclDslFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link JclDslPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @generated
   */
  public static JclDslPackage init()
  {
    if (isInited) return (JclDslPackage)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI);

    // Obtain or create and register package
    JclDslPackageImpl theJclDslPackage = (JclDslPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof JclDslPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new JclDslPackageImpl());

    isInited = true;

    // Load packages
    theJclDslPackage.loadPackage();

    // Fix loaded packages
    theJclDslPackage.fixPackageContents();

    // Mark meta-data to indicate it can't be changed
    theJclDslPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(JclDslPackage.eNS_URI, theJclDslPackage);
    return theJclDslPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModel()
  {
    if (modelEClass == null)
    {
      modelEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(0);
    }
    return modelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getKeyword()
  {
    if (keywordEClass == null)
    {
      keywordEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(1);
    }
    return keywordEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSTEPCATDD()
  {
    if (stepcatddEClass == null)
    {
      stepcatddEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(2);
    }
    return stepcatddEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSTEPCATDD_Section()
  {
        return (EReference)getSTEPCATDD().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSTEPCATDD_Sections()
  {
        return (EReference)getSTEPCATDD().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJOBLIBDD()
  {
    if (joblibddEClass == null)
    {
      joblibddEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(3);
    }
    return joblibddEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getJOBLIBDD_Section()
  {
        return (EReference)getJOBLIBDD().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getJOBLIBDD_Sections()
  {
        return (EReference)getJOBLIBDD().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJOBCATDD()
  {
    if (jobcatddEClass == null)
    {
      jobcatddEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(4);
    }
    return jobcatddEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getJOBCATDD_Section()
  {
        return (EReference)getJOBCATDD().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getJOBCATDD_Sections()
  {
        return (EReference)getJOBCATDD().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getspecddSection()
  {
    if (specddSectionEClass == null)
    {
      specddSectionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(5);
    }
    return specddSectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getspecddDisp()
  {
    if (specddDispEClass == null)
    {
      specddDispEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(6);
    }
    return specddDispEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getspecddDisp_Disp()
  {
        return (EAttribute)getspecddDisp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getspecddDsname()
  {
    if (specddDsnameEClass == null)
    {
      specddDsnameEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(7);
    }
    return specddDsnameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getspecddDsname_Specname()
  {
        return (EAttribute)getspecddDsname().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getspecddDsname_Dsname()
  {
        return (EAttribute)getspecddDsname().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIF()
  {
    if (ifEClass == null)
    {
      ifEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(8);
    }
    return ifEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIF_IFNAME()
  {
        return (EAttribute)getIF().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIF_STEPNAME()
  {
        return (EAttribute)getIF().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIF_Expression_keyword()
  {
        return (EAttribute)getIF().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIF_Operator()
  {
        return (EAttribute)getIF().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIF_Value()
  {
        return (EAttribute)getIF().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIF_Logical_op()
  {
        return (EAttribute)getIF().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIF_Expression_keyword2()
  {
        return (EAttribute)getIF().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIF_Operator2()
  {
        return (EAttribute)getIF().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIF_Value2()
  {
        return (EAttribute)getIF().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getELSE()
  {
    if (elseEClass == null)
    {
      elseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(9);
    }
    return elseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getELSE_ELSENAME()
  {
        return (EAttribute)getELSE().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getENDIF()
  {
    if (endifEClass == null)
    {
      endifEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(10);
    }
    return endifEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getENDIF_ENDIFNAME()
  {
        return (EAttribute)getENDIF().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPROC()
  {
    if (procEClass == null)
    {
      procEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(11);
    }
    return procEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPROC_PROCNAME()
  {
        return (EAttribute)getPROC().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPROC_PARMNAME()
  {
        return (EAttribute)getPROC().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPROC_Parm_value()
  {
        return (EAttribute)getPROC().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPROC_DSNAMES()
  {
        return (EAttribute)getPROC().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPROC_Value()
  {
        return (EAttribute)getPROC().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPEND()
  {
    if (pendEClass == null)
    {
      pendEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(12);
    }
    return pendEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPEND_PENDNAME()
  {
        return (EAttribute)getPEND().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSET()
  {
    if (setEClass == null)
    {
      setEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(13);
    }
    return setEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSET_SETNAME()
  {
        return (EAttribute)getSET().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSET_Parmname()
  {
        return (EAttribute)getSET().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSET_Parm_value()
  {
        return (EAttribute)getSET().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSET_Parm_valueb()
  {
        return (EAttribute)getSET().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSET_Parmname2()
  {
        return (EAttribute)getSET().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSET_Parm_value2()
  {
        return (EAttribute)getSET().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSET_Parm_valueb2()
  {
        return (EAttribute)getSET().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJCLLIB()
  {
    if (jcllibEClass == null)
    {
      jcllibEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(14);
    }
    return jcllibEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getJCLLIB_JCLLIBNAME()
  {
        return (EAttribute)getJCLLIB().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getJCLLIB_DSNAMES()
  {
        return (EAttribute)getJCLLIB().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getJCLLIB_DSNAMES2()
  {
        return (EAttribute)getJCLLIB().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEXEC()
  {
    if (execEClass == null)
    {
      execEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(15);
    }
    return execEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getEXEC_EXECNAME()
  {
        return (EAttribute)getEXEC().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEXEC_Section()
  {
        return (EReference)getEXEC().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getEXEC_PROCEDURE()
  {
        return (EAttribute)getEXEC().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEXEC_Sections()
  {
        return (EReference)getEXEC().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEXEC_Dds()
  {
        return (EReference)getEXEC().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecsection()
  {
    if (execsectionEClass == null)
    {
      execsectionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(16);
    }
    return execsectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecTime()
  {
    if (execTimeEClass == null)
    {
      execTimeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(17);
    }
    return execTimeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getexecTime_ExecTimevalue()
  {
        return (EReference)getexecTime().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecTimevalue()
  {
    if (execTimevalueEClass == null)
    {
      execTimevalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(18);
    }
    return execTimevalueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecTimevalue_Minutes()
  {
        return (EAttribute)getexecTimevalue().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecTimevalue_Seconds()
  {
        return (EAttribute)getexecTimevalue().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecPerform()
  {
    if (execPerformEClass == null)
    {
      execPerformEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(19);
    }
    return execPerformEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecPerform_Value()
  {
        return (EAttribute)getexecPerform().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecRegion()
  {
    if (execRegionEClass == null)
    {
      execRegionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(20);
    }
    return execRegionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecRegion_Region_size()
  {
        return (EAttribute)getexecRegion().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecRegion_Size()
  {
        return (EAttribute)getexecRegion().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecRd()
  {
    if (execRdEClass == null)
    {
      execRdEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(21);
    }
    return execRdEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecRd_Value()
  {
        return (EAttribute)getexecRd().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecParm()
  {
    if (execParmEClass == null)
    {
      execParmEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(22);
    }
    return execParmEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecParm_Parm()
  {
        return (EAttribute)getexecParm().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecParm_Parmkeyw()
  {
        return (EAttribute)getexecParm().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecParm_Parm2()
  {
        return (EAttribute)getexecParm().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecParm_Parmkeyw2()
  {
        return (EAttribute)getexecParm().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecParm_Parmint()
  {
        return (EAttribute)getexecParm().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecDynambr()
  {
    if (execDynambrEClass == null)
    {
      execDynambrEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(23);
    }
    return execDynambrEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecDynambr_Value()
  {
        return (EAttribute)getexecDynambr().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecCond()
  {
    if (execCondEClass == null)
    {
      execCondEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(24);
    }
    return execCondEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecCond_Rc()
  {
        return (EAttribute)getexecCond().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecCond_Execcondop()
  {
        return (EAttribute)getexecCond().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecCond_Stepname()
  {
        return (EAttribute)getexecCond().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecCond_Rc2()
  {
        return (EAttribute)getexecCond().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecCond_Stepname2()
  {
        return (EAttribute)getexecCond().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecCond_Rc3()
  {
        return (EAttribute)getexecCond().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecCond_Execcondop2()
  {
        return (EAttribute)getexecCond().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecCond_Stepname3()
  {
        return (EAttribute)getexecCond().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecAddrspc()
  {
    if (execAddrspcEClass == null)
    {
      execAddrspcEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(25);
    }
    return execAddrspcEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecAddrspc_Addr()
  {
        return (EAttribute)getexecAddrspc().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecCcsid()
  {
    if (execCcsidEClass == null)
    {
      execCcsidEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(26);
    }
    return execCcsidEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecCcsid_Value()
  {
        return (EAttribute)getexecCcsid().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecAcct()
  {
    if (execAcctEClass == null)
    {
      execAcctEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(27);
    }
    return execAcctEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecAcct_Value()
  {
        return (EAttribute)getexecAcct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecAcct_VALUE2()
  {
        return (EAttribute)getexecAcct().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecProc()
  {
    if (execProcEClass == null)
    {
      execProcEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(28);
    }
    return execProcEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecProc_PROCNAME()
  {
        return (EAttribute)getexecProc().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecPgm()
  {
    if (execPgmEClass == null)
    {
      execPgmEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(29);
    }
    return execPgmEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecPgm_PGMNAME()
  {
        return (EAttribute)getexecPgm().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getINCLUDE()
  {
    if (includeEClass == null)
    {
      includeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(30);
    }
    return includeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getINCLUDE_INCLUDENAME()
  {
        return (EAttribute)getINCLUDE().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getINCLUDE_MEMBERNAME()
  {
        return (EAttribute)getINCLUDE().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJOB()
  {
    if (jobEClass == null)
    {
      jobEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(31);
    }
    return jobEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getJOB_Keywords()
  {
        return (EReference)getJOB().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getJOB_JOBNAME()
  {
        return (EAttribute)getJOB().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getJOB_Section()
  {
        return (EReference)getJOB().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getJOB_Sections()
  {
        return (EReference)getJOB().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobsection()
  {
    if (jobsectionEClass == null)
    {
      jobsectionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(32);
    }
    return jobsectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobUser()
  {
    if (jobUserEClass == null)
    {
      jobUserEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(33);
    }
    return jobUserEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobUser_USERNAME()
  {
        return (EAttribute)getjobUser().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobTyprun()
  {
    if (jobTyprunEClass == null)
    {
      jobTyprunEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(34);
    }
    return jobTyprunEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobTyprun_Type()
  {
        return (EAttribute)getjobTyprun().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobTime()
  {
    if (jobTimeEClass == null)
    {
      jobTimeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(36);
    }
    return jobTimeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobTime_Value()
  {
        return (EAttribute)getjobTime().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getjobTime_JobTimevalue()
  {
        return (EReference)getjobTime().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobTimevalue()
  {
    if (jobTimevalueEClass == null)
    {
      jobTimevalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(37);
    }
    return jobTimevalueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobTimevalue_Minutes()
  {
        return (EAttribute)getjobTimevalue().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobTimevalue_Seconds()
  {
        return (EAttribute)getjobTimevalue().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobSchenv()
  {
    if (jobSchenvEClass == null)
    {
      jobSchenvEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(38);
    }
    return jobSchenvEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobSchenv_ENVIRONMENT()
  {
        return (EAttribute)getjobSchenv().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobSeclabel()
  {
    if (jobSeclabelEClass == null)
    {
      jobSeclabelEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(39);
    }
    return jobSeclabelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobSeclabel_SECURITYLABEL()
  {
        return (EAttribute)getjobSeclabel().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobRestart()
  {
    if (jobRestartEClass == null)
    {
      jobRestartEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(40);
    }
    return jobRestartEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobRestart_JOBSTEP()
  {
        return (EAttribute)getjobRestart().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobRegion()
  {
    if (jobRegionEClass == null)
    {
      jobRegionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(41);
    }
    return jobRegionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobRegion_Region_size()
  {
        return (EAttribute)getjobRegion().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobRegion_Size()
  {
        return (EAttribute)getjobRegion().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobRd()
  {
    if (jobRdEClass == null)
    {
      jobRdEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(42);
    }
    return jobRdEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobRd_Value()
  {
        return (EAttribute)getjobRd().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobPrty()
  {
    if (jobPrtyEClass == null)
    {
      jobPrtyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(44);
    }
    return jobPrtyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobPrty_Prtyvalue()
  {
        return (EAttribute)getjobPrty().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobProgname()
  {
    if (jobPrognameEClass == null)
    {
      jobPrognameEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(45);
    }
    return jobPrognameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobProgname_Programmer_name()
  {
        return (EAttribute)getjobProgname().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobProgname_PROGRAMMER_ID()
  {
        return (EAttribute)getjobProgname().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobPerform()
  {
    if (jobPerformEClass == null)
    {
      jobPerformEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(46);
    }
    return jobPerformEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobPerform_Value()
  {
        return (EAttribute)getjobPerform().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobPassword()
  {
    if (jobPasswordEClass == null)
    {
      jobPasswordEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(47);
    }
    return jobPasswordEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobPassword_PASSWORD()
  {
        return (EAttribute)getjobPassword().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobPassword_NEWPASSWORD()
  {
        return (EAttribute)getjobPassword().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobPages()
  {
    if (jobPagesEClass == null)
    {
      jobPagesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(48);
    }
    return jobPagesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobPages_Value()
  {
        return (EAttribute)getjobPages().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobPages_PagesDef()
  {
        return (EAttribute)getjobPages().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobNotify()
  {
    if (jobNotifyEClass == null)
    {
      jobNotifyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(50);
    }
    return jobNotifyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobNotify_USERID()
  {
        return (EAttribute)getjobNotify().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobMsglevel()
  {
    if (jobMsglevelEClass == null)
    {
      jobMsglevelEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(51);
    }
    return jobMsglevelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobMsglevel_Value()
  {
        return (EAttribute)getjobMsglevel().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobMsglevel_JobMsglevelStatements()
  {
        return (EAttribute)getjobMsglevel().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobMsglevel_JobMsglevelMessages()
  {
        return (EAttribute)getjobMsglevel().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobMsgClass()
  {
    if (jobMsgClassEClass == null)
    {
      jobMsgClassEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(52);
    }
    return jobMsgClassEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobMsgClass_Value()
  {
        return (EAttribute)getjobMsgClass().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobLines()
  {
    if (jobLinesEClass == null)
    {
      jobLinesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(53);
    }
    return jobLinesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobLines_Value()
  {
        return (EAttribute)getjobLines().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobLines_PagesDef()
  {
        return (EAttribute)getjobLines().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobGroup()
  {
    if (jobGroupEClass == null)
    {
      jobGroupEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(54);
    }
    return jobGroupEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobGroup_GROUP()
  {
        return (EAttribute)getjobGroup().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobCond()
  {
    if (jobCondEClass == null)
    {
      jobCondEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(55);
    }
    return jobCondEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobCond_Return_code()
  {
        return (EAttribute)getjobCond().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobCond_Jobcondop()
  {
        return (EAttribute)getjobCond().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobCond_Return_code2()
  {
        return (EAttribute)getjobCond().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobCond_Jobcondop2()
  {
        return (EAttribute)getjobCond().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobClass()
  {
    if (jobClassEClass == null)
    {
      jobClassEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(56);
    }
    return jobClassEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobClass_CLASS()
  {
        return (EAttribute)getjobClass().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobCcsid()
  {
    if (jobCcsidEClass == null)
    {
      jobCcsidEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(57);
    }
    return jobCcsidEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobCcsid_Value()
  {
        return (EAttribute)getjobCcsid().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobCards()
  {
    if (jobCardsEClass == null)
    {
      jobCardsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(58);
    }
    return jobCardsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobCards_Value()
  {
        return (EAttribute)getjobCards().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobCards_PagesDef()
  {
        return (EAttribute)getjobCards().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobBytes()
  {
    if (jobBytesEClass == null)
    {
      jobBytesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(59);
    }
    return jobBytesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobBytes_Values()
  {
        return (EAttribute)getjobBytes().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobBytes_PagesDef()
  {
        return (EAttribute)getjobBytes().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobAddrspc()
  {
    if (jobAddrspcEClass == null)
    {
      jobAddrspcEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(60);
    }
    return jobAddrspcEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobAddrspc_Value()
  {
        return (EAttribute)getjobAddrspc().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getjobAccinfo()
  {
    if (jobAccinfoEClass == null)
    {
      jobAccinfoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(61);
    }
    return jobAccinfoEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobAccinfo_Pano()
  {
        return (EAttribute)getjobAccinfo().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobAccinfo_Acc()
  {
        return (EAttribute)getjobAccinfo().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobAccinfo_Acc2()
  {
        return (EAttribute)getjobAccinfo().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getjobAccinfo_Acc3()
  {
        return (EAttribute)getjobAccinfo().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDD()
  {
    if (ddEClass == null)
    {
      ddEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(62);
    }
    return ddEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDD_DDNAME()
  {
        return (EAttribute)getDD().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDD_Sections()
  {
        return (EReference)getDD().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddsection()
  {
    if (ddsectionEClass == null)
    {
      ddsectionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(63);
    }
    return ddsectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddVolume()
  {
    if (ddVolumeEClass == null)
    {
      ddVolumeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(64);
    }
    return ddVolumeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddVolume_Private()
  {
        return (EAttribute)getddVolume().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddVolume_Retain()
  {
        return (EAttribute)getddVolume().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddVolume_Volseq()
  {
        return (EAttribute)getddVolume().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddVolume_Volcnt()
  {
        return (EAttribute)getddVolume().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddVolume_Serial()
  {
        return (EAttribute)getddVolume().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddVolume_Serials()
  {
        return (EAttribute)getddVolume().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddVolume_Dsname()
  {
        return (EAttribute)getddVolume().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddUnit()
  {
    if (ddUnitEClass == null)
    {
      ddUnitEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(65);
    }
    return ddUnitEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddUnit_Device()
  {
        return (EAttribute)getddUnit().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddUnit_Count()
  {
        return (EAttribute)getddUnit().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddUnit_P()
  {
        return (EAttribute)getddUnit().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddUnit_Defer()
  {
        return (EAttribute)getddUnit().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddUcs()
  {
    if (ddUcsEClass == null)
    {
      ddUcsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(66);
    }
    return ddUcsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddUcs_Charset()
  {
        return (EAttribute)getddUcs().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddUcs_Fold()
  {
        return (EAttribute)getddUcs().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddUcs_Verify()
  {
        return (EAttribute)getddUcs().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddTerm()
  {
    if (ddTermEClass == null)
    {
      ddTermEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(67);
    }
    return ddTermEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddSysout()
  {
    if (ddSysoutEClass == null)
    {
      ddSysoutEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(68);
    }
    return ddSysoutEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSysout_Class()
  {
        return (EAttribute)getddSysout().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSysout_Wtrname()
  {
        return (EAttribute)getddSysout().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSysout_Formname()
  {
        return (EAttribute)getddSysout().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddSubsys()
  {
    if (ddSubsysEClass == null)
    {
      ddSubsysEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(69);
    }
    return ddSubsysEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSubsys_Label()
  {
        return (EAttribute)getddSubsys().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSubsys_Subsys()
  {
        return (EAttribute)getddSubsys().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getddSubsys_Subparm()
  {
        return (EReference)getddSubsys().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddSubsysSubparms()
  {
    if (ddSubsysSubparmsEClass == null)
    {
      ddSubsysSubparmsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(70);
    }
    return ddSubsysSubparmsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSubsysSubparms_Subparm()
  {
        return (EAttribute)getddSubsysSubparms().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSubsysSubparms_Subparm3()
  {
        return (EAttribute)getddSubsysSubparms().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddStorclas()
  {
    if (ddStorclasEClass == null)
    {
      ddStorclasEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(71);
    }
    return ddStorclasEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddStorclas_Storageclass()
  {
        return (EAttribute)getddStorclas().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddSpin()
  {
    if (ddSpinEClass == null)
    {
      ddSpinEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(72);
    }
    return ddSpinEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSpin_Unalloc()
  {
        return (EAttribute)getddSpin().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddSpace()
  {
    if (ddSpaceEClass == null)
    {
      ddSpaceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(73);
    }
    return ddSpaceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSpace_Length()
  {
        return (EAttribute)getddSpace().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSpace_Primary()
  {
        return (EAttribute)getddSpace().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSpace_Secondary()
  {
        return (EAttribute)getddSpace().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSpace_Dirorindx()
  {
        return (EAttribute)getddSpace().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSpace_Parm()
  {
        return (EAttribute)getddSpace().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddSegment()
  {
    if (ddSegmentEClass == null)
    {
      ddSegmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(74);
    }
    return ddSegmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSegment_Page_count()
  {
        return (EAttribute)getddSegment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddSecmodel()
  {
    if (ddSecmodelEClass == null)
    {
      ddSecmodelEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(75);
    }
    return ddSecmodelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSecmodel_Dsname()
  {
        return (EAttribute)getddSecmodel().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddSecmodel_Generic()
  {
        return (EAttribute)getddSecmodel().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddRls()
  {
    if (ddRlsEClass == null)
    {
      ddRlsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(76);
    }
    return ddRlsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddRls_Rls()
  {
        return (EAttribute)getddRls().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddRetpd()
  {
    if (ddRetpdEClass == null)
    {
      ddRetpdEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(77);
    }
    return ddRetpdEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddRetpd_Value()
  {
        return (EAttribute)getddRetpd().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddRefdd()
  {
    if (ddRefddEClass == null)
    {
      ddRefddEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(78);
    }
    return ddRefddEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddRefdd_Dsname()
  {
        return (EAttribute)getddRefdd().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddRecorg()
  {
    if (ddRecorgEClass == null)
    {
      ddRecorgEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(79);
    }
    return ddRecorgEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddRecorg_Recorg()
  {
        return (EAttribute)getddRecorg().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddRecfm()
  {
    if (ddRecfmEClass == null)
    {
      ddRecfmEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(81);
    }
    return ddRecfmEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddRecfm_Recfm()
  {
        return (EAttribute)getddRecfm().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddQname()
  {
    if (ddQnameEClass == null)
    {
      ddQnameEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(83);
    }
    return ddQnameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddQname_Procname()
  {
        return (EAttribute)getddQname().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddProtect()
  {
    if (ddProtectEClass == null)
    {
      ddProtectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(84);
    }
    return ddProtectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddProtect_Yes()
  {
        return (EAttribute)getddProtect().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddPathmode()
  {
    if (ddPathmodeEClass == null)
    {
      ddPathmodeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(85);
    }
    return ddPathmodeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPathmode_Option()
  {
        return (EAttribute)getddPathmode().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPathmode_Option2()
  {
        return (EAttribute)getddPathmode().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddPathopts()
  {
    if (ddPathoptsEClass == null)
    {
      ddPathoptsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(87);
    }
    return ddPathoptsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPathopts_Option()
  {
        return (EAttribute)getddPathopts().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPathopts_Option2()
  {
        return (EAttribute)getddPathopts().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddPathdisp()
  {
    if (ddPathdispEClass == null)
    {
      ddPathdispEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(89);
    }
    return ddPathdispEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPathdisp_Disp()
  {
        return (EAttribute)getddPathdisp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPathdisp_Disp2()
  {
        return (EAttribute)getddPathdisp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPathdisp_Disp3()
  {
        return (EAttribute)getddPathdisp().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddPath()
  {
    if (ddPathEClass == null)
    {
      ddPathEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(90);
    }
    return ddPathEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPath_Path()
  {
        return (EAttribute)getddPath().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddOutput()
  {
    if (ddOutputEClass == null)
    {
      ddOutputEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(91);
    }
    return ddOutputEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddOutput_Output()
  {
        return (EAttribute)getddOutput().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddOutput_Output2()
  {
        return (EAttribute)getddOutput().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddOutlim()
  {
    if (ddOutlimEClass == null)
    {
      ddOutlimEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(92);
    }
    return ddOutlimEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddOutlim_Value()
  {
        return (EAttribute)getddOutlim().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddModify()
  {
    if (ddModifyEClass == null)
    {
      ddModifyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(93);
    }
    return ddModifyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddModify_Module()
  {
        return (EAttribute)getddModify().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddModify_Trc()
  {
        return (EAttribute)getddModify().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddMgmtclas()
  {
    if (ddMgmtclasEClass == null)
    {
      ddMgmtclasEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(94);
    }
    return ddMgmtclasEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddMgmtclas_CLASSNAME()
  {
        return (EAttribute)getddMgmtclas().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddLrecl()
  {
    if (ddLreclEClass == null)
    {
      ddLreclEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(95);
    }
    return ddLreclEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLrecl_Length_in_bytes()
  {
        return (EAttribute)getddLrecl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddLike()
  {
    if (ddLikeEClass == null)
    {
      ddLikeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(96);
    }
    return ddLikeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLike_Dsname()
  {
        return (EAttribute)getddLike().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddLgstream()
  {
    if (ddLgstreamEClass == null)
    {
      ddLgstreamEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(97);
    }
    return ddLgstreamEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLgstream_Logstream()
  {
        return (EAttribute)getddLgstream().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddLabel()
  {
    if (ddLabelEClass == null)
    {
      ddLabelEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(98);
    }
    return ddLabelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLabel_Sequence_number()
  {
        return (EAttribute)getddLabel().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLabel_Label()
  {
        return (EAttribute)getddLabel().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLabel_Password()
  {
        return (EAttribute)getddLabel().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLabel_Inorout()
  {
        return (EAttribute)getddLabel().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getddLabel_Expire()
  {
        return (EReference)getddLabel().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddLabelExpire()
  {
    if (ddLabelExpireEClass == null)
    {
      ddLabelExpireEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(99);
    }
    return ddLabelExpireEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddLabelExpdt()
  {
    if (ddLabelExpdtEClass == null)
    {
      ddLabelExpdtEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(100);
    }
    return ddLabelExpdtEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLabelExpdt_Yyddd()
  {
        return (EAttribute)getddLabelExpdt().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLabelExpdt_Yyyy()
  {
        return (EAttribute)getddLabelExpdt().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLabelExpdt_Ddd()
  {
        return (EAttribute)getddLabelExpdt().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddLabelRetpd()
  {
    if (ddLabelRetpdEClass == null)
    {
      ddLabelRetpdEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(101);
    }
    return ddLabelRetpdEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLabelRetpd_Nnnn()
  {
        return (EAttribute)getddLabelRetpd().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddKeyoff()
  {
    if (ddKeyoffEClass == null)
    {
      ddKeyoffEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(102);
    }
    return ddKeyoffEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddKeyoff_Length()
  {
        return (EAttribute)getddKeyoff().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddKeylen()
  {
    if (ddKeylenEClass == null)
    {
      ddKeylenEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(103);
    }
    return ddKeylenEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddKeylen_Length()
  {
        return (EAttribute)getddKeylen().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddHold()
  {
    if (ddHoldEClass == null)
    {
      ddHoldEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(104);
    }
    return ddHoldEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddHold_Value()
  {
        return (EAttribute)getddHold().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddFree()
  {
    if (ddFreeEClass == null)
    {
      ddFreeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(105);
    }
    return ddFreeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddFlash()
  {
    if (ddFlashEClass == null)
    {
      ddFlashEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(106);
    }
    return ddFlashEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddFlash_Value()
  {
        return (EAttribute)getddFlash().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddFlash_Count()
  {
        return (EAttribute)getddFlash().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddFiledata()
  {
    if (ddFiledataEClass == null)
    {
      ddFiledataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(107);
    }
    return ddFiledataEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddFcb()
  {
    if (ddFcbEClass == null)
    {
      ddFcbEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(108);
    }
    return ddFcbEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddFcb_Fcbname()
  {
        return (EAttribute)getddFcb().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddExpdt()
  {
    if (ddExpdtEClass == null)
    {
      ddExpdtEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(109);
    }
    return ddExpdtEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddExpdt_Date()
  {
        return (EAttribute)getddExpdt().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddExpdt_Year()
  {
        return (EAttribute)getddExpdt().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddExpdt_Day()
  {
        return (EAttribute)getddExpdt().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDsorg()
  {
    if (ddDsorgEClass == null)
    {
      ddDsorgEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(110);
    }
    return ddDsorgEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDsorg_Dsorg()
  {
        return (EAttribute)getddDsorg().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDsname()
  {
    if (ddDsnameEClass == null)
    {
      ddDsnameEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(111);
    }
    return ddDsnameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDsname_DSNAMES()
  {
        return (EAttribute)getddDsname().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDsntype()
  {
    if (ddDsntypeEClass == null)
    {
      ddDsntypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(112);
    }
    return ddDsntypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDsntype_Value()
  {
        return (EAttribute)getddDsntype().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDsid()
  {
    if (ddDsidEClass == null)
    {
      ddDsidEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(113);
    }
    return ddDsidEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDsid_Value()
  {
        return (EAttribute)getddDsid().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDsid_V()
  {
        return (EAttribute)getddDsid().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDlm()
  {
    if (ddDlmEClass == null)
    {
      ddDlmEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(114);
    }
    return ddDlmEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDlm_Delimiter()
  {
        return (EAttribute)getddDlm().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDlm_Delim()
  {
        return (EAttribute)getddDlm().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDisp()
  {
    if (ddDispEClass == null)
    {
      ddDispEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(115);
    }
    return ddDispEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDisp_Status()
  {
        return (EAttribute)getddDisp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDisp_Normal_disp()
  {
        return (EAttribute)getddDisp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDisp_Abnormal_disp()
  {
        return (EAttribute)getddDisp().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDest()
  {
    if (ddDestEClass == null)
    {
      ddDestEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(116);
    }
    return ddDestEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDest_Destination()
  {
        return (EAttribute)getddDest().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDdname()
  {
    if (ddDdnameEClass == null)
    {
      ddDdnameEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(117);
    }
    return ddDdnameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDdname_Ddname()
  {
        return (EAttribute)getddDdname().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDcb()
  {
    if (ddDcbEClass == null)
    {
      ddDcbEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(118);
    }
    return ddDcbEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getddDcb_Subparm()
  {
        return (EReference)getddDcb().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getddDcb_Subparm2()
  {
        return (EReference)getddDcb().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDcb_Dsname()
  {
        return (EAttribute)getddDcb().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDcbSubparms()
  {
    if (ddDcbSubparmsEClass == null)
    {
      ddDcbSubparmsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(119);
    }
    return ddDcbSubparmsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddTrtch()
  {
    if (ddTrtchEClass == null)
    {
      ddTrtchEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(120);
    }
    return ddTrtchEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddTrtch_Parm()
  {
        return (EAttribute)getddTrtch().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddThresh()
  {
    if (ddThreshEClass == null)
    {
      ddThreshEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(121);
    }
    return ddThreshEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddThresh_Value()
  {
        return (EAttribute)getddThresh().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddStack()
  {
    if (ddStackEClass == null)
    {
      ddStackEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(122);
    }
    return ddStackEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddStack_Value()
  {
        return (EAttribute)getddStack().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddRkp()
  {
    if (ddRkpEClass == null)
    {
      ddRkpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(123);
    }
    return ddRkpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddRkp_Value()
  {
        return (EAttribute)getddRkp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddReserve()
  {
    if (ddReserveEClass == null)
    {
      ddReserveEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(124);
    }
    return ddReserveEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddReserve_Value()
  {
        return (EAttribute)getddReserve().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddReserve_Value2()
  {
        return (EAttribute)getddReserve().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddPrtsp()
  {
    if (ddPrtspEClass == null)
    {
      ddPrtspEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(125);
    }
    return ddPrtspEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPrtsp_Value()
  {
        return (EAttribute)getddPrtsp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddPci()
  {
    if (ddPciEClass == null)
    {
      ddPciEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(126);
    }
    return ddPciEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPci_Parm()
  {
        return (EAttribute)getddPci().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddPci_Parm2()
  {
        return (EAttribute)getddPci().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddOptcd()
  {
    if (ddOptcdEClass == null)
    {
      ddOptcdEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(127);
    }
    return ddOptcdEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddOptcd_Parm()
  {
        return (EAttribute)getddOptcd().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddNtm()
  {
    if (ddNtmEClass == null)
    {
      ddNtmEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(128);
    }
    return ddNtmEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddNtm_Value()
  {
        return (EAttribute)getddNtm().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddNcp()
  {
    if (ddNcpEClass == null)
    {
      ddNcpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(129);
    }
    return ddNcpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddNcp_Value()
  {
        return (EAttribute)getddNcp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddMode()
  {
    if (ddModeEClass == null)
    {
      ddModeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(130);
    }
    return ddModeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddMode_Parm()
  {
        return (EAttribute)getddMode().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddLimct()
  {
    if (ddLimctEClass == null)
    {
      ddLimctEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(131);
    }
    return ddLimctEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddLimct_Value()
  {
        return (EAttribute)getddLimct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddIpltxid()
  {
    if (ddIpltxidEClass == null)
    {
      ddIpltxidEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(132);
    }
    return ddIpltxidEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddIpltxid_Value()
  {
        return (EAttribute)getddIpltxid().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddIntvl()
  {
    if (ddIntvlEClass == null)
    {
      ddIntvlEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(133);
    }
    return ddIntvlEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddIntvl_Value()
  {
        return (EAttribute)getddIntvl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddGncp()
  {
    if (ddGncpEClass == null)
    {
      ddGncpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(134);
    }
    return ddGncpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddGncp_Value()
  {
        return (EAttribute)getddGncp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddFunc()
  {
    if (ddFuncEClass == null)
    {
      ddFuncEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(135);
    }
    return ddFuncEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddFunc_Parm()
  {
        return (EAttribute)getddFunc().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddEropt()
  {
    if (ddEroptEClass == null)
    {
      ddEroptEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(136);
    }
    return ddEroptEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddEropt_Parm()
  {
        return (EAttribute)getddEropt().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDiagns()
  {
    if (ddDiagnsEClass == null)
    {
      ddDiagnsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(137);
    }
    return ddDiagnsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDiagns_Parm()
  {
        return (EAttribute)getddDiagns().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDen()
  {
    if (ddDenEClass == null)
    {
      ddDenEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(138);
    }
    return ddDenEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDen_Value()
  {
        return (EAttribute)getddDen().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddCylofl()
  {
    if (ddCyloflEClass == null)
    {
      ddCyloflEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(139);
    }
    return ddCyloflEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddCylofl_Value()
  {
        return (EAttribute)getddCylofl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddCpri()
  {
    if (ddCpriEClass == null)
    {
      ddCpriEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(140);
    }
    return ddCpriEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddCpri_Parm()
  {
        return (EAttribute)getddCpri().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBufsize()
  {
    if (ddBufsizeEClass == null)
    {
      ddBufsizeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(141);
    }
    return ddBufsizeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBufsize_Value()
  {
        return (EAttribute)getddBufsize().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBufout()
  {
    if (ddBufoutEClass == null)
    {
      ddBufoutEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(142);
    }
    return ddBufoutEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBufout_Value()
  {
        return (EAttribute)getddBufout().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBufoff()
  {
    if (ddBufoffEClass == null)
    {
      ddBufoffEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(143);
    }
    return ddBufoffEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBufoff_Value()
  {
        return (EAttribute)getddBufoff().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBufoff_L()
  {
        return (EAttribute)getddBufoff().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBufno()
  {
    if (ddBufnoEClass == null)
    {
      ddBufnoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(144);
    }
    return ddBufnoEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBufno_Value()
  {
        return (EAttribute)getddBufno().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBufmax()
  {
    if (ddBufmaxEClass == null)
    {
      ddBufmaxEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(145);
    }
    return ddBufmaxEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBufmax_Value()
  {
        return (EAttribute)getddBufmax().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBufl()
  {
    if (ddBuflEClass == null)
    {
      ddBuflEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(146);
    }
    return ddBuflEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBufl_Bytes()
  {
        return (EAttribute)getddBufl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBufin()
  {
    if (ddBufinEClass == null)
    {
      ddBufinEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(147);
    }
    return ddBufinEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBufin_Value()
  {
        return (EAttribute)getddBufin().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBftek()
  {
    if (ddBftekEClass == null)
    {
      ddBftekEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(148);
    }
    return ddBftekEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBftek_Parm()
  {
        return (EAttribute)getddBftek().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBfaln()
  {
    if (ddBfalnEClass == null)
    {
      ddBfalnEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(149);
    }
    return ddBfalnEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBfaln_Parm()
  {
        return (EAttribute)getddBfaln().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddDataclas()
  {
    if (ddDataclasEClass == null)
    {
      ddDataclasEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(150);
    }
    return ddDataclasEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddDataclas_Dataclass()
  {
        return (EAttribute)getddDataclas().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddData()
  {
    if (ddDataEClass == null)
    {
      ddDataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(151);
    }
    return ddDataEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddData_Dddata()
  {
        return (EAttribute)getddData().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddCopies()
  {
    if (ddCopiesEClass == null)
    {
      ddCopiesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(152);
    }
    return ddCopiesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddCopies_Value()
  {
        return (EAttribute)getddCopies().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddCopies_Group_value()
  {
        return (EAttribute)getddCopies().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddCntl()
  {
    if (ddCntlEClass == null)
    {
      ddCntlEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(153);
    }
    return ddCntlEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddCntl_Value()
  {
        return (EAttribute)getddCntl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddChkpt()
  {
    if (ddChkptEClass == null)
    {
      ddChkptEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(154);
    }
    return ddChkptEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddChars()
  {
    if (ddCharsEClass == null)
    {
      ddCharsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(155);
    }
    return ddCharsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddChars_Tablename()
  {
        return (EAttribute)getddChars().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddChars_That()
  {
        return (EAttribute)getddChars().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddChars_Tablename2()
  {
        return (EAttribute)getddChars().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddCcsid()
  {
    if (ddCcsidEClass == null)
    {
      ddCcsidEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(156);
    }
    return ddCcsidEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddCcsid_Value()
  {
        return (EAttribute)getddCcsid().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBurst()
  {
    if (ddBurstEClass == null)
    {
      ddBurstEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(157);
    }
    return ddBurstEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBurst_Value()
  {
        return (EAttribute)getddBurst().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBlkszlim()
  {
    if (ddBlkszlimEClass == null)
    {
      ddBlkszlimEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(158);
    }
    return ddBlkszlimEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBlkszlim_Size()
  {
        return (EAttribute)getddBlkszlim().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddBlksize()
  {
    if (ddBlksizeEClass == null)
    {
      ddBlksizeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(159);
    }
    return ddBlksizeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBlksize_Size()
  {
        return (EAttribute)getddBlksize().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddBlksize_Unit()
  {
        return (EAttribute)getddBlksize().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddAvgrec()
  {
    if (ddAvgrecEClass == null)
    {
      ddAvgrecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(160);
    }
    return ddAvgrecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddAvgrec_Value()
  {
        return (EAttribute)getddAvgrec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddAmp()
  {
    if (ddAmpEClass == null)
    {
      ddAmpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(161);
    }
    return ddAmpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddAmp_Value()
  {
        return (EAttribute)getddAmp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddAmp_Value2()
  {
        return (EAttribute)getddAmp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getddAccode()
  {
    if (ddAccodeEClass == null)
    {
      ddAccodeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(162);
    }
    return ddAccodeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getddAccode_Access()
  {
        return (EAttribute)getddAccode().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTyprun_type()
  {
    if (typrun_typeEEnum == null)
    {
      typrun_typeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(35);
    }
    return typrun_typeEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getjobRdValues()
  {
    if (jobRdValuesEEnum == null)
    {
      jobRdValuesEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(43);
    }
    return jobRdValuesEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getpagesDef()
  {
    if (pagesDefEEnum == null)
    {
      pagesDefEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(49);
    }
    return pagesDefEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getddRecorgValues()
  {
    if (ddRecorgValuesEEnum == null)
    {
      ddRecorgValuesEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(80);
    }
    return ddRecorgValuesEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getddRecfmValues()
  {
    if (ddRecfmValuesEEnum == null)
    {
      ddRecfmValuesEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(82);
    }
    return ddRecfmValuesEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getddPathmodeOpts()
  {
    if (ddPathmodeOptsEEnum == null)
    {
      ddPathmodeOptsEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(86);
    }
    return ddPathmodeOptsEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getddPathoptsOpts()
  {
    if (ddPathoptsOptsEEnum == null)
    {
      ddPathoptsOptsEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(JclDslPackage.eNS_URI).getEClassifiers().get(88);
    }
    return ddPathoptsOptsEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JclDslFactory getJclDslFactory()
  {
    return (JclDslFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isLoaded = false;

  /**
   * Laods the package and any sub-packages from their serialized form.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void loadPackage()
  {
    if (isLoaded) return;
    isLoaded = true;

    URL url = getClass().getResource(packageFilename);
    if (url == null)
    {
      throw new RuntimeException("Missing serialized package: " + packageFilename);
    }
    URI uri = URI.createURI(url.toString());
    Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
    try
    {
      resource.load(null);
    }
    catch (IOException exception)
    {
      throw new WrappedException(exception);
    }
    initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
    createResource(eNS_URI);
  }


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isFixed = false;

  /**
   * Fixes up the loaded package, to make it appear as if it had been programmatically built.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void fixPackageContents()
  {
    if (isFixed) return;
    isFixed = true;
    fixEClassifiers();
  }

  /**
   * Sets the instance class on the given classifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void fixInstanceClass(EClassifier eClassifier)
  {
    if (eClassifier.getInstanceClassName() == null)
    {
      eClassifier.setInstanceClassName("org.kb.jcl.jcldsl.jclDsl." + eClassifier.getName());
      setGeneratedClassName(eClassifier);
    }
  }

} //JclDslPackageImpl
