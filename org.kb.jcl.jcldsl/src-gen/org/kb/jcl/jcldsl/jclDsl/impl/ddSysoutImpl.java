/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddSysout;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Sysout</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSysoutImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSysoutImpl#getWtrname <em>Wtrname</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddSysoutImpl#getFormname <em>Formname</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddSysoutImpl extends ddsectionImpl implements ddSysout
{
  /**
   * The default value of the '{@link #getClass_() <em>Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClass_()
   * @generated
   * @ordered
   */
  protected static final String CLASS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getClass_() <em>Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClass_()
   * @generated
   * @ordered
   */
  protected String class_ = CLASS_EDEFAULT;

  /**
   * The default value of the '{@link #getWtrname() <em>Wtrname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWtrname()
   * @generated
   * @ordered
   */
  protected static final String WTRNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getWtrname() <em>Wtrname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWtrname()
   * @generated
   * @ordered
   */
  protected String wtrname = WTRNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getFormname() <em>Formname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFormname()
   * @generated
   * @ordered
   */
  protected static final String FORMNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFormname() <em>Formname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFormname()
   * @generated
   * @ordered
   */
  protected String formname = FORMNAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddSysoutImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddSysout();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getClass_()
  {
    return class_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setClass(String newClass)
  {
    String oldClass = class_;
    class_ = newClass;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SYSOUT__CLASS, oldClass, class_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getWtrname()
  {
    return wtrname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWtrname(String newWtrname)
  {
    String oldWtrname = wtrname;
    wtrname = newWtrname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SYSOUT__WTRNAME, oldWtrname, wtrname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFormname()
  {
    return formname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFormname(String newFormname)
  {
    String oldFormname = formname;
    formname = newFormname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_SYSOUT__FORMNAME, oldFormname, formname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SYSOUT__CLASS:
        return getClass_();
      case JclDslPackage.DD_SYSOUT__WTRNAME:
        return getWtrname();
      case JclDslPackage.DD_SYSOUT__FORMNAME:
        return getFormname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SYSOUT__CLASS:
        setClass((String)newValue);
        return;
      case JclDslPackage.DD_SYSOUT__WTRNAME:
        setWtrname((String)newValue);
        return;
      case JclDslPackage.DD_SYSOUT__FORMNAME:
        setFormname((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SYSOUT__CLASS:
        setClass(CLASS_EDEFAULT);
        return;
      case JclDslPackage.DD_SYSOUT__WTRNAME:
        setWtrname(WTRNAME_EDEFAULT);
        return;
      case JclDslPackage.DD_SYSOUT__FORMNAME:
        setFormname(FORMNAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_SYSOUT__CLASS:
        return CLASS_EDEFAULT == null ? class_ != null : !CLASS_EDEFAULT.equals(class_);
      case JclDslPackage.DD_SYSOUT__WTRNAME:
        return WTRNAME_EDEFAULT == null ? wtrname != null : !WTRNAME_EDEFAULT.equals(wtrname);
      case JclDslPackage.DD_SYSOUT__FORMNAME:
        return FORMNAME_EDEFAULT == null ? formname != null : !FORMNAME_EDEFAULT.equals(formname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (class: ");
    result.append(class_);
    result.append(", wtrname: ");
    result.append(wtrname);
    result.append(", formname: ");
    result.append(formname);
    result.append(')');
    return result.toString();
  }

} //ddSysoutImpl
