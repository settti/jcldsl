/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddDataclas;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Dataclas</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddDataclasImpl#getDataclass <em>Dataclass</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddDataclasImpl extends ddsectionImpl implements ddDataclas
{
  /**
   * The default value of the '{@link #getDataclass() <em>Dataclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataclass()
   * @generated
   * @ordered
   */
  protected static final String DATACLASS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDataclass() <em>Dataclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataclass()
   * @generated
   * @ordered
   */
  protected String dataclass = DATACLASS_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddDataclasImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddDataclas();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDataclass()
  {
    return dataclass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDataclass(String newDataclass)
  {
    String oldDataclass = dataclass;
    dataclass = newDataclass;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_DATACLAS__DATACLASS, oldDataclass, dataclass));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DATACLAS__DATACLASS:
        return getDataclass();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DATACLAS__DATACLASS:
        setDataclass((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DATACLAS__DATACLASS:
        setDataclass(DATACLASS_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_DATACLAS__DATACLASS:
        return DATACLASS_EDEFAULT == null ? dataclass != null : !DATACLASS_EDEFAULT.equals(dataclass);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (dataclass: ");
    result.append(dataclass);
    result.append(')');
    return result.toString();
  }

} //ddDataclasImpl
