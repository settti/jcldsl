/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dd Label Expdt</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelExpdtImpl#getYyddd <em>Yyddd</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelExpdtImpl#getYyyy <em>Yyyy</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.impl.ddLabelExpdtImpl#getDdd <em>Ddd</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ddLabelExpdtImpl extends ddLabelExpireImpl implements ddLabelExpdt
{
  /**
   * The default value of the '{@link #getYyddd() <em>Yyddd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getYyddd()
   * @generated
   * @ordered
   */
  protected static final int YYDDD_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getYyddd() <em>Yyddd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getYyddd()
   * @generated
   * @ordered
   */
  protected int yyddd = YYDDD_EDEFAULT;

  /**
   * The default value of the '{@link #getYyyy() <em>Yyyy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getYyyy()
   * @generated
   * @ordered
   */
  protected static final int YYYY_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getYyyy() <em>Yyyy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getYyyy()
   * @generated
   * @ordered
   */
  protected int yyyy = YYYY_EDEFAULT;

  /**
   * The default value of the '{@link #getDdd() <em>Ddd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDdd()
   * @generated
   * @ordered
   */
  protected static final int DDD_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getDdd() <em>Ddd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDdd()
   * @generated
   * @ordered
   */
  protected int ddd = DDD_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ddLabelExpdtImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getddLabelExpdt();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getYyddd()
  {
    return yyddd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setYyddd(int newYyddd)
  {
    int oldYyddd = yyddd;
    yyddd = newYyddd;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_LABEL_EXPDT__YYDDD, oldYyddd, yyddd));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getYyyy()
  {
    return yyyy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setYyyy(int newYyyy)
  {
    int oldYyyy = yyyy;
    yyyy = newYyyy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_LABEL_EXPDT__YYYY, oldYyyy, yyyy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getDdd()
  {
    return ddd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDdd(int newDdd)
  {
    int oldDdd = ddd;
    ddd = newDdd;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JclDslPackage.DD_LABEL_EXPDT__DDD, oldDdd, ddd));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LABEL_EXPDT__YYDDD:
        return getYyddd();
      case JclDslPackage.DD_LABEL_EXPDT__YYYY:
        return getYyyy();
      case JclDslPackage.DD_LABEL_EXPDT__DDD:
        return getDdd();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LABEL_EXPDT__YYDDD:
        setYyddd((Integer)newValue);
        return;
      case JclDslPackage.DD_LABEL_EXPDT__YYYY:
        setYyyy((Integer)newValue);
        return;
      case JclDslPackage.DD_LABEL_EXPDT__DDD:
        setDdd((Integer)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LABEL_EXPDT__YYDDD:
        setYyddd(YYDDD_EDEFAULT);
        return;
      case JclDslPackage.DD_LABEL_EXPDT__YYYY:
        setYyyy(YYYY_EDEFAULT);
        return;
      case JclDslPackage.DD_LABEL_EXPDT__DDD:
        setDdd(DDD_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JclDslPackage.DD_LABEL_EXPDT__YYDDD:
        return yyddd != YYDDD_EDEFAULT;
      case JclDslPackage.DD_LABEL_EXPDT__YYYY:
        return yyyy != YYYY_EDEFAULT;
      case JclDslPackage.DD_LABEL_EXPDT__DDD:
        return ddd != DDD_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (yyddd: ");
    result.append(yyddd);
    result.append(", yyyy: ");
    result.append(yyyy);
    result.append(", ddd: ");
    result.append(ddd);
    result.append(')');
    return result.toString();
  }

} //ddLabelExpdtImpl
