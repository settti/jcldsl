/**
 */
package org.kb.jcl.jcldsl.jclDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.kb.jcl.jcldsl.jclDsl.JclDslPackage;
import org.kb.jcl.jcldsl.jclDsl.specddSection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>specdd Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class specddSectionImpl extends MinimalEObjectImpl.Container implements specddSection
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected specddSectionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JclDslPackage.eINSTANCE.getspecddSection();
  }

} //specddSectionImpl
