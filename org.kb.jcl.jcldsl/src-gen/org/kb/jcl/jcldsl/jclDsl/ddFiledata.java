/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Filedata</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddFiledata()
 * @model
 * @generated
 */
public interface ddFiledata extends ddsection
{
} // ddFiledata
