/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>exec Time</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execTime#getExecTimevalue <em>Exec Timevalue</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecTime()
 * @model
 * @generated
 */
public interface execTime extends execsection
{
  /**
   * Returns the value of the '<em><b>Exec Timevalue</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exec Timevalue</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exec Timevalue</em>' containment reference.
   * @see #setExecTimevalue(execTimevalue)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecTime_ExecTimevalue()
   * @model containment="true"
   * @generated
   */
  execTimevalue getExecTimevalue();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execTime#getExecTimevalue <em>Exec Timevalue</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Exec Timevalue</em>' containment reference.
   * @see #getExecTimevalue()
   * @generated
   */
  void setExecTimevalue(execTimevalue value);

} // execTime
