/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Label</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#isSequence_number <em>Sequence number</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#isLabel <em>Label</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#getPassword <em>Password</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#getInorout <em>Inorout</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#getExpire <em>Expire</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabel()
 * @model
 * @generated
 */
public interface ddLabel extends ddsection
{
  /**
   * Returns the value of the '<em><b>Sequence number</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sequence number</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sequence number</em>' attribute.
   * @see #setSequence_number(boolean)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabel_Sequence_number()
   * @model
   * @generated
   */
  boolean isSequence_number();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#isSequence_number <em>Sequence number</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sequence number</em>' attribute.
   * @see #isSequence_number()
   * @generated
   */
  void setSequence_number(boolean value);

  /**
   * Returns the value of the '<em><b>Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Label</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Label</em>' attribute.
   * @see #setLabel(boolean)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabel_Label()
   * @model
   * @generated
   */
  boolean isLabel();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#isLabel <em>Label</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Label</em>' attribute.
   * @see #isLabel()
   * @generated
   */
  void setLabel(boolean value);

  /**
   * Returns the value of the '<em><b>Password</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Password</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Password</em>' attribute.
   * @see #setPassword(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabel_Password()
   * @model
   * @generated
   */
  String getPassword();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#getPassword <em>Password</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Password</em>' attribute.
   * @see #getPassword()
   * @generated
   */
  void setPassword(String value);

  /**
   * Returns the value of the '<em><b>Inorout</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Inorout</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Inorout</em>' attribute.
   * @see #setInorout(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabel_Inorout()
   * @model
   * @generated
   */
  String getInorout();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#getInorout <em>Inorout</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Inorout</em>' attribute.
   * @see #getInorout()
   * @generated
   */
  void setInorout(String value);

  /**
   * Returns the value of the '<em><b>Expire</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expire</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expire</em>' containment reference.
   * @see #setExpire(ddLabelExpire)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabel_Expire()
   * @model containment="true"
   * @generated
   */
  ddLabelExpire getExpire();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel#getExpire <em>Expire</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expire</em>' containment reference.
   * @see #getExpire()
   * @generated
   */
  void setExpire(ddLabelExpire value);

} // ddLabel
