/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Rls</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddRls#getRls <em>Rls</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddRls()
 * @model
 * @generated
 */
public interface ddRls extends ddsection
{
  /**
   * Returns the value of the '<em><b>Rls</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rls</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rls</em>' attribute.
   * @see #setRls(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddRls_Rls()
   * @model
   * @generated
   */
  String getRls();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddRls#getRls <em>Rls</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Rls</em>' attribute.
   * @see #getRls()
   * @generated
   */
  void setRls(String value);

} // ddRls
