/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Subsys</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSubsys#getLabel <em>Label</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSubsys#getSubsys <em>Subsys</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSubsys#getSubparm <em>Subparm</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSubsys()
 * @model
 * @generated
 */
public interface ddSubsys extends ddsection
{
  /**
   * Returns the value of the '<em><b>Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Label</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Label</em>' attribute.
   * @see #setLabel(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSubsys_Label()
   * @model
   * @generated
   */
  String getLabel();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsys#getLabel <em>Label</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Label</em>' attribute.
   * @see #getLabel()
   * @generated
   */
  void setLabel(String value);

  /**
   * Returns the value of the '<em><b>Subsys</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subsys</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subsys</em>' attribute.
   * @see #setSubsys(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSubsys_Subsys()
   * @model
   * @generated
   */
  String getSubsys();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsys#getSubsys <em>Subsys</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Subsys</em>' attribute.
   * @see #getSubsys()
   * @generated
   */
  void setSubsys(String value);

  /**
   * Returns the value of the '<em><b>Subparm</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subparm</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subparm</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSubsys_Subparm()
   * @model containment="true"
   * @generated
   */
  EList<ddSubsysSubparms> getSubparm();

} // ddSubsys
