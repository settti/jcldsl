/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EXEC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getEXECNAME <em>EXECNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getSection <em>Section</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getPROCEDURE <em>PROCEDURE</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getSections <em>Sections</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getDds <em>Dds</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getEXEC()
 * @model
 * @generated
 */
public interface EXEC extends Keyword
{
  /**
   * Returns the value of the '<em><b>EXECNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>EXECNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>EXECNAME</em>' attribute.
   * @see #setEXECNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getEXEC_EXECNAME()
   * @model
   * @generated
   */
  String getEXECNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getEXECNAME <em>EXECNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>EXECNAME</em>' attribute.
   * @see #getEXECNAME()
   * @generated
   */
  void setEXECNAME(String value);

  /**
   * Returns the value of the '<em><b>Section</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.execsection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Section</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Section</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getEXEC_Section()
   * @model containment="true"
   * @generated
   */
  EList<execsection> getSection();

  /**
   * Returns the value of the '<em><b>PROCEDURE</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>PROCEDURE</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>PROCEDURE</em>' attribute.
   * @see #setPROCEDURE(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getEXEC_PROCEDURE()
   * @model
   * @generated
   */
  String getPROCEDURE();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.EXEC#getPROCEDURE <em>PROCEDURE</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>PROCEDURE</em>' attribute.
   * @see #getPROCEDURE()
   * @generated
   */
  void setPROCEDURE(String value);

  /**
   * Returns the value of the '<em><b>Sections</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.execsection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sections</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sections</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getEXEC_Sections()
   * @model containment="true"
   * @generated
   */
  EList<execsection> getSections();

  /**
   * Returns the value of the '<em><b>Dds</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.DD}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dds</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dds</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getEXEC_Dds()
   * @model containment="true"
   * @generated
   */
  EList<DD> getDds();

} // EXEC
