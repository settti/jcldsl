/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Schenv</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobSchenv#getENVIRONMENT <em>ENVIRONMENT</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobSchenv()
 * @model
 * @generated
 */
public interface jobSchenv extends jobsection
{
  /**
   * Returns the value of the '<em><b>ENVIRONMENT</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>ENVIRONMENT</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>ENVIRONMENT</em>' attribute.
   * @see #setENVIRONMENT(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobSchenv_ENVIRONMENT()
   * @model
   * @generated
   */
  String getENVIRONMENT();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobSchenv#getENVIRONMENT <em>ENVIRONMENT</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>ENVIRONMENT</em>' attribute.
   * @see #getENVIRONMENT()
   * @generated
   */
  void setENVIRONMENT(String value);

} // jobSchenv
