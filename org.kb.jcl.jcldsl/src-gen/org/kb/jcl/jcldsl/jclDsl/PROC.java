/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.PROC#isPROCNAME <em>PROCNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.PROC#getPARMNAME <em>PARMNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.PROC#getParm_value <em>Parm value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.PROC#getDSNAMES <em>DSNAMES</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.PROC#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getPROC()
 * @model
 * @generated
 */
public interface PROC extends Keyword
{
  /**
   * Returns the value of the '<em><b>PROCNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>PROCNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>PROCNAME</em>' attribute.
   * @see #setPROCNAME(boolean)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getPROC_PROCNAME()
   * @model
   * @generated
   */
  boolean isPROCNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.PROC#isPROCNAME <em>PROCNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>PROCNAME</em>' attribute.
   * @see #isPROCNAME()
   * @generated
   */
  void setPROCNAME(boolean value);

  /**
   * Returns the value of the '<em><b>PARMNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>PARMNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>PARMNAME</em>' attribute.
   * @see #setPARMNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getPROC_PARMNAME()
   * @model
   * @generated
   */
  String getPARMNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.PROC#getPARMNAME <em>PARMNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>PARMNAME</em>' attribute.
   * @see #getPARMNAME()
   * @generated
   */
  void setPARMNAME(String value);

  /**
   * Returns the value of the '<em><b>Parm value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm value</em>' attribute.
   * @see #setParm_value(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getPROC_Parm_value()
   * @model
   * @generated
   */
  String getParm_value();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.PROC#getParm_value <em>Parm value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parm value</em>' attribute.
   * @see #getParm_value()
   * @generated
   */
  void setParm_value(String value);

  /**
   * Returns the value of the '<em><b>DSNAMES</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>DSNAMES</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>DSNAMES</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getPROC_DSNAMES()
   * @model unique="false"
   * @generated
   */
  EList<String> getDSNAMES();

  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getPROC_Value()
   * @model unique="false"
   * @generated
   */
  EList<String> getValue();

} // PROC
