/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>exec Timevalue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execTimevalue#getMinutes <em>Minutes</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execTimevalue#isSeconds <em>Seconds</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecTimevalue()
 * @model
 * @generated
 */
public interface execTimevalue extends EObject
{
  /**
   * Returns the value of the '<em><b>Minutes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Minutes</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Minutes</em>' attribute.
   * @see #setMinutes(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecTimevalue_Minutes()
   * @model
   * @generated
   */
  int getMinutes();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execTimevalue#getMinutes <em>Minutes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Minutes</em>' attribute.
   * @see #getMinutes()
   * @generated
   */
  void setMinutes(int value);

  /**
   * Returns the value of the '<em><b>Seconds</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Seconds</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Seconds</em>' attribute.
   * @see #setSeconds(boolean)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecTimevalue_Seconds()
   * @model
   * @generated
   */
  boolean isSeconds();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execTimevalue#isSeconds <em>Seconds</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Seconds</em>' attribute.
   * @see #isSeconds()
   * @generated
   */
  void setSeconds(boolean value);

} // execTimevalue
