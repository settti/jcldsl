/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Pathmode</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddPathmode#getOption <em>Option</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddPathmode#getOption2 <em>Option2</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPathmode()
 * @model
 * @generated
 */
public interface ddPathmode extends ddsection
{
  /**
   * Returns the value of the '<em><b>Option</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Option</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Option</em>' attribute.
   * @see #setOption(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPathmode_Option()
   * @model
   * @generated
   */
  String getOption();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddPathmode#getOption <em>Option</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Option</em>' attribute.
   * @see #getOption()
   * @generated
   */
  void setOption(String value);

  /**
   * Returns the value of the '<em><b>Option2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Option2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Option2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPathmode_Option2()
   * @model unique="false"
   * @generated
   */
  EList<String> getOption2();

} // ddPathmode
