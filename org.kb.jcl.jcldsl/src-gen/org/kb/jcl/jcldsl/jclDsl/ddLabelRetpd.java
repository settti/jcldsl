/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Label Retpd</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLabelRetpd#getNnnn <em>Nnnn</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabelRetpd()
 * @model
 * @generated
 */
public interface ddLabelRetpd extends ddLabelExpire
{
  /**
   * Returns the value of the '<em><b>Nnnn</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nnnn</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nnnn</em>' attribute.
   * @see #setNnnn(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabelRetpd_Nnnn()
   * @model
   * @generated
   */
  int getNnnn();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelRetpd#getNnnn <em>Nnnn</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nnnn</em>' attribute.
   * @see #getNnnn()
   * @generated
   */
  void setNnnn(int value);

} // ddLabelRetpd
