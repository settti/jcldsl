/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Ucs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddUcs#getCharset <em>Charset</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddUcs#getFold <em>Fold</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddUcs#getVerify <em>Verify</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddUcs()
 * @model
 * @generated
 */
public interface ddUcs extends ddsection
{
  /**
   * Returns the value of the '<em><b>Charset</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Charset</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Charset</em>' attribute.
   * @see #setCharset(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddUcs_Charset()
   * @model
   * @generated
   */
  String getCharset();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddUcs#getCharset <em>Charset</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Charset</em>' attribute.
   * @see #getCharset()
   * @generated
   */
  void setCharset(String value);

  /**
   * Returns the value of the '<em><b>Fold</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fold</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fold</em>' attribute.
   * @see #setFold(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddUcs_Fold()
   * @model
   * @generated
   */
  String getFold();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddUcs#getFold <em>Fold</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fold</em>' attribute.
   * @see #getFold()
   * @generated
   */
  void setFold(String value);

  /**
   * Returns the value of the '<em><b>Verify</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Verify</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Verify</em>' attribute.
   * @see #setVerify(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddUcs_Verify()
   * @model
   * @generated
   */
  String getVerify();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddUcs#getVerify <em>Verify</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Verify</em>' attribute.
   * @see #getVerify()
   * @generated
   */
  void setVerify(String value);

} // ddUcs
