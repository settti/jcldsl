/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Pci</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddPci#getParm <em>Parm</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddPci#getParm2 <em>Parm2</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPci()
 * @model
 * @generated
 */
public interface ddPci extends ddsection, ddDcbSubparms
{
  /**
   * Returns the value of the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm</em>' attribute.
   * @see #setParm(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPci_Parm()
   * @model
   * @generated
   */
  String getParm();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddPci#getParm <em>Parm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parm</em>' attribute.
   * @see #getParm()
   * @generated
   */
  void setParm(String value);

  /**
   * Returns the value of the '<em><b>Parm2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm2</em>' attribute.
   * @see #setParm2(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPci_Parm2()
   * @model
   * @generated
   */
  String getParm2();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddPci#getParm2 <em>Parm2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parm2</em>' attribute.
   * @see #getParm2()
   * @generated
   */
  void setParm2(String value);

} // ddPci
