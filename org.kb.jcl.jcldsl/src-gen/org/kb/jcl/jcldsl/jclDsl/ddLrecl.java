/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Lrecl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLrecl#getLength_in_bytes <em>Length in bytes</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLrecl()
 * @model
 * @generated
 */
public interface ddLrecl extends ddsection, ddDcbSubparms
{
  /**
   * Returns the value of the '<em><b>Length in bytes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Length in bytes</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Length in bytes</em>' attribute.
   * @see #setLength_in_bytes(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLrecl_Length_in_bytes()
   * @model
   * @generated
   */
  int getLength_in_bytes();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLrecl#getLength_in_bytes <em>Length in bytes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Length in bytes</em>' attribute.
   * @see #getLength_in_bytes()
   * @generated
   */
  void setLength_in_bytes(int value);

} // ddLrecl
