/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage
 * @generated
 */
public interface JclDslFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  JclDslFactory eINSTANCE = org.kb.jcl.jcldsl.jclDsl.impl.JclDslFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Model</em>'.
   * @generated
   */
  Model createModel();

  /**
   * Returns a new object of class '<em>Keyword</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Keyword</em>'.
   * @generated
   */
  Keyword createKeyword();

  /**
   * Returns a new object of class '<em>STEPCATDD</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>STEPCATDD</em>'.
   * @generated
   */
  STEPCATDD createSTEPCATDD();

  /**
   * Returns a new object of class '<em>JOBLIBDD</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>JOBLIBDD</em>'.
   * @generated
   */
  JOBLIBDD createJOBLIBDD();

  /**
   * Returns a new object of class '<em>JOBCATDD</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>JOBCATDD</em>'.
   * @generated
   */
  JOBCATDD createJOBCATDD();

  /**
   * Returns a new object of class '<em>specdd Section</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>specdd Section</em>'.
   * @generated
   */
  specddSection createspecddSection();

  /**
   * Returns a new object of class '<em>specdd Disp</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>specdd Disp</em>'.
   * @generated
   */
  specddDisp createspecddDisp();

  /**
   * Returns a new object of class '<em>specdd Dsname</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>specdd Dsname</em>'.
   * @generated
   */
  specddDsname createspecddDsname();

  /**
   * Returns a new object of class '<em>IF</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>IF</em>'.
   * @generated
   */
  IF createIF();

  /**
   * Returns a new object of class '<em>ELSE</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>ELSE</em>'.
   * @generated
   */
  ELSE createELSE();

  /**
   * Returns a new object of class '<em>ENDIF</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>ENDIF</em>'.
   * @generated
   */
  ENDIF createENDIF();

  /**
   * Returns a new object of class '<em>PROC</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>PROC</em>'.
   * @generated
   */
  PROC createPROC();

  /**
   * Returns a new object of class '<em>PEND</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>PEND</em>'.
   * @generated
   */
  PEND createPEND();

  /**
   * Returns a new object of class '<em>SET</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>SET</em>'.
   * @generated
   */
  SET createSET();

  /**
   * Returns a new object of class '<em>JCLLIB</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>JCLLIB</em>'.
   * @generated
   */
  JCLLIB createJCLLIB();

  /**
   * Returns a new object of class '<em>EXEC</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>EXEC</em>'.
   * @generated
   */
  EXEC createEXEC();

  /**
   * Returns a new object of class '<em>execsection</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>execsection</em>'.
   * @generated
   */
  execsection createexecsection();

  /**
   * Returns a new object of class '<em>exec Time</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Time</em>'.
   * @generated
   */
  execTime createexecTime();

  /**
   * Returns a new object of class '<em>exec Timevalue</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Timevalue</em>'.
   * @generated
   */
  execTimevalue createexecTimevalue();

  /**
   * Returns a new object of class '<em>exec Perform</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Perform</em>'.
   * @generated
   */
  execPerform createexecPerform();

  /**
   * Returns a new object of class '<em>exec Region</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Region</em>'.
   * @generated
   */
  execRegion createexecRegion();

  /**
   * Returns a new object of class '<em>exec Rd</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Rd</em>'.
   * @generated
   */
  execRd createexecRd();

  /**
   * Returns a new object of class '<em>exec Parm</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Parm</em>'.
   * @generated
   */
  execParm createexecParm();

  /**
   * Returns a new object of class '<em>exec Dynambr</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Dynambr</em>'.
   * @generated
   */
  execDynambr createexecDynambr();

  /**
   * Returns a new object of class '<em>exec Cond</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Cond</em>'.
   * @generated
   */
  execCond createexecCond();

  /**
   * Returns a new object of class '<em>exec Addrspc</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Addrspc</em>'.
   * @generated
   */
  execAddrspc createexecAddrspc();

  /**
   * Returns a new object of class '<em>exec Ccsid</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Ccsid</em>'.
   * @generated
   */
  execCcsid createexecCcsid();

  /**
   * Returns a new object of class '<em>exec Acct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Acct</em>'.
   * @generated
   */
  execAcct createexecAcct();

  /**
   * Returns a new object of class '<em>exec Proc</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Proc</em>'.
   * @generated
   */
  execProc createexecProc();

  /**
   * Returns a new object of class '<em>exec Pgm</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exec Pgm</em>'.
   * @generated
   */
  execPgm createexecPgm();

  /**
   * Returns a new object of class '<em>INCLUDE</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>INCLUDE</em>'.
   * @generated
   */
  INCLUDE createINCLUDE();

  /**
   * Returns a new object of class '<em>JOB</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>JOB</em>'.
   * @generated
   */
  JOB createJOB();

  /**
   * Returns a new object of class '<em>jobsection</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>jobsection</em>'.
   * @generated
   */
  jobsection createjobsection();

  /**
   * Returns a new object of class '<em>job User</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job User</em>'.
   * @generated
   */
  jobUser createjobUser();

  /**
   * Returns a new object of class '<em>job Typrun</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Typrun</em>'.
   * @generated
   */
  jobTyprun createjobTyprun();

  /**
   * Returns a new object of class '<em>job Time</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Time</em>'.
   * @generated
   */
  jobTime createjobTime();

  /**
   * Returns a new object of class '<em>job Timevalue</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Timevalue</em>'.
   * @generated
   */
  jobTimevalue createjobTimevalue();

  /**
   * Returns a new object of class '<em>job Schenv</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Schenv</em>'.
   * @generated
   */
  jobSchenv createjobSchenv();

  /**
   * Returns a new object of class '<em>job Seclabel</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Seclabel</em>'.
   * @generated
   */
  jobSeclabel createjobSeclabel();

  /**
   * Returns a new object of class '<em>job Restart</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Restart</em>'.
   * @generated
   */
  jobRestart createjobRestart();

  /**
   * Returns a new object of class '<em>job Region</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Region</em>'.
   * @generated
   */
  jobRegion createjobRegion();

  /**
   * Returns a new object of class '<em>job Rd</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Rd</em>'.
   * @generated
   */
  jobRd createjobRd();

  /**
   * Returns a new object of class '<em>job Prty</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Prty</em>'.
   * @generated
   */
  jobPrty createjobPrty();

  /**
   * Returns a new object of class '<em>job Progname</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Progname</em>'.
   * @generated
   */
  jobProgname createjobProgname();

  /**
   * Returns a new object of class '<em>job Perform</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Perform</em>'.
   * @generated
   */
  jobPerform createjobPerform();

  /**
   * Returns a new object of class '<em>job Password</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Password</em>'.
   * @generated
   */
  jobPassword createjobPassword();

  /**
   * Returns a new object of class '<em>job Pages</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Pages</em>'.
   * @generated
   */
  jobPages createjobPages();

  /**
   * Returns a new object of class '<em>job Notify</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Notify</em>'.
   * @generated
   */
  jobNotify createjobNotify();

  /**
   * Returns a new object of class '<em>job Msglevel</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Msglevel</em>'.
   * @generated
   */
  jobMsglevel createjobMsglevel();

  /**
   * Returns a new object of class '<em>job Msg Class</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Msg Class</em>'.
   * @generated
   */
  jobMsgClass createjobMsgClass();

  /**
   * Returns a new object of class '<em>job Lines</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Lines</em>'.
   * @generated
   */
  jobLines createjobLines();

  /**
   * Returns a new object of class '<em>job Group</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Group</em>'.
   * @generated
   */
  jobGroup createjobGroup();

  /**
   * Returns a new object of class '<em>job Cond</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Cond</em>'.
   * @generated
   */
  jobCond createjobCond();

  /**
   * Returns a new object of class '<em>job Class</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Class</em>'.
   * @generated
   */
  jobClass createjobClass();

  /**
   * Returns a new object of class '<em>job Ccsid</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Ccsid</em>'.
   * @generated
   */
  jobCcsid createjobCcsid();

  /**
   * Returns a new object of class '<em>job Cards</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Cards</em>'.
   * @generated
   */
  jobCards createjobCards();

  /**
   * Returns a new object of class '<em>job Bytes</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Bytes</em>'.
   * @generated
   */
  jobBytes createjobBytes();

  /**
   * Returns a new object of class '<em>job Addrspc</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Addrspc</em>'.
   * @generated
   */
  jobAddrspc createjobAddrspc();

  /**
   * Returns a new object of class '<em>job Accinfo</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>job Accinfo</em>'.
   * @generated
   */
  jobAccinfo createjobAccinfo();

  /**
   * Returns a new object of class '<em>DD</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>DD</em>'.
   * @generated
   */
  DD createDD();

  /**
   * Returns a new object of class '<em>ddsection</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>ddsection</em>'.
   * @generated
   */
  ddsection createddsection();

  /**
   * Returns a new object of class '<em>dd Volume</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Volume</em>'.
   * @generated
   */
  ddVolume createddVolume();

  /**
   * Returns a new object of class '<em>dd Unit</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Unit</em>'.
   * @generated
   */
  ddUnit createddUnit();

  /**
   * Returns a new object of class '<em>dd Ucs</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Ucs</em>'.
   * @generated
   */
  ddUcs createddUcs();

  /**
   * Returns a new object of class '<em>dd Term</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Term</em>'.
   * @generated
   */
  ddTerm createddTerm();

  /**
   * Returns a new object of class '<em>dd Sysout</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Sysout</em>'.
   * @generated
   */
  ddSysout createddSysout();

  /**
   * Returns a new object of class '<em>dd Subsys</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Subsys</em>'.
   * @generated
   */
  ddSubsys createddSubsys();

  /**
   * Returns a new object of class '<em>dd Subsys Subparms</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Subsys Subparms</em>'.
   * @generated
   */
  ddSubsysSubparms createddSubsysSubparms();

  /**
   * Returns a new object of class '<em>dd Storclas</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Storclas</em>'.
   * @generated
   */
  ddStorclas createddStorclas();

  /**
   * Returns a new object of class '<em>dd Spin</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Spin</em>'.
   * @generated
   */
  ddSpin createddSpin();

  /**
   * Returns a new object of class '<em>dd Space</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Space</em>'.
   * @generated
   */
  ddSpace createddSpace();

  /**
   * Returns a new object of class '<em>dd Segment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Segment</em>'.
   * @generated
   */
  ddSegment createddSegment();

  /**
   * Returns a new object of class '<em>dd Secmodel</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Secmodel</em>'.
   * @generated
   */
  ddSecmodel createddSecmodel();

  /**
   * Returns a new object of class '<em>dd Rls</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Rls</em>'.
   * @generated
   */
  ddRls createddRls();

  /**
   * Returns a new object of class '<em>dd Retpd</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Retpd</em>'.
   * @generated
   */
  ddRetpd createddRetpd();

  /**
   * Returns a new object of class '<em>dd Refdd</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Refdd</em>'.
   * @generated
   */
  ddRefdd createddRefdd();

  /**
   * Returns a new object of class '<em>dd Recorg</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Recorg</em>'.
   * @generated
   */
  ddRecorg createddRecorg();

  /**
   * Returns a new object of class '<em>dd Recfm</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Recfm</em>'.
   * @generated
   */
  ddRecfm createddRecfm();

  /**
   * Returns a new object of class '<em>dd Qname</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Qname</em>'.
   * @generated
   */
  ddQname createddQname();

  /**
   * Returns a new object of class '<em>dd Protect</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Protect</em>'.
   * @generated
   */
  ddProtect createddProtect();

  /**
   * Returns a new object of class '<em>dd Pathmode</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Pathmode</em>'.
   * @generated
   */
  ddPathmode createddPathmode();

  /**
   * Returns a new object of class '<em>dd Pathopts</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Pathopts</em>'.
   * @generated
   */
  ddPathopts createddPathopts();

  /**
   * Returns a new object of class '<em>dd Pathdisp</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Pathdisp</em>'.
   * @generated
   */
  ddPathdisp createddPathdisp();

  /**
   * Returns a new object of class '<em>dd Path</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Path</em>'.
   * @generated
   */
  ddPath createddPath();

  /**
   * Returns a new object of class '<em>dd Output</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Output</em>'.
   * @generated
   */
  ddOutput createddOutput();

  /**
   * Returns a new object of class '<em>dd Outlim</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Outlim</em>'.
   * @generated
   */
  ddOutlim createddOutlim();

  /**
   * Returns a new object of class '<em>dd Modify</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Modify</em>'.
   * @generated
   */
  ddModify createddModify();

  /**
   * Returns a new object of class '<em>dd Mgmtclas</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Mgmtclas</em>'.
   * @generated
   */
  ddMgmtclas createddMgmtclas();

  /**
   * Returns a new object of class '<em>dd Lrecl</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Lrecl</em>'.
   * @generated
   */
  ddLrecl createddLrecl();

  /**
   * Returns a new object of class '<em>dd Like</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Like</em>'.
   * @generated
   */
  ddLike createddLike();

  /**
   * Returns a new object of class '<em>dd Lgstream</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Lgstream</em>'.
   * @generated
   */
  ddLgstream createddLgstream();

  /**
   * Returns a new object of class '<em>dd Label</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Label</em>'.
   * @generated
   */
  ddLabel createddLabel();

  /**
   * Returns a new object of class '<em>dd Label Expire</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Label Expire</em>'.
   * @generated
   */
  ddLabelExpire createddLabelExpire();

  /**
   * Returns a new object of class '<em>dd Label Expdt</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Label Expdt</em>'.
   * @generated
   */
  ddLabelExpdt createddLabelExpdt();

  /**
   * Returns a new object of class '<em>dd Label Retpd</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Label Retpd</em>'.
   * @generated
   */
  ddLabelRetpd createddLabelRetpd();

  /**
   * Returns a new object of class '<em>dd Keyoff</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Keyoff</em>'.
   * @generated
   */
  ddKeyoff createddKeyoff();

  /**
   * Returns a new object of class '<em>dd Keylen</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Keylen</em>'.
   * @generated
   */
  ddKeylen createddKeylen();

  /**
   * Returns a new object of class '<em>dd Hold</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Hold</em>'.
   * @generated
   */
  ddHold createddHold();

  /**
   * Returns a new object of class '<em>dd Free</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Free</em>'.
   * @generated
   */
  ddFree createddFree();

  /**
   * Returns a new object of class '<em>dd Flash</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Flash</em>'.
   * @generated
   */
  ddFlash createddFlash();

  /**
   * Returns a new object of class '<em>dd Filedata</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Filedata</em>'.
   * @generated
   */
  ddFiledata createddFiledata();

  /**
   * Returns a new object of class '<em>dd Fcb</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Fcb</em>'.
   * @generated
   */
  ddFcb createddFcb();

  /**
   * Returns a new object of class '<em>dd Expdt</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Expdt</em>'.
   * @generated
   */
  ddExpdt createddExpdt();

  /**
   * Returns a new object of class '<em>dd Dsorg</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Dsorg</em>'.
   * @generated
   */
  ddDsorg createddDsorg();

  /**
   * Returns a new object of class '<em>dd Dsname</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Dsname</em>'.
   * @generated
   */
  ddDsname createddDsname();

  /**
   * Returns a new object of class '<em>dd Dsntype</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Dsntype</em>'.
   * @generated
   */
  ddDsntype createddDsntype();

  /**
   * Returns a new object of class '<em>dd Dsid</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Dsid</em>'.
   * @generated
   */
  ddDsid createddDsid();

  /**
   * Returns a new object of class '<em>dd Dlm</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Dlm</em>'.
   * @generated
   */
  ddDlm createddDlm();

  /**
   * Returns a new object of class '<em>dd Disp</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Disp</em>'.
   * @generated
   */
  ddDisp createddDisp();

  /**
   * Returns a new object of class '<em>dd Dest</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Dest</em>'.
   * @generated
   */
  ddDest createddDest();

  /**
   * Returns a new object of class '<em>dd Ddname</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Ddname</em>'.
   * @generated
   */
  ddDdname createddDdname();

  /**
   * Returns a new object of class '<em>dd Dcb</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Dcb</em>'.
   * @generated
   */
  ddDcb createddDcb();

  /**
   * Returns a new object of class '<em>dd Dcb Subparms</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Dcb Subparms</em>'.
   * @generated
   */
  ddDcbSubparms createddDcbSubparms();

  /**
   * Returns a new object of class '<em>dd Trtch</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Trtch</em>'.
   * @generated
   */
  ddTrtch createddTrtch();

  /**
   * Returns a new object of class '<em>dd Thresh</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Thresh</em>'.
   * @generated
   */
  ddThresh createddThresh();

  /**
   * Returns a new object of class '<em>dd Stack</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Stack</em>'.
   * @generated
   */
  ddStack createddStack();

  /**
   * Returns a new object of class '<em>dd Rkp</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Rkp</em>'.
   * @generated
   */
  ddRkp createddRkp();

  /**
   * Returns a new object of class '<em>dd Reserve</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Reserve</em>'.
   * @generated
   */
  ddReserve createddReserve();

  /**
   * Returns a new object of class '<em>dd Prtsp</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Prtsp</em>'.
   * @generated
   */
  ddPrtsp createddPrtsp();

  /**
   * Returns a new object of class '<em>dd Pci</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Pci</em>'.
   * @generated
   */
  ddPci createddPci();

  /**
   * Returns a new object of class '<em>dd Optcd</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Optcd</em>'.
   * @generated
   */
  ddOptcd createddOptcd();

  /**
   * Returns a new object of class '<em>dd Ntm</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Ntm</em>'.
   * @generated
   */
  ddNtm createddNtm();

  /**
   * Returns a new object of class '<em>dd Ncp</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Ncp</em>'.
   * @generated
   */
  ddNcp createddNcp();

  /**
   * Returns a new object of class '<em>dd Mode</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Mode</em>'.
   * @generated
   */
  ddMode createddMode();

  /**
   * Returns a new object of class '<em>dd Limct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Limct</em>'.
   * @generated
   */
  ddLimct createddLimct();

  /**
   * Returns a new object of class '<em>dd Ipltxid</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Ipltxid</em>'.
   * @generated
   */
  ddIpltxid createddIpltxid();

  /**
   * Returns a new object of class '<em>dd Intvl</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Intvl</em>'.
   * @generated
   */
  ddIntvl createddIntvl();

  /**
   * Returns a new object of class '<em>dd Gncp</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Gncp</em>'.
   * @generated
   */
  ddGncp createddGncp();

  /**
   * Returns a new object of class '<em>dd Func</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Func</em>'.
   * @generated
   */
  ddFunc createddFunc();

  /**
   * Returns a new object of class '<em>dd Eropt</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Eropt</em>'.
   * @generated
   */
  ddEropt createddEropt();

  /**
   * Returns a new object of class '<em>dd Diagns</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Diagns</em>'.
   * @generated
   */
  ddDiagns createddDiagns();

  /**
   * Returns a new object of class '<em>dd Den</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Den</em>'.
   * @generated
   */
  ddDen createddDen();

  /**
   * Returns a new object of class '<em>dd Cylofl</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Cylofl</em>'.
   * @generated
   */
  ddCylofl createddCylofl();

  /**
   * Returns a new object of class '<em>dd Cpri</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Cpri</em>'.
   * @generated
   */
  ddCpri createddCpri();

  /**
   * Returns a new object of class '<em>dd Bufsize</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Bufsize</em>'.
   * @generated
   */
  ddBufsize createddBufsize();

  /**
   * Returns a new object of class '<em>dd Bufout</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Bufout</em>'.
   * @generated
   */
  ddBufout createddBufout();

  /**
   * Returns a new object of class '<em>dd Bufoff</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Bufoff</em>'.
   * @generated
   */
  ddBufoff createddBufoff();

  /**
   * Returns a new object of class '<em>dd Bufno</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Bufno</em>'.
   * @generated
   */
  ddBufno createddBufno();

  /**
   * Returns a new object of class '<em>dd Bufmax</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Bufmax</em>'.
   * @generated
   */
  ddBufmax createddBufmax();

  /**
   * Returns a new object of class '<em>dd Bufl</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Bufl</em>'.
   * @generated
   */
  ddBufl createddBufl();

  /**
   * Returns a new object of class '<em>dd Bufin</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Bufin</em>'.
   * @generated
   */
  ddBufin createddBufin();

  /**
   * Returns a new object of class '<em>dd Bftek</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Bftek</em>'.
   * @generated
   */
  ddBftek createddBftek();

  /**
   * Returns a new object of class '<em>dd Bfaln</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Bfaln</em>'.
   * @generated
   */
  ddBfaln createddBfaln();

  /**
   * Returns a new object of class '<em>dd Dataclas</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Dataclas</em>'.
   * @generated
   */
  ddDataclas createddDataclas();

  /**
   * Returns a new object of class '<em>dd Data</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Data</em>'.
   * @generated
   */
  ddData createddData();

  /**
   * Returns a new object of class '<em>dd Copies</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Copies</em>'.
   * @generated
   */
  ddCopies createddCopies();

  /**
   * Returns a new object of class '<em>dd Cntl</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Cntl</em>'.
   * @generated
   */
  ddCntl createddCntl();

  /**
   * Returns a new object of class '<em>dd Chkpt</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Chkpt</em>'.
   * @generated
   */
  ddChkpt createddChkpt();

  /**
   * Returns a new object of class '<em>dd Chars</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Chars</em>'.
   * @generated
   */
  ddChars createddChars();

  /**
   * Returns a new object of class '<em>dd Ccsid</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Ccsid</em>'.
   * @generated
   */
  ddCcsid createddCcsid();

  /**
   * Returns a new object of class '<em>dd Burst</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Burst</em>'.
   * @generated
   */
  ddBurst createddBurst();

  /**
   * Returns a new object of class '<em>dd Blkszlim</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Blkszlim</em>'.
   * @generated
   */
  ddBlkszlim createddBlkszlim();

  /**
   * Returns a new object of class '<em>dd Blksize</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Blksize</em>'.
   * @generated
   */
  ddBlksize createddBlksize();

  /**
   * Returns a new object of class '<em>dd Avgrec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Avgrec</em>'.
   * @generated
   */
  ddAvgrec createddAvgrec();

  /**
   * Returns a new object of class '<em>dd Amp</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Amp</em>'.
   * @generated
   */
  ddAmp createddAmp();

  /**
   * Returns a new object of class '<em>dd Accode</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>dd Accode</em>'.
   * @generated
   */
  ddAccode createddAccode();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  JclDslPackage getJclDslPackage();

} //JclDslFactory
