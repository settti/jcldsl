/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Accinfo</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getPano <em>Pano</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc <em>Acc</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc2 <em>Acc2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc3 <em>Acc3</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobAccinfo()
 * @model
 * @generated
 */
public interface jobAccinfo extends jobsection
{
  /**
   * Returns the value of the '<em><b>Pano</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pano</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pano</em>' attribute.
   * @see #setPano(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobAccinfo_Pano()
   * @model
   * @generated
   */
  String getPano();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getPano <em>Pano</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pano</em>' attribute.
   * @see #getPano()
   * @generated
   */
  void setPano(String value);

  /**
   * Returns the value of the '<em><b>Acc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Acc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Acc</em>' attribute.
   * @see #setAcc(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobAccinfo_Acc()
   * @model
   * @generated
   */
  int getAcc();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc <em>Acc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Acc</em>' attribute.
   * @see #getAcc()
   * @generated
   */
  void setAcc(int value);

  /**
   * Returns the value of the '<em><b>Acc2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Acc2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Acc2</em>' attribute.
   * @see #setAcc2(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobAccinfo_Acc2()
   * @model
   * @generated
   */
  String getAcc2();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc2 <em>Acc2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Acc2</em>' attribute.
   * @see #getAcc2()
   * @generated
   */
  void setAcc2(String value);

  /**
   * Returns the value of the '<em><b>Acc3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Acc3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Acc3</em>' attribute.
   * @see #setAcc3(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobAccinfo_Acc3()
   * @model
   * @generated
   */
  String getAcc3();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo#getAcc3 <em>Acc3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Acc3</em>' attribute.
   * @see #getAcc3()
   * @generated
   */
  void setAcc3(String value);

} // jobAccinfo
