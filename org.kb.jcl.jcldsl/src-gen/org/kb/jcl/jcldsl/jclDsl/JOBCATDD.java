/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JOBCATDD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.JOBCATDD#getSection <em>Section</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.JOBCATDD#getSections <em>Sections</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJOBCATDD()
 * @model
 * @generated
 */
public interface JOBCATDD extends Keyword
{
  /**
   * Returns the value of the '<em><b>Section</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.specddSection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Section</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Section</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJOBCATDD_Section()
   * @model containment="true"
   * @generated
   */
  EList<specddSection> getSection();

  /**
   * Returns the value of the '<em><b>Sections</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.specddSection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sections</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sections</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJOBCATDD_Sections()
   * @model containment="true"
   * @generated
   */
  EList<specddSection> getSections();

} // JOBCATDD
