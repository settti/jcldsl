/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Seclabel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobSeclabel#getSECURITYLABEL <em>SECURITYLABEL</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobSeclabel()
 * @model
 * @generated
 */
public interface jobSeclabel extends jobsection
{
  /**
   * Returns the value of the '<em><b>SECURITYLABEL</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>SECURITYLABEL</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>SECURITYLABEL</em>' attribute.
   * @see #setSECURITYLABEL(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobSeclabel_SECURITYLABEL()
   * @model
   * @generated
   */
  String getSECURITYLABEL();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobSeclabel#getSECURITYLABEL <em>SECURITYLABEL</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>SECURITYLABEL</em>' attribute.
   * @see #getSECURITYLABEL()
   * @generated
   */
  void setSECURITYLABEL(String value);

} // jobSeclabel
