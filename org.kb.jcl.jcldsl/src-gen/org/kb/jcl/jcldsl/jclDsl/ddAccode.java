/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Accode</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddAccode#getAccess <em>Access</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddAccode()
 * @model
 * @generated
 */
public interface ddAccode extends ddsection
{
  /**
   * Returns the value of the '<em><b>Access</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Access</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Access</em>' attribute.
   * @see #setAccess(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddAccode_Access()
   * @model
   * @generated
   */
  String getAccess();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddAccode#getAccess <em>Access</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Access</em>' attribute.
   * @see #getAccess()
   * @generated
   */
  void setAccess(String value);

} // ddAccode
