/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>job Rd Values</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobRdValues()
 * @model
 * @generated
 */
public enum jobRdValues implements Enumerator
{
  /**
   * The '<em><b>R</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #R_VALUE
   * @generated
   * @ordered
   */
  R(0, "R", "R"),

  /**
   * The '<em><b>RNC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #RNC_VALUE
   * @generated
   * @ordered
   */
  RNC(1, "RNC", "RNC"),

  /**
   * The '<em><b>NR</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #NR_VALUE
   * @generated
   * @ordered
   */
  NR(2, "NR", "NR"),

  /**
   * The '<em><b>NC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #NC_VALUE
   * @generated
   * @ordered
   */
  NC(3, "NC", "NC");

  /**
   * The '<em><b>R</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>R</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #R
   * @model
   * @generated
   * @ordered
   */
  public static final int R_VALUE = 0;

  /**
   * The '<em><b>RNC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>RNC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #RNC
   * @model
   * @generated
   * @ordered
   */
  public static final int RNC_VALUE = 1;

  /**
   * The '<em><b>NR</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>NR</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #NR
   * @model
   * @generated
   * @ordered
   */
  public static final int NR_VALUE = 2;

  /**
   * The '<em><b>NC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>NC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #NC
   * @model
   * @generated
   * @ordered
   */
  public static final int NC_VALUE = 3;

  /**
   * An array of all the '<em><b>job Rd Values</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final jobRdValues[] VALUES_ARRAY =
    new jobRdValues[]
    {
      R,
      RNC,
      NR,
      NC,
    };

  /**
   * A public read-only list of all the '<em><b>job Rd Values</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<jobRdValues> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>job Rd Values</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static jobRdValues get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      jobRdValues result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>job Rd Values</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static jobRdValues getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      jobRdValues result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>job Rd Values</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static jobRdValues get(int value)
  {
    switch (value)
    {
      case R_VALUE: return R;
      case RNC_VALUE: return RNC;
      case NR_VALUE: return NR;
      case NC_VALUE: return NC;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private jobRdValues(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //jobRdValues
