/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>dd Pathmode Opts</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPathmodeOpts()
 * @model
 * @generated
 */
public enum ddPathmodeOpts implements Enumerator
{
  /**
   * The '<em><b>SIRUSR</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIRUSR_VALUE
   * @generated
   * @ordered
   */
  SIRUSR(0, "SIRUSR", "SIRUSR"),

  /**
   * The '<em><b>SIWUSR</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIWUSR_VALUE
   * @generated
   * @ordered
   */
  SIWUSR(1, "SIWUSR", "SIWUSR"),

  /**
   * The '<em><b>SIXUSR</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIXUSR_VALUE
   * @generated
   * @ordered
   */
  SIXUSR(2, "SIXUSR", "SIXUSR"),

  /**
   * The '<em><b>SIRWXU</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIRWXU_VALUE
   * @generated
   * @ordered
   */
  SIRWXU(3, "SIRWXU", "SIRWXU"),

  /**
   * The '<em><b>SIRGRP</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIRGRP_VALUE
   * @generated
   * @ordered
   */
  SIRGRP(4, "SIRGRP", "SIRGRP"),

  /**
   * The '<em><b>SIWGRP</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIWGRP_VALUE
   * @generated
   * @ordered
   */
  SIWGRP(5, "SIWGRP", "SIWGRP"),

  /**
   * The '<em><b>SIXGRP</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIXGRP_VALUE
   * @generated
   * @ordered
   */
  SIXGRP(6, "SIXGRP", "SIXGRP"),

  /**
   * The '<em><b>SIRWXG</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIRWXG_VALUE
   * @generated
   * @ordered
   */
  SIRWXG(7, "SIRWXG", "SIRWXG"),

  /**
   * The '<em><b>SIROTH</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIROTH_VALUE
   * @generated
   * @ordered
   */
  SIROTH(8, "SIROTH", "SIROTH"),

  /**
   * The '<em><b>SIWOTH</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIWOTH_VALUE
   * @generated
   * @ordered
   */
  SIWOTH(9, "SIWOTH", "SIWOTH"),

  /**
   * The '<em><b>SIXOTH</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIXOTH_VALUE
   * @generated
   * @ordered
   */
  SIXOTH(10, "SIXOTH", "SIXOTH"),

  /**
   * The '<em><b>SIRWXO</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SIRWXO_VALUE
   * @generated
   * @ordered
   */
  SIRWXO(11, "SIRWXO", "SIRWXO"),

  /**
   * The '<em><b>SISUID</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SISUID_VALUE
   * @generated
   * @ordered
   */
  SISUID(12, "SISUID", "SISUID"),

  /**
   * The '<em><b>SISGID</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SISGID_VALUE
   * @generated
   * @ordered
   */
  SISGID(13, "SISGID", "SISGID");

  /**
   * The '<em><b>SIRUSR</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIRUSR</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIRUSR
   * @model
   * @generated
   * @ordered
   */
  public static final int SIRUSR_VALUE = 0;

  /**
   * The '<em><b>SIWUSR</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIWUSR</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIWUSR
   * @model
   * @generated
   * @ordered
   */
  public static final int SIWUSR_VALUE = 1;

  /**
   * The '<em><b>SIXUSR</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIXUSR</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIXUSR
   * @model
   * @generated
   * @ordered
   */
  public static final int SIXUSR_VALUE = 2;

  /**
   * The '<em><b>SIRWXU</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIRWXU</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIRWXU
   * @model
   * @generated
   * @ordered
   */
  public static final int SIRWXU_VALUE = 3;

  /**
   * The '<em><b>SIRGRP</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIRGRP</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIRGRP
   * @model
   * @generated
   * @ordered
   */
  public static final int SIRGRP_VALUE = 4;

  /**
   * The '<em><b>SIWGRP</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIWGRP</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIWGRP
   * @model
   * @generated
   * @ordered
   */
  public static final int SIWGRP_VALUE = 5;

  /**
   * The '<em><b>SIXGRP</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIXGRP</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIXGRP
   * @model
   * @generated
   * @ordered
   */
  public static final int SIXGRP_VALUE = 6;

  /**
   * The '<em><b>SIRWXG</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIRWXG</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIRWXG
   * @model
   * @generated
   * @ordered
   */
  public static final int SIRWXG_VALUE = 7;

  /**
   * The '<em><b>SIROTH</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIROTH</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIROTH
   * @model
   * @generated
   * @ordered
   */
  public static final int SIROTH_VALUE = 8;

  /**
   * The '<em><b>SIWOTH</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIWOTH</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIWOTH
   * @model
   * @generated
   * @ordered
   */
  public static final int SIWOTH_VALUE = 9;

  /**
   * The '<em><b>SIXOTH</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIXOTH</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIXOTH
   * @model
   * @generated
   * @ordered
   */
  public static final int SIXOTH_VALUE = 10;

  /**
   * The '<em><b>SIRWXO</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SIRWXO</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SIRWXO
   * @model
   * @generated
   * @ordered
   */
  public static final int SIRWXO_VALUE = 11;

  /**
   * The '<em><b>SISUID</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SISUID</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SISUID
   * @model
   * @generated
   * @ordered
   */
  public static final int SISUID_VALUE = 12;

  /**
   * The '<em><b>SISGID</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SISGID</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SISGID
   * @model
   * @generated
   * @ordered
   */
  public static final int SISGID_VALUE = 13;

  /**
   * An array of all the '<em><b>dd Pathmode Opts</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final ddPathmodeOpts[] VALUES_ARRAY =
    new ddPathmodeOpts[]
    {
      SIRUSR,
      SIWUSR,
      SIXUSR,
      SIRWXU,
      SIRGRP,
      SIWGRP,
      SIXGRP,
      SIRWXG,
      SIROTH,
      SIWOTH,
      SIXOTH,
      SIRWXO,
      SISUID,
      SISGID,
    };

  /**
   * A public read-only list of all the '<em><b>dd Pathmode Opts</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<ddPathmodeOpts> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>dd Pathmode Opts</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddPathmodeOpts get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      ddPathmodeOpts result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>dd Pathmode Opts</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddPathmodeOpts getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      ddPathmodeOpts result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>dd Pathmode Opts</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddPathmodeOpts get(int value)
  {
    switch (value)
    {
      case SIRUSR_VALUE: return SIRUSR;
      case SIWUSR_VALUE: return SIWUSR;
      case SIXUSR_VALUE: return SIXUSR;
      case SIRWXU_VALUE: return SIRWXU;
      case SIRGRP_VALUE: return SIRGRP;
      case SIWGRP_VALUE: return SIWGRP;
      case SIXGRP_VALUE: return SIXGRP;
      case SIRWXG_VALUE: return SIRWXG;
      case SIROTH_VALUE: return SIROTH;
      case SIWOTH_VALUE: return SIWOTH;
      case SIXOTH_VALUE: return SIXOTH;
      case SIRWXO_VALUE: return SIRWXO;
      case SISUID_VALUE: return SISUID;
      case SISGID_VALUE: return SISGID;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private ddPathmodeOpts(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //ddPathmodeOpts
