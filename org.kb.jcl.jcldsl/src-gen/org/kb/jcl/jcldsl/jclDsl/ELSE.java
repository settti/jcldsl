/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ELSE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ELSE#getELSENAME <em>ELSENAME</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getELSE()
 * @model
 * @generated
 */
public interface ELSE extends Keyword
{
  /**
   * Returns the value of the '<em><b>ELSENAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>ELSENAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>ELSENAME</em>' attribute.
   * @see #setELSENAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getELSE_ELSENAME()
   * @model
   * @generated
   */
  String getELSENAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ELSE#getELSENAME <em>ELSENAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>ELSENAME</em>' attribute.
   * @see #getELSENAME()
   * @generated
   */
  void setELSENAME(String value);

} // ELSE
