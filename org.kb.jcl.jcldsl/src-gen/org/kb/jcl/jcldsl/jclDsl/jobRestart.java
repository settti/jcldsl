/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Restart</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobRestart#getJOBSTEP <em>JOBSTEP</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobRestart()
 * @model
 * @generated
 */
public interface jobRestart extends jobsection
{
  /**
   * Returns the value of the '<em><b>JOBSTEP</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>JOBSTEP</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>JOBSTEP</em>' attribute.
   * @see #setJOBSTEP(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobRestart_JOBSTEP()
   * @model
   * @generated
   */
  String getJOBSTEP();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobRestart#getJOBSTEP <em>JOBSTEP</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>JOBSTEP</em>' attribute.
   * @see #getJOBSTEP()
   * @generated
   */
  void setJOBSTEP(String value);

} // jobRestart
