/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Qname</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddQname#getProcname <em>Procname</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddQname()
 * @model
 * @generated
 */
public interface ddQname extends ddsection
{
  /**
   * Returns the value of the '<em><b>Procname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Procname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Procname</em>' attribute.
   * @see #setProcname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddQname_Procname()
   * @model
   * @generated
   */
  String getProcname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddQname#getProcname <em>Procname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Procname</em>' attribute.
   * @see #getProcname()
   * @generated
   */
  void setProcname(String value);

} // ddQname
