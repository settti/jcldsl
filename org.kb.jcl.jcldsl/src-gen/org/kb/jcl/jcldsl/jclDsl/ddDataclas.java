/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Dataclas</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDataclas#getDataclass <em>Dataclass</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDataclas()
 * @model
 * @generated
 */
public interface ddDataclas extends ddsection
{
  /**
   * Returns the value of the '<em><b>Dataclass</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dataclass</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dataclass</em>' attribute.
   * @see #setDataclass(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDataclas_Dataclass()
   * @model
   * @generated
   */
  String getDataclass();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDataclas#getDataclass <em>Dataclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dataclass</em>' attribute.
   * @see #getDataclass()
   * @generated
   */
  void setDataclass(String value);

} // ddDataclas
