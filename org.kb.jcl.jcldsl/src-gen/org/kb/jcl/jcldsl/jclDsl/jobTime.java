/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Time</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobTime#getValue <em>Value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobTime#getJobTimevalue <em>Job Timevalue</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobTime()
 * @model
 * @generated
 */
public interface jobTime extends jobsection
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobTime_Value()
   * @model
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobTime#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

  /**
   * Returns the value of the '<em><b>Job Timevalue</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Job Timevalue</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Job Timevalue</em>' containment reference.
   * @see #setJobTimevalue(jobTimevalue)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobTime_JobTimevalue()
   * @model containment="true"
   * @generated
   */
  jobTimevalue getJobTimevalue();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobTime#getJobTimevalue <em>Job Timevalue</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Job Timevalue</em>' containment reference.
   * @see #getJobTimevalue()
   * @generated
   */
  void setJobTimevalue(jobTimevalue value);

} // jobTime
