/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Disp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDisp#getStatus <em>Status</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDisp#getNormal_disp <em>Normal disp</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDisp#getAbnormal_disp <em>Abnormal disp</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDisp()
 * @model
 * @generated
 */
public interface ddDisp extends ddsection
{
  /**
   * Returns the value of the '<em><b>Status</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Status</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Status</em>' attribute.
   * @see #setStatus(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDisp_Status()
   * @model
   * @generated
   */
  String getStatus();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDisp#getStatus <em>Status</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Status</em>' attribute.
   * @see #getStatus()
   * @generated
   */
  void setStatus(String value);

  /**
   * Returns the value of the '<em><b>Normal disp</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Normal disp</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Normal disp</em>' attribute.
   * @see #setNormal_disp(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDisp_Normal_disp()
   * @model
   * @generated
   */
  String getNormal_disp();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDisp#getNormal_disp <em>Normal disp</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Normal disp</em>' attribute.
   * @see #getNormal_disp()
   * @generated
   */
  void setNormal_disp(String value);

  /**
   * Returns the value of the '<em><b>Abnormal disp</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Abnormal disp</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Abnormal disp</em>' attribute.
   * @see #setAbnormal_disp(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDisp_Abnormal_disp()
   * @model
   * @generated
   */
  String getAbnormal_disp();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDisp#getAbnormal_disp <em>Abnormal disp</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Abnormal disp</em>' attribute.
   * @see #getAbnormal_disp()
   * @generated
   */
  void setAbnormal_disp(String value);

} // ddDisp
