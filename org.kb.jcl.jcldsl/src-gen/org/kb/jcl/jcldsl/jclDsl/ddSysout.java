/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Sysout</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSysout#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSysout#getWtrname <em>Wtrname</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSysout#getFormname <em>Formname</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSysout()
 * @model
 * @generated
 */
public interface ddSysout extends ddsection
{
  /**
   * Returns the value of the '<em><b>Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Class</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Class</em>' attribute.
   * @see #setClass(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSysout_Class()
   * @model
   * @generated
   */
  String getClass_();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSysout#getClass_ <em>Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Class</em>' attribute.
   * @see #getClass_()
   * @generated
   */
  void setClass(String value);

  /**
   * Returns the value of the '<em><b>Wtrname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Wtrname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Wtrname</em>' attribute.
   * @see #setWtrname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSysout_Wtrname()
   * @model
   * @generated
   */
  String getWtrname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSysout#getWtrname <em>Wtrname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Wtrname</em>' attribute.
   * @see #getWtrname()
   * @generated
   */
  void setWtrname(String value);

  /**
   * Returns the value of the '<em><b>Formname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Formname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Formname</em>' attribute.
   * @see #setFormname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSysout_Formname()
   * @model
   * @generated
   */
  String getFormname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSysout#getFormname <em>Formname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Formname</em>' attribute.
   * @see #getFormname()
   * @generated
   */
  void setFormname(String value);

} // ddSysout
