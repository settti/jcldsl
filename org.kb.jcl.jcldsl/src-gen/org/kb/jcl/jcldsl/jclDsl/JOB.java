/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JOB</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.JOB#getKeywords <em>Keywords</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.JOB#getJOBNAME <em>JOBNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.JOB#getSection <em>Section</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.JOB#getSections <em>Sections</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJOB()
 * @model
 * @generated
 */
public interface JOB extends Model
{
  /**
   * Returns the value of the '<em><b>Keywords</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.Keyword}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Keywords</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Keywords</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJOB_Keywords()
   * @model containment="true"
   * @generated
   */
  EList<Keyword> getKeywords();

  /**
   * Returns the value of the '<em><b>JOBNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>JOBNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>JOBNAME</em>' attribute.
   * @see #setJOBNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJOB_JOBNAME()
   * @model
   * @generated
   */
  String getJOBNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.JOB#getJOBNAME <em>JOBNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>JOBNAME</em>' attribute.
   * @see #getJOBNAME()
   * @generated
   */
  void setJOBNAME(String value);

  /**
   * Returns the value of the '<em><b>Section</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.jobsection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Section</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Section</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJOB_Section()
   * @model containment="true"
   * @generated
   */
  EList<jobsection> getSection();

  /**
   * Returns the value of the '<em><b>Sections</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.jobsection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sections</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sections</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getJOB_Sections()
   * @model containment="true"
   * @generated
   */
  EList<jobsection> getSections();

} // JOB
