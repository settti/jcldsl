/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Protect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddProtect#getYes <em>Yes</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddProtect()
 * @model
 * @generated
 */
public interface ddProtect extends ddsection
{
  /**
   * Returns the value of the '<em><b>Yes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Yes</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Yes</em>' attribute.
   * @see #setYes(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddProtect_Yes()
   * @model
   * @generated
   */
  String getYes();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddProtect#getYes <em>Yes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Yes</em>' attribute.
   * @see #getYes()
   * @generated
   */
  void setYes(String value);

} // ddProtect
