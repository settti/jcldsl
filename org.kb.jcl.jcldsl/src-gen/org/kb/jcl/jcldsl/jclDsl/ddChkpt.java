/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Chkpt</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddChkpt()
 * @model
 * @generated
 */
public interface ddChkpt extends ddsection
{
} // ddChkpt
