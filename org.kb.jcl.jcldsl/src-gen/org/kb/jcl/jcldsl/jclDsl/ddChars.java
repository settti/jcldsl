/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Chars</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddChars#getTablename <em>Tablename</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddChars#getThat <em>That</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddChars#getTablename2 <em>Tablename2</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddChars()
 * @model
 * @generated
 */
public interface ddChars extends ddsection
{
  /**
   * Returns the value of the '<em><b>Tablename</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tablename</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tablename</em>' attribute.
   * @see #setTablename(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddChars_Tablename()
   * @model
   * @generated
   */
  String getTablename();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddChars#getTablename <em>Tablename</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Tablename</em>' attribute.
   * @see #getTablename()
   * @generated
   */
  void setTablename(String value);

  /**
   * Returns the value of the '<em><b>That</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>That</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>That</em>' attribute.
   * @see #setThat(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddChars_That()
   * @model
   * @generated
   */
  String getThat();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddChars#getThat <em>That</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>That</em>' attribute.
   * @see #getThat()
   * @generated
   */
  void setThat(String value);

  /**
   * Returns the value of the '<em><b>Tablename2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tablename2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tablename2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddChars_Tablename2()
   * @model unique="false"
   * @generated
   */
  EList<String> getTablename2();

} // ddChars
