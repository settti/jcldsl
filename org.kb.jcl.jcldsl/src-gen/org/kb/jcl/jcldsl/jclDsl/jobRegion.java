/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Region</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobRegion#getRegion_size <em>Region size</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobRegion#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobRegion()
 * @model
 * @generated
 */
public interface jobRegion extends jobsection
{
  /**
   * Returns the value of the '<em><b>Region size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Region size</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Region size</em>' attribute.
   * @see #setRegion_size(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobRegion_Region_size()
   * @model
   * @generated
   */
  int getRegion_size();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobRegion#getRegion_size <em>Region size</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Region size</em>' attribute.
   * @see #getRegion_size()
   * @generated
   */
  void setRegion_size(int value);

  /**
   * Returns the value of the '<em><b>Size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Size</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Size</em>' attribute.
   * @see #setSize(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobRegion_Size()
   * @model
   * @generated
   */
  String getSize();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobRegion#getSize <em>Size</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Size</em>' attribute.
   * @see #getSize()
   * @generated
   */
  void setSize(String value);

} // jobRegion
