/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>exec Pgm</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execPgm#getPGMNAME <em>PGMNAME</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecPgm()
 * @model
 * @generated
 */
public interface execPgm extends execsection
{
  /**
   * Returns the value of the '<em><b>PGMNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>PGMNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>PGMNAME</em>' attribute.
   * @see #setPGMNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecPgm_PGMNAME()
   * @model
   * @generated
   */
  String getPGMNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execPgm#getPGMNAME <em>PGMNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>PGMNAME</em>' attribute.
   * @see #getPGMNAME()
   * @generated
   */
  void setPGMNAME(String value);

} // execPgm
