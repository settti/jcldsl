/**
 */
package org.kb.jcl.jcldsl.jclDsl.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.kb.jcl.jcldsl.jclDsl.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage
 * @generated
 */
public class JclDslSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static JclDslPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JclDslSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = JclDslPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case JclDslPackage.MODEL:
      {
        Model model = (Model)theEObject;
        T result = caseModel(model);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.KEYWORD:
      {
        Keyword keyword = (Keyword)theEObject;
        T result = caseKeyword(keyword);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.STEPCATDD:
      {
        STEPCATDD stepcatdd = (STEPCATDD)theEObject;
        T result = caseSTEPCATDD(stepcatdd);
        if (result == null) result = caseKeyword(stepcatdd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOBLIBDD:
      {
        JOBLIBDD joblibdd = (JOBLIBDD)theEObject;
        T result = caseJOBLIBDD(joblibdd);
        if (result == null) result = caseKeyword(joblibdd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOBCATDD:
      {
        JOBCATDD jobcatdd = (JOBCATDD)theEObject;
        T result = caseJOBCATDD(jobcatdd);
        if (result == null) result = caseKeyword(jobcatdd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.SPECDD_SECTION:
      {
        specddSection specddSection = (specddSection)theEObject;
        T result = casespecddSection(specddSection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.SPECDD_DISP:
      {
        specddDisp specddDisp = (specddDisp)theEObject;
        T result = casespecddDisp(specddDisp);
        if (result == null) result = casespecddSection(specddDisp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.SPECDD_DSNAME:
      {
        specddDsname specddDsname = (specddDsname)theEObject;
        T result = casespecddDsname(specddDsname);
        if (result == null) result = casespecddSection(specddDsname);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.IF:
      {
        IF if_ = (IF)theEObject;
        T result = caseIF(if_);
        if (result == null) result = caseKeyword(if_);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.ELSE:
      {
        ELSE else_ = (ELSE)theEObject;
        T result = caseELSE(else_);
        if (result == null) result = caseKeyword(else_);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.ENDIF:
      {
        ENDIF endif = (ENDIF)theEObject;
        T result = caseENDIF(endif);
        if (result == null) result = caseKeyword(endif);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.PROC:
      {
        PROC proc = (PROC)theEObject;
        T result = casePROC(proc);
        if (result == null) result = caseKeyword(proc);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.PEND:
      {
        PEND pend = (PEND)theEObject;
        T result = casePEND(pend);
        if (result == null) result = caseKeyword(pend);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.SET:
      {
        SET set = (SET)theEObject;
        T result = caseSET(set);
        if (result == null) result = caseKeyword(set);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JCLLIB:
      {
        JCLLIB jcllib = (JCLLIB)theEObject;
        T result = caseJCLLIB(jcllib);
        if (result == null) result = caseKeyword(jcllib);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC:
      {
        EXEC exec = (EXEC)theEObject;
        T result = caseEXEC(exec);
        if (result == null) result = caseKeyword(exec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXECSECTION:
      {
        execsection execsection = (execsection)theEObject;
        T result = caseexecsection(execsection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_TIME:
      {
        execTime execTime = (execTime)theEObject;
        T result = caseexecTime(execTime);
        if (result == null) result = caseexecsection(execTime);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_TIMEVALUE:
      {
        execTimevalue execTimevalue = (execTimevalue)theEObject;
        T result = caseexecTimevalue(execTimevalue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_PERFORM:
      {
        execPerform execPerform = (execPerform)theEObject;
        T result = caseexecPerform(execPerform);
        if (result == null) result = caseexecsection(execPerform);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_REGION:
      {
        execRegion execRegion = (execRegion)theEObject;
        T result = caseexecRegion(execRegion);
        if (result == null) result = caseexecsection(execRegion);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_RD:
      {
        execRd execRd = (execRd)theEObject;
        T result = caseexecRd(execRd);
        if (result == null) result = caseexecsection(execRd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_PARM:
      {
        execParm execParm = (execParm)theEObject;
        T result = caseexecParm(execParm);
        if (result == null) result = caseexecsection(execParm);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_DYNAMBR:
      {
        execDynambr execDynambr = (execDynambr)theEObject;
        T result = caseexecDynambr(execDynambr);
        if (result == null) result = caseexecsection(execDynambr);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_COND:
      {
        execCond execCond = (execCond)theEObject;
        T result = caseexecCond(execCond);
        if (result == null) result = caseexecsection(execCond);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_ADDRSPC:
      {
        execAddrspc execAddrspc = (execAddrspc)theEObject;
        T result = caseexecAddrspc(execAddrspc);
        if (result == null) result = caseexecsection(execAddrspc);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_CCSID:
      {
        execCcsid execCcsid = (execCcsid)theEObject;
        T result = caseexecCcsid(execCcsid);
        if (result == null) result = caseexecsection(execCcsid);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_ACCT:
      {
        execAcct execAcct = (execAcct)theEObject;
        T result = caseexecAcct(execAcct);
        if (result == null) result = caseexecsection(execAcct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_PROC:
      {
        execProc execProc = (execProc)theEObject;
        T result = caseexecProc(execProc);
        if (result == null) result = caseexecsection(execProc);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.EXEC_PGM:
      {
        execPgm execPgm = (execPgm)theEObject;
        T result = caseexecPgm(execPgm);
        if (result == null) result = caseexecsection(execPgm);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.INCLUDE:
      {
        INCLUDE include = (INCLUDE)theEObject;
        T result = caseINCLUDE(include);
        if (result == null) result = caseKeyword(include);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB:
      {
        JOB job = (JOB)theEObject;
        T result = caseJOB(job);
        if (result == null) result = caseModel(job);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOBSECTION:
      {
        jobsection jobsection = (jobsection)theEObject;
        T result = casejobsection(jobsection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_USER:
      {
        jobUser jobUser = (jobUser)theEObject;
        T result = casejobUser(jobUser);
        if (result == null) result = casejobsection(jobUser);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_TYPRUN:
      {
        jobTyprun jobTyprun = (jobTyprun)theEObject;
        T result = casejobTyprun(jobTyprun);
        if (result == null) result = casejobsection(jobTyprun);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_TIME:
      {
        jobTime jobTime = (jobTime)theEObject;
        T result = casejobTime(jobTime);
        if (result == null) result = casejobsection(jobTime);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_TIMEVALUE:
      {
        jobTimevalue jobTimevalue = (jobTimevalue)theEObject;
        T result = casejobTimevalue(jobTimevalue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_SCHENV:
      {
        jobSchenv jobSchenv = (jobSchenv)theEObject;
        T result = casejobSchenv(jobSchenv);
        if (result == null) result = casejobsection(jobSchenv);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_SECLABEL:
      {
        jobSeclabel jobSeclabel = (jobSeclabel)theEObject;
        T result = casejobSeclabel(jobSeclabel);
        if (result == null) result = casejobsection(jobSeclabel);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_RESTART:
      {
        jobRestart jobRestart = (jobRestart)theEObject;
        T result = casejobRestart(jobRestart);
        if (result == null) result = casejobsection(jobRestart);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_REGION:
      {
        jobRegion jobRegion = (jobRegion)theEObject;
        T result = casejobRegion(jobRegion);
        if (result == null) result = casejobsection(jobRegion);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_RD:
      {
        jobRd jobRd = (jobRd)theEObject;
        T result = casejobRd(jobRd);
        if (result == null) result = casejobsection(jobRd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_PRTY:
      {
        jobPrty jobPrty = (jobPrty)theEObject;
        T result = casejobPrty(jobPrty);
        if (result == null) result = casejobsection(jobPrty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_PROGNAME:
      {
        jobProgname jobProgname = (jobProgname)theEObject;
        T result = casejobProgname(jobProgname);
        if (result == null) result = casejobsection(jobProgname);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_PERFORM:
      {
        jobPerform jobPerform = (jobPerform)theEObject;
        T result = casejobPerform(jobPerform);
        if (result == null) result = casejobsection(jobPerform);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_PASSWORD:
      {
        jobPassword jobPassword = (jobPassword)theEObject;
        T result = casejobPassword(jobPassword);
        if (result == null) result = casejobsection(jobPassword);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_PAGES:
      {
        jobPages jobPages = (jobPages)theEObject;
        T result = casejobPages(jobPages);
        if (result == null) result = casejobsection(jobPages);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_NOTIFY:
      {
        jobNotify jobNotify = (jobNotify)theEObject;
        T result = casejobNotify(jobNotify);
        if (result == null) result = casejobsection(jobNotify);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_MSGLEVEL:
      {
        jobMsglevel jobMsglevel = (jobMsglevel)theEObject;
        T result = casejobMsglevel(jobMsglevel);
        if (result == null) result = casejobsection(jobMsglevel);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_MSG_CLASS:
      {
        jobMsgClass jobMsgClass = (jobMsgClass)theEObject;
        T result = casejobMsgClass(jobMsgClass);
        if (result == null) result = casejobsection(jobMsgClass);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_LINES:
      {
        jobLines jobLines = (jobLines)theEObject;
        T result = casejobLines(jobLines);
        if (result == null) result = casejobsection(jobLines);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_GROUP:
      {
        jobGroup jobGroup = (jobGroup)theEObject;
        T result = casejobGroup(jobGroup);
        if (result == null) result = casejobsection(jobGroup);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_COND:
      {
        jobCond jobCond = (jobCond)theEObject;
        T result = casejobCond(jobCond);
        if (result == null) result = casejobsection(jobCond);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_CLASS:
      {
        jobClass jobClass = (jobClass)theEObject;
        T result = casejobClass(jobClass);
        if (result == null) result = casejobsection(jobClass);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_CCSID:
      {
        jobCcsid jobCcsid = (jobCcsid)theEObject;
        T result = casejobCcsid(jobCcsid);
        if (result == null) result = casejobsection(jobCcsid);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_CARDS:
      {
        jobCards jobCards = (jobCards)theEObject;
        T result = casejobCards(jobCards);
        if (result == null) result = casejobsection(jobCards);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_BYTES:
      {
        jobBytes jobBytes = (jobBytes)theEObject;
        T result = casejobBytes(jobBytes);
        if (result == null) result = casejobsection(jobBytes);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_ADDRSPC:
      {
        jobAddrspc jobAddrspc = (jobAddrspc)theEObject;
        T result = casejobAddrspc(jobAddrspc);
        if (result == null) result = casejobsection(jobAddrspc);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.JOB_ACCINFO:
      {
        jobAccinfo jobAccinfo = (jobAccinfo)theEObject;
        T result = casejobAccinfo(jobAccinfo);
        if (result == null) result = casejobsection(jobAccinfo);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD:
      {
        DD dd = (DD)theEObject;
        T result = caseDD(dd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DDSECTION:
      {
        ddsection ddsection = (ddsection)theEObject;
        T result = caseddsection(ddsection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_VOLUME:
      {
        ddVolume ddVolume = (ddVolume)theEObject;
        T result = caseddVolume(ddVolume);
        if (result == null) result = caseddsection(ddVolume);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_UNIT:
      {
        ddUnit ddUnit = (ddUnit)theEObject;
        T result = caseddUnit(ddUnit);
        if (result == null) result = caseddsection(ddUnit);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_UCS:
      {
        ddUcs ddUcs = (ddUcs)theEObject;
        T result = caseddUcs(ddUcs);
        if (result == null) result = caseddsection(ddUcs);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_TERM:
      {
        ddTerm ddTerm = (ddTerm)theEObject;
        T result = caseddTerm(ddTerm);
        if (result == null) result = caseddsection(ddTerm);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_SYSOUT:
      {
        ddSysout ddSysout = (ddSysout)theEObject;
        T result = caseddSysout(ddSysout);
        if (result == null) result = caseddsection(ddSysout);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_SUBSYS:
      {
        ddSubsys ddSubsys = (ddSubsys)theEObject;
        T result = caseddSubsys(ddSubsys);
        if (result == null) result = caseddsection(ddSubsys);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_SUBSYS_SUBPARMS:
      {
        ddSubsysSubparms ddSubsysSubparms = (ddSubsysSubparms)theEObject;
        T result = caseddSubsysSubparms(ddSubsysSubparms);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_STORCLAS:
      {
        ddStorclas ddStorclas = (ddStorclas)theEObject;
        T result = caseddStorclas(ddStorclas);
        if (result == null) result = caseddsection(ddStorclas);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_SPIN:
      {
        ddSpin ddSpin = (ddSpin)theEObject;
        T result = caseddSpin(ddSpin);
        if (result == null) result = caseddsection(ddSpin);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_SPACE:
      {
        ddSpace ddSpace = (ddSpace)theEObject;
        T result = caseddSpace(ddSpace);
        if (result == null) result = caseddsection(ddSpace);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_SEGMENT:
      {
        ddSegment ddSegment = (ddSegment)theEObject;
        T result = caseddSegment(ddSegment);
        if (result == null) result = caseddsection(ddSegment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_SECMODEL:
      {
        ddSecmodel ddSecmodel = (ddSecmodel)theEObject;
        T result = caseddSecmodel(ddSecmodel);
        if (result == null) result = caseddsection(ddSecmodel);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_RLS:
      {
        ddRls ddRls = (ddRls)theEObject;
        T result = caseddRls(ddRls);
        if (result == null) result = caseddsection(ddRls);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_RETPD:
      {
        ddRetpd ddRetpd = (ddRetpd)theEObject;
        T result = caseddRetpd(ddRetpd);
        if (result == null) result = caseddsection(ddRetpd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_REFDD:
      {
        ddRefdd ddRefdd = (ddRefdd)theEObject;
        T result = caseddRefdd(ddRefdd);
        if (result == null) result = caseddsection(ddRefdd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_RECORG:
      {
        ddRecorg ddRecorg = (ddRecorg)theEObject;
        T result = caseddRecorg(ddRecorg);
        if (result == null) result = caseddsection(ddRecorg);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_RECFM:
      {
        ddRecfm ddRecfm = (ddRecfm)theEObject;
        T result = caseddRecfm(ddRecfm);
        if (result == null) result = caseddsection(ddRecfm);
        if (result == null) result = caseddDcbSubparms(ddRecfm);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_QNAME:
      {
        ddQname ddQname = (ddQname)theEObject;
        T result = caseddQname(ddQname);
        if (result == null) result = caseddsection(ddQname);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_PROTECT:
      {
        ddProtect ddProtect = (ddProtect)theEObject;
        T result = caseddProtect(ddProtect);
        if (result == null) result = caseddsection(ddProtect);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_PATHMODE:
      {
        ddPathmode ddPathmode = (ddPathmode)theEObject;
        T result = caseddPathmode(ddPathmode);
        if (result == null) result = caseddsection(ddPathmode);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_PATHOPTS:
      {
        ddPathopts ddPathopts = (ddPathopts)theEObject;
        T result = caseddPathopts(ddPathopts);
        if (result == null) result = caseddsection(ddPathopts);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_PATHDISP:
      {
        ddPathdisp ddPathdisp = (ddPathdisp)theEObject;
        T result = caseddPathdisp(ddPathdisp);
        if (result == null) result = caseddsection(ddPathdisp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_PATH:
      {
        ddPath ddPath = (ddPath)theEObject;
        T result = caseddPath(ddPath);
        if (result == null) result = caseddsection(ddPath);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_OUTPUT:
      {
        ddOutput ddOutput = (ddOutput)theEObject;
        T result = caseddOutput(ddOutput);
        if (result == null) result = caseddsection(ddOutput);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_OUTLIM:
      {
        ddOutlim ddOutlim = (ddOutlim)theEObject;
        T result = caseddOutlim(ddOutlim);
        if (result == null) result = caseddsection(ddOutlim);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_MODIFY:
      {
        ddModify ddModify = (ddModify)theEObject;
        T result = caseddModify(ddModify);
        if (result == null) result = caseddsection(ddModify);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_MGMTCLAS:
      {
        ddMgmtclas ddMgmtclas = (ddMgmtclas)theEObject;
        T result = caseddMgmtclas(ddMgmtclas);
        if (result == null) result = caseddsection(ddMgmtclas);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_LRECL:
      {
        ddLrecl ddLrecl = (ddLrecl)theEObject;
        T result = caseddLrecl(ddLrecl);
        if (result == null) result = caseddsection(ddLrecl);
        if (result == null) result = caseddDcbSubparms(ddLrecl);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_LIKE:
      {
        ddLike ddLike = (ddLike)theEObject;
        T result = caseddLike(ddLike);
        if (result == null) result = caseddsection(ddLike);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_LGSTREAM:
      {
        ddLgstream ddLgstream = (ddLgstream)theEObject;
        T result = caseddLgstream(ddLgstream);
        if (result == null) result = caseddsection(ddLgstream);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_LABEL:
      {
        ddLabel ddLabel = (ddLabel)theEObject;
        T result = caseddLabel(ddLabel);
        if (result == null) result = caseddsection(ddLabel);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_LABEL_EXPIRE:
      {
        ddLabelExpire ddLabelExpire = (ddLabelExpire)theEObject;
        T result = caseddLabelExpire(ddLabelExpire);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_LABEL_EXPDT:
      {
        ddLabelExpdt ddLabelExpdt = (ddLabelExpdt)theEObject;
        T result = caseddLabelExpdt(ddLabelExpdt);
        if (result == null) result = caseddLabelExpire(ddLabelExpdt);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_LABEL_RETPD:
      {
        ddLabelRetpd ddLabelRetpd = (ddLabelRetpd)theEObject;
        T result = caseddLabelRetpd(ddLabelRetpd);
        if (result == null) result = caseddLabelExpire(ddLabelRetpd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_KEYOFF:
      {
        ddKeyoff ddKeyoff = (ddKeyoff)theEObject;
        T result = caseddKeyoff(ddKeyoff);
        if (result == null) result = caseddsection(ddKeyoff);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_KEYLEN:
      {
        ddKeylen ddKeylen = (ddKeylen)theEObject;
        T result = caseddKeylen(ddKeylen);
        if (result == null) result = caseddsection(ddKeylen);
        if (result == null) result = caseddDcbSubparms(ddKeylen);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_HOLD:
      {
        ddHold ddHold = (ddHold)theEObject;
        T result = caseddHold(ddHold);
        if (result == null) result = caseddsection(ddHold);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_FREE:
      {
        ddFree ddFree = (ddFree)theEObject;
        T result = caseddFree(ddFree);
        if (result == null) result = caseddsection(ddFree);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_FLASH:
      {
        ddFlash ddFlash = (ddFlash)theEObject;
        T result = caseddFlash(ddFlash);
        if (result == null) result = caseddsection(ddFlash);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_FILEDATA:
      {
        ddFiledata ddFiledata = (ddFiledata)theEObject;
        T result = caseddFiledata(ddFiledata);
        if (result == null) result = caseddsection(ddFiledata);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_FCB:
      {
        ddFcb ddFcb = (ddFcb)theEObject;
        T result = caseddFcb(ddFcb);
        if (result == null) result = caseddsection(ddFcb);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_EXPDT:
      {
        ddExpdt ddExpdt = (ddExpdt)theEObject;
        T result = caseddExpdt(ddExpdt);
        if (result == null) result = caseddsection(ddExpdt);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DSORG:
      {
        ddDsorg ddDsorg = (ddDsorg)theEObject;
        T result = caseddDsorg(ddDsorg);
        if (result == null) result = caseddsection(ddDsorg);
        if (result == null) result = caseddDcbSubparms(ddDsorg);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DSNAME:
      {
        ddDsname ddDsname = (ddDsname)theEObject;
        T result = caseddDsname(ddDsname);
        if (result == null) result = caseddsection(ddDsname);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DSNTYPE:
      {
        ddDsntype ddDsntype = (ddDsntype)theEObject;
        T result = caseddDsntype(ddDsntype);
        if (result == null) result = caseddsection(ddDsntype);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DSID:
      {
        ddDsid ddDsid = (ddDsid)theEObject;
        T result = caseddDsid(ddDsid);
        if (result == null) result = caseddsection(ddDsid);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DLM:
      {
        ddDlm ddDlm = (ddDlm)theEObject;
        T result = caseddDlm(ddDlm);
        if (result == null) result = caseddsection(ddDlm);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DISP:
      {
        ddDisp ddDisp = (ddDisp)theEObject;
        T result = caseddDisp(ddDisp);
        if (result == null) result = caseddsection(ddDisp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DEST:
      {
        ddDest ddDest = (ddDest)theEObject;
        T result = caseddDest(ddDest);
        if (result == null) result = caseddsection(ddDest);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DDNAME:
      {
        ddDdname ddDdname = (ddDdname)theEObject;
        T result = caseddDdname(ddDdname);
        if (result == null) result = caseddsection(ddDdname);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DCB:
      {
        ddDcb ddDcb = (ddDcb)theEObject;
        T result = caseddDcb(ddDcb);
        if (result == null) result = caseddsection(ddDcb);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DCB_SUBPARMS:
      {
        ddDcbSubparms ddDcbSubparms = (ddDcbSubparms)theEObject;
        T result = caseddDcbSubparms(ddDcbSubparms);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_TRTCH:
      {
        ddTrtch ddTrtch = (ddTrtch)theEObject;
        T result = caseddTrtch(ddTrtch);
        if (result == null) result = caseddsection(ddTrtch);
        if (result == null) result = caseddDcbSubparms(ddTrtch);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_THRESH:
      {
        ddThresh ddThresh = (ddThresh)theEObject;
        T result = caseddThresh(ddThresh);
        if (result == null) result = caseddsection(ddThresh);
        if (result == null) result = caseddDcbSubparms(ddThresh);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_STACK:
      {
        ddStack ddStack = (ddStack)theEObject;
        T result = caseddStack(ddStack);
        if (result == null) result = caseddsection(ddStack);
        if (result == null) result = caseddDcbSubparms(ddStack);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_RKP:
      {
        ddRkp ddRkp = (ddRkp)theEObject;
        T result = caseddRkp(ddRkp);
        if (result == null) result = caseddsection(ddRkp);
        if (result == null) result = caseddDcbSubparms(ddRkp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_RESERVE:
      {
        ddReserve ddReserve = (ddReserve)theEObject;
        T result = caseddReserve(ddReserve);
        if (result == null) result = caseddsection(ddReserve);
        if (result == null) result = caseddDcbSubparms(ddReserve);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_PRTSP:
      {
        ddPrtsp ddPrtsp = (ddPrtsp)theEObject;
        T result = caseddPrtsp(ddPrtsp);
        if (result == null) result = caseddsection(ddPrtsp);
        if (result == null) result = caseddDcbSubparms(ddPrtsp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_PCI:
      {
        ddPci ddPci = (ddPci)theEObject;
        T result = caseddPci(ddPci);
        if (result == null) result = caseddsection(ddPci);
        if (result == null) result = caseddDcbSubparms(ddPci);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_OPTCD:
      {
        ddOptcd ddOptcd = (ddOptcd)theEObject;
        T result = caseddOptcd(ddOptcd);
        if (result == null) result = caseddsection(ddOptcd);
        if (result == null) result = caseddDcbSubparms(ddOptcd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_NTM:
      {
        ddNtm ddNtm = (ddNtm)theEObject;
        T result = caseddNtm(ddNtm);
        if (result == null) result = caseddsection(ddNtm);
        if (result == null) result = caseddDcbSubparms(ddNtm);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_NCP:
      {
        ddNcp ddNcp = (ddNcp)theEObject;
        T result = caseddNcp(ddNcp);
        if (result == null) result = caseddsection(ddNcp);
        if (result == null) result = caseddDcbSubparms(ddNcp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_MODE:
      {
        ddMode ddMode = (ddMode)theEObject;
        T result = caseddMode(ddMode);
        if (result == null) result = caseddsection(ddMode);
        if (result == null) result = caseddDcbSubparms(ddMode);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_LIMCT:
      {
        ddLimct ddLimct = (ddLimct)theEObject;
        T result = caseddLimct(ddLimct);
        if (result == null) result = caseddsection(ddLimct);
        if (result == null) result = caseddDcbSubparms(ddLimct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_IPLTXID:
      {
        ddIpltxid ddIpltxid = (ddIpltxid)theEObject;
        T result = caseddIpltxid(ddIpltxid);
        if (result == null) result = caseddsection(ddIpltxid);
        if (result == null) result = caseddDcbSubparms(ddIpltxid);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_INTVL:
      {
        ddIntvl ddIntvl = (ddIntvl)theEObject;
        T result = caseddIntvl(ddIntvl);
        if (result == null) result = caseddsection(ddIntvl);
        if (result == null) result = caseddDcbSubparms(ddIntvl);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_GNCP:
      {
        ddGncp ddGncp = (ddGncp)theEObject;
        T result = caseddGncp(ddGncp);
        if (result == null) result = caseddsection(ddGncp);
        if (result == null) result = caseddDcbSubparms(ddGncp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_FUNC:
      {
        ddFunc ddFunc = (ddFunc)theEObject;
        T result = caseddFunc(ddFunc);
        if (result == null) result = caseddsection(ddFunc);
        if (result == null) result = caseddDcbSubparms(ddFunc);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_EROPT:
      {
        ddEropt ddEropt = (ddEropt)theEObject;
        T result = caseddEropt(ddEropt);
        if (result == null) result = caseddsection(ddEropt);
        if (result == null) result = caseddDcbSubparms(ddEropt);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DIAGNS:
      {
        ddDiagns ddDiagns = (ddDiagns)theEObject;
        T result = caseddDiagns(ddDiagns);
        if (result == null) result = caseddsection(ddDiagns);
        if (result == null) result = caseddDcbSubparms(ddDiagns);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DEN:
      {
        ddDen ddDen = (ddDen)theEObject;
        T result = caseddDen(ddDen);
        if (result == null) result = caseddsection(ddDen);
        if (result == null) result = caseddDcbSubparms(ddDen);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_CYLOFL:
      {
        ddCylofl ddCylofl = (ddCylofl)theEObject;
        T result = caseddCylofl(ddCylofl);
        if (result == null) result = caseddsection(ddCylofl);
        if (result == null) result = caseddDcbSubparms(ddCylofl);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_CPRI:
      {
        ddCpri ddCpri = (ddCpri)theEObject;
        T result = caseddCpri(ddCpri);
        if (result == null) result = caseddsection(ddCpri);
        if (result == null) result = caseddDcbSubparms(ddCpri);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BUFSIZE:
      {
        ddBufsize ddBufsize = (ddBufsize)theEObject;
        T result = caseddBufsize(ddBufsize);
        if (result == null) result = caseddsection(ddBufsize);
        if (result == null) result = caseddDcbSubparms(ddBufsize);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BUFOUT:
      {
        ddBufout ddBufout = (ddBufout)theEObject;
        T result = caseddBufout(ddBufout);
        if (result == null) result = caseddsection(ddBufout);
        if (result == null) result = caseddDcbSubparms(ddBufout);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BUFOFF:
      {
        ddBufoff ddBufoff = (ddBufoff)theEObject;
        T result = caseddBufoff(ddBufoff);
        if (result == null) result = caseddsection(ddBufoff);
        if (result == null) result = caseddDcbSubparms(ddBufoff);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BUFNO:
      {
        ddBufno ddBufno = (ddBufno)theEObject;
        T result = caseddBufno(ddBufno);
        if (result == null) result = caseddsection(ddBufno);
        if (result == null) result = caseddDcbSubparms(ddBufno);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BUFMAX:
      {
        ddBufmax ddBufmax = (ddBufmax)theEObject;
        T result = caseddBufmax(ddBufmax);
        if (result == null) result = caseddsection(ddBufmax);
        if (result == null) result = caseddDcbSubparms(ddBufmax);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BUFL:
      {
        ddBufl ddBufl = (ddBufl)theEObject;
        T result = caseddBufl(ddBufl);
        if (result == null) result = caseddsection(ddBufl);
        if (result == null) result = caseddDcbSubparms(ddBufl);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BUFIN:
      {
        ddBufin ddBufin = (ddBufin)theEObject;
        T result = caseddBufin(ddBufin);
        if (result == null) result = caseddsection(ddBufin);
        if (result == null) result = caseddDcbSubparms(ddBufin);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BFTEK:
      {
        ddBftek ddBftek = (ddBftek)theEObject;
        T result = caseddBftek(ddBftek);
        if (result == null) result = caseddsection(ddBftek);
        if (result == null) result = caseddDcbSubparms(ddBftek);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BFALN:
      {
        ddBfaln ddBfaln = (ddBfaln)theEObject;
        T result = caseddBfaln(ddBfaln);
        if (result == null) result = caseddsection(ddBfaln);
        if (result == null) result = caseddDcbSubparms(ddBfaln);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DATACLAS:
      {
        ddDataclas ddDataclas = (ddDataclas)theEObject;
        T result = caseddDataclas(ddDataclas);
        if (result == null) result = caseddsection(ddDataclas);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_DATA:
      {
        ddData ddData = (ddData)theEObject;
        T result = caseddData(ddData);
        if (result == null) result = caseddsection(ddData);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_COPIES:
      {
        ddCopies ddCopies = (ddCopies)theEObject;
        T result = caseddCopies(ddCopies);
        if (result == null) result = caseddsection(ddCopies);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_CNTL:
      {
        ddCntl ddCntl = (ddCntl)theEObject;
        T result = caseddCntl(ddCntl);
        if (result == null) result = caseddsection(ddCntl);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_CHKPT:
      {
        ddChkpt ddChkpt = (ddChkpt)theEObject;
        T result = caseddChkpt(ddChkpt);
        if (result == null) result = caseddsection(ddChkpt);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_CHARS:
      {
        ddChars ddChars = (ddChars)theEObject;
        T result = caseddChars(ddChars);
        if (result == null) result = caseddsection(ddChars);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_CCSID:
      {
        ddCcsid ddCcsid = (ddCcsid)theEObject;
        T result = caseddCcsid(ddCcsid);
        if (result == null) result = caseddsection(ddCcsid);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BURST:
      {
        ddBurst ddBurst = (ddBurst)theEObject;
        T result = caseddBurst(ddBurst);
        if (result == null) result = caseddsection(ddBurst);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BLKSZLIM:
      {
        ddBlkszlim ddBlkszlim = (ddBlkszlim)theEObject;
        T result = caseddBlkszlim(ddBlkszlim);
        if (result == null) result = caseddsection(ddBlkszlim);
        if (result == null) result = caseddDcbSubparms(ddBlkszlim);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_BLKSIZE:
      {
        ddBlksize ddBlksize = (ddBlksize)theEObject;
        T result = caseddBlksize(ddBlksize);
        if (result == null) result = caseddsection(ddBlksize);
        if (result == null) result = caseddDcbSubparms(ddBlksize);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_AVGREC:
      {
        ddAvgrec ddAvgrec = (ddAvgrec)theEObject;
        T result = caseddAvgrec(ddAvgrec);
        if (result == null) result = caseddsection(ddAvgrec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_AMP:
      {
        ddAmp ddAmp = (ddAmp)theEObject;
        T result = caseddAmp(ddAmp);
        if (result == null) result = caseddsection(ddAmp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case JclDslPackage.DD_ACCODE:
      {
        ddAccode ddAccode = (ddAccode)theEObject;
        T result = caseddAccode(ddAccode);
        if (result == null) result = caseddsection(ddAccode);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Model</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModel(Model object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Keyword</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Keyword</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseKeyword(Keyword object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>STEPCATDD</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>STEPCATDD</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSTEPCATDD(STEPCATDD object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>JOBLIBDD</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>JOBLIBDD</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJOBLIBDD(JOBLIBDD object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>JOBCATDD</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>JOBCATDD</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJOBCATDD(JOBCATDD object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>specdd Section</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>specdd Section</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casespecddSection(specddSection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>specdd Disp</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>specdd Disp</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casespecddDisp(specddDisp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>specdd Dsname</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>specdd Dsname</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casespecddDsname(specddDsname object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>IF</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>IF</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIF(IF object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>ELSE</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>ELSE</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseELSE(ELSE object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>ENDIF</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>ENDIF</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseENDIF(ENDIF object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>PROC</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>PROC</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePROC(PROC object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>PEND</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>PEND</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePEND(PEND object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>SET</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>SET</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSET(SET object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>JCLLIB</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>JCLLIB</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJCLLIB(JCLLIB object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EXEC</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EXEC</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEXEC(EXEC object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>execsection</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>execsection</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecsection(execsection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Time</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Time</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecTime(execTime object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Timevalue</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Timevalue</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecTimevalue(execTimevalue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Perform</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Perform</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecPerform(execPerform object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Region</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Region</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecRegion(execRegion object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Rd</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Rd</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecRd(execRd object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Parm</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Parm</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecParm(execParm object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Dynambr</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Dynambr</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecDynambr(execDynambr object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Cond</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Cond</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecCond(execCond object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Addrspc</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Addrspc</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecAddrspc(execAddrspc object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Ccsid</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Ccsid</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecCcsid(execCcsid object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Acct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Acct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecAcct(execAcct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Proc</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Proc</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecProc(execProc object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exec Pgm</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exec Pgm</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecPgm(execPgm object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>INCLUDE</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>INCLUDE</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseINCLUDE(INCLUDE object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>JOB</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>JOB</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJOB(JOB object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>jobsection</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>jobsection</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobsection(jobsection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job User</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job User</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobUser(jobUser object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Typrun</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Typrun</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobTyprun(jobTyprun object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Time</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Time</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobTime(jobTime object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Timevalue</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Timevalue</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobTimevalue(jobTimevalue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Schenv</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Schenv</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobSchenv(jobSchenv object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Seclabel</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Seclabel</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobSeclabel(jobSeclabel object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Restart</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Restart</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobRestart(jobRestart object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Region</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Region</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobRegion(jobRegion object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Rd</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Rd</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobRd(jobRd object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Prty</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Prty</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobPrty(jobPrty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Progname</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Progname</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobProgname(jobProgname object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Perform</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Perform</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobPerform(jobPerform object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Password</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Password</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobPassword(jobPassword object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Pages</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Pages</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobPages(jobPages object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Notify</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Notify</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobNotify(jobNotify object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Msglevel</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Msglevel</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobMsglevel(jobMsglevel object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Msg Class</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Msg Class</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobMsgClass(jobMsgClass object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Lines</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Lines</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobLines(jobLines object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Group</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Group</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobGroup(jobGroup object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Cond</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Cond</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobCond(jobCond object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Class</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Class</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobClass(jobClass object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Ccsid</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Ccsid</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobCcsid(jobCcsid object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Cards</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Cards</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobCards(jobCards object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Bytes</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Bytes</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobBytes(jobBytes object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Addrspc</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Addrspc</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobAddrspc(jobAddrspc object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>job Accinfo</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>job Accinfo</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casejobAccinfo(jobAccinfo object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>DD</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>DD</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDD(DD object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>ddsection</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>ddsection</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddsection(ddsection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Volume</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Volume</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddVolume(ddVolume object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Unit</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Unit</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddUnit(ddUnit object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Ucs</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Ucs</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddUcs(ddUcs object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Term</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Term</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddTerm(ddTerm object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Sysout</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Sysout</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddSysout(ddSysout object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Subsys</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Subsys</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddSubsys(ddSubsys object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Subsys Subparms</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Subsys Subparms</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddSubsysSubparms(ddSubsysSubparms object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Storclas</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Storclas</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddStorclas(ddStorclas object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Spin</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Spin</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddSpin(ddSpin object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Space</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Space</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddSpace(ddSpace object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Segment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Segment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddSegment(ddSegment object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Secmodel</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Secmodel</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddSecmodel(ddSecmodel object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Rls</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Rls</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddRls(ddRls object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Retpd</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Retpd</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddRetpd(ddRetpd object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Refdd</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Refdd</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddRefdd(ddRefdd object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Recorg</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Recorg</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddRecorg(ddRecorg object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Recfm</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Recfm</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddRecfm(ddRecfm object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Qname</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Qname</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddQname(ddQname object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Protect</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Protect</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddProtect(ddProtect object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Pathmode</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Pathmode</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddPathmode(ddPathmode object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Pathopts</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Pathopts</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddPathopts(ddPathopts object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Pathdisp</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Pathdisp</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddPathdisp(ddPathdisp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Path</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Path</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddPath(ddPath object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Output</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Output</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddOutput(ddOutput object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Outlim</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Outlim</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddOutlim(ddOutlim object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Modify</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Modify</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddModify(ddModify object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Mgmtclas</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Mgmtclas</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddMgmtclas(ddMgmtclas object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Lrecl</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Lrecl</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddLrecl(ddLrecl object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Like</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Like</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddLike(ddLike object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Lgstream</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Lgstream</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddLgstream(ddLgstream object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Label</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Label</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddLabel(ddLabel object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Label Expire</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Label Expire</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddLabelExpire(ddLabelExpire object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Label Expdt</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Label Expdt</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddLabelExpdt(ddLabelExpdt object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Label Retpd</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Label Retpd</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddLabelRetpd(ddLabelRetpd object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Keyoff</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Keyoff</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddKeyoff(ddKeyoff object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Keylen</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Keylen</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddKeylen(ddKeylen object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Hold</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Hold</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddHold(ddHold object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Free</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Free</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddFree(ddFree object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Flash</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Flash</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddFlash(ddFlash object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Filedata</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Filedata</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddFiledata(ddFiledata object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Fcb</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Fcb</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddFcb(ddFcb object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Expdt</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Expdt</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddExpdt(ddExpdt object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Dsorg</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Dsorg</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDsorg(ddDsorg object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Dsname</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Dsname</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDsname(ddDsname object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Dsntype</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Dsntype</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDsntype(ddDsntype object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Dsid</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Dsid</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDsid(ddDsid object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Dlm</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Dlm</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDlm(ddDlm object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Disp</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Disp</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDisp(ddDisp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Dest</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Dest</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDest(ddDest object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Ddname</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Ddname</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDdname(ddDdname object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Dcb</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Dcb</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDcb(ddDcb object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Dcb Subparms</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Dcb Subparms</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDcbSubparms(ddDcbSubparms object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Trtch</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Trtch</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddTrtch(ddTrtch object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Thresh</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Thresh</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddThresh(ddThresh object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Stack</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Stack</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddStack(ddStack object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Rkp</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Rkp</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddRkp(ddRkp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Reserve</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Reserve</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddReserve(ddReserve object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Prtsp</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Prtsp</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddPrtsp(ddPrtsp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Pci</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Pci</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddPci(ddPci object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Optcd</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Optcd</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddOptcd(ddOptcd object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Ntm</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Ntm</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddNtm(ddNtm object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Ncp</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Ncp</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddNcp(ddNcp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Mode</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Mode</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddMode(ddMode object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Limct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Limct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddLimct(ddLimct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Ipltxid</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Ipltxid</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddIpltxid(ddIpltxid object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Intvl</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Intvl</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddIntvl(ddIntvl object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Gncp</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Gncp</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddGncp(ddGncp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Func</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Func</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddFunc(ddFunc object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Eropt</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Eropt</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddEropt(ddEropt object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Diagns</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Diagns</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDiagns(ddDiagns object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Den</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Den</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDen(ddDen object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Cylofl</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Cylofl</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddCylofl(ddCylofl object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Cpri</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Cpri</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddCpri(ddCpri object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Bufsize</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Bufsize</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBufsize(ddBufsize object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Bufout</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Bufout</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBufout(ddBufout object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Bufoff</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Bufoff</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBufoff(ddBufoff object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Bufno</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Bufno</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBufno(ddBufno object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Bufmax</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Bufmax</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBufmax(ddBufmax object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Bufl</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Bufl</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBufl(ddBufl object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Bufin</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Bufin</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBufin(ddBufin object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Bftek</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Bftek</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBftek(ddBftek object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Bfaln</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Bfaln</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBfaln(ddBfaln object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Dataclas</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Dataclas</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddDataclas(ddDataclas object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Data</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Data</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddData(ddData object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Copies</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Copies</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddCopies(ddCopies object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Cntl</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Cntl</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddCntl(ddCntl object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Chkpt</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Chkpt</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddChkpt(ddChkpt object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Chars</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Chars</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddChars(ddChars object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Ccsid</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Ccsid</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddCcsid(ddCcsid object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Burst</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Burst</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBurst(ddBurst object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Blkszlim</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Blkszlim</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBlkszlim(ddBlkszlim object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Blksize</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Blksize</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddBlksize(ddBlksize object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Avgrec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Avgrec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddAvgrec(ddAvgrec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Amp</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Amp</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddAmp(ddAmp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dd Accode</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dd Accode</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseddAccode(ddAccode object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //JclDslSwitch
