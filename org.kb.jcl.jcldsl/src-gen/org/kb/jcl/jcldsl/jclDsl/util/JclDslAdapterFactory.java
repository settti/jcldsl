/**
 */
package org.kb.jcl.jcldsl.jclDsl.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.kb.jcl.jcldsl.jclDsl.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage
 * @generated
 */
public class JclDslAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static JclDslPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JclDslAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = JclDslPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected JclDslSwitch<Adapter> modelSwitch =
    new JclDslSwitch<Adapter>()
    {
      @Override
      public Adapter caseModel(Model object)
      {
        return createModelAdapter();
      }
      @Override
      public Adapter caseKeyword(Keyword object)
      {
        return createKeywordAdapter();
      }
      @Override
      public Adapter caseSTEPCATDD(STEPCATDD object)
      {
        return createSTEPCATDDAdapter();
      }
      @Override
      public Adapter caseJOBLIBDD(JOBLIBDD object)
      {
        return createJOBLIBDDAdapter();
      }
      @Override
      public Adapter caseJOBCATDD(JOBCATDD object)
      {
        return createJOBCATDDAdapter();
      }
      @Override
      public Adapter casespecddSection(specddSection object)
      {
        return createspecddSectionAdapter();
      }
      @Override
      public Adapter casespecddDisp(specddDisp object)
      {
        return createspecddDispAdapter();
      }
      @Override
      public Adapter casespecddDsname(specddDsname object)
      {
        return createspecddDsnameAdapter();
      }
      @Override
      public Adapter caseIF(IF object)
      {
        return createIFAdapter();
      }
      @Override
      public Adapter caseELSE(ELSE object)
      {
        return createELSEAdapter();
      }
      @Override
      public Adapter caseENDIF(ENDIF object)
      {
        return createENDIFAdapter();
      }
      @Override
      public Adapter casePROC(PROC object)
      {
        return createPROCAdapter();
      }
      @Override
      public Adapter casePEND(PEND object)
      {
        return createPENDAdapter();
      }
      @Override
      public Adapter caseSET(SET object)
      {
        return createSETAdapter();
      }
      @Override
      public Adapter caseJCLLIB(JCLLIB object)
      {
        return createJCLLIBAdapter();
      }
      @Override
      public Adapter caseEXEC(EXEC object)
      {
        return createEXECAdapter();
      }
      @Override
      public Adapter caseexecsection(execsection object)
      {
        return createexecsectionAdapter();
      }
      @Override
      public Adapter caseexecTime(execTime object)
      {
        return createexecTimeAdapter();
      }
      @Override
      public Adapter caseexecTimevalue(execTimevalue object)
      {
        return createexecTimevalueAdapter();
      }
      @Override
      public Adapter caseexecPerform(execPerform object)
      {
        return createexecPerformAdapter();
      }
      @Override
      public Adapter caseexecRegion(execRegion object)
      {
        return createexecRegionAdapter();
      }
      @Override
      public Adapter caseexecRd(execRd object)
      {
        return createexecRdAdapter();
      }
      @Override
      public Adapter caseexecParm(execParm object)
      {
        return createexecParmAdapter();
      }
      @Override
      public Adapter caseexecDynambr(execDynambr object)
      {
        return createexecDynambrAdapter();
      }
      @Override
      public Adapter caseexecCond(execCond object)
      {
        return createexecCondAdapter();
      }
      @Override
      public Adapter caseexecAddrspc(execAddrspc object)
      {
        return createexecAddrspcAdapter();
      }
      @Override
      public Adapter caseexecCcsid(execCcsid object)
      {
        return createexecCcsidAdapter();
      }
      @Override
      public Adapter caseexecAcct(execAcct object)
      {
        return createexecAcctAdapter();
      }
      @Override
      public Adapter caseexecProc(execProc object)
      {
        return createexecProcAdapter();
      }
      @Override
      public Adapter caseexecPgm(execPgm object)
      {
        return createexecPgmAdapter();
      }
      @Override
      public Adapter caseINCLUDE(INCLUDE object)
      {
        return createINCLUDEAdapter();
      }
      @Override
      public Adapter caseJOB(JOB object)
      {
        return createJOBAdapter();
      }
      @Override
      public Adapter casejobsection(jobsection object)
      {
        return createjobsectionAdapter();
      }
      @Override
      public Adapter casejobUser(jobUser object)
      {
        return createjobUserAdapter();
      }
      @Override
      public Adapter casejobTyprun(jobTyprun object)
      {
        return createjobTyprunAdapter();
      }
      @Override
      public Adapter casejobTime(jobTime object)
      {
        return createjobTimeAdapter();
      }
      @Override
      public Adapter casejobTimevalue(jobTimevalue object)
      {
        return createjobTimevalueAdapter();
      }
      @Override
      public Adapter casejobSchenv(jobSchenv object)
      {
        return createjobSchenvAdapter();
      }
      @Override
      public Adapter casejobSeclabel(jobSeclabel object)
      {
        return createjobSeclabelAdapter();
      }
      @Override
      public Adapter casejobRestart(jobRestart object)
      {
        return createjobRestartAdapter();
      }
      @Override
      public Adapter casejobRegion(jobRegion object)
      {
        return createjobRegionAdapter();
      }
      @Override
      public Adapter casejobRd(jobRd object)
      {
        return createjobRdAdapter();
      }
      @Override
      public Adapter casejobPrty(jobPrty object)
      {
        return createjobPrtyAdapter();
      }
      @Override
      public Adapter casejobProgname(jobProgname object)
      {
        return createjobPrognameAdapter();
      }
      @Override
      public Adapter casejobPerform(jobPerform object)
      {
        return createjobPerformAdapter();
      }
      @Override
      public Adapter casejobPassword(jobPassword object)
      {
        return createjobPasswordAdapter();
      }
      @Override
      public Adapter casejobPages(jobPages object)
      {
        return createjobPagesAdapter();
      }
      @Override
      public Adapter casejobNotify(jobNotify object)
      {
        return createjobNotifyAdapter();
      }
      @Override
      public Adapter casejobMsglevel(jobMsglevel object)
      {
        return createjobMsglevelAdapter();
      }
      @Override
      public Adapter casejobMsgClass(jobMsgClass object)
      {
        return createjobMsgClassAdapter();
      }
      @Override
      public Adapter casejobLines(jobLines object)
      {
        return createjobLinesAdapter();
      }
      @Override
      public Adapter casejobGroup(jobGroup object)
      {
        return createjobGroupAdapter();
      }
      @Override
      public Adapter casejobCond(jobCond object)
      {
        return createjobCondAdapter();
      }
      @Override
      public Adapter casejobClass(jobClass object)
      {
        return createjobClassAdapter();
      }
      @Override
      public Adapter casejobCcsid(jobCcsid object)
      {
        return createjobCcsidAdapter();
      }
      @Override
      public Adapter casejobCards(jobCards object)
      {
        return createjobCardsAdapter();
      }
      @Override
      public Adapter casejobBytes(jobBytes object)
      {
        return createjobBytesAdapter();
      }
      @Override
      public Adapter casejobAddrspc(jobAddrspc object)
      {
        return createjobAddrspcAdapter();
      }
      @Override
      public Adapter casejobAccinfo(jobAccinfo object)
      {
        return createjobAccinfoAdapter();
      }
      @Override
      public Adapter caseDD(DD object)
      {
        return createDDAdapter();
      }
      @Override
      public Adapter caseddsection(ddsection object)
      {
        return createddsectionAdapter();
      }
      @Override
      public Adapter caseddVolume(ddVolume object)
      {
        return createddVolumeAdapter();
      }
      @Override
      public Adapter caseddUnit(ddUnit object)
      {
        return createddUnitAdapter();
      }
      @Override
      public Adapter caseddUcs(ddUcs object)
      {
        return createddUcsAdapter();
      }
      @Override
      public Adapter caseddTerm(ddTerm object)
      {
        return createddTermAdapter();
      }
      @Override
      public Adapter caseddSysout(ddSysout object)
      {
        return createddSysoutAdapter();
      }
      @Override
      public Adapter caseddSubsys(ddSubsys object)
      {
        return createddSubsysAdapter();
      }
      @Override
      public Adapter caseddSubsysSubparms(ddSubsysSubparms object)
      {
        return createddSubsysSubparmsAdapter();
      }
      @Override
      public Adapter caseddStorclas(ddStorclas object)
      {
        return createddStorclasAdapter();
      }
      @Override
      public Adapter caseddSpin(ddSpin object)
      {
        return createddSpinAdapter();
      }
      @Override
      public Adapter caseddSpace(ddSpace object)
      {
        return createddSpaceAdapter();
      }
      @Override
      public Adapter caseddSegment(ddSegment object)
      {
        return createddSegmentAdapter();
      }
      @Override
      public Adapter caseddSecmodel(ddSecmodel object)
      {
        return createddSecmodelAdapter();
      }
      @Override
      public Adapter caseddRls(ddRls object)
      {
        return createddRlsAdapter();
      }
      @Override
      public Adapter caseddRetpd(ddRetpd object)
      {
        return createddRetpdAdapter();
      }
      @Override
      public Adapter caseddRefdd(ddRefdd object)
      {
        return createddRefddAdapter();
      }
      @Override
      public Adapter caseddRecorg(ddRecorg object)
      {
        return createddRecorgAdapter();
      }
      @Override
      public Adapter caseddRecfm(ddRecfm object)
      {
        return createddRecfmAdapter();
      }
      @Override
      public Adapter caseddQname(ddQname object)
      {
        return createddQnameAdapter();
      }
      @Override
      public Adapter caseddProtect(ddProtect object)
      {
        return createddProtectAdapter();
      }
      @Override
      public Adapter caseddPathmode(ddPathmode object)
      {
        return createddPathmodeAdapter();
      }
      @Override
      public Adapter caseddPathopts(ddPathopts object)
      {
        return createddPathoptsAdapter();
      }
      @Override
      public Adapter caseddPathdisp(ddPathdisp object)
      {
        return createddPathdispAdapter();
      }
      @Override
      public Adapter caseddPath(ddPath object)
      {
        return createddPathAdapter();
      }
      @Override
      public Adapter caseddOutput(ddOutput object)
      {
        return createddOutputAdapter();
      }
      @Override
      public Adapter caseddOutlim(ddOutlim object)
      {
        return createddOutlimAdapter();
      }
      @Override
      public Adapter caseddModify(ddModify object)
      {
        return createddModifyAdapter();
      }
      @Override
      public Adapter caseddMgmtclas(ddMgmtclas object)
      {
        return createddMgmtclasAdapter();
      }
      @Override
      public Adapter caseddLrecl(ddLrecl object)
      {
        return createddLreclAdapter();
      }
      @Override
      public Adapter caseddLike(ddLike object)
      {
        return createddLikeAdapter();
      }
      @Override
      public Adapter caseddLgstream(ddLgstream object)
      {
        return createddLgstreamAdapter();
      }
      @Override
      public Adapter caseddLabel(ddLabel object)
      {
        return createddLabelAdapter();
      }
      @Override
      public Adapter caseddLabelExpire(ddLabelExpire object)
      {
        return createddLabelExpireAdapter();
      }
      @Override
      public Adapter caseddLabelExpdt(ddLabelExpdt object)
      {
        return createddLabelExpdtAdapter();
      }
      @Override
      public Adapter caseddLabelRetpd(ddLabelRetpd object)
      {
        return createddLabelRetpdAdapter();
      }
      @Override
      public Adapter caseddKeyoff(ddKeyoff object)
      {
        return createddKeyoffAdapter();
      }
      @Override
      public Adapter caseddKeylen(ddKeylen object)
      {
        return createddKeylenAdapter();
      }
      @Override
      public Adapter caseddHold(ddHold object)
      {
        return createddHoldAdapter();
      }
      @Override
      public Adapter caseddFree(ddFree object)
      {
        return createddFreeAdapter();
      }
      @Override
      public Adapter caseddFlash(ddFlash object)
      {
        return createddFlashAdapter();
      }
      @Override
      public Adapter caseddFiledata(ddFiledata object)
      {
        return createddFiledataAdapter();
      }
      @Override
      public Adapter caseddFcb(ddFcb object)
      {
        return createddFcbAdapter();
      }
      @Override
      public Adapter caseddExpdt(ddExpdt object)
      {
        return createddExpdtAdapter();
      }
      @Override
      public Adapter caseddDsorg(ddDsorg object)
      {
        return createddDsorgAdapter();
      }
      @Override
      public Adapter caseddDsname(ddDsname object)
      {
        return createddDsnameAdapter();
      }
      @Override
      public Adapter caseddDsntype(ddDsntype object)
      {
        return createddDsntypeAdapter();
      }
      @Override
      public Adapter caseddDsid(ddDsid object)
      {
        return createddDsidAdapter();
      }
      @Override
      public Adapter caseddDlm(ddDlm object)
      {
        return createddDlmAdapter();
      }
      @Override
      public Adapter caseddDisp(ddDisp object)
      {
        return createddDispAdapter();
      }
      @Override
      public Adapter caseddDest(ddDest object)
      {
        return createddDestAdapter();
      }
      @Override
      public Adapter caseddDdname(ddDdname object)
      {
        return createddDdnameAdapter();
      }
      @Override
      public Adapter caseddDcb(ddDcb object)
      {
        return createddDcbAdapter();
      }
      @Override
      public Adapter caseddDcbSubparms(ddDcbSubparms object)
      {
        return createddDcbSubparmsAdapter();
      }
      @Override
      public Adapter caseddTrtch(ddTrtch object)
      {
        return createddTrtchAdapter();
      }
      @Override
      public Adapter caseddThresh(ddThresh object)
      {
        return createddThreshAdapter();
      }
      @Override
      public Adapter caseddStack(ddStack object)
      {
        return createddStackAdapter();
      }
      @Override
      public Adapter caseddRkp(ddRkp object)
      {
        return createddRkpAdapter();
      }
      @Override
      public Adapter caseddReserve(ddReserve object)
      {
        return createddReserveAdapter();
      }
      @Override
      public Adapter caseddPrtsp(ddPrtsp object)
      {
        return createddPrtspAdapter();
      }
      @Override
      public Adapter caseddPci(ddPci object)
      {
        return createddPciAdapter();
      }
      @Override
      public Adapter caseddOptcd(ddOptcd object)
      {
        return createddOptcdAdapter();
      }
      @Override
      public Adapter caseddNtm(ddNtm object)
      {
        return createddNtmAdapter();
      }
      @Override
      public Adapter caseddNcp(ddNcp object)
      {
        return createddNcpAdapter();
      }
      @Override
      public Adapter caseddMode(ddMode object)
      {
        return createddModeAdapter();
      }
      @Override
      public Adapter caseddLimct(ddLimct object)
      {
        return createddLimctAdapter();
      }
      @Override
      public Adapter caseddIpltxid(ddIpltxid object)
      {
        return createddIpltxidAdapter();
      }
      @Override
      public Adapter caseddIntvl(ddIntvl object)
      {
        return createddIntvlAdapter();
      }
      @Override
      public Adapter caseddGncp(ddGncp object)
      {
        return createddGncpAdapter();
      }
      @Override
      public Adapter caseddFunc(ddFunc object)
      {
        return createddFuncAdapter();
      }
      @Override
      public Adapter caseddEropt(ddEropt object)
      {
        return createddEroptAdapter();
      }
      @Override
      public Adapter caseddDiagns(ddDiagns object)
      {
        return createddDiagnsAdapter();
      }
      @Override
      public Adapter caseddDen(ddDen object)
      {
        return createddDenAdapter();
      }
      @Override
      public Adapter caseddCylofl(ddCylofl object)
      {
        return createddCyloflAdapter();
      }
      @Override
      public Adapter caseddCpri(ddCpri object)
      {
        return createddCpriAdapter();
      }
      @Override
      public Adapter caseddBufsize(ddBufsize object)
      {
        return createddBufsizeAdapter();
      }
      @Override
      public Adapter caseddBufout(ddBufout object)
      {
        return createddBufoutAdapter();
      }
      @Override
      public Adapter caseddBufoff(ddBufoff object)
      {
        return createddBufoffAdapter();
      }
      @Override
      public Adapter caseddBufno(ddBufno object)
      {
        return createddBufnoAdapter();
      }
      @Override
      public Adapter caseddBufmax(ddBufmax object)
      {
        return createddBufmaxAdapter();
      }
      @Override
      public Adapter caseddBufl(ddBufl object)
      {
        return createddBuflAdapter();
      }
      @Override
      public Adapter caseddBufin(ddBufin object)
      {
        return createddBufinAdapter();
      }
      @Override
      public Adapter caseddBftek(ddBftek object)
      {
        return createddBftekAdapter();
      }
      @Override
      public Adapter caseddBfaln(ddBfaln object)
      {
        return createddBfalnAdapter();
      }
      @Override
      public Adapter caseddDataclas(ddDataclas object)
      {
        return createddDataclasAdapter();
      }
      @Override
      public Adapter caseddData(ddData object)
      {
        return createddDataAdapter();
      }
      @Override
      public Adapter caseddCopies(ddCopies object)
      {
        return createddCopiesAdapter();
      }
      @Override
      public Adapter caseddCntl(ddCntl object)
      {
        return createddCntlAdapter();
      }
      @Override
      public Adapter caseddChkpt(ddChkpt object)
      {
        return createddChkptAdapter();
      }
      @Override
      public Adapter caseddChars(ddChars object)
      {
        return createddCharsAdapter();
      }
      @Override
      public Adapter caseddCcsid(ddCcsid object)
      {
        return createddCcsidAdapter();
      }
      @Override
      public Adapter caseddBurst(ddBurst object)
      {
        return createddBurstAdapter();
      }
      @Override
      public Adapter caseddBlkszlim(ddBlkszlim object)
      {
        return createddBlkszlimAdapter();
      }
      @Override
      public Adapter caseddBlksize(ddBlksize object)
      {
        return createddBlksizeAdapter();
      }
      @Override
      public Adapter caseddAvgrec(ddAvgrec object)
      {
        return createddAvgrecAdapter();
      }
      @Override
      public Adapter caseddAmp(ddAmp object)
      {
        return createddAmpAdapter();
      }
      @Override
      public Adapter caseddAccode(ddAccode object)
      {
        return createddAccodeAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.Model
   * @generated
   */
  public Adapter createModelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.Keyword <em>Keyword</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.Keyword
   * @generated
   */
  public Adapter createKeywordAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.STEPCATDD <em>STEPCATDD</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.STEPCATDD
   * @generated
   */
  public Adapter createSTEPCATDDAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.JOBLIBDD <em>JOBLIBDD</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.JOBLIBDD
   * @generated
   */
  public Adapter createJOBLIBDDAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.JOBCATDD <em>JOBCATDD</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.JOBCATDD
   * @generated
   */
  public Adapter createJOBCATDDAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.specddSection <em>specdd Section</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.specddSection
   * @generated
   */
  public Adapter createspecddSectionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.specddDisp <em>specdd Disp</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.specddDisp
   * @generated
   */
  public Adapter createspecddDispAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.specddDsname <em>specdd Dsname</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.specddDsname
   * @generated
   */
  public Adapter createspecddDsnameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.IF <em>IF</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.IF
   * @generated
   */
  public Adapter createIFAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ELSE <em>ELSE</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ELSE
   * @generated
   */
  public Adapter createELSEAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ENDIF <em>ENDIF</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ENDIF
   * @generated
   */
  public Adapter createENDIFAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.PROC <em>PROC</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.PROC
   * @generated
   */
  public Adapter createPROCAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.PEND <em>PEND</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.PEND
   * @generated
   */
  public Adapter createPENDAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.SET <em>SET</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.SET
   * @generated
   */
  public Adapter createSETAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.JCLLIB <em>JCLLIB</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.JCLLIB
   * @generated
   */
  public Adapter createJCLLIBAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.EXEC <em>EXEC</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.EXEC
   * @generated
   */
  public Adapter createEXECAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execsection <em>execsection</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execsection
   * @generated
   */
  public Adapter createexecsectionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execTime <em>exec Time</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execTime
   * @generated
   */
  public Adapter createexecTimeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execTimevalue <em>exec Timevalue</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execTimevalue
   * @generated
   */
  public Adapter createexecTimevalueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execPerform <em>exec Perform</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execPerform
   * @generated
   */
  public Adapter createexecPerformAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execRegion <em>exec Region</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execRegion
   * @generated
   */
  public Adapter createexecRegionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execRd <em>exec Rd</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execRd
   * @generated
   */
  public Adapter createexecRdAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execParm <em>exec Parm</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execParm
   * @generated
   */
  public Adapter createexecParmAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execDynambr <em>exec Dynambr</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execDynambr
   * @generated
   */
  public Adapter createexecDynambrAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execCond <em>exec Cond</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execCond
   * @generated
   */
  public Adapter createexecCondAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execAddrspc <em>exec Addrspc</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execAddrspc
   * @generated
   */
  public Adapter createexecAddrspcAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execCcsid <em>exec Ccsid</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execCcsid
   * @generated
   */
  public Adapter createexecCcsidAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execAcct <em>exec Acct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execAcct
   * @generated
   */
  public Adapter createexecAcctAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execProc <em>exec Proc</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execProc
   * @generated
   */
  public Adapter createexecProcAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.execPgm <em>exec Pgm</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.execPgm
   * @generated
   */
  public Adapter createexecPgmAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.INCLUDE <em>INCLUDE</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.INCLUDE
   * @generated
   */
  public Adapter createINCLUDEAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.JOB <em>JOB</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.JOB
   * @generated
   */
  public Adapter createJOBAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobsection <em>jobsection</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobsection
   * @generated
   */
  public Adapter createjobsectionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobUser <em>job User</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobUser
   * @generated
   */
  public Adapter createjobUserAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobTyprun <em>job Typrun</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTyprun
   * @generated
   */
  public Adapter createjobTyprunAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobTime <em>job Time</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTime
   * @generated
   */
  public Adapter createjobTimeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobTimevalue <em>job Timevalue</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobTimevalue
   * @generated
   */
  public Adapter createjobTimevalueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobSchenv <em>job Schenv</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobSchenv
   * @generated
   */
  public Adapter createjobSchenvAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobSeclabel <em>job Seclabel</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobSeclabel
   * @generated
   */
  public Adapter createjobSeclabelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobRestart <em>job Restart</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRestart
   * @generated
   */
  public Adapter createjobRestartAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobRegion <em>job Region</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRegion
   * @generated
   */
  public Adapter createjobRegionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobRd <em>job Rd</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobRd
   * @generated
   */
  public Adapter createjobRdAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobPrty <em>job Prty</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPrty
   * @generated
   */
  public Adapter createjobPrtyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobProgname <em>job Progname</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobProgname
   * @generated
   */
  public Adapter createjobPrognameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobPerform <em>job Perform</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPerform
   * @generated
   */
  public Adapter createjobPerformAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobPassword <em>job Password</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPassword
   * @generated
   */
  public Adapter createjobPasswordAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobPages <em>job Pages</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobPages
   * @generated
   */
  public Adapter createjobPagesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobNotify <em>job Notify</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobNotify
   * @generated
   */
  public Adapter createjobNotifyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobMsglevel <em>job Msglevel</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobMsglevel
   * @generated
   */
  public Adapter createjobMsglevelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobMsgClass <em>job Msg Class</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobMsgClass
   * @generated
   */
  public Adapter createjobMsgClassAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobLines <em>job Lines</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobLines
   * @generated
   */
  public Adapter createjobLinesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobGroup <em>job Group</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobGroup
   * @generated
   */
  public Adapter createjobGroupAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobCond <em>job Cond</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCond
   * @generated
   */
  public Adapter createjobCondAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobClass <em>job Class</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobClass
   * @generated
   */
  public Adapter createjobClassAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobCcsid <em>job Ccsid</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCcsid
   * @generated
   */
  public Adapter createjobCcsidAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobCards <em>job Cards</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobCards
   * @generated
   */
  public Adapter createjobCardsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobBytes <em>job Bytes</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobBytes
   * @generated
   */
  public Adapter createjobBytesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobAddrspc <em>job Addrspc</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobAddrspc
   * @generated
   */
  public Adapter createjobAddrspcAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.jobAccinfo <em>job Accinfo</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.jobAccinfo
   * @generated
   */
  public Adapter createjobAccinfoAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.DD <em>DD</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.DD
   * @generated
   */
  public Adapter createDDAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddsection <em>ddsection</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddsection
   * @generated
   */
  public Adapter createddsectionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume <em>dd Volume</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddVolume
   * @generated
   */
  public Adapter createddVolumeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddUnit <em>dd Unit</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUnit
   * @generated
   */
  public Adapter createddUnitAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddUcs <em>dd Ucs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddUcs
   * @generated
   */
  public Adapter createddUcsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddTerm <em>dd Term</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddTerm
   * @generated
   */
  public Adapter createddTermAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddSysout <em>dd Sysout</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSysout
   * @generated
   */
  public Adapter createddSysoutAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsys <em>dd Subsys</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSubsys
   * @generated
   */
  public Adapter createddSubsysAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms <em>dd Subsys Subparms</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms
   * @generated
   */
  public Adapter createddSubsysSubparmsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddStorclas <em>dd Storclas</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddStorclas
   * @generated
   */
  public Adapter createddStorclasAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddSpin <em>dd Spin</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSpin
   * @generated
   */
  public Adapter createddSpinAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace <em>dd Space</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSpace
   * @generated
   */
  public Adapter createddSpaceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddSegment <em>dd Segment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSegment
   * @generated
   */
  public Adapter createddSegmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddSecmodel <em>dd Secmodel</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddSecmodel
   * @generated
   */
  public Adapter createddSecmodelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddRls <em>dd Rls</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRls
   * @generated
   */
  public Adapter createddRlsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddRetpd <em>dd Retpd</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRetpd
   * @generated
   */
  public Adapter createddRetpdAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddRefdd <em>dd Refdd</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRefdd
   * @generated
   */
  public Adapter createddRefddAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddRecorg <em>dd Recorg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRecorg
   * @generated
   */
  public Adapter createddRecorgAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddRecfm <em>dd Recfm</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRecfm
   * @generated
   */
  public Adapter createddRecfmAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddQname <em>dd Qname</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddQname
   * @generated
   */
  public Adapter createddQnameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddProtect <em>dd Protect</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddProtect
   * @generated
   */
  public Adapter createddProtectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddPathmode <em>dd Pathmode</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathmode
   * @generated
   */
  public Adapter createddPathmodeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddPathopts <em>dd Pathopts</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathopts
   * @generated
   */
  public Adapter createddPathoptsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddPathdisp <em>dd Pathdisp</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPathdisp
   * @generated
   */
  public Adapter createddPathdispAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddPath <em>dd Path</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPath
   * @generated
   */
  public Adapter createddPathAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddOutput <em>dd Output</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddOutput
   * @generated
   */
  public Adapter createddOutputAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddOutlim <em>dd Outlim</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddOutlim
   * @generated
   */
  public Adapter createddOutlimAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddModify <em>dd Modify</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddModify
   * @generated
   */
  public Adapter createddModifyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddMgmtclas <em>dd Mgmtclas</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddMgmtclas
   * @generated
   */
  public Adapter createddMgmtclasAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddLrecl <em>dd Lrecl</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLrecl
   * @generated
   */
  public Adapter createddLreclAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddLike <em>dd Like</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLike
   * @generated
   */
  public Adapter createddLikeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddLgstream <em>dd Lgstream</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLgstream
   * @generated
   */
  public Adapter createddLgstreamAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddLabel <em>dd Label</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabel
   * @generated
   */
  public Adapter createddLabelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpire <em>dd Label Expire</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabelExpire
   * @generated
   */
  public Adapter createddLabelExpireAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt <em>dd Label Expdt</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt
   * @generated
   */
  public Adapter createddLabelExpdtAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelRetpd <em>dd Label Retpd</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLabelRetpd
   * @generated
   */
  public Adapter createddLabelRetpdAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddKeyoff <em>dd Keyoff</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddKeyoff
   * @generated
   */
  public Adapter createddKeyoffAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddKeylen <em>dd Keylen</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddKeylen
   * @generated
   */
  public Adapter createddKeylenAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddHold <em>dd Hold</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddHold
   * @generated
   */
  public Adapter createddHoldAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddFree <em>dd Free</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFree
   * @generated
   */
  public Adapter createddFreeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddFlash <em>dd Flash</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFlash
   * @generated
   */
  public Adapter createddFlashAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddFiledata <em>dd Filedata</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFiledata
   * @generated
   */
  public Adapter createddFiledataAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddFcb <em>dd Fcb</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFcb
   * @generated
   */
  public Adapter createddFcbAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddExpdt <em>dd Expdt</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddExpdt
   * @generated
   */
  public Adapter createddExpdtAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDsorg <em>dd Dsorg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsorg
   * @generated
   */
  public Adapter createddDsorgAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDsname <em>dd Dsname</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsname
   * @generated
   */
  public Adapter createddDsnameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDsntype <em>dd Dsntype</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsntype
   * @generated
   */
  public Adapter createddDsntypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDsid <em>dd Dsid</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDsid
   * @generated
   */
  public Adapter createddDsidAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDlm <em>dd Dlm</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDlm
   * @generated
   */
  public Adapter createddDlmAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDisp <em>dd Disp</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDisp
   * @generated
   */
  public Adapter createddDispAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDest <em>dd Dest</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDest
   * @generated
   */
  public Adapter createddDestAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDdname <em>dd Ddname</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDdname
   * @generated
   */
  public Adapter createddDdnameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDcb <em>dd Dcb</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDcb
   * @generated
   */
  public Adapter createddDcbAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDcbSubparms <em>dd Dcb Subparms</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDcbSubparms
   * @generated
   */
  public Adapter createddDcbSubparmsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddTrtch <em>dd Trtch</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddTrtch
   * @generated
   */
  public Adapter createddTrtchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddThresh <em>dd Thresh</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddThresh
   * @generated
   */
  public Adapter createddThreshAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddStack <em>dd Stack</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddStack
   * @generated
   */
  public Adapter createddStackAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddRkp <em>dd Rkp</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddRkp
   * @generated
   */
  public Adapter createddRkpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddReserve <em>dd Reserve</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddReserve
   * @generated
   */
  public Adapter createddReserveAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddPrtsp <em>dd Prtsp</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPrtsp
   * @generated
   */
  public Adapter createddPrtspAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddPci <em>dd Pci</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddPci
   * @generated
   */
  public Adapter createddPciAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddOptcd <em>dd Optcd</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddOptcd
   * @generated
   */
  public Adapter createddOptcdAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddNtm <em>dd Ntm</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddNtm
   * @generated
   */
  public Adapter createddNtmAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddNcp <em>dd Ncp</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddNcp
   * @generated
   */
  public Adapter createddNcpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddMode <em>dd Mode</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddMode
   * @generated
   */
  public Adapter createddModeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddLimct <em>dd Limct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddLimct
   * @generated
   */
  public Adapter createddLimctAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddIpltxid <em>dd Ipltxid</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddIpltxid
   * @generated
   */
  public Adapter createddIpltxidAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddIntvl <em>dd Intvl</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddIntvl
   * @generated
   */
  public Adapter createddIntvlAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddGncp <em>dd Gncp</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddGncp
   * @generated
   */
  public Adapter createddGncpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddFunc <em>dd Func</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddFunc
   * @generated
   */
  public Adapter createddFuncAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddEropt <em>dd Eropt</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddEropt
   * @generated
   */
  public Adapter createddEroptAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDiagns <em>dd Diagns</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDiagns
   * @generated
   */
  public Adapter createddDiagnsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDen <em>dd Den</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDen
   * @generated
   */
  public Adapter createddDenAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddCylofl <em>dd Cylofl</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCylofl
   * @generated
   */
  public Adapter createddCyloflAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddCpri <em>dd Cpri</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCpri
   * @generated
   */
  public Adapter createddCpriAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufsize <em>dd Bufsize</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufsize
   * @generated
   */
  public Adapter createddBufsizeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufout <em>dd Bufout</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufout
   * @generated
   */
  public Adapter createddBufoutAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufoff <em>dd Bufoff</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufoff
   * @generated
   */
  public Adapter createddBufoffAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufno <em>dd Bufno</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufno
   * @generated
   */
  public Adapter createddBufnoAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufmax <em>dd Bufmax</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufmax
   * @generated
   */
  public Adapter createddBufmaxAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufl <em>dd Bufl</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufl
   * @generated
   */
  public Adapter createddBuflAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBufin <em>dd Bufin</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBufin
   * @generated
   */
  public Adapter createddBufinAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBftek <em>dd Bftek</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBftek
   * @generated
   */
  public Adapter createddBftekAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBfaln <em>dd Bfaln</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBfaln
   * @generated
   */
  public Adapter createddBfalnAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddDataclas <em>dd Dataclas</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddDataclas
   * @generated
   */
  public Adapter createddDataclasAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddData <em>dd Data</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddData
   * @generated
   */
  public Adapter createddDataAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddCopies <em>dd Copies</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCopies
   * @generated
   */
  public Adapter createddCopiesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddCntl <em>dd Cntl</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCntl
   * @generated
   */
  public Adapter createddCntlAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddChkpt <em>dd Chkpt</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddChkpt
   * @generated
   */
  public Adapter createddChkptAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddChars <em>dd Chars</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddChars
   * @generated
   */
  public Adapter createddCharsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddCcsid <em>dd Ccsid</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddCcsid
   * @generated
   */
  public Adapter createddCcsidAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBurst <em>dd Burst</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBurst
   * @generated
   */
  public Adapter createddBurstAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBlkszlim <em>dd Blkszlim</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBlkszlim
   * @generated
   */
  public Adapter createddBlkszlimAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddBlksize <em>dd Blksize</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddBlksize
   * @generated
   */
  public Adapter createddBlksizeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddAvgrec <em>dd Avgrec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddAvgrec
   * @generated
   */
  public Adapter createddAvgrecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddAmp <em>dd Amp</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddAmp
   * @generated
   */
  public Adapter createddAmpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.kb.jcl.jcldsl.jclDsl.ddAccode <em>dd Accode</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.kb.jcl.jcldsl.jclDsl.ddAccode
   * @generated
   */
  public Adapter createddAccodeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //JclDslAdapterFactory
