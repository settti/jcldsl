/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Secmodel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSecmodel#getDsname <em>Dsname</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSecmodel#getGeneric <em>Generic</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSecmodel()
 * @model
 * @generated
 */
public interface ddSecmodel extends ddsection
{
  /**
   * Returns the value of the '<em><b>Dsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dsname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dsname</em>' attribute.
   * @see #setDsname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSecmodel_Dsname()
   * @model
   * @generated
   */
  String getDsname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSecmodel#getDsname <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dsname</em>' attribute.
   * @see #getDsname()
   * @generated
   */
  void setDsname(String value);

  /**
   * Returns the value of the '<em><b>Generic</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Generic</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Generic</em>' attribute.
   * @see #setGeneric(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSecmodel_Generic()
   * @model
   * @generated
   */
  String getGeneric();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSecmodel#getGeneric <em>Generic</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Generic</em>' attribute.
   * @see #getGeneric()
   * @generated
   */
  void setGeneric(String value);

} // ddSecmodel
