/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>exec Parm</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParm <em>Parm</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParmkeyw <em>Parmkeyw</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParm2 <em>Parm2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParmkeyw2 <em>Parmkeyw2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParmint <em>Parmint</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecParm()
 * @model
 * @generated
 */
public interface execParm extends execsection
{
  /**
   * Returns the value of the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm</em>' attribute.
   * @see #setParm(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecParm_Parm()
   * @model
   * @generated
   */
  String getParm();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParm <em>Parm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parm</em>' attribute.
   * @see #getParm()
   * @generated
   */
  void setParm(String value);

  /**
   * Returns the value of the '<em><b>Parmkeyw</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parmkeyw</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parmkeyw</em>' attribute.
   * @see #setParmkeyw(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecParm_Parmkeyw()
   * @model
   * @generated
   */
  String getParmkeyw();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParmkeyw <em>Parmkeyw</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parmkeyw</em>' attribute.
   * @see #getParmkeyw()
   * @generated
   */
  void setParmkeyw(String value);

  /**
   * Returns the value of the '<em><b>Parm2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecParm_Parm2()
   * @model unique="false"
   * @generated
   */
  EList<String> getParm2();

  /**
   * Returns the value of the '<em><b>Parmkeyw2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parmkeyw2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parmkeyw2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecParm_Parmkeyw2()
   * @model unique="false"
   * @generated
   */
  EList<String> getParmkeyw2();

  /**
   * Returns the value of the '<em><b>Parmint</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parmint</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parmint</em>' attribute.
   * @see #setParmint(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecParm_Parmint()
   * @model
   * @generated
   */
  int getParmint();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execParm#getParmint <em>Parmint</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parmint</em>' attribute.
   * @see #getParmint()
   * @generated
   */
  void setParmint(int value);

} // execParm
