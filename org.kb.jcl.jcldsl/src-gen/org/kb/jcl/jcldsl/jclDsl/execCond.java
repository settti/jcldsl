/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>exec Cond</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execCond#getRc <em>Rc</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execCond#getExeccondop <em>Execcondop</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execCond#isStepname <em>Stepname</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execCond#getRc2 <em>Rc2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execCond#isStepname2 <em>Stepname2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execCond#getRc3 <em>Rc3</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execCond#getExeccondop2 <em>Execcondop2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.execCond#getStepname3 <em>Stepname3</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecCond()
 * @model
 * @generated
 */
public interface execCond extends execsection
{
  /**
   * Returns the value of the '<em><b>Rc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rc</em>' attribute.
   * @see #setRc(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecCond_Rc()
   * @model
   * @generated
   */
  int getRc();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execCond#getRc <em>Rc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Rc</em>' attribute.
   * @see #getRc()
   * @generated
   */
  void setRc(int value);

  /**
   * Returns the value of the '<em><b>Execcondop</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Execcondop</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Execcondop</em>' attribute.
   * @see #setExeccondop(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecCond_Execcondop()
   * @model
   * @generated
   */
  String getExeccondop();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execCond#getExeccondop <em>Execcondop</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Execcondop</em>' attribute.
   * @see #getExeccondop()
   * @generated
   */
  void setExeccondop(String value);

  /**
   * Returns the value of the '<em><b>Stepname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Stepname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Stepname</em>' attribute.
   * @see #setStepname(boolean)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecCond_Stepname()
   * @model
   * @generated
   */
  boolean isStepname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execCond#isStepname <em>Stepname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Stepname</em>' attribute.
   * @see #isStepname()
   * @generated
   */
  void setStepname(boolean value);

  /**
   * Returns the value of the '<em><b>Rc2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rc2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rc2</em>' attribute.
   * @see #setRc2(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecCond_Rc2()
   * @model
   * @generated
   */
  int getRc2();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execCond#getRc2 <em>Rc2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Rc2</em>' attribute.
   * @see #getRc2()
   * @generated
   */
  void setRc2(int value);

  /**
   * Returns the value of the '<em><b>Stepname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Stepname2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Stepname2</em>' attribute.
   * @see #setStepname2(boolean)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecCond_Stepname2()
   * @model
   * @generated
   */
  boolean isStepname2();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.execCond#isStepname2 <em>Stepname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Stepname2</em>' attribute.
   * @see #isStepname2()
   * @generated
   */
  void setStepname2(boolean value);

  /**
   * Returns the value of the '<em><b>Rc3</b></em>' attribute list.
   * The list contents are of type {@link java.lang.Integer}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rc3</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rc3</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecCond_Rc3()
   * @model unique="false"
   * @generated
   */
  EList<Integer> getRc3();

  /**
   * Returns the value of the '<em><b>Execcondop2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Execcondop2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Execcondop2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecCond_Execcondop2()
   * @model unique="false"
   * @generated
   */
  EList<String> getExeccondop2();

  /**
   * Returns the value of the '<em><b>Stepname3</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Stepname3</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Stepname3</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getexecCond_Stepname3()
   * @model unique="false"
   * @generated
   */
  EList<String> getStepname3();

} // execCond
