/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job User</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobUser#getUSERNAME <em>USERNAME</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobUser()
 * @model
 * @generated
 */
public interface jobUser extends jobsection
{
  /**
   * Returns the value of the '<em><b>USERNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>USERNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>USERNAME</em>' attribute.
   * @see #setUSERNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobUser_USERNAME()
   * @model
   * @generated
   */
  String getUSERNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobUser#getUSERNAME <em>USERNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>USERNAME</em>' attribute.
   * @see #getUSERNAME()
   * @generated
   */
  void setUSERNAME(String value);

} // jobUser
