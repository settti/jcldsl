/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Cond</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobCond#getReturn_code <em>Return code</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobCond#getJobcondop <em>Jobcondop</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobCond#getReturn_code2 <em>Return code2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobCond#getJobcondop2 <em>Jobcondop2</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobCond()
 * @model
 * @generated
 */
public interface jobCond extends jobsection
{
  /**
   * Returns the value of the '<em><b>Return code</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Return code</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return code</em>' attribute.
   * @see #setReturn_code(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobCond_Return_code()
   * @model
   * @generated
   */
  int getReturn_code();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobCond#getReturn_code <em>Return code</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Return code</em>' attribute.
   * @see #getReturn_code()
   * @generated
   */
  void setReturn_code(int value);

  /**
   * Returns the value of the '<em><b>Jobcondop</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Jobcondop</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Jobcondop</em>' attribute.
   * @see #setJobcondop(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobCond_Jobcondop()
   * @model
   * @generated
   */
  String getJobcondop();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobCond#getJobcondop <em>Jobcondop</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Jobcondop</em>' attribute.
   * @see #getJobcondop()
   * @generated
   */
  void setJobcondop(String value);

  /**
   * Returns the value of the '<em><b>Return code2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.Integer}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Return code2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return code2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobCond_Return_code2()
   * @model unique="false"
   * @generated
   */
  EList<Integer> getReturn_code2();

  /**
   * Returns the value of the '<em><b>Jobcondop2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Jobcondop2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Jobcondop2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobCond_Jobcondop2()
   * @model unique="false"
   * @generated
   */
  EList<String> getJobcondop2();

} // jobCond
