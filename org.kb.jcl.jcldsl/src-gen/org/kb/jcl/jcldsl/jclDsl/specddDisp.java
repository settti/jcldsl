/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>specdd Disp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.specddDisp#getDisp <em>Disp</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getspecddDisp()
 * @model
 * @generated
 */
public interface specddDisp extends specddSection
{
  /**
   * Returns the value of the '<em><b>Disp</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Disp</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Disp</em>' attribute.
   * @see #setDisp(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getspecddDisp_Disp()
   * @model
   * @generated
   */
  String getDisp();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.specddDisp#getDisp <em>Disp</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Disp</em>' attribute.
   * @see #getDisp()
   * @generated
   */
  void setDisp(String value);

} // specddDisp
