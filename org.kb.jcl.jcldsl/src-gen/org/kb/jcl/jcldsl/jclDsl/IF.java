/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IF</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.IF#isIFNAME <em>IFNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.IF#getSTEPNAME <em>STEPNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.IF#getExpression_keyword <em>Expression keyword</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.IF#getOperator <em>Operator</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.IF#getValue <em>Value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.IF#getLogical_op <em>Logical op</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.IF#getExpression_keyword2 <em>Expression keyword2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.IF#getOperator2 <em>Operator2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.IF#getValue2 <em>Value2</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getIF()
 * @model
 * @generated
 */
public interface IF extends Keyword
{
  /**
   * Returns the value of the '<em><b>IFNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>IFNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>IFNAME</em>' attribute.
   * @see #setIFNAME(boolean)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getIF_IFNAME()
   * @model
   * @generated
   */
  boolean isIFNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.IF#isIFNAME <em>IFNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>IFNAME</em>' attribute.
   * @see #isIFNAME()
   * @generated
   */
  void setIFNAME(boolean value);

  /**
   * Returns the value of the '<em><b>STEPNAME</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>STEPNAME</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>STEPNAME</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getIF_STEPNAME()
   * @model unique="false"
   * @generated
   */
  EList<String> getSTEPNAME();

  /**
   * Returns the value of the '<em><b>Expression keyword</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression keyword</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression keyword</em>' attribute.
   * @see #setExpression_keyword(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getIF_Expression_keyword()
   * @model
   * @generated
   */
  String getExpression_keyword();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.IF#getExpression_keyword <em>Expression keyword</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression keyword</em>' attribute.
   * @see #getExpression_keyword()
   * @generated
   */
  void setExpression_keyword(String value);

  /**
   * Returns the value of the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operator</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operator</em>' attribute.
   * @see #setOperator(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getIF_Operator()
   * @model
   * @generated
   */
  String getOperator();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.IF#getOperator <em>Operator</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Operator</em>' attribute.
   * @see #getOperator()
   * @generated
   */
  void setOperator(String value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getIF_Value()
   * @model
   * @generated
   */
  int getValue();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.IF#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(int value);

  /**
   * Returns the value of the '<em><b>Logical op</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Logical op</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Logical op</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getIF_Logical_op()
   * @model unique="false"
   * @generated
   */
  EList<String> getLogical_op();

  /**
   * Returns the value of the '<em><b>Expression keyword2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression keyword2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression keyword2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getIF_Expression_keyword2()
   * @model unique="false"
   * @generated
   */
  EList<String> getExpression_keyword2();

  /**
   * Returns the value of the '<em><b>Operator2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operator2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operator2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getIF_Operator2()
   * @model unique="false"
   * @generated
   */
  EList<String> getOperator2();

  /**
   * Returns the value of the '<em><b>Value2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.Integer}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getIF_Value2()
   * @model unique="false"
   * @generated
   */
  EList<Integer> getValue2();

} // IF
