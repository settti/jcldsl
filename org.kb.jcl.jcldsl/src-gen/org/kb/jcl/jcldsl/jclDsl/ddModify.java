/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Modify</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddModify#getModule <em>Module</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddModify#getTrc <em>Trc</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddModify()
 * @model
 * @generated
 */
public interface ddModify extends ddsection
{
  /**
   * Returns the value of the '<em><b>Module</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Module</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Module</em>' attribute.
   * @see #setModule(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddModify_Module()
   * @model
   * @generated
   */
  String getModule();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddModify#getModule <em>Module</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Module</em>' attribute.
   * @see #getModule()
   * @generated
   */
  void setModule(String value);

  /**
   * Returns the value of the '<em><b>Trc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Trc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Trc</em>' attribute.
   * @see #setTrc(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddModify_Trc()
   * @model
   * @generated
   */
  int getTrc();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddModify#getTrc <em>Trc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Trc</em>' attribute.
   * @see #getTrc()
   * @generated
   */
  void setTrc(int value);

} // ddModify
