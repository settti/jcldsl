/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PEND</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.PEND#isPENDNAME <em>PENDNAME</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getPEND()
 * @model
 * @generated
 */
public interface PEND extends Keyword
{
  /**
   * Returns the value of the '<em><b>PENDNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>PENDNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>PENDNAME</em>' attribute.
   * @see #setPENDNAME(boolean)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getPEND_PENDNAME()
   * @model
   * @generated
   */
  boolean isPENDNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.PEND#isPENDNAME <em>PENDNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>PENDNAME</em>' attribute.
   * @see #isPENDNAME()
   * @generated
   */
  void setPENDNAME(boolean value);

} // PEND
