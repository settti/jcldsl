/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Dsname</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDsname#getDSNAMES <em>DSNAMES</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDsname()
 * @model
 * @generated
 */
public interface ddDsname extends ddsection
{
  /**
   * Returns the value of the '<em><b>DSNAMES</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>DSNAMES</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>DSNAMES</em>' attribute.
   * @see #setDSNAMES(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDsname_DSNAMES()
   * @model
   * @generated
   */
  String getDSNAMES();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDsname#getDSNAMES <em>DSNAMES</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>DSNAMES</em>' attribute.
   * @see #getDSNAMES()
   * @generated
   */
  void setDSNAMES(String value);

} // ddDsname
