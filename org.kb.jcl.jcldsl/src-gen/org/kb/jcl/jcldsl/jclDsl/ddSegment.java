/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Segment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSegment#getPage_count <em>Page count</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSegment()
 * @model
 * @generated
 */
public interface ddSegment extends ddsection
{
  /**
   * Returns the value of the '<em><b>Page count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Page count</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Page count</em>' attribute.
   * @see #setPage_count(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSegment_Page_count()
   * @model
   * @generated
   */
  int getPage_count();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSegment#getPage_count <em>Page count</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Page count</em>' attribute.
   * @see #getPage_count()
   * @generated
   */
  void setPage_count(int value);

} // ddSegment
