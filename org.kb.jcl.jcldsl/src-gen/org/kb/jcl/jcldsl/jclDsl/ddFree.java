/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Free</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddFree()
 * @model
 * @generated
 */
public interface ddFree extends ddsection
{
} // ddFree
