/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Dsorg</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDsorg#getDsorg <em>Dsorg</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDsorg()
 * @model
 * @generated
 */
public interface ddDsorg extends ddsection, ddDcbSubparms
{
  /**
   * Returns the value of the '<em><b>Dsorg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dsorg</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dsorg</em>' attribute.
   * @see #setDsorg(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDsorg_Dsorg()
   * @model
   * @generated
   */
  String getDsorg();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDsorg#getDsorg <em>Dsorg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dsorg</em>' attribute.
   * @see #getDsorg()
   * @generated
   */
  void setDsorg(String value);

} // ddDsorg
