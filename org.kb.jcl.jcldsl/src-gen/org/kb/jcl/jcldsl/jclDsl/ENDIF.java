/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ENDIF</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ENDIF#getENDIFNAME <em>ENDIFNAME</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getENDIF()
 * @model
 * @generated
 */
public interface ENDIF extends Keyword
{
  /**
   * Returns the value of the '<em><b>ENDIFNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>ENDIFNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>ENDIFNAME</em>' attribute.
   * @see #setENDIFNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getENDIF_ENDIFNAME()
   * @model
   * @generated
   */
  String getENDIFNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ENDIF#getENDIFNAME <em>ENDIFNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>ENDIFNAME</em>' attribute.
   * @see #getENDIFNAME()
   * @generated
   */
  void setENDIFNAME(String value);

} // ENDIF
