/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Dcb Subparms</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDcbSubparms()
 * @model
 * @generated
 */
public interface ddDcbSubparms extends EObject
{
} // ddDcbSubparms
