/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INCLUDE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.INCLUDE#getINCLUDENAME <em>INCLUDENAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.INCLUDE#getMEMBERNAME <em>MEMBERNAME</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getINCLUDE()
 * @model
 * @generated
 */
public interface INCLUDE extends Keyword
{
  /**
   * Returns the value of the '<em><b>INCLUDENAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>INCLUDENAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>INCLUDENAME</em>' attribute.
   * @see #setINCLUDENAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getINCLUDE_INCLUDENAME()
   * @model
   * @generated
   */
  String getINCLUDENAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.INCLUDE#getINCLUDENAME <em>INCLUDENAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>INCLUDENAME</em>' attribute.
   * @see #getINCLUDENAME()
   * @generated
   */
  void setINCLUDENAME(String value);

  /**
   * Returns the value of the '<em><b>MEMBERNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>MEMBERNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>MEMBERNAME</em>' attribute.
   * @see #setMEMBERNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getINCLUDE_MEMBERNAME()
   * @model
   * @generated
   */
  String getMEMBERNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.INCLUDE#getMEMBERNAME <em>MEMBERNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>MEMBERNAME</em>' attribute.
   * @see #getMEMBERNAME()
   * @generated
   */
  void setMEMBERNAME(String value);

} // INCLUDE
