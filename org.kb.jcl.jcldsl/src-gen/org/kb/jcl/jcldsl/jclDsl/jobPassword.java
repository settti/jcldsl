/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Password</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobPassword#getPASSWORD <em>PASSWORD</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobPassword#getNEWPASSWORD <em>NEWPASSWORD</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobPassword()
 * @model
 * @generated
 */
public interface jobPassword extends jobsection
{
  /**
   * Returns the value of the '<em><b>PASSWORD</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>PASSWORD</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>PASSWORD</em>' attribute.
   * @see #setPASSWORD(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobPassword_PASSWORD()
   * @model
   * @generated
   */
  String getPASSWORD();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobPassword#getPASSWORD <em>PASSWORD</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>PASSWORD</em>' attribute.
   * @see #getPASSWORD()
   * @generated
   */
  void setPASSWORD(String value);

  /**
   * Returns the value of the '<em><b>NEWPASSWORD</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>NEWPASSWORD</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>NEWPASSWORD</em>' attribute.
   * @see #setNEWPASSWORD(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobPassword_NEWPASSWORD()
   * @model
   * @generated
   */
  String getNEWPASSWORD();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobPassword#getNEWPASSWORD <em>NEWPASSWORD</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>NEWPASSWORD</em>' attribute.
   * @see #getNEWPASSWORD()
   * @generated
   */
  void setNEWPASSWORD(String value);

} // jobPassword
