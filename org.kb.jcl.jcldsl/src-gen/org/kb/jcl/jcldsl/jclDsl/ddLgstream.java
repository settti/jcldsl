/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Lgstream</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLgstream#getLogstream <em>Logstream</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLgstream()
 * @model
 * @generated
 */
public interface ddLgstream extends ddsection
{
  /**
   * Returns the value of the '<em><b>Logstream</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Logstream</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Logstream</em>' attribute.
   * @see #setLogstream(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLgstream_Logstream()
   * @model
   * @generated
   */
  String getLogstream();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLgstream#getLogstream <em>Logstream</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Logstream</em>' attribute.
   * @see #getLogstream()
   * @generated
   */
  void setLogstream(String value);

} // ddLgstream
