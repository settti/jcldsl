/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>dd Recorg Values</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddRecorgValues()
 * @model
 * @generated
 */
public enum ddRecorgValues implements Enumerator
{
  /**
   * The '<em><b>KS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #KS_VALUE
   * @generated
   * @ordered
   */
  KS(0, "KS", "KS"),

  /**
   * The '<em><b>ES</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ES_VALUE
   * @generated
   * @ordered
   */
  ES(1, "ES", "ES"),

  /**
   * The '<em><b>RR</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #RR_VALUE
   * @generated
   * @ordered
   */
  RR(2, "RR", "RR"),

  /**
   * The '<em><b>LS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #LS_VALUE
   * @generated
   * @ordered
   */
  LS(3, "LS", "LS");

  /**
   * The '<em><b>KS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>KS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #KS
   * @model
   * @generated
   * @ordered
   */
  public static final int KS_VALUE = 0;

  /**
   * The '<em><b>ES</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ES</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ES
   * @model
   * @generated
   * @ordered
   */
  public static final int ES_VALUE = 1;

  /**
   * The '<em><b>RR</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>RR</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #RR
   * @model
   * @generated
   * @ordered
   */
  public static final int RR_VALUE = 2;

  /**
   * The '<em><b>LS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>LS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #LS
   * @model
   * @generated
   * @ordered
   */
  public static final int LS_VALUE = 3;

  /**
   * An array of all the '<em><b>dd Recorg Values</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final ddRecorgValues[] VALUES_ARRAY =
    new ddRecorgValues[]
    {
      KS,
      ES,
      RR,
      LS,
    };

  /**
   * A public read-only list of all the '<em><b>dd Recorg Values</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<ddRecorgValues> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>dd Recorg Values</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddRecorgValues get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      ddRecorgValues result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>dd Recorg Values</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddRecorgValues getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      ddRecorgValues result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>dd Recorg Values</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddRecorgValues get(int value)
  {
    switch (value)
    {
      case KS_VALUE: return KS;
      case ES_VALUE: return ES;
      case RR_VALUE: return RR;
      case LS_VALUE: return LS;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private ddRecorgValues(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //ddRecorgValues
