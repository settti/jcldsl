/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Ddname</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDdname#getDdname <em>Ddname</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDdname()
 * @model
 * @generated
 */
public interface ddDdname extends ddsection
{
  /**
   * Returns the value of the '<em><b>Ddname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ddname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ddname</em>' attribute.
   * @see #setDdname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDdname_Ddname()
   * @model
   * @generated
   */
  String getDdname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDdname#getDdname <em>Ddname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ddname</em>' attribute.
   * @see #getDdname()
   * @generated
   */
  void setDdname(String value);

} // ddDdname
