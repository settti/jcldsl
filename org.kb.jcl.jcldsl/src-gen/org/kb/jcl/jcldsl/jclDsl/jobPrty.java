/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Prty</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobPrty#getPrtyvalue <em>Prtyvalue</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobPrty()
 * @model
 * @generated
 */
public interface jobPrty extends jobsection
{
  /**
   * Returns the value of the '<em><b>Prtyvalue</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prtyvalue</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prtyvalue</em>' attribute.
   * @see #setPrtyvalue(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobPrty_Prtyvalue()
   * @model
   * @generated
   */
  int getPrtyvalue();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobPrty#getPrtyvalue <em>Prtyvalue</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prtyvalue</em>' attribute.
   * @see #getPrtyvalue()
   * @generated
   */
  void setPrtyvalue(int value);

} // jobPrty
