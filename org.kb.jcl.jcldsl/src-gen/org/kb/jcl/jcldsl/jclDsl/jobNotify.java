/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Notify</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobNotify#getUSERID <em>USERID</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobNotify()
 * @model
 * @generated
 */
public interface jobNotify extends jobsection
{
  /**
   * Returns the value of the '<em><b>USERID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>USERID</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>USERID</em>' attribute.
   * @see #setUSERID(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobNotify_USERID()
   * @model
   * @generated
   */
  String getUSERID();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobNotify#getUSERID <em>USERID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>USERID</em>' attribute.
   * @see #getUSERID()
   * @generated
   */
  void setUSERID(String value);

} // jobNotify
