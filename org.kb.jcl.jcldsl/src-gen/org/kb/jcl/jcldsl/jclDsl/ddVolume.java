/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Volume</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getPrivate <em>Private</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getRetain <em>Retain</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getVolseq <em>Volseq</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getVolcnt <em>Volcnt</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getSerial <em>Serial</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getSerials <em>Serials</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getDsname <em>Dsname</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddVolume()
 * @model
 * @generated
 */
public interface ddVolume extends ddsection
{
  /**
   * Returns the value of the '<em><b>Private</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Private</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Private</em>' attribute.
   * @see #setPrivate(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddVolume_Private()
   * @model
   * @generated
   */
  String getPrivate();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getPrivate <em>Private</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Private</em>' attribute.
   * @see #getPrivate()
   * @generated
   */
  void setPrivate(String value);

  /**
   * Returns the value of the '<em><b>Retain</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Retain</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Retain</em>' attribute.
   * @see #setRetain(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddVolume_Retain()
   * @model
   * @generated
   */
  String getRetain();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getRetain <em>Retain</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Retain</em>' attribute.
   * @see #getRetain()
   * @generated
   */
  void setRetain(String value);

  /**
   * Returns the value of the '<em><b>Volseq</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Volseq</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Volseq</em>' attribute.
   * @see #setVolseq(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddVolume_Volseq()
   * @model
   * @generated
   */
  int getVolseq();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getVolseq <em>Volseq</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Volseq</em>' attribute.
   * @see #getVolseq()
   * @generated
   */
  void setVolseq(int value);

  /**
   * Returns the value of the '<em><b>Volcnt</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Volcnt</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Volcnt</em>' attribute.
   * @see #setVolcnt(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddVolume_Volcnt()
   * @model
   * @generated
   */
  int getVolcnt();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getVolcnt <em>Volcnt</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Volcnt</em>' attribute.
   * @see #getVolcnt()
   * @generated
   */
  void setVolcnt(int value);

  /**
   * Returns the value of the '<em><b>Serial</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Serial</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Serial</em>' attribute.
   * @see #setSerial(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddVolume_Serial()
   * @model
   * @generated
   */
  String getSerial();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getSerial <em>Serial</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Serial</em>' attribute.
   * @see #getSerial()
   * @generated
   */
  void setSerial(String value);

  /**
   * Returns the value of the '<em><b>Serials</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Serials</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Serials</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddVolume_Serials()
   * @model unique="false"
   * @generated
   */
  EList<String> getSerials();

  /**
   * Returns the value of the '<em><b>Dsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dsname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dsname</em>' attribute.
   * @see #setDsname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddVolume_Dsname()
   * @model
   * @generated
   */
  String getDsname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddVolume#getDsname <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dsname</em>' attribute.
   * @see #getDsname()
   * @generated
   */
  void setDsname(String value);

} // ddVolume
