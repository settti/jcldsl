/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Bufl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddBufl#getBytes <em>Bytes</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddBufl()
 * @model
 * @generated
 */
public interface ddBufl extends ddsection, ddDcbSubparms
{
  /**
   * Returns the value of the '<em><b>Bytes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bytes</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bytes</em>' attribute.
   * @see #setBytes(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddBufl_Bytes()
   * @model
   * @generated
   */
  int getBytes();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddBufl#getBytes <em>Bytes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bytes</em>' attribute.
   * @see #getBytes()
   * @generated
   */
  void setBytes(int value);

} // ddBufl
