/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Output</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddOutput#getOutput <em>Output</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddOutput#getOutput2 <em>Output2</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddOutput()
 * @model
 * @generated
 */
public interface ddOutput extends ddsection
{
  /**
   * Returns the value of the '<em><b>Output</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Output</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Output</em>' attribute.
   * @see #setOutput(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddOutput_Output()
   * @model
   * @generated
   */
  String getOutput();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddOutput#getOutput <em>Output</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Output</em>' attribute.
   * @see #getOutput()
   * @generated
   */
  void setOutput(String value);

  /**
   * Returns the value of the '<em><b>Output2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Output2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Output2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddOutput_Output2()
   * @model unique="false"
   * @generated
   */
  EList<String> getOutput2();

} // ddOutput
