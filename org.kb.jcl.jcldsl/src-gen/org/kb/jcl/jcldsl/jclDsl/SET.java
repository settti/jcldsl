/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SET</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.SET#getSETNAME <em>SETNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.SET#getParmname <em>Parmname</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.SET#getParm_value <em>Parm value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.SET#getParm_valueb <em>Parm valueb</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.SET#getParmname2 <em>Parmname2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.SET#getParm_value2 <em>Parm value2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.SET#getParm_valueb2 <em>Parm valueb2</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getSET()
 * @model
 * @generated
 */
public interface SET extends Keyword
{
  /**
   * Returns the value of the '<em><b>SETNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>SETNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>SETNAME</em>' attribute.
   * @see #setSETNAME(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getSET_SETNAME()
   * @model
   * @generated
   */
  String getSETNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.SET#getSETNAME <em>SETNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>SETNAME</em>' attribute.
   * @see #getSETNAME()
   * @generated
   */
  void setSETNAME(String value);

  /**
   * Returns the value of the '<em><b>Parmname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parmname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parmname</em>' attribute.
   * @see #setParmname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getSET_Parmname()
   * @model
   * @generated
   */
  String getParmname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.SET#getParmname <em>Parmname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parmname</em>' attribute.
   * @see #getParmname()
   * @generated
   */
  void setParmname(String value);

  /**
   * Returns the value of the '<em><b>Parm value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm value</em>' attribute.
   * @see #setParm_value(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getSET_Parm_value()
   * @model
   * @generated
   */
  String getParm_value();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.SET#getParm_value <em>Parm value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parm value</em>' attribute.
   * @see #getParm_value()
   * @generated
   */
  void setParm_value(String value);

  /**
   * Returns the value of the '<em><b>Parm valueb</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm valueb</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm valueb</em>' attribute.
   * @see #setParm_valueb(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getSET_Parm_valueb()
   * @model
   * @generated
   */
  String getParm_valueb();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.SET#getParm_valueb <em>Parm valueb</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parm valueb</em>' attribute.
   * @see #getParm_valueb()
   * @generated
   */
  void setParm_valueb(String value);

  /**
   * Returns the value of the '<em><b>Parmname2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parmname2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parmname2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getSET_Parmname2()
   * @model unique="false"
   * @generated
   */
  EList<String> getParmname2();

  /**
   * Returns the value of the '<em><b>Parm value2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm value2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm value2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getSET_Parm_value2()
   * @model unique="false"
   * @generated
   */
  EList<String> getParm_value2();

  /**
   * Returns the value of the '<em><b>Parm valueb2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm valueb2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm valueb2</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getSET_Parm_valueb2()
   * @model unique="false"
   * @generated
   */
  EList<String> getParm_valueb2();

} // SET
