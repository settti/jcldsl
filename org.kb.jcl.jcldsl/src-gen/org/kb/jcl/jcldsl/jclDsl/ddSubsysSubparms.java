/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Subsys Subparms</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms#getSubparm <em>Subparm</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms#getSubparm3 <em>Subparm3</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSubsysSubparms()
 * @model
 * @generated
 */
public interface ddSubsysSubparms extends EObject
{
  /**
   * Returns the value of the '<em><b>Subparm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subparm</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subparm</em>' attribute.
   * @see #setSubparm(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSubsysSubparms_Subparm()
   * @model
   * @generated
   */
  String getSubparm();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms#getSubparm <em>Subparm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Subparm</em>' attribute.
   * @see #getSubparm()
   * @generated
   */
  void setSubparm(String value);

  /**
   * Returns the value of the '<em><b>Subparm3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subparm3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subparm3</em>' attribute.
   * @see #setSubparm3(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSubsysSubparms_Subparm3()
   * @model
   * @generated
   */
  String getSubparm3();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSubsysSubparms#getSubparm3 <em>Subparm3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Subparm3</em>' attribute.
   * @see #getSubparm3()
   * @generated
   */
  void setSubparm3(String value);

} // ddSubsysSubparms
