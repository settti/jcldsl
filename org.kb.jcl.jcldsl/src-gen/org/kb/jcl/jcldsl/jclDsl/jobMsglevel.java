/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Msglevel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobMsglevel#getValue <em>Value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobMsglevel#getJobMsglevelStatements <em>Job Msglevel Statements</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobMsglevel#getJobMsglevelMessages <em>Job Msglevel Messages</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobMsglevel()
 * @model
 * @generated
 */
public interface jobMsglevel extends jobsection
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobMsglevel_Value()
   * @model
   * @generated
   */
  int getValue();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobMsglevel#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(int value);

  /**
   * Returns the value of the '<em><b>Job Msglevel Statements</b></em>' attribute list.
   * The list contents are of type {@link java.lang.Integer}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Job Msglevel Statements</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Job Msglevel Statements</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobMsglevel_JobMsglevelStatements()
   * @model unique="false"
   * @generated
   */
  EList<Integer> getJobMsglevelStatements();

  /**
   * Returns the value of the '<em><b>Job Msglevel Messages</b></em>' attribute list.
   * The list contents are of type {@link java.lang.Integer}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Job Msglevel Messages</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Job Msglevel Messages</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobMsglevel_JobMsglevelMessages()
   * @model unique="false"
   * @generated
   */
  EList<Integer> getJobMsglevelMessages();

} // jobMsglevel
