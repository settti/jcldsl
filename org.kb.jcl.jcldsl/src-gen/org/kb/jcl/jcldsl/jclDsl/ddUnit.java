/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getDevice <em>Device</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getCount <em>Count</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getP <em>P</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getDefer <em>Defer</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddUnit()
 * @model
 * @generated
 */
public interface ddUnit extends ddsection
{
  /**
   * Returns the value of the '<em><b>Device</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Device</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Device</em>' attribute.
   * @see #setDevice(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddUnit_Device()
   * @model
   * @generated
   */
  String getDevice();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getDevice <em>Device</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Device</em>' attribute.
   * @see #getDevice()
   * @generated
   */
  void setDevice(String value);

  /**
   * Returns the value of the '<em><b>Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Count</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Count</em>' attribute.
   * @see #setCount(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddUnit_Count()
   * @model
   * @generated
   */
  int getCount();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getCount <em>Count</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Count</em>' attribute.
   * @see #getCount()
   * @generated
   */
  void setCount(int value);

  /**
   * Returns the value of the '<em><b>P</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>P</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>P</em>' attribute.
   * @see #setP(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddUnit_P()
   * @model
   * @generated
   */
  String getP();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getP <em>P</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>P</em>' attribute.
   * @see #getP()
   * @generated
   */
  void setP(String value);

  /**
   * Returns the value of the '<em><b>Defer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Defer</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Defer</em>' attribute.
   * @see #setDefer(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddUnit_Defer()
   * @model
   * @generated
   */
  String getDefer();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddUnit#getDefer <em>Defer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Defer</em>' attribute.
   * @see #getDefer()
   * @generated
   */
  void setDefer(String value);

} // ddUnit
