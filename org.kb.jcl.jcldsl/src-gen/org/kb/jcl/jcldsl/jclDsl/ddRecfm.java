/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Recfm</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddRecfm#getRecfm <em>Recfm</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddRecfm()
 * @model
 * @generated
 */
public interface ddRecfm extends ddsection, ddDcbSubparms
{
  /**
   * Returns the value of the '<em><b>Recfm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Recfm</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Recfm</em>' attribute.
   * @see #setRecfm(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddRecfm_Recfm()
   * @model
   * @generated
   */
  String getRecfm();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddRecfm#getRecfm <em>Recfm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Recfm</em>' attribute.
   * @see #getRecfm()
   * @generated
   */
  void setRecfm(String value);

} // ddRecfm
