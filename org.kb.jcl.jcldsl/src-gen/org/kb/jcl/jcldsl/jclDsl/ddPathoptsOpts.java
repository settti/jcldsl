/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>dd Pathopts Opts</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddPathoptsOpts()
 * @model
 * @generated
 */
public enum ddPathoptsOpts implements Enumerator
{
  /**
   * The '<em><b>ORDONLY</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ORDONLY_VALUE
   * @generated
   * @ordered
   */
  ORDONLY(0, "ORDONLY", "ORDONLY"),

  /**
   * The '<em><b>OWRONLY</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #OWRONLY_VALUE
   * @generated
   * @ordered
   */
  OWRONLY(1, "OWRONLY", "OWRONLY"),

  /**
   * The '<em><b>ORDWR</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ORDWR_VALUE
   * @generated
   * @ordered
   */
  ORDWR(2, "ORDWR", "ORDWR"),

  /**
   * The '<em><b>OAPPEND</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #OAPPEND_VALUE
   * @generated
   * @ordered
   */
  OAPPEND(3, "OAPPEND", "OAPPEND"),

  /**
   * The '<em><b>OCREAT</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #OCREAT_VALUE
   * @generated
   * @ordered
   */
  OCREAT(4, "OCREAT", "OCREAT"),

  /**
   * The '<em><b>OEXCL</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #OEXCL_VALUE
   * @generated
   * @ordered
   */
  OEXCL(5, "OEXCL", "OEXCL"),

  /**
   * The '<em><b>ONOCTTY</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ONOCTTY_VALUE
   * @generated
   * @ordered
   */
  ONOCTTY(6, "ONOCTTY", "ONOCTTY"),

  /**
   * The '<em><b>ONONBLOCK</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ONONBLOCK_VALUE
   * @generated
   * @ordered
   */
  ONONBLOCK(7, "ONONBLOCK", "ONONBLOCK"),

  /**
   * The '<em><b>OSYNC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #OSYNC_VALUE
   * @generated
   * @ordered
   */
  OSYNC(8, "OSYNC", "OSYNC"),

  /**
   * The '<em><b>OTRUNC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #OTRUNC_VALUE
   * @generated
   * @ordered
   */
  OTRUNC(9, "OTRUNC", "OTRUNC");

  /**
   * The '<em><b>ORDONLY</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ORDONLY</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ORDONLY
   * @model
   * @generated
   * @ordered
   */
  public static final int ORDONLY_VALUE = 0;

  /**
   * The '<em><b>OWRONLY</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>OWRONLY</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #OWRONLY
   * @model
   * @generated
   * @ordered
   */
  public static final int OWRONLY_VALUE = 1;

  /**
   * The '<em><b>ORDWR</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ORDWR</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ORDWR
   * @model
   * @generated
   * @ordered
   */
  public static final int ORDWR_VALUE = 2;

  /**
   * The '<em><b>OAPPEND</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>OAPPEND</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #OAPPEND
   * @model
   * @generated
   * @ordered
   */
  public static final int OAPPEND_VALUE = 3;

  /**
   * The '<em><b>OCREAT</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>OCREAT</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #OCREAT
   * @model
   * @generated
   * @ordered
   */
  public static final int OCREAT_VALUE = 4;

  /**
   * The '<em><b>OEXCL</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>OEXCL</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #OEXCL
   * @model
   * @generated
   * @ordered
   */
  public static final int OEXCL_VALUE = 5;

  /**
   * The '<em><b>ONOCTTY</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ONOCTTY</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ONOCTTY
   * @model
   * @generated
   * @ordered
   */
  public static final int ONOCTTY_VALUE = 6;

  /**
   * The '<em><b>ONONBLOCK</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ONONBLOCK</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ONONBLOCK
   * @model
   * @generated
   * @ordered
   */
  public static final int ONONBLOCK_VALUE = 7;

  /**
   * The '<em><b>OSYNC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>OSYNC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #OSYNC
   * @model
   * @generated
   * @ordered
   */
  public static final int OSYNC_VALUE = 8;

  /**
   * The '<em><b>OTRUNC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>OTRUNC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #OTRUNC
   * @model
   * @generated
   * @ordered
   */
  public static final int OTRUNC_VALUE = 9;

  /**
   * An array of all the '<em><b>dd Pathopts Opts</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final ddPathoptsOpts[] VALUES_ARRAY =
    new ddPathoptsOpts[]
    {
      ORDONLY,
      OWRONLY,
      ORDWR,
      OAPPEND,
      OCREAT,
      OEXCL,
      ONOCTTY,
      ONONBLOCK,
      OSYNC,
      OTRUNC,
    };

  /**
   * A public read-only list of all the '<em><b>dd Pathopts Opts</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<ddPathoptsOpts> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>dd Pathopts Opts</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddPathoptsOpts get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      ddPathoptsOpts result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>dd Pathopts Opts</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddPathoptsOpts getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      ddPathoptsOpts result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>dd Pathopts Opts</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static ddPathoptsOpts get(int value)
  {
    switch (value)
    {
      case ORDONLY_VALUE: return ORDONLY;
      case OWRONLY_VALUE: return OWRONLY;
      case ORDWR_VALUE: return ORDWR;
      case OAPPEND_VALUE: return OAPPEND;
      case OCREAT_VALUE: return OCREAT;
      case OEXCL_VALUE: return OEXCL;
      case ONOCTTY_VALUE: return ONOCTTY;
      case ONONBLOCK_VALUE: return ONONBLOCK;
      case OSYNC_VALUE: return OSYNC;
      case OTRUNC_VALUE: return OTRUNC;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private ddPathoptsOpts(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //ddPathoptsOpts
