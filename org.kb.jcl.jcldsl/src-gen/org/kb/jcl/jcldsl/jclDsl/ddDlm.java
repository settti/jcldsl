/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Dlm</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDlm#getDelimiter <em>Delimiter</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDlm#getDelim <em>Delim</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDlm()
 * @model
 * @generated
 */
public interface ddDlm extends ddsection
{
  /**
   * Returns the value of the '<em><b>Delimiter</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Delimiter</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Delimiter</em>' attribute.
   * @see #setDelimiter(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDlm_Delimiter()
   * @model
   * @generated
   */
  String getDelimiter();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDlm#getDelimiter <em>Delimiter</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Delimiter</em>' attribute.
   * @see #getDelimiter()
   * @generated
   */
  void setDelimiter(String value);

  /**
   * Returns the value of the '<em><b>Delim</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Delim</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Delim</em>' attribute.
   * @see #setDelim(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDlm_Delim()
   * @model
   * @generated
   */
  String getDelim();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDlm#getDelim <em>Delim</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Delim</em>' attribute.
   * @see #getDelim()
   * @generated
   */
  void setDelim(String value);

} // ddDlm
