/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddData#getDddata <em>Dddata</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddData()
 * @model
 * @generated
 */
public interface ddData extends ddsection
{
  /**
   * Returns the value of the '<em><b>Dddata</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dddata</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dddata</em>' attribute.
   * @see #setDddata(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddData_Dddata()
   * @model
   * @generated
   */
  String getDddata();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddData#getDddata <em>Dddata</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dddata</em>' attribute.
   * @see #getDddata()
   * @generated
   */
  void setDddata(String value);

} // ddData
