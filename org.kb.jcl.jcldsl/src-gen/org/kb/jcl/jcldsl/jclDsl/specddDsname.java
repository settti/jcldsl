/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>specdd Dsname</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.specddDsname#getSpecname <em>Specname</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.specddDsname#getDsname <em>Dsname</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getspecddDsname()
 * @model
 * @generated
 */
public interface specddDsname extends specddSection
{
  /**
   * Returns the value of the '<em><b>Specname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Specname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Specname</em>' attribute.
   * @see #setSpecname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getspecddDsname_Specname()
   * @model
   * @generated
   */
  String getSpecname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.specddDsname#getSpecname <em>Specname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Specname</em>' attribute.
   * @see #getSpecname()
   * @generated
   */
  void setSpecname(String value);

  /**
   * Returns the value of the '<em><b>Dsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dsname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dsname</em>' attribute.
   * @see #setDsname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getspecddDsname_Dsname()
   * @model
   * @generated
   */
  String getDsname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.specddDsname#getDsname <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dsname</em>' attribute.
   * @see #getDsname()
   * @generated
   */
  void setDsname(String value);

} // specddDsname
