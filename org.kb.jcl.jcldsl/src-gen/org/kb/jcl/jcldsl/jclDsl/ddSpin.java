/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Spin</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSpin#getUnalloc <em>Unalloc</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSpin()
 * @model
 * @generated
 */
public interface ddSpin extends ddsection
{
  /**
   * Returns the value of the '<em><b>Unalloc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Unalloc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Unalloc</em>' attribute.
   * @see #setUnalloc(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSpin_Unalloc()
   * @model
   * @generated
   */
  String getUnalloc();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSpin#getUnalloc <em>Unalloc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Unalloc</em>' attribute.
   * @see #getUnalloc()
   * @generated
   */
  void setUnalloc(String value);

} // ddSpin
