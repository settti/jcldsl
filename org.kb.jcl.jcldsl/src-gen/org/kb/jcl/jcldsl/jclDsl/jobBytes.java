/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Bytes</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobBytes#getValues <em>Values</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobBytes#getPagesDef <em>Pages Def</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobBytes()
 * @model
 * @generated
 */
public interface jobBytes extends jobsection
{
  /**
   * Returns the value of the '<em><b>Values</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Values</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Values</em>' attribute.
   * @see #setValues(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobBytes_Values()
   * @model
   * @generated
   */
  int getValues();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobBytes#getValues <em>Values</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Values</em>' attribute.
   * @see #getValues()
   * @generated
   */
  void setValues(int value);

  /**
   * Returns the value of the '<em><b>Pages Def</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pages Def</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pages Def</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobBytes_PagesDef()
   * @model unique="false"
   * @generated
   */
  EList<String> getPagesDef();

} // jobBytes
