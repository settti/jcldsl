/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Space</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getLength <em>Length</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getPrimary <em>Primary</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getSecondary <em>Secondary</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getDirorindx <em>Dirorindx</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getParm <em>Parm</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSpace()
 * @model
 * @generated
 */
public interface ddSpace extends ddsection
{
  /**
   * Returns the value of the '<em><b>Length</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Length</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Length</em>' attribute.
   * @see #setLength(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSpace_Length()
   * @model
   * @generated
   */
  int getLength();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getLength <em>Length</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Length</em>' attribute.
   * @see #getLength()
   * @generated
   */
  void setLength(int value);

  /**
   * Returns the value of the '<em><b>Primary</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Primary</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Primary</em>' attribute.
   * @see #setPrimary(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSpace_Primary()
   * @model
   * @generated
   */
  int getPrimary();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getPrimary <em>Primary</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Primary</em>' attribute.
   * @see #getPrimary()
   * @generated
   */
  void setPrimary(int value);

  /**
   * Returns the value of the '<em><b>Secondary</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Secondary</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Secondary</em>' attribute.
   * @see #setSecondary(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSpace_Secondary()
   * @model
   * @generated
   */
  int getSecondary();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getSecondary <em>Secondary</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Secondary</em>' attribute.
   * @see #getSecondary()
   * @generated
   */
  void setSecondary(int value);

  /**
   * Returns the value of the '<em><b>Dirorindx</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dirorindx</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dirorindx</em>' attribute.
   * @see #setDirorindx(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSpace_Dirorindx()
   * @model
   * @generated
   */
  int getDirorindx();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddSpace#getDirorindx <em>Dirorindx</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dirorindx</em>' attribute.
   * @see #getDirorindx()
   * @generated
   */
  void setDirorindx(int value);

  /**
   * Returns the value of the '<em><b>Parm</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddSpace_Parm()
   * @model unique="false"
   * @generated
   */
  EList<String> getParm();

} // ddSpace
