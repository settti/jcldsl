/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobGroup#getGROUP <em>GROUP</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobGroup()
 * @model
 * @generated
 */
public interface jobGroup extends jobsection
{
  /**
   * Returns the value of the '<em><b>GROUP</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>GROUP</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>GROUP</em>' attribute.
   * @see #setGROUP(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobGroup_GROUP()
   * @model
   * @generated
   */
  String getGROUP();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobGroup#getGROUP <em>GROUP</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>GROUP</em>' attribute.
   * @see #getGROUP()
   * @generated
   */
  void setGROUP(String value);

} // jobGroup
