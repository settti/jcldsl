/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Dcb</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDcb#getSubparm <em>Subparm</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDcb#getSubparm2 <em>Subparm2</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddDcb#getDsname <em>Dsname</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDcb()
 * @model
 * @generated
 */
public interface ddDcb extends ddsection
{
  /**
   * Returns the value of the '<em><b>Subparm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subparm</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subparm</em>' containment reference.
   * @see #setSubparm(ddDcbSubparms)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDcb_Subparm()
   * @model containment="true"
   * @generated
   */
  ddDcbSubparms getSubparm();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDcb#getSubparm <em>Subparm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Subparm</em>' containment reference.
   * @see #getSubparm()
   * @generated
   */
  void setSubparm(ddDcbSubparms value);

  /**
   * Returns the value of the '<em><b>Subparm2</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.ddDcbSubparms}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subparm2</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subparm2</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDcb_Subparm2()
   * @model containment="true"
   * @generated
   */
  EList<ddDcbSubparms> getSubparm2();

  /**
   * Returns the value of the '<em><b>Dsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dsname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dsname</em>' attribute.
   * @see #setDsname(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddDcb_Dsname()
   * @model
   * @generated
   */
  String getDsname();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddDcb#getDsname <em>Dsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dsname</em>' attribute.
   * @see #getDsname()
   * @generated
   */
  void setDsname(String value);

} // ddDcb
