/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Progname</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobProgname#getProgrammer_name <em>Programmer name</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobProgname#getPROGRAMMER_ID <em>PROGRAMMER ID</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobProgname()
 * @model
 * @generated
 */
public interface jobProgname extends jobsection
{
  /**
   * Returns the value of the '<em><b>Programmer name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Programmer name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Programmer name</em>' attribute.
   * @see #setProgrammer_name(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobProgname_Programmer_name()
   * @model
   * @generated
   */
  String getProgrammer_name();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobProgname#getProgrammer_name <em>Programmer name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Programmer name</em>' attribute.
   * @see #getProgrammer_name()
   * @generated
   */
  void setProgrammer_name(String value);

  /**
   * Returns the value of the '<em><b>PROGRAMMER ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>PROGRAMMER ID</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>PROGRAMMER ID</em>' attribute.
   * @see #setPROGRAMMER_ID(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobProgname_PROGRAMMER_ID()
   * @model
   * @generated
   */
  String getPROGRAMMER_ID();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobProgname#getPROGRAMMER_ID <em>PROGRAMMER ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>PROGRAMMER ID</em>' attribute.
   * @see #getPROGRAMMER_ID()
   * @generated
   */
  void setPROGRAMMER_ID(String value);

} // jobProgname
