/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Storclas</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddStorclas#getStorageclass <em>Storageclass</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddStorclas()
 * @model
 * @generated
 */
public interface ddStorclas extends ddsection
{
  /**
   * Returns the value of the '<em><b>Storageclass</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Storageclass</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Storageclass</em>' attribute.
   * @see #setStorageclass(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddStorclas_Storageclass()
   * @model
   * @generated
   */
  String getStorageclass();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddStorclas#getStorageclass <em>Storageclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Storageclass</em>' attribute.
   * @see #getStorageclass()
   * @generated
   */
  void setStorageclass(String value);

} // ddStorclas
