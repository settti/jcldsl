/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>job Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.jobClass#getCLASS <em>CLASS</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobClass()
 * @model
 * @generated
 */
public interface jobClass extends jobsection
{
  /**
   * Returns the value of the '<em><b>CLASS</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>CLASS</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>CLASS</em>' attribute.
   * @see #setCLASS(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getjobClass_CLASS()
   * @model
   * @generated
   */
  String getCLASS();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.jobClass#getCLASS <em>CLASS</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>CLASS</em>' attribute.
   * @see #getCLASS()
   * @generated
   */
  void setCLASS(String value);

} // jobClass
