/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Bfaln</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddBfaln#getParm <em>Parm</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddBfaln()
 * @model
 * @generated
 */
public interface ddBfaln extends ddsection, ddDcbSubparms
{
  /**
   * Returns the value of the '<em><b>Parm</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parm</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parm</em>' attribute.
   * @see #setParm(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddBfaln_Parm()
   * @model
   * @generated
   */
  String getParm();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddBfaln#getParm <em>Parm</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parm</em>' attribute.
   * @see #getParm()
   * @generated
   */
  void setParm(String value);

} // ddBfaln
