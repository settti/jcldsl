/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.DD#isDDNAME <em>DDNAME</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.DD#getSections <em>Sections</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getDD()
 * @model
 * @generated
 */
public interface DD extends EObject
{
  /**
   * Returns the value of the '<em><b>DDNAME</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>DDNAME</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>DDNAME</em>' attribute.
   * @see #setDDNAME(boolean)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getDD_DDNAME()
   * @model
   * @generated
   */
  boolean isDDNAME();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.DD#isDDNAME <em>DDNAME</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>DDNAME</em>' attribute.
   * @see #isDDNAME()
   * @generated
   */
  void setDDNAME(boolean value);

  /**
   * Returns the value of the '<em><b>Sections</b></em>' containment reference list.
   * The list contents are of type {@link org.kb.jcl.jcldsl.jclDsl.ddsection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sections</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sections</em>' containment reference list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getDD_Sections()
   * @model containment="true"
   * @generated
   */
  EList<ddsection> getSections();

} // DD
