/**
 */
package org.kb.jcl.jcldsl.jclDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Copies</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddCopies#getValue <em>Value</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddCopies#getGroup_value <em>Group value</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddCopies()
 * @model
 * @generated
 */
public interface ddCopies extends ddsection
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddCopies_Value()
   * @model
   * @generated
   */
  int getValue();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddCopies#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(int value);

  /**
   * Returns the value of the '<em><b>Group value</b></em>' attribute list.
   * The list contents are of type {@link java.lang.Integer}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group value</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group value</em>' attribute list.
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddCopies_Group_value()
   * @model unique="false"
   * @generated
   */
  EList<Integer> getGroup_value();

} // ddCopies
