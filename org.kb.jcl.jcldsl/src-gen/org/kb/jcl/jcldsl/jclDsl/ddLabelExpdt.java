/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Label Expdt</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getYyddd <em>Yyddd</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getYyyy <em>Yyyy</em>}</li>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getDdd <em>Ddd</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabelExpdt()
 * @model
 * @generated
 */
public interface ddLabelExpdt extends ddLabelExpire
{
  /**
   * Returns the value of the '<em><b>Yyddd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Yyddd</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Yyddd</em>' attribute.
   * @see #setYyddd(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabelExpdt_Yyddd()
   * @model
   * @generated
   */
  int getYyddd();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getYyddd <em>Yyddd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Yyddd</em>' attribute.
   * @see #getYyddd()
   * @generated
   */
  void setYyddd(int value);

  /**
   * Returns the value of the '<em><b>Yyyy</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Yyyy</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Yyyy</em>' attribute.
   * @see #setYyyy(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabelExpdt_Yyyy()
   * @model
   * @generated
   */
  int getYyyy();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getYyyy <em>Yyyy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Yyyy</em>' attribute.
   * @see #getYyyy()
   * @generated
   */
  void setYyyy(int value);

  /**
   * Returns the value of the '<em><b>Ddd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ddd</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ddd</em>' attribute.
   * @see #setDdd(int)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddLabelExpdt_Ddd()
   * @model
   * @generated
   */
  int getDdd();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddLabelExpdt#getDdd <em>Ddd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ddd</em>' attribute.
   * @see #getDdd()
   * @generated
   */
  void setDdd(int value);

} // ddLabelExpdt
