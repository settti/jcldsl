/**
 */
package org.kb.jcl.jcldsl.jclDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dd Recorg</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.kb.jcl.jcldsl.jclDsl.ddRecorg#getRecorg <em>Recorg</em>}</li>
 * </ul>
 *
 * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddRecorg()
 * @model
 * @generated
 */
public interface ddRecorg extends ddsection
{
  /**
   * Returns the value of the '<em><b>Recorg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Recorg</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Recorg</em>' attribute.
   * @see #setRecorg(String)
   * @see org.kb.jcl.jcldsl.jclDsl.JclDslPackage#getddRecorg_Recorg()
   * @model
   * @generated
   */
  String getRecorg();

  /**
   * Sets the value of the '{@link org.kb.jcl.jcldsl.jclDsl.ddRecorg#getRecorg <em>Recorg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Recorg</em>' attribute.
   * @see #getRecorg()
   * @generated
   */
  void setRecorg(String value);

} // ddRecorg
